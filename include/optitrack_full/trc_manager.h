//#pragma once
#ifndef TRCManager_h
#define TRCManager_h

// could be installed by sudo apt-get install libpugixml-dev
#include "pugixml.hpp"

#include <map>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/LU>
#include <vector>
#include <string>
#include "fosim/fosim.h"
//#include <optitrack_full/data_model.h>
//#include "optitrack_full/natnet/natnet_messages.h"
#include <optitrack_full/markers_management.h>
#include <random>

namespace file_management{

    class TRCManager{
    public:

    std::map<double,std::map<int,Eigen::Vector3d> > optitrackData;
    std::map<double,std::map<std::string,Eigen::Vector3d> > tempTRCData;

    std::map<std::string,int> trcCorresNameId;
    int Nframe;
    int dataRate;
    int markerIndex;
    int numMarkers;
    std::map<int,int> recordedAttributedIndex;


    std::vector<int> lastRecordedIndex;

    std::vector<int> currentRecordedIndex;
    std::vector<std::string> necessaryMarkers;
    int lastMaxID;
    bool brutalFiltering;
    bool recordTRC;
    int numRepeat;
    std::string filenamePath;

    int firstCompleteLabelizedFrame = -1;
    // detect if we have checked all the frames
    // already set to True for a labelized .trc file.
    bool allFramesSeen = false;

    // Two types : 
    // * Speaker : reads a TRC file, give its value
    // * Listener : gets all the informations about the experimentation, might then
    // save the experimentation as a TRC.
    std::string typeTRCManager;

    TRCManager();

    TRCManager(std::string filename, bool record_trc, bool brutal_filtering = true, std::vector<std::string> necessaryMarkers={} );
    void init(std::string filename, bool record_trc, bool brutal_filtering = true, std::vector<std::string> necessaryMarkers={} );
    void run(bool mustBeLabelized=false);
    // dealt by same way as real + repeat case.
    //void getNextOptitrackFrame(optitrack_full::DataModel * dataModel);

    void readTRCAsUnlabeledOptitrackMarkers(std::map<double,std::map<std::string,Eigen::Vector3d> > TRCData,
    std::map<double,std::map<int,Eigen::Vector3d> > & optitrackData );

    void readTRCAsLabeledOptitrackMarkers(std::map<double,std::map<std::string,Eigen::Vector3d> > TRCData,
    std::map<double,std::map<int,Eigen::Vector3d> > & optitrackData,
    std::map<std::string,int> & corresNameId,
    std::vector<std::string> necessaryMarkers,
    int & Nframe,
    int iBeg = 0 );

    void writeLabeledTRCFile(std::map<int, std::map<int,LabelCarac> > allCorresIDName, int calibFrame, int typeSave, std::string & trcPath,       std::vector< std::vector<Eigen::Vector3d> > forbidden_areas= {});
    
    void writeLabeledTRCFile(std::map<int, std::map<int,LabelCarac> > allCorresIDName, int calibFrame, int typeSave, std::string & trcPath, std::vector<std::string> markNames,       std::vector< std::vector<Eigen::Vector3d> > forbidden_areas = {});
    
    bool readFileCaracs();

    bool isOneExtremeFrameComplete(std::map<double,std::map<std::string,Eigen::Vector3d> > TRCData,
    bool startByBegin,
    double percentThreshold,
    int & iFrame,
    bool verbose = false
    );

    bool isFrameComplete(std::map<std::string,Eigen::Vector3d> frame,
    bool startByBegin = true,
    bool verbose = false
    );

    bool isOneMarkerInFrame(std::map<std::string,Eigen::Vector3d> frame,
    bool verbose = false
    );    


    };




}


#endif