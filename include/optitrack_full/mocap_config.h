/* 
 * Copyright (c) 2018, Houston Mechatronics Inc., JD Yamokoski
 * Copyright (c) 2012, Clearpath Robotics, Inc., Alex Bencz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its 
 *    contributors may be used to endorse or promote products derived from 
 *    this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef __MOCAP_OPTITRACK_MOCAP_CONFIG_H__
#define __MOCAP_OPTITRACK_MOCAP_CONFIG_H__

#include <vector>
#include <string>

#include <ros/ros.h>

namespace optitrack_full
{

/// \brief Server communication info
struct ServerDescription
{
  struct Default
  {
    static const int CommandPort;
    static const int DataPort;
    static const int FrameRate;
    static const std::string MulticastIpAddress;
    static const bool simulation;
    static const double speed;
    static const int dissapear;
    static const double maximum_residual;
    static const int same_frames;
    static const double noise;
    static const bool simu_verbose;
    static const bool reception_verbose;
    static const bool association_verbose;
    static const bool icp_verbose;    
    static const bool guess_verbose;    
    static const bool filtering_verbose;    
    static const bool error_verbose;      
    static const bool calibration_verbose;  
    static const bool ik_verbose;  
    static const bool never_verbose;  
    static const bool stream_markers_only;
    static const bool repeat;

    static const std::string ros_package_path;
    static const std::string scale_file;
    static const std::string data_file;

    static const std::string trc_file;
    static const std::string type_record;    
    static const bool record_trc;

    static const bool fast_trc_read;
    static const std::string  forbidden_areas_file;


  };

  ServerDescription();
  int commandPort;
  int dataPort;
  int frameRate;
  std::string multicastIpAddress;
  std::vector<int> version;
  bool simulation;
  double speed;
  int dissapear;
  int same_frames;
  double maximum_residual;
  double noise;
  bool simu_verbose;
  bool reception_verbose;
  bool association_verbose;  
  bool icp_verbose;    
  bool guess_verbose;    
  bool error_verbose;      
  bool calibration_verbose;
  bool filtering_verbose;   
  bool ik_verbose;
  bool never_verbose;  
  bool stream_markers_only;
  bool repeat;

  std::string forbidden_areas_file;



  std::string ros_package_path;
  std::string scale_file;
  std::string data_file;
  std::string trc_file;
  std::string type_record;  
  bool record_trc;

  bool fast_trc_read;

  std::map<std::string, std::map<std::string,int> > allRigidBodiesMarkersAxis;
  
  std::map<std::string, std::vector<std::string> > allRigidBodiesMarkersNames;
  

};


/// \brief ROS publisher configuration
struct PublisherConfiguration
{
  int rigidBodyId;
  std::string poseTopicName;
  std::string pose2dTopicName;
  std::string childFrameId;
  std::string parentFrameId;

  bool publishPose;
  bool publishPose2d;
  bool publishTf;
};

struct ModelConfiguration
{
  std::vector<double> q_calib;
  std::string osim_path;
  std::string urdf_path;
  std::string geometry_path;
  std::string setup_ik_path;
  std::string topic_q;
  bool record_trc;
  std::string trc_file;
  
};

typedef std::vector<PublisherConfiguration> PublisherConfigurations;

/// \brief Handles loading node configuration from different sources
struct NodeConfiguration
{
  static void fromRosParam(ros::NodeHandle& nh, 
    ServerDescription& serverDescription, 
    PublisherConfigurations& pubConfigs,
    ModelConfiguration & modelConfig);
};

} // namespace

#endif  // __MOCAP_OPTITRACK_MOCAP_CONFIG_H__
