/* 
 * Copyright (c) 2011 University of Bonn, Computer Science Institute, 
 * Kathrin Gräve
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its 
 *    contributors may be used to endorse or promote products derived from 
 *    this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
//#pragma once
#ifndef __MOCAP_OPTITRACK_DATA_MODEL_H__
#define __MOCAP_OPTITRACK_DATA_MODEL_H__

#include <optitrack_full/version.h>
#include <map>
#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/LU>
#include <list>
#include <numeric>
#include "optitrack_full/markers_management.h"
#include "optitrack_full/mocap_config.h"


namespace optitrack_full
{


/// \brief Data object holding the position of a single mocap marker in 3d space
struct Marker
{
  float x;
  float y;
  float z;
};

struct __attribute__ ((__packed__)) Pose
{
  struct __attribute__ ((__packed__)) {
    float x;
    float y;
    float z;
  } position;
  struct __attribute__ ((__packed__)) {
    float x;
    float y;
    float z;
    float w;
  } orientation;
};

/// \brief Data object holding information about a single rigid body within a mocap skeleton
struct RigidBody
{
    RigidBody();
    int bodyId;
    Pose pose;
    float meanMarkerError;
    bool isTrackingValid;

    bool hasValidData() const;
};


/// \brief Data object describing a single tracked model
struct ModelDescription
{
    ModelDescription();
    void clear();

    std::string name;
    std::vector<std::string> markerNames;
};

struct MarkerSet
{
    void clear();

    char nameCh[256];
    std::vector<Marker> markers;

    std::string name;
    std::map<std::string, Eigen::Vector3d > markersPosition;
    
    std::map<std::string, bool > markersDetected;
    std::map<std::string, std::map<std::string, double > > markersDistance;
    bool markerMissing;
    bool crucialMarkerMissing;

    std::map<std::string,int> markerNamesAxis;    
    Eigen::Matrix4f currentMotivePose;
    std::map<std::string, Eigen::Vector3d > markersPositionOrigin;
    Eigen::Matrix4f initTransform;
    Eigen::Matrix4f prevTransf;    
    bool alreadyFullyDetected;

    bool verbosePoseNotDetected;

    // values that are initialized only once
    bool objectInitialized;
    std::vector<std::string> markersNames;

};

/// \brief Data object holding poses of a tracked model's components
struct ModelFrame
{
    ModelFrame();
    void clear();

    std::vector<MarkerSet> markerSets;
    std::vector<Marker> otherMarkers;
    std::vector<RigidBody> rigidBodies;

    float latency;
};

/// \brief Data object holding server info
struct ServerInfo
{
    ServerInfo();
    Version natNetVersion;
    Version serverVersion;
};


/**
 * @brief All data used for the reception of Optitrack markers and their association
 * with markers of different objects.
 * 
 */
struct OptitrackNameMarkers
{

    // Current number associated to treated frame
    // (same value as dataModel.frameNumber, just easier access)
    int frameNumber;

// Bodies order to check for labelisation
std::vector<std::string> bodies_label_order;

bool debugAssos = false;
  // (repeat mode only) Object containing, for each of the concerned
  // frame, the optitrack markers associations that need to be inversed
  // it->first : frame concerned by an inversion
  // it->second : map object (called next it2)
  // it2->first : concerned new optitrack ID associated with the marker
  // it2->second : optitrack index and marker name concerned by the change : 
  // it2->second->first : marker name concerned by change
  // it2->second->second : old optitrack ID associated with the marker name starting
  // from this frame
  std::map< int, std::map< int,std::pair<std::string,int> > > inverseIDs;

  // Forbidden areas for markers, expressed in global Opensim Referential and as boxes.
  // box[i] = [ min, max]
  std::vector< std::vector<Eigen::Vector3d> > forbidden_areas;

  markersManagement::globalPublisher debugPub;
  // Parameters about the calibration process
  markersManagement::managementParameters manParams;  

  markersManagement::managementParameters originManParams; 
  // Simulation parameters

  // Boolean checking if we're into a simulation or a real situation
  bool simulation;
  // All parameters used for simulation of Optitrack markers  
  markersManagement::SimulationGestion simu_gest;

  // Reception parameters

  // Boolean indicating whether data streamed by the Optitrack server are issued 
  // from a record or not
  bool repeat;
  // (Reality only). Debugging tool, for publishing informations about 
  // the reception of Optitrack markers.
  bool reception_verbose;
  bool selection_verbose;

  // Desactivate all the time verbose
  bool never_verbose;

  // lastID of the group : <id of the markers of the group : position>
  std::map<int,std::map<int,Eigen::Vector3d> > opMarkRigidBodies;

  // Dictionary containing the position of the Optitrack Markers that were not assigned on
  // the last frame (according to Optitrack).
  std::map<int, Eigen::Vector3d > lastOptitrackMarkersNotAssigned;

  // Dictionary containing the position of all the Optitrack Markers on the 
  // last frame received.
  std::map<int, Eigen::Vector3d > lastOptitrackMarkers;
  // Time associated with the Optitrack Markers of the last frame received.
  ros::Time lastOptitrackTime;
  // Time associated with the reception of the last Optitrack message
  ros::Time messageReceptionTime;
  // Time of the first Optitrack message. 
  // If Optitrack data are from a record, this is the time since the first publication of the beginning of the record.
  // If Optitrack data are from a live experiment, this is the time since the start of Motive Software.
  ros::Time originTime;
  // Frame rate of Optitrack markers
  int frameRate;

  // Set at true if no more associations are possible in the frame
  // (aka if opMarkers.size() == 0 || numOsMarkersNotAssigned == 0)
  bool noMoreAssosPossible;



  // Calibration parameters

  // Vector Q corresponding to the calibration pose
  std::vector<double> q_calib;  
  // (Repeat case only). Boolean indicating whether the pose used for calibration
  // has been found again. 
  bool foundCalibFrame;

  // Asspociation optitrack / opensim found at calibration. Considered as perfect, and cannot be changed
  std::map<int, std::string> CINCalib;

  // Association parameters

  // Dictionary indicating, for each marker, the maximum variation of its position (in m)
  std::map<std::string,double> maxGapVariation;
  // Dictionary indicating, for each marker, its distance from the other markers of its body
  // at the last Optitrack frame recorded
  std::map<std::string, std::map<std::string, double> > D_mark_all_dist;
  // Maximum residual value obtained from the calibration of the Optitrack system (in m).
  double maximum_residual;
  // Dictionary indicating, for each Opensim Marker, the index of the Optitrack marker with which
  // it is currently associated.
  std::map<std::string,int> corresNameID;

  
  // (Repeat case only). Dictionnary indicating, for each optitrack ID,
  // which Opensim name was associated and the last frame where this association
  // was valid.
  // If the last frame is equal to -1, then the association is considered as always valid (starting from the
  // biggest last valid frame.) 
  //Ex : id : 2 : [125] --> "STR"
  //    [-1] --> "OS"
  // ==> from 0 to 125, 2 --> "STR"
  // ==> from 126 to the last time the id is seen, 2 --> "OS"
  // std::map<int, std::map<int,std::string> > allCorresIDName;
  std::map<int, std::map<int,LabelCarac> > allCorresIDName;


  // Indicates last frame where Opensim markers were associated
  // Will be used notably to update allCorresIDName with the exact value
  // of the end of the association
  // (to treat maybe cases where association could take some time)
  std::map<std::string,int> lastFrameOsAssos;


  // std::map<int,std::string> allCorresIDName;  
  // Debugging tool, for publishing informations about the association of Optitrack
  // markers to Opensim Markers.
  bool association_verbose;

  bool select_message;

  // Indicates which Opensim marker was assigned (=0) or not assigned (=1) at the last frame.
  std::map<std::string,int> lastOsMarkersNotAssigned;

  // IK parameters

  // Dictionary containing the position of each Opensim Marker
  std::map<std::string, Eigen::Vector3d > opensimMarkersPos;
  // Counter of the number of frames that occured since the last IK.
  int framesSinceIK;
  // Limit of the number of frames before an IK operation is tried, in order
  // to be sure that informations about the position of the model would not be
  // completely blocked.
  // This limit will be dynamically adjusted to the frequency of successful and 
  // failed IK operations. 
  int limFramesSinceIK;
  // Original limit of the number of frames before an IK operation is tried, in order
  // to be sure that informations about the position of the model would not be
  // completely blocked.
  int oriLimFramesSinceIK;
  // Double value adjusting the limFramesSinceIK variable, according to the frequency of successful and 
  // failed IK operations. 
  double limAdjustFramesSinceIK;

  // Counter of the number of frames that occured since the last Assemble operation for IK.
  // (Assemble = IK done without any guess about the current joint configuration of the model).
  int framesSinceAssemble;
  // Limit of the number of frames before an Assemble IK operation is tried, in order
  // to be sure that no noise on the joint configuration of the model will be accumulated.
  int limFramesSinceAssemble;
  
  // Additionnal weight parameter on the markers for the IK operation. This parameter can be 
  // adjusted during the management of the different Optitrack frames to give more or less importance 
  // to the position of the markers in relation to the last joint configuration of the model.
  double constraintWeight;
  // Initial weight parameter on the markers for the IK operation. 
  double initConstraintWeight;
  // Dictionnary containing, for each Opensim Marker, its associated weight.
  std::map<std::string, double> markersWeights;

  double minima_distance_coeff;

  markersManagement::ErrorsPublicationGestion error_pub;

  bool error_verbose;

  bool pause_mode;
  bool user_pause_mode;
  
  bool calibration_verbose;

  

};

struct OptitrackMessage
{
  std::map<int, Eigen::Vector3d > optitrackMarkers;
  std::map<int, Eigen::Vector3d > optitrackMarkersNotAssigned; 
  std::map<int,std::map<int,Eigen::Vector3d> > opMarkRigidBodies;   

  double receptionTime;
  double trueTimestamp;

};



/// \brief The data model for this node
class DataModel
{
public:
    DataModel();
    DataModel(ModelConfiguration const& modelConfig);
    
    std::map<int,OptitrackMessage> queueMessage;
    int sizeMaxQueue;

    // Current number associated to treated frame
    int frameNumber;
    // Number associated to first frame treated 
    int frameOrigin;
    // Number associated to the last frame treated, just before
    // the current frame
    int lastFrame;
    // Variable reporting the number of frames treated since the first
    // treated frame
    int numFramesCons;
    int lastNumFramesCons;
    // Boolean indicating if only markers are streamed from the Optitrack
    // server.
    bool streamMarkersOnly;
    // Boolean indicating if an Optitrack message is currently dispatched or not.
    bool dispatching;

    bool doPCScaling;


    // Frequency of the Optitrack server. Used to get the exact time when
    // Optitrack data were recorded by camera.
    double highResClockFrequency;
    // OsimModel object, used for various operations (in order to use various
    // Opensim methods and caracteristics about the model of the subject).
    fosim::OsimModel osMod;    
    // Data object holding poses of a tracked model's components. Used only
    // by original optitrack_full package, for rigid bodies.
    ModelFrame dataFrame;
    // Object used for all the processing of Optitrack markers.
    OptitrackNameMarkers opNameMarkers;

    std::vector<MarkerSet> allMarkerSets;
    // Control whether there is a new markerset or not
    bool newMarkerSet;

    void clear();

    void setVersions(int* nver, int* sver);
    Version const& getNatNetVersion() const;
    Version const& getServerVersion() const;
    bool hasServerInfo() const {return hasValidServerInfo;};

private:
    ServerInfo serverInfo;
    bool hasValidServerInfo;
};

}

#endif