/* 
 * Copyright (c) 2018, Houston Mechatronics Inc., JD Yamokoski
 * Copyright (c) 2012, Clearpath Robotics, Inc., Alex Bencz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its 
 *    contributors may be used to endorse or promote products derived from 
 *    this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Local includes
//#pragma once
#ifndef __OPENSIM_MANAGEMENT_H__
#define __OPENSIM_MANAGEMENT_H__

#include <optitrack_full/data_model.h>
#include <optitrack_full/mocap_config.h>

#include "optitrack_full/natnet/natnet_messages.h"
#include "optitrack_full/calibration.h"
#include "optitrack_full/scaling.h"

#include "optitrack_full/trc_manager.h"

#include "optitrack_full/saveTxt.h"
#include "std_srvs/SetBool.h"
// ROS includes
// #include <ros/ros.h>

// Others
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

namespace opensim_management
{

  /**
   * @brief Class managing the bridge from Optitrack message to ROS.
   * It also manages simulation and scaling operations.
   * 
   */
  class OpensimManagement
  {
  public:
    OpensimManagement(std::string osim_path,
      std::string geometry_path,
      std::string setup_ik_path,
      std::vector<double> q_calib,
      ros::NodeHandle& nh);

  /**
   * @brief (Used only for simulation purpose). Generate a dictionary of index:point_position from a dictionary of name:point_position.
   * Applies to the generated dictionary a transformation (called transform), a random noise on position of amplitude ampl_noise, and 
   * a stretching set by the stretch vector.
   * 
   * @param D_mark_pos Dictionnary name:point_position. Original position of the points.
   * @param transform Transformation matrix to apply on the points.
   * @param ampl_noise Maximal amplitude of the noise in position (m). The noise applied follows an uniform distribution law.
   * @param stretch Vector < Stretching_on_x, Stretching_on_y, Stretching_on_z >, parameterising the stretch to be applied on each point.
   * @return std::map<int, Eigen::Vector3d > Dictionnary index:point_position, obtains after all operations.
   */
  std::map<int, Eigen::Vector3d > transformMarkers(std::map<std::string, Eigen::Vector3d> D_mark_pos, Eigen::Matrix4d transform, double ampl_noise, std::map<std::string,std::vector<double> > stretch_vector );
/**
 * 
 * 
 * Concerned objects : can be Markers or body Parts
 * cleanFORCE : will clean whatever the safeguards. USE WITH CAUTION
*/
void clean_associations(optitrack_full::DataModel * dataModel, 
std::vector<std::string> concerned_objects_parts, 
bool cleanFORCE = false);

/**
 * @brief Method managing the communication between the .py node (into scaling_manager.py) and
 * the .cpp node (mocap_node.cpp) for the scaling operation. This scaling operation is applied on 
 * the .osim model of the OsimModel object osMod.
 * 
 * @param osMod OsimModel object; this object will be remplaced by its scaled version at the end of the method.
 * @param markersLocation Dictionary name:point_position, giving the position of each of the .osim markers.
 * @return bool Indicates whether the scaling process made by scaling_manager.py was a success or not.
 * 
 */

bool start_scaling(fosim::OsimModel & osMod,
 std::map<std::string,Eigen::Vector3d> markersLocation,
 std::map<std::string,std::vector<double> > all_scaling_factors);

/**
 * @brief Convenience method to reboot the calibration process, after
 * a correspondance between the .osim markers and the optitrack markers was
 * computed.
 * 
 */
void reboot_calib(optitrack_full::DataModel * dataModel);

/**
 * @brief Method managing all the necessary operations to estimate the CoR of a body part,
 * and rescaling this body part accordingly.
 * At this step, we suppose all correspondences for the calibration pose are correct. 
 * 
 * @param osMod 
 * @param body_part 
 * @param markerData 
 * @param corresNameID 
 * @param scaling_factors 
 * @param ground2ParentTransform 
 * @param calib_time 
 * @param globalPublish 
 */
void CoREstimateManagement(fosim::OsimModel & osMod, 
optitrack_full::OptitrackNameMarkers & opNameMarkers,
std::string body_part,
std::vector<std::map<int,Eigen::Vector3d> > markerData, 
std::map<std::string,int> corresNameID,
std::vector<double> scaling_factors,
std::map<int,std::map<std::string, Eigen::Matrix4f> > & ground2ParentTransform,
ros::Time calib_time,
markersManagement::globalPublisher globalPublish );

/**
 * @brief Method managing all the necessary operations to estimate the CoR between two body parts.
 * For now, no scaling.
 * At this step, we suppose all correspondences for the calibration pose are correct. 
 * 
 * @param osMod 
 * @param body_part 
 * @param markerData 
 * @param corresNameID 
 * @param scaling_factors 
 * @param ground2ParentTransform 
 * @param calib_time 
 * @param globalPublish 
 */
void CoREstimateManagement_DoublePart(fosim::OsimModel & osMod, 
optitrack_full::OptitrackNameMarkers & opNameMarkers,
std::string body_part_X,
std::string body_part_Y,
std::vector<std::map<int,Eigen::Vector3d> > markerData, 
std::vector<std::map<std::string,Eigen::Vector3f> > & allLocalX,
std::vector<std::map<std::string,Eigen::Vector3f> > & allLocalY,
std::map<std::string,int> corresNameID,
std::vector<double> scaling_factors,
ros::Time calib_time,
markersManagement::globalPublisher globalPublish );

/**
 * @brief Once a global idea of correspondences has been found (= each theorical body part is associated with optitrack markers that fits the most with the
 * theoretical model ), the goal of this method is to adjust this 
 * 
 * @return int 
 */
int enhanceRobustnessByICP(markersManagement::managementParameters & manParams,
            optitrack_full::OptitrackNameMarkers & opNameMarkers,
            std::vector<std::map<int,Eigen::Vector3d> > markerData,
            std::map<int,std::map<std::string, Eigen::Matrix4f> > & ground2ParentTransform,
            std::map<int, Eigen::Vector3d > optitrack_pos_calib,
            fosim::OsimModel & osMod,
            std::vector<double> & adapted_q_calib,
            std::map<std::string,int> & corresNameID,
            std::map<std::string,Eigen::Vector3d> & adapted_opensim_positions,
            std::map<std::string,std::vector<double> > & all_scaling_factors,
            ros::Time calib_time,
            markersManagement::globalPublisher globalPublish );
/**
 * @brief Once a global idea of correspondences has been found (= each theorical body part is associated with optitrack markers that fits the most with the
 * theoretical model ), the goal of this method is to adjust this 
 * 
 * @return int 
 */
int enhanceRobustnessByICP_DoublePart(markersManagement::managementParameters & manParams,
            optitrack_full::OptitrackNameMarkers & opNameMarkers,
            std::vector<std::map<int,Eigen::Vector3d> > markerData,
            std::map<int,std::map<std::string, Eigen::Matrix4f> > & ground2ParentTransform,
            std::map<int, Eigen::Vector3d > optitrack_pos_calib,
            fosim::OsimModel & osMod,
            std::vector<double> & adapted_q_calib,
            std::map<std::string,int> & corresNameID,
            std::map<std::string,Eigen::Vector3d> & adapted_opensim_positions,
            std::map<std::string,std::vector<double> > & all_scaling_factors,
            ros::Time calib_time,
            markersManagement::globalPublisher globalPublish );
/**
 * @brief Method managing the calibration process, whether it is in simulation or in reality,
 * with recorded or live data.
 * 
 */
void start_calib(optitrack_full::DataModel * dataModel, markersManagement::globalPublisher globalPublish);

/**
 * @brief Initialize the mocap_node. Inspired from initialize function of original optitrack_mocap package.
 * 
 */
void initialize_opensim(optitrack_full::DataModel * dataModel, 
bool record_trc, 
file_management::TRCManager & TRCManager,
optitrack_full::ServerDescription serverDescription,
markersManagement::globalPublisher globalPublish
);

void opensim_trc_calibration(optitrack_full::DataModel * dataModel, 
markersManagement::globalPublisher globalPublish,
bool doCalib = true,
bool isLabelized = true
);

/**
 * @brief Execute the publishing operations for an Optitrack message,
 * when the calibration process was called.
 * This allows to display the joint configuration of the calibration pose chosen by the user, how
 * the markers should be placed according to this calibration pose, and the current
 * positions of the streamed Optitrack markers.
 * 
 */
void calibration_opensim_run(optitrack_full::DataModel * dataModel, markersManagement::globalPublisher globalPublish, bool & calibration_asked );

/**
 * @brief Execute the classic publishing operations for an Optitrack message.
 * 
 */
void classic_opensim_run(optitrack_full::DataModel * dataModel, markersManagement::globalPublisher globalPublish, ros::Time time);

    bool isActivated;
    // Original generic model of the subject
    fosim::OsimModel osMod_origin;
    // Calibration

    // Duration before the calibration process should be executed
    double duration_bef_calib;
    // Duration between the beginning of the execution of the 
    // calibration process and the end of the recording of datas 
    // for the estimation of the center of rotations.
    double duration_for_CoR_estimate;
    // Boolean checking if the calibration service was called
    bool calibration_asked;    
    // Time at which the calibration service was called
    ros::Time time_calib;
    // Optitrack markers used for calibration
    std::vector<std::map<int,Eigen::Vector3d> > calibMarkerData;

    std::map<int, Eigen::Vector3d > optitrack_pos_calib;
    // Timestamp of the selected optitrack frame for calibration.
    ros::Time optitrack_pos_calib_time;
    // Frame number associated to Optitrack markers used for calibration
    int calibFrame;

    std::map<int,std::map<std::string, Eigen::Matrix4f> > ground2ParentTransform;

    optitrack_full::OptitrackNameMarkers opNameMarkersCoR;

    // body_part_name:[ frame of begin of movement, frame of end of movement ]
    std::map<std::string,std::vector<int> > CoRDecoupl;
    // body_part_name:[ index of begin of movement in calibMarkerData, frame of end of movement in calibMarkerData ]
    std::map<std::string, std::vector<int> > CoRDecouplReadapt;
    // Scaling

    bool scaling;

    int type_calib;
    // Mass of the subject
    double mass;
    // XML file that will be used for scaling
    std::string scale_file;
    // Json file that will be used for informations about the osim to URDF transformation
    std::string data_file;
    // Path to the ros package containing the URDF files
    std::string ros_package_path;    

    // Boolean checking if we're into a simulation or a real situation
    bool simulation;

    // Frame rate of Optitrack markers
    int frameRate;

    bool deb_tst;

    std::string osim_basic_path;
    std::vector<double> origin_q_calib;

    ros::ServiceClient python_scaling;
    // Client service, used to confirm or cancel the scaling of the model
    ros::ServiceClient confirmation_scaling;

  };

} // namespace

#endif