#ifndef markersManagement_h
#define markersManagement_h

#include "fosim/fosim.h"


#include <cstdlib>
#include <time.h>
#include <random>

#include <chrono>

#include <Eigen/Eigenvalues> 
#include <pcl/registration/icp.h>
#include <pcl/registration/transformation_estimation_svd.h>
#include <pcl/common/pca.h>
#include <pcl/common/common.h>
#include <pcl/conversions.h>
#include <pcl_conversions/pcl_conversions.h>

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/JointState.h>

#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>

#include "optitrack_full/all_errors.h"
#include "optitrack_full/markers_names_indexes.h"

struct LabelCarac
{
  // opensim marker associated with labelization
  std::string osim_name = "";

  // Whether this labeling was set by user or not.
  // If set by user ==> cannot change anymore with code.
  // The only possibility to change it is by doing a "FORCE"
  // demand for change.
  // Rules of byUser variable : 
  // - This parameter can only be true if the labelization
  // concerns a calibration frame or an adding directly made by 
  // the user (ex : "RRS=47")
  // - When set, the association is considered as true starting from
  // the frame ([47][-1] = "RRS"). Directly last association ([-1]) won't 
  // be saved (if an user had to interfere, we consider that it should be because
  // last association was false). Other previous associations won't be changed
  // (unless "FORCE" command is activated, which will cancel all previous 
  // associations, EVEN byUser associations).
  // (byUser suppression by "FORCE" might not be kept, but for now there is just too much possibilities that
  // a set byUser might be false; so unless it is calibration frame, we must keep
  // a possibility to change it)
  // - An automatic labelization CANNOT change this association; e.g the
  // id <-> osim_name marker association will be always kept by the automatic
  // labelization system, starting from the frame i. Updating this id <-> osim_name 
  // association for future frames could then only be done by the user. 
  // In addition, update id <-> osim_marker for previous frames could be done only by User.
  // To explain : 
  //
  //  --------------[prev_assos until frame i : byUser and bySystem]--------[-1 false association]----[future (maybe false) associations]----->
  //  
  // After user intervention : 
  //
  // ----------------[prev_assos until frame i : byUser and bySystem]--------[-1 : byUser only]--->

  bool byUser = false;
};

namespace markersManagement
{
    /**
     * @brief Structure containing all necessary elements to manage the simulation of
     * markers.
     * 
     */
    struct SimulationGestion
    {
        std::vector<int> joint_state;
        int i_joint;
        double speed;
        double j_val;
        int dissapear;
        int max_index;
        int same_frames;
        int cur_frames;
        double noise;
        std::map<std::string,int> hiddenCorresNameID;
        std::map<std::string,double> hiddenJointsValues;
        bool simu_verbose;

        bool calibCompleted;

    };

    /**
     * @brief Structure containing all necessary elements for the publication of errors in
     * real time.
     * 
     */
    struct ErrorsPublicationGestion
    {
        std::vector<std::string> joint_names;
        std::vector<double> articulation_errors;
        std::vector<std::string> associated_markers_names;
        std::vector<double> IK_markers_pose_errors;
        std::vector<std::string> markers_error;
        std::vector<int> id_errors;
        std::vector<int> true_id;
        std::vector<double> association_tolerance_value;
        std::vector<std::string> all_markers_names;
    };    

    /**
     * @brief Structure who manage the publication of a specific type of point cloud of markers,
     * with a list containing the rgb color.
     * 
     * 
     */
    struct markerPubInfo
    {
        // ROS publisher object of the point cloud.
        ros::Publisher markerPub;
        // Color associated with the point cloud (rgb format)
        int rgb[3];
    };
    /**
     * @brief Class containing important parameters that are fixed for marker management.
     * 
     */
    class managementParameters
    {
        public:
            managementParameters();
            // Maximum allowable translation squared difference between two consecutive transformations.
            double transformation_epsilon;
            // Maximum allowed Euclidean error between two consecutive steps in the ICP loop, before 
            // the algorithm is considered to have converged.
            // The error is estimated as the sum of the differences between correspondences in an Euclidean 
            // sense, divided by the number of correspondences.
            double euclidean_fitness_epsilon;
            // Maximum number of iterations for icp algorithm
            int icp_max_iter;
            // Verbose parameter for the ICP association of markers.
            bool icp_verbose;
            // Verbose parameter for the initial guess of the transformation from Opensim to Optitrack
            bool guess_verbose;
            // Verbose parameter for the filtering of the ICP association of markers.
            bool filtering_verbose;

            bool ik_verbose;

            double maximum_translation;

            double maximum_rotation;


    };


    /**
     * @brief Class managing the publication of all the point clouds of markers.
     * 
     */
    class globalPublisher
    {
        public:

            globalPublisher();
            globalPublisher(ros::NodeHandle & nh, std::string topic_q);

            void markerPublisher(ros::Time time,std::map<std::string,Eigen::Vector3d> markers, markerPubInfo markPub, bool optitrackRef = true, std::map<int, std::vector<int> > colors = {} );
            void markerPublisher(ros::Time time,std::map<int,Eigen::Vector3d> markers, markerPubInfo markPub, std::map<int, std::vector<int> > colors= {} );            
            void publishOpensimOptitrack(ros::Time time, std::map<int,Eigen::Vector3d> optitrackMarkers, fosim::OsimModel & osMod, Eigen::Matrix4f result_transf=Eigen::Matrix4f::Zero());
            void publishJointState(ros::Time time, std::map<std::string, double> D_joints_values);
            void publishOpensim(ros::Time time, std::map< std::string, Eigen::Vector3d > osim_pos, std::string name = "", std::map<std::string,int> lastOsNotAssigned = {});

            void publishOptitrackOpensimAssociation(ros::Time time, std::map<std::string,int> corresNameID,std::map<int,Eigen::Vector3d> opti_markers);

            void publishErrors(ros::Time time, ErrorsPublicationGestion & error_pub);

            void displayArrows(ros::Time time, Eigen::Matrix4f transf, markerPubInfo displayObject, std::string frameID = "ground");
  
            // MarkerPubInfo structure for publication of optitrack pointclouds
            markerPubInfo optitrack_publisher;
            // MarkerPubInfo structure for publication of optitrack pointclouds
            markerPubInfo optitrack_robust_publisher;
            // MarkerPubInfo structure for publication of optitrack pointclouds that are associated with Opensim markers
            markerPubInfo optitrack_associated_publisher;            
            // MarkerPubInfo structure for publication of opensim pointclouds. Those
            // points are directly attached to the Opensim model.             
            markerPubInfo opensim_publisher;

            markerPubInfo opensim_noassos_publisher;            
            // MarkerPubInfo structure used only to display the theorical pose of
            // the opensim markers according to the calibration pose.            
            markerPubInfo opensim_theorical_publisher;
            // Publisher of the configuration of the joints of the model (Q vector)
            ros::Publisher js_pub;
            // Transform matrix from Opensim (XZY) to URDF referential (XYZ).
            Eigen::Matrix3d osMpi;
            Eigen::Matrix3f osMpi_f;

            markerPubInfo ArrowsOs;

            markerPubInfo ArrowsOp;

            markerPubInfo osBody;

            markerPubInfo opBody;

            ros::Publisher publish_errors;

            std::map<std::string,markerPubInfo> allPublishers;

    };

void transformFiltering(Eigen::Matrix4f & result_transf, double maximum_translation, double maximum_rotation);


int startBodyFiltering(  managementParameters & manParams,
                    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp,
                    fosim::OsimModel & osMod,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr osimPC,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr optiPC,
                      std::vector<std::string> osim_name,
                      std::vector<int> opti_indexes,
                      std::map<int, Eigen::Vector3d > optitrackPoints,
                      Eigen::Matrix4f result_transf ,
                      std::map< std::string, int > & corresNameID,
                      std::map<std::string,double> & distOsimOpti,
                      bool verbose = true);

int startBodyFiltering(  managementParameters & manParams,
                    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp,
                    std::map<std::string,std::string> D_markers_body,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr osimPC,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr optiPC,
                      std::vector<std::string> osim_name,
                      std::vector<int> opti_indexes,
                      std::map<int, Eigen::Vector3d > optitrackPoints,
                      Eigen::Matrix4f result_transf ,
                      std::map< std::string, int > & corresNameID,
                      std::map<std::string,double> & distOsimOpti,
                      bool verbose);

int startGlobalFiltering(  managementParameters & manParams,
                    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp,
                    fosim::OsimModel & osMod,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr osimPC,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr optiPC,
                      std::vector<std::string> osim_name,
                      std::vector<int> opti_indexes,
                      std::vector<double> q_calib,
                      std::map<int, Eigen::Vector3d > optitrackPoints,
                      Eigen::Matrix4f result_transf ,
                      std::map< std::string, int > & corresNameID);                      
    
  std::string printAssociation(std::map< std::string, int > corresNameID);

  std::string printAssociationByBody(std::map< std::string, int > corresNameID, fosim::OsimModel & osMod);

    void getGuess(pcl::PointCloud<pcl::PointXYZ>::Ptr PC, 
    pcl::PointXYZ farthest_osim, 
    int side,
    Eigen::Vector4f & opti_centroid, double & angle, managementParameters manParams);

    int getFarthestUppestFromCentroid(Eigen::Vector4f centroid,
    int & index_side_1,
    int & index_side_2,
    int & number_pos,
    int & number_neg,
    pcl::PointCloud<pcl::PointXYZ>::Ptr PC);

    int startManualCorrection(std::map< std::string, int > & corresNameID,
                           std::map<int, Eigen::Vector3d > optitrackPoints,
                           fosim::OsimModel & osMod,
                           globalPublisher globalPublish,
                           Eigen::Matrix4f result_transf
                            );

std::vector<int> tryToCorrectCalibPose( managementParameters & manParams,
            std::map<std::string,int> & corresNameID,
 fosim::OsimModel & osMod_origin,
  std::map<int, Eigen::Vector3d >optitrack_pos_calib,
   std::vector<double> origin_q_calib,
    std::vector<double>  & scaling_factors,
ros::Time optitrack_pos_calib_time,
    fosim::OsimModel & rescale_osMod,
   globalPublisher globalPublish, 
     Eigen::Matrix4f & result_transf,
     bool doPCScaling = false);


    void allDistanceVariationStatic( std::map< std::string, std::map<std::string,double> > D_mark_all_dist_ori, std::map< std::string,  std::map< std::string, double> > D_mark_all_dist, std::map<std::string, double>& variation, double maximum_residual, double mdc);

    void allDistanceVariationDynamic( std::map< std::string, std::map<std::string,double> > D_mark_all_dist_ori,
     std::map< std::string,  std::map< std::string, double> > D_mark_all_dist,
     std::map<std::string, std::map<std::string,double> > D_mark_all_dist_theorical,
      std::map<std::string, double>& variation , double maximum_residual, double dt, double mdc , bool verbose=false);

    int calibrationByICP(
                managementParameters & manParams,
                std::map<std::string, Eigen::Vector3d > & theoricalOpensimMarkers,
                std::map<int, Eigen::Vector3d > optitrackPoints,
                fosim::OsimModel & osMod,
                std::vector<double> q_calib,
                std::map<std::string,int> & corresNameID,
                Eigen::Matrix4f & result_transf,
                std::vector<double> & scaling_factors,
                bool useResultTransfAsGuess = false,
                bool doPCScaling = true
                ) ;
        

    std::map<int, Eigen::Vector3d> simulateMarkers(fosim::OsimModel & osMod, 
                                                int frame_rate, 
                                                SimulationGestion & simu_gest);


    int checkScaling(fosim::OsimModel & osMod, 
    std::map<std::string,double> D_j_v_ori,
    std::map<int,Eigen::Vector3d> opti_pos_calib,
    std::map<std::string,Eigen::Vector3d> markersLocation,
    globalPublisher globalPublish);

    //void rotationFiltering(Eigen::Matrix4f & result_transf);

    void rescaleClouds(pcl::PointCloud<pcl::PointXYZ>::Ptr& sourceCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& targetCloud, Eigen::Matrix4f transf, std::vector<double> & scaling);

    void rescaleClouds2(pcl::PointCloud<pcl::PointXYZ>::Ptr& sourceCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& targetCloud, Eigen::Matrix4f transf, std::vector<double> & scaling);


    void rescaleCloudsNoTransform(pcl::PointCloud<pcl::PointXYZ>::Ptr& sourceCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& targetCloud, std::vector<double> & scaling, Eigen::Matrix4f & transf);

    std::map<std::string,Eigen::Vector3d> applyScalingToMarkers(std::map<std::string, Eigen::Vector3d> markersPos, std::vector<double> scaling_factors);

// int enhanceRobustnessByICP(managementParameters & manParams,
//             std::vector<std::map<int,Eigen::Vector3d> > markerData,
//             std::vector<std::map<std::string, Eigen::Matrix4f> > & ground2ParentTransform,
//             std::map<int, Eigen::Vector3d > optitrack_pos_calib,
//             fosim::OsimModel & osMod,
//             std::vector<double> & adapted_q_calib,
//             std::map<std::string,int> & corresNameID,
//             std::map<std::string,Eigen::Vector3d> & adapted_opensim_positions,
//             std::map<std::string,std::vector<double> > & all_scaling_factors,
//             ros::Time calib_time,
//             globalPublisher globalPublish );



int linreg(std::vector<double> x, std::vector<double> y, double & m, double & b, double & r);

int linreg2(std::vector<double> x, std::vector<double> y, double & m);

void rescaleCloudsWithAssociation2(std::map<int,Eigen::Vector3d> optitrackPoints, 
std::map<std::string,Eigen::Vector3d> & opensimPoints, 
fosim::OsimModel & osMod, 
std::map<std::string,int> corresNameID, Eigen::Matrix4f transf, 
std::vector<double> & scaling,
globalPublisher globalPublish  );


void checkSimulationAssociation(std::map<std::string,int> corresNameId,
std::map<std::string,int> trueCorresNameId,
ErrorsPublicationGestion & error_pub,
bool verbose);

void checkSimulationIKConfiguration(std::map<std::string,double> D_joints_values,
std::map<std::string,double> true_D_joints_values,
ErrorsPublicationGestion & error_pub,
bool verbose);

void checkAssignedMarkers(std::map<std::string, int> osMarkersNotAssigned,
std::map<std::string, std::string> D_marker_body,
bool verbose);

void computeMarkersErrors(std::map<std::string, Eigen::Vector3d> osMarkersExperimental,
std::map<std::string, Eigen::Vector3d> osMarkersTheorical,
std::map<std::string,double> markerWeights,
ErrorsPublicationGestion & error_pub,
bool verbose);

void publishToleranceValue(std::map<std::string,double> toleranceValue,
ErrorsPublicationGestion & error_pub,
bool verbose);

void clearErrorPublicationGestion(ErrorsPublicationGestion & error_pub);

void findTransformWithAssociation(std::map<int,Eigen::Vector3d> optitrackPoints, 
std::map<std::string,Eigen::Vector3d> opensimPoints, 
std::map<std::string,int> corresNameID,
Eigen::Matrix4f & transf);


// void CoREstimateManagement(fosim::OsimModel & osMod, 
// std::string body_part,
// std::vector<std::map<int,Eigen::Vector3d> > markerData, 
// std::map<std::string,int> corresNameID,
// std::vector<double> scaling_factors,
// std::vector<std::map<std::string, Eigen::Matrix4f> > & ground2ParentTransform,
// ros::Time calib_time,
// globalPublisher globalPublish );

void CoREstimateWithMarkers_NM(std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerData, 
Eigen::Vector3f & m,
ros::Time calib_time,
globalPublisher globalPublish );

void CoREstimateWithMarkers_Gam(std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerData, 
Eigen::Vector3f & m,
ros::Time calib_time,
globalPublisher globalPublish );

void replacePointsAndEstimateScaling(std::map<int,Eigen::Vector3d> optitrackPoints, 
Eigen::Vector3f optitrackCoR,
fosim::OsimModel & osMod, 
std::map<std::string,int> corresNameID,
std::vector<double> & scaling,
ros::Time calib_time,
globalPublisher globalPublish  );

void suppressParentInfluenceInData(fosim::OsimModel & osMod, 
std::string body_part,
std::vector<std::map<int,Eigen::Vector3d> > markerData,
std::map<std::string,int> corresNameID,
std::map<std::string,double> markersWeights,
std::map<int,std::map<std::string, Eigen::Matrix4f> > & ground2ParentTransform,
ros::Time calib_time,
globalPublisher globalPublish);

void recoveryMarkersProcedure(std::vector<std::map<std::string, Eigen::Vector3d> > & opensimMissingMarkers,
std::map<std::string,Eigen::Vector3d> originOpensimMarkers,
globalPublisher globalPublish,
ros::Time calib_time,
bool verbose = false);

void recoveryMarkersProcedure(std::map<std::string, Eigen::Vector3d> & opensimMissingMarkers,
std::map<std::string,Eigen::Vector3d> originOpensimMarkers,
globalPublisher globalPublish,
ros::Time calib_time,
bool verbose = false);

void CoREstimateWithMarkers_DoublePart(std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerDataX, 
std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerDataY,
std::map<std::string,Eigen::Vector3f> & localPlacementX,
std::map<std::string,Eigen::Vector3f> & localPlacementY,
ros::Time calib_time,
globalPublisher globalPublish );

int bodyMarkerRecoveryByICP( managementParameters & manParams,
            std::map<std::string, Eigen::Vector3d > theoricalOpensimMarkers,
            std::map<int, Eigen::Vector3d > optitrackPoints,
            fosim::OsimModel & osMod,
            std::map<std::string,int> & corresNameID,
            double lim,
            globalPublisher globalPublish,
            std::map<int,std::string> CINCalib = {},
            bool canTryPartial = true
             );

int bodyMarkerRecoveryByICP(
            managementParameters & manParams,
            std::map<std::string, Eigen::Vector3d > theoricalOpensimMarkers,
            std::map<int, Eigen::Vector3d > optitrackPoints,
            std::map<std::string,std::map<std::string,double> > D_mark_all_dist,
            double min_mark_dist,
            std::map<std::string,std::string> D_markers_body,
            std::map<std::string,int> & corresNameID,
            double lim,
            globalPublisher globalPublish,
            std::map<int,std::string> CINCalib = {},
            bool canTryPartial = true
             );


void getTransformationMatrixOfPointCloudByPCA(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
Eigen::Matrix4f & transformation);

void getTransformationMatrixOfPointCloudByPCA(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
Eigen::Matrix4f & transformation,
Eigen::Vector4f translation);

void rotationFromVec1ToVec2(Eigen::Vector3f vec1, Eigen::Vector3f vec2, Eigen::Matrix3f & rot);

void getBodyGuess(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::vector<Eigen::Vector3d> corresPoints, 
Eigen::Matrix4f & transf,
std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> & returnPC,
std::vector< std::map<int,Eigen::Vector3d> > & returnPCSimpl,
bool multiZeroCase,
std::map<int,int> & axCorres);

void getBodyGuess2(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, std::vector<Eigen::Vector3d> corresPoints, 
Eigen::Matrix4f & transf,
std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> & returnPC,
std::vector< std::map<int,Eigen::Vector3d> > & returnPCSimpl,
bool multiZeroCase);

void mapIntEigen2PCL(std::map<int,Eigen::Vector3d> map, pcl::PointCloud<pcl::PointXYZ>::Ptr & PC);

void mapStringEigen2PCL(std::map<std::string,Eigen::Vector3d> map, pcl::PointCloud<pcl::PointXYZ>::Ptr & PC, std::vector<std::string> & nameOrder);

 void controlForbiddenAreas( std::map<int, Eigen::Vector3d> & frame, std::vector<std::vector<Eigen::Vector3d> > forbid_areas);

  void manageACINAdding(
    std::string name,
    int id,
    int frameNumber,
    std::map<int, std::map<int,LabelCarac> > & allCorresIDName,
    bool repeat = false,
          bool byUser = false,
    bool verbose = false,
      bool canClean = false

  );



     bool checkACINCorres(
    int id,
    int frameNumber,
    std::map<int, std::map<int,LabelCarac> > allCorresIDName,
    std::string & name,
    int & framePicked,
    bool repeat = true,
    bool verbose = false
  );

  void manageACINBeginReturn(
    int maxFrameNumber,
    std::map<int, Eigen::Vector3d> begFrame,
    std::map<int,Eigen::Vector3d> lastFrame,
    std::map<int, std::map<int,LabelCarac> > & allCorresIDName,
    std::map<std::string, int> & corresNameID,    
    bool repeat = true,
    bool verbose = false
  );

    void suppressACINCorres(
    int optiID,
    int currentFrame,
    std::map<int, std::map<int,LabelCarac> > & allCorresIDName,
    bool cleanFORCE = false,
    bool byUser = false
  );

}



#endif