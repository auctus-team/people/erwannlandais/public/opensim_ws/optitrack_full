/* 
 * Copyright (c) 2018, Houston Mechatronics Inc., JD Yamokoski
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its 
 *    contributors may be used to endorse or promote products derived from 
 *    this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
//#pragma once
#ifndef __MOCAP_OPTITRACK_NATNET_MESSAGES_H__
#define __MOCAP_OPTITRACK_NATNET_MESSAGES_H__

#include <vector>
#include <optitrack_full/data_model.h>

namespace natnet
{
    typedef std::vector<char> MessageBuffer;

    struct MessageInterface
    {
        virtual void serialize(MessageBuffer&, optitrack_full::DataModel const*) {};
        virtual void deserialize(MessageBuffer const&, optitrack_full::DataModel*) {};
    };

    struct ConnectionRequestMessage : public MessageInterface
    {
        virtual void serialize(MessageBuffer& msgBuffer, optitrack_full::DataModel const*);
    };

    struct ServerInfoMessage : public MessageInterface
    {
        virtual void deserialize(MessageBuffer const&, optitrack_full::DataModel*);
    };

    class DataFrameMessage : public MessageInterface
    {
        struct RigidBodyMessagePart
        {
            void deserialize(MessageBuffer::const_iterator&, 
                optitrack_full::RigidBody&,
                optitrack_full::Version const&,
                bool verbose = false);
        };


    public:
        virtual void deserialize(MessageBuffer const&, optitrack_full::DataModel*);
        virtual void deserializeFast(MessageBuffer const&, optitrack_full::DataModel*);
        virtual void receptionMessage(MessageBuffer const&, optitrack_full::DataModel*);

        struct LabeledMarkersMessagePart
        {
            void deserialize(MessageBuffer::const_iterator&, 
                std::map<int,Eigen::Vector3d> &,
                std::map<int,Eigen::Vector3d> &,
                std::map<int, std::map<int,Eigen::Vector3d> > &,
                bool,
                optitrack_full::Version const&);
        };

        struct MarkerSetsPart
        {
            void deserialize(MessageBuffer::const_iterator&, 
            std::vector<optitrack_full::MarkerSet> &,
            bool, 
            bool &);

        };

    };

    struct MessageDispatcher
    {
        static void dispatch(MessageBuffer const&, optitrack_full::DataModel*);
    };



    namespace utilities
    {
        void seek(MessageBuffer::const_iterator& iter, size_t offset);
        template <typename T> 
        void read_and_seek(MessageBuffer::const_iterator& iter, T& target);
        void decode_marker_id(int sourceID, int& pOutEntityID, int& pOutMemberID);
        void decode_timecode(unsigned int inTimecode, unsigned int inTimecodeSubframe, int& hour, int& minute, int& second, int& frame, int& subframe);
        void stringify_timecode(unsigned int inTimecode, unsigned int inTimecodeSubframe, char *Buffer, int BufferSize);
        void selectOptitrackMessage(std::map<int,optitrack_full::OptitrackMessage>);
          bool checkIKPossible(std::map<std::string, int> osMarkersNotAssigned,
    std::map<std::string, std::vector<std::string> > D_body_markers,
    int & numOsNotAssigned,
        std::map<std::string,bool> & enoughOsAssigned,
    std::map<std::string,int> & numOsByBodyPart,
    bool forIKOnly = true);
      void markersRecognitionAndIK(  optitrack_full::OptitrackNameMarkers& opNameMarkers,
  fosim::OsimModel & osMod);

  void markersAssociation(  optitrack_full::OptitrackNameMarkers & opNameMarkers,
  fosim::OsimModel & osMod,
  std::map<std::string,int> & osMarkersNotAssigned,
  bool & IK_possible,
  int & numOsNotAssigned,
  bool & called);
  void markersAssociation(  optitrack_full::OptitrackNameMarkers & opNameMarkers,
  std::vector<optitrack_full::MarkerSet> & allMarkerSets);

    void selectOptitrackMessage(std::map<int,optitrack_full::OptitrackMessage> messageQueue, optitrack_full::DataModel* dataModel);
   void manageTime(optitrack_full::DataModel * dataModel, double timeFrame);


  void manageRepeatAssociation( std::string treatedMark, int id_selected,
  optitrack_full::OptitrackNameMarkers& opNameMarkers,
  bool canClean = false
  );
 
  bool isIDConcernedByInversion( optitrack_full::OptitrackNameMarkers& opNameMarkers,
  int optiID,
  std::pair<std::string,int> & markCorrection);

    }


}

#endif