/* 
 * Copyright (c) 2018, Houston Mechatronics Inc., JD Yamokoski
 * Copyright (c) 2012, Clearpath Robotics, Inc., Alex Bencz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its 
 *    contributors may be used to endorse or promote products derived from 
 *    this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Local includes
#ifndef __RIGID_BODY_MANAGEMENT_H__
#define __RIGID_BODY_MANAGEMENT_H__

#include <optitrack_full/data_model.h>
#include <optitrack_full/mocap_config.h>

#include "optitrack_full/natnet/natnet_messages.h"
#include "optitrack_full/calibration.h"
#include "optitrack_full/scaling.h"

#include "optitrack_full/trc_manager.h"

#include "optitrack_full/saveTxt.h"
#include "std_srvs/SetBool.h"
// ROS includes

#include <ros/ros.h>
#include "geometry_msgs/PoseStamped.h"

// Others
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <experimental/filesystem>

namespace fs = std::experimental::filesystem;

namespace rigid_body_management
{

  /**
   * @brief Class managing the bridge from Optitrack message to ROS.
   * It also manages simulation and scaling operations.
   * 
   */
  class RigidBodyManagement
  {
  public:
    RigidBodyManagement(optitrack_full::ServerDescription serverDescription);

/**
 * @brief Initialize the mocap_node. Inspired from initialize function of original optitrack_mocap package.
 * 
 */
void initialize_rigid_body(optitrack_full::DataModel * dataModel, 
bool record_trc, 
file_management::TRCManager & TRCManager,
optitrack_full::ServerDescription serverDescription,
markersManagement::globalPublisher & globalPublish,
ros::NodeHandle& nh
);

void initRigidBodyWithMarkers(std::map<std::string,int> markerNamesAxis,
std::map<std::string,Eigen::Vector3d> markersPosition,
bool allMarkers,
Eigen::Matrix4f currentMotivePose,
Eigen::Matrix4f & initTransform,
std::map<std::string,Eigen::Vector3d> & markersPositionOrigin,
std::map<std::string,std::map<std::string,double> > & markersDistance,
std::vector<std::string> & markerNames);

void reexpressCalibFrameInLocalBody(pcl::PointCloud<pcl::PointXYZ>::Ptr notAlignedCloud,
std::map<std::string,Eigen::Vector3d> markersPosition,
bool allMarkers,
Eigen::Matrix4f currentMotivePose);

void initialize_new_markerSets(optitrack_full::DataModel * dataModel, markersManagement::globalPublisher & globalPublish,
ros::NodeHandle& nh);

/**
 * @brief Execute the classic publishing operations for an Optitrack message.
 * 
 */
void classic_rigid_body_run(optitrack_full::DataModel * dataModel, markersManagement::globalPublisher globalPublish, ros::Time time);


void set_init_markerSet(optitrack_full::MarkerSet & markSet,
markersManagement::globalPublisher & globalPublish,
ros::NodeHandle& nh,
int N = -1,
int l = -1);

void associateRigidBodiesMarkers(optitrack_full::DataModel * dataModel); 

  bool isActivated;

  Eigen::Matrix4f initTransform;  

  Eigen::Matrix3d osMpi;

  Eigen::Matrix4f osMpi_4f;   
  Eigen::Matrix4f prevTransf;

  std::map<int, std::string> corresIDNameInitFrame;

  std::map< std::string,ros::Publisher> allPosePublishers;
  std::map<std::string, std::vector<std::string> > displayPublisherNames;
  int colorValue;
  int seqHeader;


};

} // namespace

#endif