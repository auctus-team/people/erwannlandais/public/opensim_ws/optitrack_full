/* 
 * Copyright (c) 2018, Houston Mechatronics Inc., JD Yamokoski
 * Copyright (c) 2012, Clearpath Robotics, Inc., Alex Bencz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its 
 *    contributors may be used to endorse or promote products derived from 
 *    this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Local includes
#include <optitrack_full/socket.h>
#include <optitrack_full/data_model.h>
#include <optitrack_full/mocap_config.h>
#include <optitrack_full/rigid_body_publisher.h>
#include "optitrack_full/natnet/natnet_messages.h"
#include "optitrack_full/calibration.h"
#include "optitrack_full/scaling.h"
#include "optitrack_full/trcParameters.h"
#include "std_srvs/SetBool.h"
#include "optitrack_full/time.h"

#include "optitrack_full/trc_manager.h"

#include "optitrack_full/saveTxt.h"
#include "optitrack_full/listString.h"
#include "optitrack_full/SetFloat.h"
// ROS includes
// #include <ros/ros.h>

// Others
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <experimental/filesystem>

#include "optitrack_full/opensim_management.h"
#include "optitrack_full/rigid_body_management.h"

#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Int64.h>
#include <std_msgs/MultiArrayDimension.h>

namespace fs = std::experimental::filesystem;

namespace optitrack_full
{

  /**
   * @brief Class managing the bridge from Optitrack message to ROS.
   * It also manages simulation and scaling operations.
   * 
   */
  class OptiTrackRosBridge
  {
  public:
    OptiTrackRosBridge(ros::NodeHandle& nh,
      ServerDescription const& serverDescr, 
      PublisherConfigurations const& pubConfigs,
      ModelConfiguration const& modelConfig) :
        nh(nh),
        serverDescription(serverDescr),
        publisherConfigurations(pubConfigs),
        modelConfig(modelConfig),
        dataModel(modelConfig),
        opensimManagement(modelConfig.osim_path,modelConfig.geometry_path,modelConfig.setup_ik_path,modelConfig.q_calib,nh),
        rigidBodyManagement(serverDescr),
        TRCManager(modelConfig.trc_file,( serverDescr.type_record == "trc") ),
        globalPublish(nh, modelConfig.topic_q)

    {
      if (serverDescr.type_record == "trc")
      {
        trcFile = modelConfig.trc_file;
        withTRC=true;
      }
    }
/**
 * @brief Gets the optitrack markers from a ROS message
 * 
 * @param msg 
 */
void markers_callback(const sensor_msgs::PointCloud2ConstPtr& msg){

    bool ready = ( (dataModel.lastFrame == dataModel.frameNumber) || (dataModel.lastFrame == -1) );

    if (!dataModel.dispatching && (ready) )
    {
      
      pcl::PointCloud<pcl::PointXYZRGBL> pc;
      pcl::fromROSMsg(*msg,pc);
      dataModel.opNameMarkers.lastOptitrackMarkers.clear();
      dataModel.opNameMarkers.lastOptitrackMarkersNotAssigned.clear();

      for (int i = 0; i < pc.points.size(); i++ )
      {
        pcl::PointXYZRGBL pt = pc.points[i];
        Eigen::Vector3d v(pt.x, pt.y, pt.z);
        dataModel.opNameMarkers.lastOptitrackMarkers[pt.label] = osMpi_i*v;
      }


      markersManagement::controlForbiddenAreas(dataModel.opNameMarkers.lastOptitrackMarkers,dataModel.opNameMarkers.forbidden_areas);
      markersManagement::controlForbiddenAreas(dataModel.opNameMarkers.lastOptitrackMarkersNotAssigned,dataModel.opNameMarkers.forbidden_areas);

      // time determination

      dataModel.opNameMarkers.messageReceptionTime = msg->header.stamp;

      double mesTime = dataModel.opNameMarkers.messageReceptionTime.toSec();
      if (dataModel.frameOrigin == -1)
      {
        dataModel.opNameMarkers.originTime = dataModel.opNameMarkers.messageReceptionTime;
        dataModel.frameOrigin = 0;
        dataModel.lastFrame = dataModel.frameOrigin;    
        lastRecordedIndex.clear();    
        currentRecordedIndex.clear();
        it_tRAI = timeRecordedAttributedIndex.begin();        

      }

      else if ( dataModel.opNameMarkers.messageReceptionTime.toSec() <= std::max(dataModel.opNameMarkers.lastOptitrackTime.toSec()-5/frameRate,dataModel.opNameMarkers.originTime.toSec() ) )
      {
        dataModel.opNameMarkers.originTime = dataModel.opNameMarkers.messageReceptionTime;
        dataModel.lastFrame = dataModel.frameOrigin;
        lastRecordedIndex.clear();
        currentRecordedIndex.clear();
        it_tRAI = timeRecordedAttributedIndex.begin();
        //std::cout << "--Reboot--" << std::endl;
      }
      else
      {
        dataModel.lastFrame += 1;
      }

      // determine which recordedAttributedIndex to use
      bool recc = false;
      if (mesTime < maxRosTime)
      {
        //std::cout << "find recAI" << std::endl;
        std::map<double, std::map<int,int> >::iterator it = it_tRAI;  //timeRecordedAttributedIndex.begin();
        double timeStud = it->first;
        while (it!=timeRecordedAttributedIndex.end() && mesTime>it->first)
        {
          std::advance(it,1);
        }
        if (it!=timeRecordedAttributedIndex.end())
        {
          //std::cout << "found recAI : diff time : " << it->first-mesTime << std::endl;
          recordedAttributedIndex = it->second;
          it_tRAI = it;
        }
        else
        {
          it_tRAI = timeRecordedAttributedIndex.begin();
          recc = true;
        }

      }

      else
      {
        maxRosTime = mesTime;
        recc = true;
      }


      // filtering step
      std::map<int,Eigen::Vector3d> frameOpti;
      for (std::map<int,Eigen::Vector3d>::iterator it = dataModel.opNameMarkers.lastOptitrackMarkers.begin(); it!=dataModel.opNameMarkers.lastOptitrackMarkers.end(); it++)
      {
        int id = it->first;
        int attributedID;
        if (brutalFiltering)
        {
          // first time the marker appears
          if ( recordedAttributedIndex.find(id) == recordedAttributedIndex.end() )
          {
              attributedID = lastMaxID;
              lastMaxID++;
              recordedAttributedIndex[id] = attributedID;
          }
          // marker has dissapear and then reappear
          // issue : we cannot trust Optitrack; maybe the association to the
          // new marker is wrong
          else if ( (lastRecordedIndex.size() >0) && (std::find(lastRecordedIndex.begin(), lastRecordedIndex.end(), id) == lastRecordedIndex.end()) )
          {
              //std::cout << "Error : blinking marker" << std::endl;
              attributedID = lastMaxID;
              lastMaxID++;
              recordedAttributedIndex[id] = attributedID;                        
          }
          else
          {
              attributedID = recordedAttributedIndex[id];
          }
          currentRecordedIndex.push_back(id);
        }
        else
        {
            attributedID = id;
        }

        frameOpti[attributedID] = it->second;
      }



      if (brutalFiltering)
      {
        lastRecordedIndex = currentRecordedIndex;
        currentRecordedIndex.clear();
      }
      if (recc)
      {
        timeRecordedAttributedIndex[mesTime] = recordedAttributedIndex;
      }

      dataModel.opNameMarkers.lastOptitrackMarkers = frameOpti;
      dataModel.opNameMarkers.lastOptitrackMarkersNotAssigned = frameOpti;


    }

}

void get_forbidden_areas(std::string filename)
{
  std::ifstream csvFile;
    std::string line;
    std::vector<std::string> cols;
  csvFile.open(filename.c_str());
  if (csvFile.is_open())
  {
    // first line is just a legend, skip it
    getline(csvFile, line);
    while (getline(csvFile,line))
    {  
      cols.clear();

      cols = fosim::splitString(line,",");    

      if (cols.size() >= 7)
      {
        Eigen::Vector3d min;
        Eigen::Vector3d max;
        min(0,0) = std::stod(cols[0]);
        min(1,0) = std::stod(cols[1]);
        min(2,0) = std::stod(cols[2]);
        max(0,0) = std::stod(cols[3]);
        max(1,0) = std::stod(cols[4]);
        max(2,0) = std::stod(cols[5]);  

        std::string ref = cols[6];

        if (ref == "ROS")
        {
          Eigen::Matrix3d correcMat = osMpi_i;
          // reexpress in Opensim referential
          min = correcMat*min;
          max = correcMat*max;
        }



        std::vector<Eigen::Vector3d> v;
        v.push_back(min);
        v.push_back(max);

        dataModel.opNameMarkers.forbidden_areas.push_back(v);  
        std::cout << "forbidden box " << dataModel.opNameMarkers.forbidden_areas.size() << " : " << "min : " << min << " ; max : " << max << std::endl;  
      }

    }
  }
}

/**
 * @brief Service triggering the calibration process. This service could be called even during the duration
 * of an other calibration process (ex : if you're not sure about the time it will take for your subject to move to
 * the calibration pose : you can call this service a first time with a very long duration, use the displayed topic to
 * help the subject to move to the calibration pose, and then recall this service with a shorter duration when the pose is satisfactory ).
 * 
 * @param req calibration Service Request. Contains the duration before the
 * calibration process should be activated, whether scaling should be applied, and
 * the mass of the subject (scaling purpose).
 * @param res calibration Service Response.
 * @return true
 */
bool ask_calib(optitrack_full::calibration::Request &req,
                optitrack_full::calibration::Response &res)
{
  if (record_trc && TRCManager.trcCorresNameId.size() > 0)
  {
    std::printf("Error : correspondences already found thanks to .trc file. Request for calibration denied. \n");
  }

  else
  {

    while (dataModel.dispatching)
    {
      continue;
    }

    calibration_asked = true;
    if (opensimManagement.isActivated)
    {
      if (pauseMode == 1)
      {
        isPaused = false;
        dataModel.opNameMarkers.pause_mode = false;
      }

      opensimManagement.reboot_calib(&dataModel);

      opensimManagement.duration_bef_calib = req.time;
      opensimManagement.scaling = req.scaling;
      opensimManagement.type_calib = req.type_calib;
      opensimManagement.mass = req.mass;
      if (opensimManagement.type_calib == 0)
      {
        opensimManagement.duration_for_CoR_estimate = req.recording_time;
      }
      else
      {
        opensimManagement.duration_for_CoR_estimate = 0.0;
      }


      opensimManagement.time_calib = ros::Time::now();

      ROS_INFO("Calibration will begin in : %3.2f seconds.",opensimManagement.duration_bef_calib);
      ROS_INFO("Please hold the calibration pose associated with the model.");

      res.success = true;
        
      // apply the calibration pose to the non-scaled model, to show
      // how the user should be placed according to calibration pose.
      opensimManagement.osMod_origin.setQ(opensimManagement.origin_q_calib);
      globalPublish.publishJointState(opensimManagement.time_calib, opensimManagement.osMod_origin.D_joints_values);
      //fosim::printJointsValues(opensimManagement.osMod_origin.osimModel,opensimManagement.osMod_origin.osimState);
    }
    return(true);
    }
  return(true);

} 

bool ask_pause_continue(std_srvs::SetBool::Request &req,
               std_srvs::SetBool::Response &res)
{
  // while (dataModel.dispatching)
  // {
  //   continue;
  // }

  isPaused = req.data;

  res.success = true;
  res.message = "pause_continue ok";

  return(true);

}

bool get_speed(optitrack_full::SetFloat::Request &req,
               optitrack_full::SetFloat::Response &res)
{
  if (req.data == 0)
  {
    speed = 1e40;
  }
  else
  {
    speed = 1.0/req.data;
  }
  res.success = true;

  return(true);


}


bool ask_fast_read(std_srvs::SetBool::Request &req,
               std_srvs::SetBool::Response &res)
{
  // while (dataModel.dispatching)
  // {
  //   continue;
  // }

  fastRead = req.data;
  speed = 1.0;

  res.success = true;
  res.message = "fast_read ok";

  return(true);

}

bool ask_for_new_trc_file(optitrack_full::saveTxt::Request &req,
               optitrack_full::saveTxt::Response &res)
{

  trcFile = req.pathToFolder;
  withTRC = true;
  record_trc = true;

  // 0 : don't keep markers positions in local bodies
  // 1 : keep markers positions in local bodies (useful if it's the same person)
  typeReboot = req.typeSave;
  
  
  // need for reboot after such a change
  state = 2;

  res.success = true;

  return(true);

}

bool ask_trc_params(optitrack_full::trcParameters::Request &req,
               optitrack_full::trcParameters::Response &res)
{
  if (state != 2)
  {
  res.dataFrame = trcParam.dataFrame;
  res.maxTime = trcParam.maxTime;
  res.success = true;
  }
  else
  {
    res.success = false;
  }

  
  return(true);

}

bool ask_to_time(optitrack_full::time::Request &req,
               optitrack_full::time::Response &res)
{
  //int wantedFrame = 0;
  int val1 =  (req.time*frameRate) -1;
  int val2 = TRCManager.optitrackData.size()-2;
  int wantedFrame = std::max( val1, 0);
  wantedFrame = std::min(wantedFrame,val2);

  bool canGoBack = (wantedFrame <= dataModel.frameNumber);
  // if (!TRCManager.allFramesSeen)
  // {
  //   ROS_WARN("Warning : not all frames seen at time %d. It will be an issue if you want to labelize", req.time);
  // }

  // authorize only to go to time < currentTime

  // if all frames have been seen OR calibration has not been tried yet
   if (TRCManager.allFramesSeen || dataModel.opNameMarkers.corresNameID.size() ==0|| canGoBack)
   {

    toFrame = wantedFrame;
    //ROS_INFO("GOTO FRAME : %d", toFrame);
    pauseMode = req.pauseMode;

    res.success = true;

  }
  else
  {
    res.success = false;
  }

  return(true);


}               

bool ask_print_association(std_srvs::SetBool::Request &req,
               std_srvs::SetBool::Response &res)
{
  std::string s;
  if (dataModel.opNameMarkers.corresNameID.size()> 0)
  {
      
    if (req.data)
    {
      // print by body
      s =  markersManagement::printAssociationByBody(dataModel.opNameMarkers.corresNameID, dataModel.osMod);
    }
    else
    {
      // print in global
      s = markersManagement::printAssociation(dataModel.opNameMarkers.corresNameID);
    }
    std::stringstream ss;
    ss <<"FrameNumber : " << dataModel.opNameMarkers.frameNumber << ";" << std::endl;

    s = ss.str() + s;

    res.message = s;
    res.success = true;
  }
  else{
    res.success = false;
  }

  return(true);
}

bool ask_clear_association(optitrack_full::listString::Request & req,
optitrack_full::listString::Response & res)
{
  // takes all concerned body parts, and launch the suppress of all associations
  std::vector<std::string> data = req.data;
  cleanBodyPartAssos.clear();
  cleanFORCE = false;
  for (int i = 0; i < data.size(); i++)
  {
    if (data[i] == "FORCE")
    {
      cleanFORCE = true;
    }
    else
    {
      // check for object will happen in function
      cleanBodyPartAssos.push_back(data[i]);
      cleanAssos = true;
    }

  }

  res.success = true;
  return(true);
}


bool ask_save_trc(optitrack_full::saveTxt::Request &req,
               optitrack_full::saveTxt::Response &res)
{

  while (dataModel.dispatching)
  {
    continue;
  }

  if (TRCManager.numRepeat > 0 && dataModel.opNameMarkers.corresNameID.size() > 0)
  {
    std::cout << "Begin saving process ..." << std::endl;
    std::cout << req.pathToFolder << std::endl;
    std::cout << req.data.size() << std::endl;
    // add for opensim TRC + RigidBodyTRC

    std::vector<std::string> markNames;
    for (std::map<std::string,int>::iterator it = dataModel.opNameMarkers.corresNameID.begin(); it!=  dataModel.opNameMarkers.corresNameID.end(); it++ )
    {
      markNames.push_back(it->first);
    }

    TRCManager.writeLabeledTRCFile(dataModel.opNameMarkers.allCorresIDName,opensimManagement.calibFrame, req.typeSave,req.pathToFolder, markNames,dataModel.opNameMarkers.forbidden_areas);

    res.success = true;
    res.message = "Labeled .trc file is now save in "+req.pathToFolder;

  }
  else
  {
    res.success = false;
    res.message = "Labelisation process has not occured yet on all the .trc file. Please wait.";

  }

  return(true);

}



/**
 * @brief Initialize the mocap_node. Inspired from initialize function of original optitrack_mocap package.
 * 
 */

void initialize()
{
  Eigen::AngleAxisd x_rot(M_PI_2, Eigen::Vector3d::UnitX ());
  Eigen::AngleAxisd z_rot(M_PI_2, Eigen::Vector3d::UnitZ ());
  Eigen::Quaternion<double> q = z_rot * x_rot;

  Eigen::Matrix3d osMpi = q.matrix();
  osMpi_i = osMpi.inverse();

  lastMaxID = 0;
  brutalFiltering = true;
  numRepeat = 0;
  maxRosTime = ros::Time::now().toSec();
  it_tRAI = timeRecordedAttributedIndex.begin();  

  dataModel.opNameMarkers = {};
  prev_state = -1;

  toFrame = -1;
  pauseMode = -1;  

  cleanAssos = false;
  cleanFORCE = false;

  deb_tst = false;
  // calibration parameters
  calibration_asked = false;
  dataModel.opNameMarkers.foundCalibFrame= true;
  dataModel.opNameMarkers.pause_mode = false;
  dataModel.opNameMarkers.user_pause_mode = false;
  dataModel.opNameMarkers.debugAssos = false;

  dataModel.opNameMarkers.noMoreAssosPossible = false;
   
  dataModel.opNameMarkers.q_calib = modelConfig.q_calib;


  dataModel.opNameMarkers.manParams.filtering_verbose = serverDescription.filtering_verbose;
  dataModel.opNameMarkers.manParams.ik_verbose = serverDescription.ik_verbose;  
  dataModel.opNameMarkers.manParams.guess_verbose = serverDescription.guess_verbose;
  dataModel.opNameMarkers.manParams.icp_verbose = serverDescription.icp_verbose;


  dataModel.opNameMarkers.originManParams.filtering_verbose = serverDescription.filtering_verbose;
  dataModel.opNameMarkers.originManParams.ik_verbose = serverDescription.ik_verbose;  
  dataModel.opNameMarkers.originManParams.guess_verbose = serverDescription.guess_verbose;
  dataModel.opNameMarkers.originManParams.icp_verbose = serverDescription.icp_verbose;

  dataModel.opNameMarkers.debugPub = globalPublish;

  TRCManager.init(trcFile, withTRC);


  get_forbidden_areas(serverDescription.forbidden_areas_file);

  
  // trc gestion parameters

  if (serverDescription.type_record == "trc")
  {
    record_trc = true;
    record_ros = false;
    TRCManager.typeTRCManager = "Speaker";
    fastRead = serverDescription.fast_trc_read;
    std::cout << "fast read : " << fastRead << std::endl;
     //std::cout << "type : " << serverDescription.type_record << std::endl;
  }
  else if (serverDescription.type_record == "rosbag")
  {
    record_trc = false;
    record_ros = true;   
    TRCManager.typeTRCManager = "Listener";    
  }
  else if (serverDescription.type_record == "none")
  {
    record_trc = false;
    record_ros = false;   
    TRCManager.typeTRCManager = "Listener";    
  }

  global_record = (record_trc || record_ros);

    set_trc = nh.advertiseService("set_trc",  &OptiTrackRosBridge::ask_for_new_trc_file,this);

  if (record_trc)
  {

    TRCManager.readFileCaracs();

    frameRate = TRCManager.dataRate;    

    save_trc = nh.advertiseService("save_trc",  &OptiTrackRosBridge::ask_save_trc,this);



    // std::vector<int> v;
    // v.push_back(360);
    // v.push_back(1320);
    // CoRDecoupl["torso"] = v;
    // v.clear();

    // v.push_back(1400);
    // v.push_back(2120);
    // CoRDecoupl["humerus_l"] = v;
    // v.clear();

    // v.push_back(2150);
    // v.push_back(2600);
    // CoRDecoupl["radius_l"] = v;
    // v.clear();

    // v.push_back(2700);
    // v.push_back(3500);
    // CoRDecoupl["humerus_r"] = v;
    // v.clear();

    // v.push_back(3700);
    // v.push_back(3950);
    // CoRDecoupl["radius_l"] = v;
    // v.clear();

    
  }
  else
  {
    frameRate = serverDescription.frameRate;
  }

  dataModel.opNameMarkers.frameRate = frameRate;
  dataModel.opNameMarkers.reception_verbose = serverDescription.reception_verbose;
  dataModel.opNameMarkers.association_verbose = serverDescription.association_verbose;  
  dataModel.opNameMarkers.maximum_residual = serverDescription.maximum_residual;
  dataModel.opNameMarkers.repeat = serverDescription.repeat;  
  dataModel.opNameMarkers.never_verbose = serverDescription.never_verbose;
  
  nVerb =  serverDescription.never_verbose;
  
  // streaming parameters

  // for dataModel : 
  

  dataModel.frameNumber = 0;
  dataModel.opNameMarkers.frameNumber = dataModel.frameNumber;
  dataModel.frameOrigin = -1;
  dataModel.lastFrame = -1;
  dataModel.numFramesCons = 0;
  dataModel.highResClockFrequency = frameRate;

  dataModel.streamMarkersOnly = serverDescription.stream_markers_only;

  if (dataModel.streamMarkersOnly && (!serverDescription.simulation && !global_record) )
  {
    std::string choice = "";
    usleep(0.5*1e6);
    ROS_WARN("WARNING : stream_markers_only option has been activated. Be sure that only the option 'Unlabeled Markers' is activated in Motive software (in Data Streaming Pane).");
    ROS_WARN("Please also take care that no assets of any kind are activated (in Assets Pane). \n");
    ROS_INFO("Press any key + enter to continue.");
    std::cin >> choice;
  }
  
  // ROS services

  if (record_ros)
  {
    optiMarkSub = nh.subscribe("opti_mark_input", 1000, &OptiTrackRosBridge::markers_callback, this);
  }

  calibration_service = nh.advertiseService("calibration", &OptiTrackRosBridge::ask_calib,this);


  pause_continue = nh.advertiseService("pause_continue",  &OptiTrackRosBridge::ask_pause_continue,this);
  isPaused = false;

  statePublisher = nh.advertise<std_msgs::Int64>("state",1000);


  // simulation parameters
  simulation = serverDescription.simulation;
  dataModel.opNameMarkers.simulation = simulation;

  dataModel.doPCScaling = true;

  // reality parameters
  // (return to classic optitrack_full initialization)

  // Create socket
  if (!simulation && !global_record)
  {

    multicastClientSocketPtr.reset(
      new UdpMulticastSocket(serverDescription.dataPort, 
      serverDescription.multicastIpAddress)); 

    if (!serverDescription.version.empty())
    {
      dataModel.setVersions(&serverDescription.version[0], &serverDescription.version[0]);
    }

    // Need verion information from the server to properly decode any of their packets.
    // If we have not recieved that yet, send another request.  
    while(ros::ok() && !dataModel.hasServerInfo())
    {
      natnet::ConnectionRequestMessage connectionRequestMsg;
      natnet::MessageBuffer connectionRequestMsgBuffer;
      connectionRequestMsg.serialize(connectionRequestMsgBuffer, NULL);
      int ret = multicastClientSocketPtr->send(
        &connectionRequestMsgBuffer[0], 
        connectionRequestMsgBuffer.size(), 
        serverDescription.commandPort);

      if (updateDataModelFromServer()) usleep(1e6/frameRate);
    }

    // Once we have the server info, create publishers
    if (!dataModel.streamMarkersOnly)
    {
    publishDispatcherPtr.reset(
      new RigidBodyPublishDispatcher(nh, 
        dataModel.getNatNetVersion(), 
        publisherConfigurations)); 
    }
  }

  // INITIALIZE HERE OPENSIM / RIGID BODIES

  if (opensimManagement.isActivated)
  {
    std::map<std::string,Eigen::Vector3d> local_pos = dataModel.osMod.D_mark_pos_local;
    opensimManagement.initialize_opensim(&dataModel,record_trc,TRCManager,serverDescription,globalPublish);
    if (typeReboot == 1)
    {
      ROS_WARN("KEEP LOCAL MARKERS POSITION");
      opensimManagement.osMod_origin.setMarkersPositions(local_pos,"local");
      dataModel.osMod.setMarkersPositions(local_pos,"local");
      typeReboot = 0;
      // to test
      dataModel.doPCScaling =false;
    }

  }
  if (rigidBodyManagement.isActivated)
  {
    rigidBodyManagement.initialize_rigid_body(&dataModel,record_trc,TRCManager,serverDescription,globalPublish,nh);
  }


  TRCManager.run();


  if (record_trc && opensimManagement.isActivated) 
  {
    bool isLabelized = (TRCManager.trcCorresNameId.size() > 0);
    if (isLabelized)
    {
      std::printf("Use with labeled TRC file detected. \n");
      std::printf("Please enter the time at which the calibration pose should be taken. Set to a negative value to cancel the calibration step (and use the given model). \n");

      double calib_time_start;
      std::cin >> calib_time_start;
      bool doCalib = true;
      dataModel.opNameMarkers.corresNameID = TRCManager.trcCorresNameId;    
      if (calib_time_start > 0)
      {  
        int nFrameStart = (calib_time_start*frameRate) -1;
        TRCManager.Nframe = nFrameStart;
        std::printf("Now, please enter the time at which the gathering of data of CoR estimation should be stopped. Set to a negative value to cancel.  \n");
        double CoR_time;
        std::cin >> CoR_time;
        int nFrameLim = CoR_time/frameRate;

        Eigen::Matrix4f I; I.setIdentity();

      
        getNextOptitrackFrame(&dataModel,TRCManager);
        opensimManagement.optitrack_pos_calib = dataModel.opNameMarkers.lastOptitrackMarkers;
        opensimManagement.optitrack_pos_calib_time = dataModel.opNameMarkers.lastOptitrackTime;

        // for now, takes the 15 first seconds of file
        for (int i = nFrameStart; i < nFrameLim; i++)
        {
          opensimManagement.calibMarkerData.push_back(dataModel.opNameMarkers.lastOptitrackMarkers);
          std::map<std::string,Eigen::Matrix4f> frameTransform;
          for (std::map<std::string, std::vector<std::string> >::iterator it= opensimManagement.osMod_origin.D_body_markers.begin(); it!=opensimManagement.osMod_origin.D_body_markers.end(); it++)
          {
            frameTransform[it->first] = I;
          }
          opensimManagement.ground2ParentTransform[i-nFrameStart] = (frameTransform);
          getNextOptitrackFrame(&dataModel,TRCManager);
        } 
      }
      else
      {
        /**
         * Need to setup : 
         * 
         *  dataModel->opNameMarkers.foundCalibFrame = true
         *  dataModel->opNameMarkers.corresNameID = TRCManager.trcCorresNameId
         *   opensimManagement.osMod_origin.clone3(dataModel->osMod,true,true);
         * 
         */


          TRCManager.Nframe = TRCManager.firstCompleteLabelizedFrame;
          getNextOptitrackFrame(&dataModel,TRCManager);
          opensimManagement.optitrack_pos_calib = dataModel.opNameMarkers.lastOptitrackMarkers;
          opensimManagement.optitrack_pos_calib_time = dataModel.opNameMarkers.lastOptitrackTime;
          opensimManagement.osMod_origin.clone3(dataModel.osMod,true,true);

          dataModel.opNameMarkers.foundCalibFrame = true;
          dataModel.opNameMarkers.limFramesSinceIK = 0;
          //dataModel.opNameMarkers.never_verbose = true;


        doCalib = false;
      }

      opensimManagement.opensim_trc_calibration(&dataModel,globalPublish, doCalib, isLabelized);      


    }
    else
    {
      std::printf("Use with unlabeled TRC file detected. \n");
      std::printf("Procedure recall for calibration : \n");
      std::printf(" 1 : use '/to_time' service to go to starting calibration frame. Recording will be paused. \n");
      std::printf(" 2 : use '/mocap_node/calibration' service to set the duration of the calibration step and all necessary parameters. \n");
      std::printf(" 3 : use enter to restart recording. Wait for duration to stop and launch calibration step. \n");


    }
      trcParametersPublisher = nh.advertise<std_msgs::Float64MultiArray>("trc_parameters",1000);

    if (record_trc)
    {
      // take last time with tempTRCData, dernier element
      std::map<double,std::map<std::string,Eigen::Vector3d> >::iterator it_time = TRCManager.tempTRCData.begin();
      std::advance(it_time,TRCManager.tempTRCData.size()-1);

      double max_time = it_time->first;

      trcParam.maxTime = max_time;
      trcParam.dataFrame = TRCManager.dataRate;


      trcPServer = nh.advertiseService("trc_params_server",  &OptiTrackRosBridge::ask_trc_params,this);
      
      clearAssosServer = nh.advertiseService("clear_assos",  &OptiTrackRosBridge::ask_clear_association,this);
      
      trcFRServer = nh.advertiseService("fast_read",  &OptiTrackRosBridge::ask_fast_read,this);

      speedServer = nh.advertiseService("reading_speed", &OptiTrackRosBridge::get_speed, this);

      std_msgs::Float64MultiArray msg;
      std_msgs::MultiArrayDimension dim;

      dim.size = 2;

      msg.layout.dim.push_back(dim);

      msg.data.push_back(frameRate);
      msg.data.push_back(max_time);

      trcParametersPublisher.publish(msg);



      ros::spinOnce();

    }


    // setup subscriber to set to asked frame (and set pause)
    to_time = nh.advertiseService("to_time",  &OptiTrackRosBridge::ask_to_time,this);

  }
  print_assos = nh.advertiseService("print_assos",  &OptiTrackRosBridge::ask_print_association,this);
  ROS_INFO("Initialization complete");

};

/**
 * @brief Execute the publishing operations for an Optitrack message,
 * when the calibration process was called.
 * This allows to display the joint configuration of the calibration pose chosen by the user, how
 * the markers should be placed according to this calibration pose, and the current
 * positions of the streamed Optitrack markers.
 * 
 */
void calibration_run()
{
  if (opensimManagement.isActivated)
  {
    opensimManagement.calibration_opensim_run(&dataModel,globalPublish,calibration_asked);
  }
};

/**
 * @brief Execute the classic publishing operations for an Optitrack message.
 * 
 */
void classic_run()
{

  if (rigidBodyManagement.isActivated)
  {
    //std::cout << "here" << std::endl;
    // not developped anymore
    //rigidBodyManagement.associateRigidBodiesMarkers(&dataModel);
    //std::cout << "here2" << std::endl;
    natnet::utilities::markersAssociation(dataModel.opNameMarkers,dataModel.allMarkerSets);
    rigidBodyManagement.initialize_new_markerSets(&dataModel, globalPublish, nh);
  }

  if (opensimManagement.isActivated)
  {
    natnet::utilities::markersRecognitionAndIK(dataModel.opNameMarkers,dataModel.osMod);
  }

  ros::Time time = dataModel.opNameMarkers.lastOptitrackTime;
  // publish all necessary topics
  if (opensimManagement.isActivated)
  {

    opensimManagement.classic_opensim_run(&dataModel, globalPublish,time);
    globalPublish.publishErrors(time, dataModel.opNameMarkers.error_pub);

  }
  if (rigidBodyManagement.isActivated)
  {
    rigidBodyManagement.classic_rigid_body_run(&dataModel, globalPublish,time);
  }

  if (!dataModel.streamMarkersOnly && !simulation) {publishDispatcherPtr->publish(time, dataModel.dataFrame.rigidBodies);}
  globalPublish.markerPublisher(time, dataModel.opNameMarkers.lastOptitrackMarkers, globalPublish.optitrack_publisher);

  if (dataModel.opNameMarkers.pause_mode)
  {
    if (opensimManagement.isActivated)
    {
      if (dataModel.opNameMarkers.corresNameID.size() > 0)
      {
      std::string deb;
      std::cin.clear();
      std::cin >> deb;
      }
    } 
    dataModel.opNameMarkers.pause_mode = false;
  }

}

/**
 * @brief Global method to execute all operatons
 * 
 */
void run()
{
  while (ros::ok())
  {   
      if (cleanAssos)
      {
        if (opensimManagement.isActivated)
        {
          opensimManagement.clean_associations(&dataModel, cleanBodyPartAssos,cleanFORCE);
          cleanBodyPartAssos.clear();
        }
        cleanAssos = false;
        cleanFORCE = false;
      }

      if (!isPaused)
      {

        if (state == 2)
        {
          // reboot state

          // shutdown all serviceServer
          set_trc.shutdown();
          save_trc.shutdown();
          to_time.shutdown();
          pause_continue.shutdown();
          trcPServer.shutdown();
          trcFRServer.shutdown();
          calibration_service.shutdown();
          clearAssosServer.shutdown();
          print_assos.shutdown();
          speedServer.shutdown();

          initialize();
          state = 0;

        }
        else
        {

          beg_run = ros::Time::now().toNSec();
          // search if some data were recorded; if it is the case, compute them and publish
          // them according to running situations
          if (updateDataModelFromServer())
          {   
            if (dataModel.dispatching  == false)
            {
              //std::cout << "Num frame : " << dataModel.frameNumber << std::endl;



              if (calibration_asked)
              {
                if (state == 0)
                {
                  state = 1;
                }
                calibration_run();
              }

              else
              {
                if (state == 1)
                {
                  state = 0;
                }
                classic_run();
              }





              // in order to control services, spinOnce
              ros::spinOnce();

              // Clear out the model to prepare for the next frame of data
              dataModel.clear();

              if (dataModel.opNameMarkers.user_pause_mode)
              {
                dataModel.opNameMarkers.user_pause_mode = false;
                std::cout << "Pause mode asked by user." << std::endl;
                dataModel.opNameMarkers.pause_mode = true;
              }

              if (dataModel.opNameMarkers.pause_mode)
              {

                if (pauseMode == 0)
                {
                  // Wait for user input
                  std::string pause;
                  std::cin.clear();
                  std::cin >> pause;
                  dataModel.opNameMarkers.pause_mode = false;
                }
                else if (pauseMode == 1)
                {
                  // Wait for calibration rosservice input
                  updateDataModelFromServer();
                  isPaused = true;
                }

              }

              // 
              if (prev_state != state)
              {
                // publish current state, then update
                std_msgs::Int64 msg;
                msg.data = state;
                statePublisher.publish(msg);
                prev_state = state;
              }


              // If we processed some data, take a short break
              if ( (!fastRead || (!dataModel.opNameMarkers.noMoreAssosPossible && dataModel.opNameMarkers.corresNameID.size()>0) ) || calibration_asked)
              {
                // if fastRead BUT in classic_run AND a association was still possible, wait a bit
                // (to be sure that user may react to this situation)

                // nano seconds
                uint64_t end_run = ros::Time::now().toNSec();
                // nano to micro ==> 1e-3
                double dur_run = (end_run - beg_run )*1e-3;

                // std::cout << "dur run : " << dur_run << std::endl;
              

                //ROS_INFO("Need to sleep : %f micro s.", (1e6*1.0/frameRate - dur_run) ); 

                usleep( std::max(1e6*speed/frameRate - dur_run,0.0) );
                //std::cout << "------------" << std::endl;                
              }

      if (dataModel.opNameMarkers.debugAssos)
      {
        std::cout << "debug assos" << std::endl;
                      std::string debRec;
                      std::cin.clear();
             std::cin >> debRec;
        dataModel.opNameMarkers.debugAssos = false;

        dataModel.opNameMarkers.never_verbose = nVerb;

        dataModel.opNameMarkers.manParams = dataModel.opNameMarkers.originManParams;
      }

            }
          }
          else
          {
            ros::spinOnce();
          }
        }        
      }
      else
      {
        ros::spinOnce();
        usleep( std::max(1e6/frameRate,0.0) );
      }


    
  }
}

    void getNextOptitrackFrame(optitrack_full::DataModel * dataModel, file_management::TRCManager & TRCManager)
    {
        dataModel->opNameMarkers.messageReceptionTime = ros::Time::now();
        TRCManager.Nframe++;
        //std::cout << TRCManager.Nframe << std::endl;

        if (TRCManager.Nframe >= TRCManager.optitrackData.size())
        {

            std::map<double,std::map<int,Eigen::Vector3d> >::iterator it = TRCManager.optitrackData.begin();
            std::advance(it,TRCManager.optitrackData.size() - 1);          
            std::map<int,Eigen::Vector3d> lastFrame = it->second;
            it = TRCManager.optitrackData.begin();
            std::advance(it,0);          
            std::map<int,Eigen::Vector3d> firstFrame = it->second;            
            ROS_INFO("BACK TO BEGIN OF FILE");
            markersManagement::manageACINBeginReturn(TRCManager.Nframe,firstFrame,lastFrame,dataModel->opNameMarkers.allCorresIDName, dataModel->opNameMarkers.corresNameID,true,true);
            // reset all values of corresNameID; they will be corrected by allCorresNameID
            // for (std::map<std::string,int>::iterator it3 = dataModel->opNameMarkers.corresNameID.begin();it3!=  dataModel->opNameMarkers.corresNameID.end(); it3++)
            // {
            //    dataModel->opNameMarkers.corresNameID[it3->first] = -1;
            // }

            // also reset values of lastOsNotAssigned, lastframeOsAssos
            dataModel->opNameMarkers.lastOsMarkersNotAssigned.clear();
            dataModel->opNameMarkers.lastFrameOsAssos.clear();


            TRCManager.Nframe = 0;          
            TRCManager.numRepeat++;
            if (TRCManager.numRepeat == 2)
            {
              TRCManager.allFramesSeen = true;
            }
        }

        dataModel->frameNumber = TRCManager.Nframe;

        if (dataModel->frameOrigin == -1)
        {
            dataModel->frameOrigin = dataModel->frameNumber;
        }

        else if ( dataModel->frameNumber <= std::max(dataModel->lastFrame*0.1,dataModel->frameOrigin*1.0) )
        {
            dataModel->frameOrigin = dataModel->frameNumber;
            dataModel->lastFrame = dataModel->frameNumber;
            dataModel->numFramesCons = 0;
            // (now necessary?)
            //dataModel->opNameMarkers.allCorresIDName.clear();
        }

        dataModel->opNameMarkers.frameNumber = dataModel->frameNumber;        


        double time;
        std::map<int, Eigen::Vector3d> frame;
        std::map<int, Eigen::Vector3d>::iterator itframe;

        std::map<double,std::map<int,Eigen::Vector3d> >::iterator it = TRCManager.optitrackData.begin();
        std::advance(it,TRCManager.Nframe);
        
        time = it->first;
        frame = it->second;

        markersManagement::controlForbiddenAreas(frame,dataModel->opNameMarkers.forbidden_areas);

        //

        // itframe = frame.find(1466);
        // if (itframe!= frame.end())
        // {
        //     frame.erase(itframe);
        // }
        // itframe = frame.find(1474);
        // if (itframe!= frame.end())
        // {
        //     frame.erase(itframe);
        // }
        

        natnet::utilities::manageTime(dataModel,time);
        dataModel->opNameMarkers.lastOptitrackMarkers.clear();
        dataModel->opNameMarkers.lastOptitrackMarkers = frame;

        dataModel->opNameMarkers.lastOptitrackMarkersNotAssigned.clear();
        dataModel->opNameMarkers.lastOptitrackMarkersNotAssigned = frame;


        dataModel->lastFrame = dataModel->frameNumber;
        if (dataModel->numFramesCons >=0)
        {
          dataModel->numFramesCons += 1;
        }



    }


  private:
    /**
     * @brief Retrieves and computes all informations from the Optitrack server.
     * In simulation mode, retrieves simulated data.
     * 
     * @return true : if information has been retrieved
     * @return false : otherwise.
     */
    bool updateDataModelFromServer()
    {
      
      if (!simulation)
      {
        // Get data from mocap server
        
        if (record_trc)
        {
          if (toFrame != -1)
          {

            TRCManager.Nframe = toFrame;
   
            dataModel.opNameMarkers.framesSinceAssemble = dataModel.opNameMarkers.limFramesSinceAssemble+1;
            if (pauseMode != -1)
            {

              dataModel.opNameMarkers.user_pause_mode = true;    
            } 
            if (pauseMode == 0)          
            {
            ROS_WARN("Time well set. Please set any key + enter to launch movement again.");
            }
            toFrame = -1;
          }

          getNextOptitrackFrame(&dataModel,TRCManager);
          return(true);
        }

        else if (record_ros)
        {
           bool ready = ( (dataModel.lastFrame != dataModel.frameNumber) && (dataModel.lastFrame != -1) );
           if (ready)
           {
             dataModel.frameNumber = dataModel.lastFrame;
            dataModel.opNameMarkers.frameNumber = dataModel.frameNumber;             
             dataModel.opNameMarkers.lastOptitrackTime = dataModel.opNameMarkers.messageReceptionTime;
            //  if (rigidBodyManagement.isActivated)
            //  {
            //    natnet::utilities::markersAssociation(dataModel.opNameMarkers,dataModel.allMarkerSets);
            //    rigidBodyManagement.initialize_new_markerSets(&dataModel);
            //  }

            //  if (opensimManagement.isActivated)
            //  {
            //   natnet::utilities::markersRecognitionAndIK(dataModel.opNameMarkers,dataModel.osMod);
            //  }
           }
           return(ready);

        }


        else
        {
          int numBytesReceived = multicastClientSocketPtr->recv();
          if( numBytesReceived > 0 )
          {
            dataModel.opNameMarkers.messageReceptionTime = ros::Time::now();
            // Grab latest message buffer
            const char* pMsgBuffer = multicastClientSocketPtr->getBuffer();

            // Copy char* buffer into MessageBuffer and dispatch to be deserialized
            natnet::MessageBuffer msgBuffer(pMsgBuffer, pMsgBuffer + numBytesReceived);
            natnet::MessageDispatcher::dispatch(msgBuffer, &dataModel);

            markersManagement::controlForbiddenAreas(dataModel.opNameMarkers.lastOptitrackMarkers,dataModel.opNameMarkers.forbidden_areas);
            markersManagement::controlForbiddenAreas(dataModel.opNameMarkers.lastOptitrackMarkersNotAssigned,dataModel.opNameMarkers.forbidden_areas);


            return true;
          }

          return false;
        }

      }

       // simulation case
      else 
      {
          dataModel.dispatching = true;

          if (opensimManagement.isActivated)
          {
            std::map<int,Eigen::Vector3d> opMarkers;
            opMarkers = markersManagement::simulateMarkers( dataModel.osMod,dataModel.opNameMarkers.frameRate, dataModel.opNameMarkers.simu_gest);
            dataModel.opNameMarkers.lastOptitrackMarkers = opMarkers;
            dataModel.opNameMarkers.lastOptitrackTime = ros::Time::now();
            natnet::utilities::markersRecognitionAndIK(dataModel.opNameMarkers,dataModel.osMod);
          }
          dataModel.dispatching = false;
          return( true);

      }

    };


    bool calibration_asked;

    opensim_management::OpensimManagement opensimManagement;
    rigid_body_management::RigidBodyManagement rigidBodyManagement;

    // Socket dedicated to the reception of Optitrack informations
    std::unique_ptr<UdpMulticastSocket> multicastClientSocketPtr;

    // ROS node handler
    ros::NodeHandle& nh;
    // Object containing informations about the server (IP address for example)
    ServerDescription serverDescription;
    
    // Variables for rigid_body_publication
    PublisherConfigurations publisherConfigurations;
    ModelConfiguration modelConfig;
    std::unique_ptr<RigidBodyPublishDispatcher> publishDispatcherPtr;    

    // Global variables for markers publication
    DataModel dataModel;
 
    // Global object managing the publication of point clouds
    markersManagement::globalPublisher globalPublish;

    // Calibration

    // Server service, for starting calibration
    ros::ServiceServer calibration_service;
    // Duration before the calibration process should be executed
 
    // Scaling


    // Boolean checking if scaling was asked (with the call to calibration service)
 
    // Boolean checking if we're into a simulation or a real situation
    bool simulation;

    // Frame rate of Optitrack markers
    int frameRate;

    ros::ServiceServer pause_continue;

    ros::ServiceServer save_trc;

    ros::ServiceServer set_trc;

    ros::ServiceServer to_time;

    ros::ServiceServer trcPServer;

    ros::ServiceServer clearAssosServer;
    ros::ServiceServer print_assos;

    ros::ServiceServer trcFRServer;
    ros::ServiceServer speedServer;

    ros::Publisher trcParametersPublisher;
    ros::Publisher statePublisher;

    int toFrame = -1;
    int pauseMode = -1;

    bool isPaused;

    bool record_trc;
    bool global_record;
    bool record_ros;

    file_management::TRCManager TRCManager;

    uint64_t beg_run;


    bool deb_tst;

    ros::Subscriber optiMarkSub;

    Eigen::Matrix3d osMpi_i;

    std::map<double, std::map<int,int> > timeRecordedAttributedIndex;
    std::map<double, std::map<int,int> >::iterator it_tRAI;
    std::map<int,int> recordedAttributedIndex;    
    double maxRosTime;
    std::vector<int> lastRecordedIndex;
    std::vector<int> currentRecordedIndex;
    int lastMaxID;
    bool brutalFiltering;    
    int numRepeat;

    bool fastRead = false;

    bool nVerb = false;

    // state of the machine state associated with the reading of
    // the frames
    // 0 : classic_run
    // 1 : calibration_run
    // 2 : reboot_run
    int state = 0;

    int prev_state = -1;
    

    optitrack_full::trcParameters::Response trcParam;

    std::string trcFile = "";
    bool withTRC = false;

    int typeReboot = 0;

    double speed = 1.0;

    bool cleanAssos = false;
    bool cleanFORCE = false;
    std::vector<std::string> cleanBodyPartAssos;

    
  };

} // namespace

////////////////////////////////////////////////////////////////////////
int main( int argc, char* argv[] )
{
  // Initialize ROS node
  ros::init(argc, argv, "mocap_node");
  ros::NodeHandle nh("~");

  // Grab node configuration from rosparam
  optitrack_full::ServerDescription serverDescription;
  optitrack_full::PublisherConfigurations publisherConfigurations;
  optitrack_full::ModelConfiguration modelConfig;
  optitrack_full::NodeConfiguration::fromRosParam(nh, serverDescription, publisherConfigurations,modelConfig);

  // Create node object, initialize and run
  optitrack_full::OptiTrackRosBridge node(nh, serverDescription, publisherConfigurations, modelConfig);
  node.initialize();
  node.run();

  return 0;
}
