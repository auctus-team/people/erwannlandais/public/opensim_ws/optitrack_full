/* 
 * Copyright (c) 2018, Houston Mechatronics Inc., JD Yamokoski
 * Copyright (c) 2012, Clearpath Robotics, Inc., Alex Bencz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its 
 *    contributors may be used to endorse or promote products derived from 
 *    this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Local includes
#include "optitrack_full/opensim_management.h"

namespace fs = std::experimental::filesystem;

namespace opensim_management
{

  /**
   * @brief Class managing the bridge from Optitrack message to ROS.
   * It also manages simulation and scaling operations.
   * 
   */
    OpensimManagement::OpensimManagement(
      std::string osim_path,
      std::string geometry_path,
      std::string setup_ik_path,
      std::vector<double> q_calib,
      ros::NodeHandle& nh) :
        osMod_origin(osim_path,geometry_path,setup_ik_path)
    {
      isActivated = osim_path != "";
      if (isActivated)
      {
        ROS_INFO("OPENSIM MANAGEMENT ACTIVATED");
       osim_basic_path = osim_path;
       origin_q_calib = q_calib;

       python_scaling = nh.serviceClient<optitrack_full::scaling>("/mocap_node/execute_scaling");
       confirmation_scaling = nh.serviceClient<std_srvs::SetBool>("/mocap_node/confirm_scaling");       
      }
    }
  /** 
   * @brief (Used only for simulation purpose). Generate a dictionary of index:point_position from a dictionary of name:point_position.
   * Applies to the generated dictionary a transformation (called transform), a random noise on position of amplitude ampl_noise, and 
   * a stretching set by the stretch vector.
   * 
   * @param D_mark_pos Dictionnary name:point_position. Original position of the points.
   * @param transform Transformation matrix to apply on the points.
   * @param ampl_noise Maximal amplitude of the noise in position (m). The noise applied follows an uniform distribution law.
   * @param stretch Vector < Stretching_on_x, Stretching_on_y, Stretching_on_z >, parameterising the stretch to be applied on each point.
   * @return std::map<int, Eigen::Vector3d > Dictionnary index:point_position, obtains after all operations.
   */
  std::map<int, Eigen::Vector3d > OpensimManagement::transformMarkers(std::map<std::string, Eigen::Vector3d> D_mark_pos, Eigen::Matrix4d transform, double ampl_noise, std::map<std::string,std::vector<double> > stretch_vector )
  {
    std::map<int, Eigen::Vector3d> transform_markers;

    if (isActivated)
    {
      int i = 0;
      
      std::uniform_real_distribution<double> unif(-ampl_noise,ampl_noise);
      std::random_device rd;
      std::mt19937 gen(rd());

      for ( std::map<std::string, Eigen::Vector3d >::iterator it=D_mark_pos.begin(); it!=D_mark_pos.end(); it++ )
        {

          std::string body_name = osMod_origin.D_markers_body[it->first];
          std::vector<double> stretch_body = stretch_vector[body_name];
          // double xS = stretch_body[0];
          // double yS = stretch_body[1];
          // double zS = stretch_body[2];
          double rx = unif(gen);
          double ry = unif(gen);
          double rz = unif(gen);  
          // first, apply stretching


          //Eigen::Vector4d vec; vec << (it->second[0])*xS, (it->second[1])*yS, (it->second[2])*zS,1;
          Eigen::Vector4d vec; vec << (it->second[0]), (it->second[1]), (it->second[2]),1;
          // next, apply transform
          Eigen::Vector4d vec_t = (transform*vec);
          // then, apply noise on position
          Eigen::Vector3d vec2(vec_t[0]+rx,vec_t[1]+ry,vec_t[2]+rz);
          transform_markers[i] = vec2;
          i++;
        }
    }
    return(transform_markers);


  }

/**
 * @brief Method managing the communication between the .py node (into scaling_manager.py) and
 * the .cpp node (mocap_node.cpp) for the scaling operation. This scaling operation is applied on 
 * the .osim model of the OsimModel object osMod.
 * 
 * @param osMod OsimModel object; this object will be remplaced by its scaled version at the end of the method.
 * @param markersLocation Dictionary name:point_position, giving the position of each of the .osim markers.
 * @return bool Indicates whether the scaling process made by scaling_manager.py was a success or not.
 * 
 */
bool OpensimManagement::start_scaling(fosim::OsimModel & osMod,
 std::map<std::string,Eigen::Vector3d> markersLocation,
 std::map<std::string,std::vector<double> > all_scaling_factors)
{

  // create folder where temporary files should be saved

  std::string path = fs::current_path();
  std::string temp_folder = path + "/temp_folder";
  fs::create_directories(temp_folder);

  // generate the .trc file used for scaling.
  std::map<double,std::map<std::string,Eigen::Vector3d> > TRCData;

  TRCData[0.0] = markersLocation;
  for (int i =1; i <8; i++)
  {
      TRCData[1.0*i/frameRate] = markersLocation;
  }  

  fosim::writeTRCFile( temp_folder + "/temp_markers_pos.trc", TRCData, frameRate );

  // generate the .mot file used for scaling.
  std::map<double, std::map<std::string,double> > MOTData;
  MOTData[0.0] = osMod.D_joints_values;
  for (int i =1; i <8; i++)
  {
    MOTData[i*1.0/frameRate] = osMod.D_joints_values;
  }

  fosim::writeMOTFile(temp_folder+ "/temp_joint_val.mot", MOTData);
  
  //std::cout << "Call scaling manager... " << std::endl;

  // launch the scaling_manager node.
  system("rosrun optitrack_full scaling_manager.py &");

  // activates the service triggering the scaling operation on 
  // scaling manager.py
  optitrack_full::scaling srv;

  srv.request.osim_path = osMod.model_path;
  srv.request.TRCPath = temp_folder + "/temp_markers_pos.trc";
  srv.request.MOTPath =  temp_folder+ "/temp_joint_val.mot";
  srv.request.mass = mass;
  srv.request.scaleFile = scale_file;
  srv.request.dataFile = data_file;
  srv.request.rosPackagePath = ros_package_path;

  for (std::map<std::string,std::vector<double> >::iterator it = all_scaling_factors.begin(); it!= all_scaling_factors.end(); it++)
  { 
    //std::cout << it->first << std::endl;
    srv.request.bodyNames.push_back(it->first);
    for (int i = 0; i<3; i++)
    {
      //std::cout << it->second[i] << " ";
      srv.request.scalingFactors.push_back(it->second[i]);
    }
    //std::cout << std::endl;
  }

  //std::cout << "here" << std::endl;
  bool success;
  python_scaling.waitForExistence();

  // wait for the scaling operation on scaling_manager.py to end.
  if (!python_scaling.call(srv) )
  {
    ROS_INFO("Scaling failed... \n");
    success = false;
  }
  else
  {
    ROS_INFO("Scaling completed, return to Cpp! \n");
    success = true;


    std::vector<std::string> Losim = fosim::splitString(osim_basic_path,"/");

    std::string osim_path = Losim[0];

    for (int i = 1; i < Losim.size()-1; i++)
    {
      osim_path+= "/"+ Losim[i];
    }
    osim_path+="/";

    std::string model_name = fosim::splitString(Losim[Losim.size()-1], ".")[0];

    std::string rescale_path = osim_path + model_name + "ScaledPlaced.osim";

    osMod.model_path = rescale_path;
    osMod.fromScratch();

    ROS_INFO("All scaling completed, congrats! \n");
 
  }

  return(success);

}

/**
 * @brief Convenience method to reboot the calibration process, after
 * a correspondance between the .osim markers and the optitrack markers was
 * computed.
 * 
 */
void OpensimManagement::reboot_calib(optitrack_full::DataModel * dataModel)
{
  
  dataModel->opNameMarkers.corresNameID.clear();
  dataModel->opNameMarkers.CINCalib.clear();
  dataModel->opNameMarkers.q_calib = origin_q_calib;
  opNameMarkersCoR.corresNameID.clear();
  optitrack_pos_calib.clear();
  
  calibMarkerData.clear();
  ground2ParentTransform.clear();
  if (dataModel->opNameMarkers.repeat)
  {
    dataModel->opNameMarkers.allCorresIDName.clear();
    opNameMarkersCoR.allCorresIDName.clear();
  }

  if (dataModel->opNameMarkers.simulation)
  {
    dataModel->opNameMarkers.simu_gest.calibCompleted = false;
  }

}



/**
 * @brief Method managing all the necessary operations to estimate the CoR of a body part,
 * and rescaling this body part accordingly.
 * At this step, we suppose all correspondences for the calibration pose are correct. 
 * 
 * @param osMod 
 * @param body_part 
 * @param markerData 
 * @param corresNameID 
 * @param scaling_factors 
 * @param ground2ParentTransform 
 * @param calib_time 
 * @param globalPublish 
 */
void OpensimManagement::CoREstimateManagement(fosim::OsimModel & osMod, 
optitrack_full::OptitrackNameMarkers & opNameMarkers,
std::string body_part,
std::vector<std::map<int,Eigen::Vector3d> > markerData, 
std::map<std::string,int> corresNameID,
std::vector<double> scaling_factors,
std::map<int,std::map<std::string, Eigen::Matrix4f> > & ground2ParentTransform,
ros::Time calib_time,
markersManagement::globalPublisher globalPublish )
{

  std::vector<std::string> D_p_a_c = osMod.D_parent_all_children[body_part];

  std::vector<std::string> bodyMarkers = osMod.D_body_markers[body_part];
  
  for (int i = 0; i < bodyMarkers.size(); i++)
  {
    opNameMarkers.corresNameID[bodyMarkers[i]] = corresNameID[bodyMarkers[i]];
    markersManagement::manageACINAdding(bodyMarkers[i],corresNameID[bodyMarkers[i]], opNameMarkers.frameNumber, opNameMarkers.allCorresIDName, opNameMarkers.repeat);
  }

  std::map<std::string,Eigen::Vector3d> originMarkersPos = opNameMarkers.opensimMarkersPos;

  std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerData;

  Eigen::Matrix4f transform;

  std::cout << "Marker identification in datas for CoR estimation (warning : this may takes a few seconds...)." << std::endl;

  int beg;
  int end;
  // case where movements are coupled
  if (CoRDecouplReadapt.size() == 0)
  {
    beg = 0;
    end = markerData.size();
  }
  // case where movements are decoupled
  else
  {
    beg = CoRDecouplReadapt[body_part][0];
    end = CoRDecouplReadapt[body_part][1];
  // std::cout << "in function : " << end - beg << std::endl;
  // std::cout << "fixed : " << CoRDecoupl[body_part][1] -  CoRDecoupl[body_part][0]  << std::endl;
  //   std::string sds;
  //   std::cout << "debug" << std::endl;
  //   std::cin >> sds;
  }



  for (int i = beg; i < end; i++)
  {
    if (ground2ParentTransform[i].find(body_part) != ground2ParentTransform[i].end() )
    {

      opNameMarkers.lastOptitrackMarkers = markerData[i];
      std::cout << "Identifiant of frame : " << i << std::endl;
  
      bool verbose = opNameMarkers.association_verbose;
      bool called = false;
      std::map<std::string,int> osMarkersNotAssigned;
      bool IK_possible;

      for ( int j = 0; j < bodyMarkers.size(); j++)
      {
        osMarkersNotAssigned[bodyMarkers[j]] = 1;
      }

      int numOsNotAssigned = 0;   
      //std::cout << "ok"  <<std::endl;

      natnet::utilities::markersAssociation(opNameMarkers,osMod,osMarkersNotAssigned,IK_possible, numOsNotAssigned, called);

      // add gestion of loss of markers

      // - opNameMarkers.D_mark_all_dist mis juste pour body_part

      if (numOsNotAssigned == 0 || originMarkersPos.size() - numOsNotAssigned>= 3 )
      {
        std::map<std::string, Eigen::Vector3d> markersTreated;

        for (std::map<std::string,int>::iterator it = osMarkersNotAssigned.begin(); it!= osMarkersNotAssigned.end(); it++)
        {
          if (it->second == 0)
          {
            markersTreated[it->first] = markerData[i][opNameMarkers.corresNameID[ it->first  ] ];
          }
        }

        if (numOsNotAssigned !=0 && originMarkersPos.size() - numOsNotAssigned>= 3)
        {
          std::vector<std::map<std::string, Eigen::Vector3d> > vect;
          vect.push_back(markersTreated);
          markersManagement::recoveryMarkersProcedure(vect,originMarkersPos, globalPublish, calib_time);

          globalPublish.markerPublisher(calib_time,markerData[i], globalPublish.optitrack_robust_publisher);

          if (body_part == "radius_l")
          {
            // std::string bla;
            // std::cout << "debug " << std::endl;
            // std::cin >> bla;
          }


          markersTreated = vect[0];
        }

        transform = ground2ParentTransform[i][body_part];


        std::map<std::string,Eigen::Vector3d> bodyMarkerFrame;

        for (int j = 0; j < bodyMarkers.size(); j++)
        {
          Eigen::Vector3d marker = markersTreated[bodyMarkers[j]];
          Eigen::Vector4f marker_homo(marker[0], marker[1], marker[2], 1);
          marker_homo = transform*marker_homo;
          marker[0] = marker_homo[0];
          marker[1] = marker_homo[1];
          marker[2] = marker_homo[2];
          bodyMarkerFrame[ bodyMarkers[j] ] = marker;
        }

          // if (body_part == "radius_l")
          // {
          //   globalPublish.publishOpensim(calib_time, bodyMarkerFrame);
          //   // std::string bla;
          //   // std::cout << "debug " << std::endl;
          //   // std::cin >> bla;
          // }

        bodyMarkerData.push_back(bodyMarkerFrame);

      }
      // otherwise, suppress the frame for the CoR estimation of the child
      // (because the next transform may not be guaranted)
      else
      {
        for (int j = 0; j < D_p_a_c.size(); j++)
        {
          ground2ParentTransform[i].erase(D_p_a_c[j]);
        }

      }

    }
 
  }
  std::cout << "Number of datas : " << markerData.size() << std::endl;
  std::cout << "Number of datas after filtering : " << bodyMarkerData.size() << std::endl;


  Eigen::Vector3f m2;
  
  markersManagement::CoREstimateWithMarkers_Gam(bodyMarkerData, m2, calib_time, globalPublish);

  // NB : from empirical tests, Gamage and NM conducts to the same solution.
  // From literature (In vivo estimation of the glenohumeral joint centre by functional methods: Accuracy and repeatability assessment),
  // Gamage method is slightly better than NM; furthermore, the requested operations are less complex in Gamage algorithm.
  // Finally, the current implementation of NM is (for unknown reason) not the same as described in the paper; from tests, it is impossible
  // to get a good result with the condition u.T*C*u = 1. 
  // For those reasons, Gamage method is prefered from NM.

  Eigen::Vector3f m;

  CoREstimateWithMarkers_NM(bodyMarkerData, m, calib_time, globalPublish);

  if ( body_part == "torso")
  {
    // = resultat a trouver
    //m2[0] = -0.706512 + 0.0640859  ; 
    m2[1] = 0.03; // -0.08
    //m2[2] = - 0.042371 + 0.0253454;
  }


  // if ( body_part == "humerus_l")
  // {
  //   // = resultat a trouver
  //   //m2[0] = 0.003155; 
  //   m2[1] =  0.3715;
  //   //m2[2] = -0.17;
  // }

  // if ( body_part == "humerus_r")
  // {
  //   // = resultat a trouver
  //   //m2[0] = 0.003155; 
  //   m2[1] =  0.3715;
  //   //m2[2] = -0.17;
  // }



  // if ( body_part == "radius_l")
  // {
  //   // = resultat a trouver
  //   //m2[0] = 0.013;
  //   m2[1] =  -0.286;
  //   //m2[2] = 0.0095;
  // }

  // if ( body_part == "radius_r")
  // {
  //   // = resultat a trouver
  //   //m2[0] = 0.013;
  //   m2[1] =  -0.286;
  //   //m2[2] = 0.0095;
  // }  

  // we now have m, the position of the CoR for bodyMarkerData[0] (in the parent body referential)
  // NB : if no translation movements were made, it is also the position of the CoR for all
  // markers data.
  std::map<std::string,int> currentCorresNameID;
  std::map<int,Eigen::Vector3d> optitrackPoints;
  int l = 0;

  for (std::map<std::string,Eigen::Vector3d>::iterator it = bodyMarkerData[0].begin(); it!= bodyMarkerData[0].end(); it++ )
  {
    optitrackPoints[l] = bodyMarkerData[0][it->first];
    currentCorresNameID[it->first] = l;
    l++;
  }




  markersManagement::replacePointsAndEstimateScaling(optitrackPoints, m2, osMod, currentCorresNameID, scaling_factors, 
  calib_time, globalPublish);

}



/**
 * @brief Method managing all the necessary operations to estimate the CoR between two body parts.
 * For now, no scaling.
 * At this step, we suppose all correspondences for the calibration pose are correct. 
 * 
 * @param osMod 
 * @param body_part 
 * @param markerData 
 * @param corresNameID 
 * @param scaling_factors 
 * @param ground2ParentTransform 
 * @param calib_time 
 * @param globalPublish 
 */
void OpensimManagement::CoREstimateManagement_DoublePart(fosim::OsimModel & osMod, 
optitrack_full::OptitrackNameMarkers & opNameMarkers,
std::string body_part_X,
std::string body_part_Y,
std::vector<std::map<int,Eigen::Vector3d> > markerData, 
std::vector<std::map<std::string,Eigen::Vector3f> > & allLocalX,
std::vector<std::map<std::string,Eigen::Vector3f> > & allLocalY,
std::map<std::string,int> corresNameID,
std::vector<double> scaling_factors,
ros::Time calib_time,
markersManagement::globalPublisher globalPublish )
{

  std::vector<std::string> bodyMarkersX = osMod.D_body_markers[body_part_X];
  std::vector<std::string> bodyMarkersY = osMod.D_body_markers[body_part_Y];

  for (int i = 0; i < bodyMarkersX.size(); i++)
  {
    opNameMarkers.corresNameID[bodyMarkersX[i]] = corresNameID[bodyMarkersX[i]];
    markersManagement::manageACINAdding(bodyMarkersX[i],corresNameID[bodyMarkersX[i]], opNameMarkers.frameNumber, opNameMarkers.allCorresIDName, opNameMarkers.repeat, true);
  }

  for (int i = 0; i < bodyMarkersY.size(); i++)
  {
    opNameMarkers.corresNameID[bodyMarkersY[i]] = corresNameID[bodyMarkersY[i]];
    markersManagement::manageACINAdding(bodyMarkersY[i],corresNameID[bodyMarkersY[i]], opNameMarkers.frameNumber, opNameMarkers.allCorresIDName, opNameMarkers.repeat,true);
  }

  std::map<std::string,Eigen::Vector3d> originMarkersPos = opNameMarkers.opensimMarkersPos;

  std::map<std::string,Eigen::Vector3d> originMarkersPosX;
  std::map<std::string,Eigen::Vector3d> originMarkersPosY;

  for ( int j = 0; j < bodyMarkersX.size(); j++)
  {
    originMarkersPosX[bodyMarkersX[j]] = originMarkersPos[bodyMarkersX[j]];
  }

  for ( int j = 0; j < bodyMarkersY.size(); j++)
  {
    originMarkersPosY[bodyMarkersY[j]] = originMarkersPos[bodyMarkersY[j]];
  }

  std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerDataX;
  std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerDataY;

  std::vector<std::map<std::string,Eigen::Vector3d> > missVectX;
  std::vector<std::map<std::string,Eigen::Vector3d> > missVectY;  

  std::cout << "Marker identification in datas for CoR estimation (warning : this may takes a few seconds...)." << std::endl;

  for (int i = 0; i < markerData.size(); i++)
  {
      bool Xmiss = false;
      bool Ymiss = false;

      opNameMarkers.lastOptitrackMarkers = markerData[i];
      std::cout << "Identifiant of frame : " << i << std::endl;
  
      bool verbose = opNameMarkers.association_verbose;
      bool called = false;
      std::map<std::string,int> osMarkersNotAssigned;
      bool IK_possible;

      for ( int j = 0; j < bodyMarkersX.size(); j++)
      {
        osMarkersNotAssigned[bodyMarkersX[j]] = 1;
      }

      for ( int j = 0; j < bodyMarkersY.size(); j++)
      {
        osMarkersNotAssigned[bodyMarkersY[j]] = 1;
      }

      int numOsNotAssigned = 0;   
      //std::cout << "ok"  <<std::endl;

      natnet::utilities::markersAssociation(opNameMarkers,osMod,osMarkersNotAssigned,IK_possible, numOsNotAssigned, called);

      int numOsNotAssignedX = 0;
      int numOsNotAssignedY = 0;

      for ( int j = 0; j < bodyMarkersX.size(); j++)
      {
        numOsNotAssignedX += osMarkersNotAssigned[bodyMarkersX[j]];
      }

      for ( int j = 0; j < bodyMarkersY.size(); j++)
      {
        numOsNotAssignedY += osMarkersNotAssigned[bodyMarkersY[j]];
      }


      if ( (numOsNotAssignedX == 0 && numOsNotAssignedY==0) || (bodyMarkersX.size() - numOsNotAssignedX >= 3 && bodyMarkersY.size() - numOsNotAssignedY >= 3 ) )
      {

        // body_part_X first

        std::map<std::string, Eigen::Vector3d> markersTreatedX;

        for (int j = 0; j < bodyMarkersX.size(); j++)
        {
          if (osMarkersNotAssigned[bodyMarkersX[j] ] == 0)
          {
            markersTreatedX[bodyMarkersX[j]] = markerData[i][opNameMarkers.corresNameID[ bodyMarkersX[j] ] ];
          }
        }

        if (numOsNotAssignedX !=0 && originMarkersPosX.size() - numOsNotAssignedX>= 3)
        {
          missVectX.push_back(markersTreatedX);
          Xmiss = true;
        }

        std::map<std::string, Eigen::Vector3d> markersTreatedY;

        for (int j = 0; j < bodyMarkersY.size(); j++)
        {
          if (osMarkersNotAssigned[bodyMarkersY[j] ] == 0)
          {
            markersTreatedY[bodyMarkersY[j]] = markerData[i][opNameMarkers.corresNameID[ bodyMarkersY[j] ] ];
          }
        }

        if (numOsNotAssignedY !=0 && originMarkersPosY.size() - numOsNotAssignedY>= 3)
        {
          missVectY.push_back(markersTreatedY);
          Ymiss = true;
        }

        if (!Xmiss && !Ymiss)
        {
          bodyMarkerDataX.push_back(markersTreatedX);
          bodyMarkerDataY.push_back(markersTreatedY);
        }
        else if (Xmiss && !Ymiss)
        {
          missVectY.push_back(markersTreatedY);
        }
        else if (!Xmiss && Ymiss)
        {
          missVectX.push_back(markersTreatedX);
        }
        else
        {
          // means that everything already put in missVect.
          continue;
        }

      }
      // otherwise, suppress the frame for the CoR estimation of the child
      // (because the next transform may not be guaranted)

  }

  // add frames that could be recovered thanks to recoveryProcedure.
  
  markersManagement::recoveryMarkersProcedure(missVectX, originMarkersPosX, globalPublish, calib_time);
  markersManagement::recoveryMarkersProcedure(missVectY, originMarkersPosY, globalPublish, calib_time);

  bodyMarkerDataX.insert(bodyMarkerDataX.end(), missVectX.begin(), missVectX.end() );
  bodyMarkerDataY.insert(bodyMarkerDataY.end(), missVectY.begin(), missVectY.end() );
    
  std::cout << "Number of datas : " << markerData.size() << std::endl;
  std::cout << "Number of datas after filtering : " << bodyMarkerDataX.size() << std::endl;

  std::map<std::string,Eigen::Vector3f> localPlacementX;
  std::map<std::string,Eigen::Vector3f> localPlacementY;  

  markersManagement::CoREstimateWithMarkers_DoublePart(bodyMarkerDataX, bodyMarkerDataY, localPlacementX, localPlacementY, calib_time, globalPublish);

  allLocalX.push_back(localPlacementX);
  allLocalY.push_back(localPlacementY);


}

/**
 * @brief Once a global idea of correspondences has been found (= each theorical body part is associated with optitrack markers that fits the most with the
 * theoretical model ), the goal of this method is to adjust this 
 * 
 * @return int 
 */
int OpensimManagement::enhanceRobustnessByICP(markersManagement::managementParameters & manParams,
            optitrack_full::OptitrackNameMarkers & opNameMarkers,
            std::vector<std::map<int,Eigen::Vector3d> > markerData,
            std::map<int,std::map<std::string, Eigen::Matrix4f> > & ground2ParentTransform,
            std::map<int, Eigen::Vector3d > optitrack_pos_calib,
            fosim::OsimModel & osMod,
            std::vector<double> & adapted_q_calib,
            std::map<std::string,int> & corresNameID,
            std::map<std::string,Eigen::Vector3d> & adapted_opensim_positions,
            std::map<std::string,std::vector<double> > & all_scaling_factors,
            ros::Time calib_time,
            markersManagement::globalPublisher globalPublish )
{



  std::string bla;
  std::map<int, Eigen::Vector3d > optitrackPoints = optitrack_pos_calib;
  int manual_choice = 1;
  std::map<std::string, std::vector<std::string> > D_body_markers = osMod.D_body_markers;
   std::map<int, Eigen::Vector3d> osimMarkersIndexAdapted;
  std::vector<std::string> already_treated_markers;

  std::map<std::string,double> markersWeights;
  std::map<std::string,Eigen::Vector3d> D_mark_pos = osMod.D_mark_pos;

  for (std::map<std::string,Eigen::Vector3d>::iterator it = D_mark_pos.begin(); it!= D_mark_pos.end(); it++)
  {
    markersWeights[it->first] = 0.0;
  }

  std::vector< std::string > body_in_multiorder = osMod.markers_bodies_multiorder;

  // for (int i = 0; i < body_in_multiorder.size(); i++)
  // {
  //   std::cout << "body in multi : " << body_in_multiorder[i] << std::endl;
  // }
  if (opNameMarkers.calibration_verbose)
  {
    std::printf("------ Joint value origin ------ \n");
    fosim::printJointsValues(osMod.osimModel,osMod.osimState);
    globalPublish.publishJointState(calib_time, osMod.D_joints_values);
  }

  for (int i = 0; i < body_in_multiorder.size(); i++)
  {
    
    //    - MAJ q_calib

    std::map<std::string,double> D_j_v = osMod.D_joints_values;
    std::map<std::string,double>::iterator itjv;

    for (int k =0; k < D_j_v.size(); k++)
    {
      itjv = D_j_v.begin();
      std::advance(itjv,k);
      int mult_index = osMod.D_alph_multi[k];
      adapted_q_calib[mult_index] = itjv->second;

    }

    osMod.setQ(adapted_q_calib);

    std::string body_name = body_in_multiorder[i];
    std::vector<std::string> osimMarkersNames = D_body_markers[body_name];
    std::map<std::string, Eigen::Vector3d> osimMarkers;
    std::map<int,Eigen::Vector3d> optiMarkers;
    std::map<std::string,Eigen::Vector3d> adaptedBodyMarkers;
    for (int l = 0; l < osimMarkersNames.size(); l++)
    {
      std::string name = osimMarkersNames[l];
      int opti_index = corresNameID[name];
      osimMarkers[name] = osMod.D_mark_pos[name];
      markersWeights[name] = 1.0*osMod.D_markers_weight[name];
      optiMarkers[opti_index] = optitrackPoints[opti_index];
      already_treated_markers.push_back(name);
    }

    Eigen::Matrix4f articulation_transform;
    std::vector<double> body_scaling = all_scaling_factors[body_name];

    std::map<std::string, Eigen::Vector3d> osimMarkersOrigin = osimMarkers;

    if (opNameMarkers.calibration_verbose)
    {
      std::cout << "ready calib specific for : " << body_name << std::endl;


      globalPublish.publishJointState(calib_time, osMod.D_joints_values);
      globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
      globalPublish.publishOpensim(calib_time, osimMarkersOrigin);
      
      std::string bla13;
      std::cout << "debug purpose : initial situation" << std::endl;
      std::cin >> bla13;

    }

    // std::cout << "---- in robustness ----"<<std::endl;
    // markersManagement::printAssociation(corresNameID);

    int result = calibrationByICP(manParams, osimMarkers, optiMarkers, osMod, adapted_q_calib, corresNameID, articulation_transform, body_scaling);
    
    if (opNameMarkers.calibration_verbose)
    {
      globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
      globalPublish.publishOpensim(calib_time, osimMarkers);
      std::string bla12;
      std::cout << "debug purpose : icp result" << std::endl;
      std::cin >> bla12;
    }

    //std::map<std::string, Eigen::Vector3d> osimMarkersRescaled;
    std::map<int, Eigen::Vector3d> osimMarkersIndex;

    // must be corrected in order to be rescaled into referential of body part

    int j = 0;

    for (std::map<std::string,Eigen::Vector3d>::iterator it = osimMarkers.begin(); it!=osimMarkers.end(); it++)
    {
      osimMarkersIndex[j] = osimMarkersOrigin[it->first];
      j++;
    }

    if (result < 1)
    {

      globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
      globalPublish.markerPublisher(calib_time, optitrackPoints, globalPublish.optitrack_robust_publisher);
      globalPublish.markerPublisher(calib_time, osimMarkersIndex, globalPublish.opensim_theorical_publisher);
      
      std::map<std::string,int> bodyCNID;
      for (std::map<std::string,Eigen::Vector3d>::iterator it = osimMarkers.begin(); it!=osimMarkers.end(); it++)
      {
        bodyCNID[it->first] = corresNameID[it->first];
      }

      Eigen::Matrix4f O; O.setZero();
      std::printf("Necessary correction for body part : %s \n", body_name.c_str());
      manual_choice = startManualCorrection(bodyCNID, optiMarkers, osMod, globalPublish, O);
      if (manual_choice != 0)
      {

        for (std::map<std::string,int>::iterator it = bodyCNID.begin(); it!=bodyCNID.end(); it++)
        {
          corresNameID[it->first] = it->second;
        }

      }
    }

    if (manual_choice != 0)
    {

      for (std::map<std::string,Eigen::Vector3d>::iterator it = osimMarkers.begin(); it!=osimMarkers.end(); it++ )
      {
        adapted_opensim_positions[it->first] = optiMarkers[corresNameID[it->first]];
        adaptedBodyMarkers[it->first] = optiMarkers[corresNameID[it->first]];
      }

      // case pose perfect
      if (type_calib == 2)
      {
        osMod.setMarkersPositions(adaptedBodyMarkers);
      }

      else
      {
        if (type_calib == 1)
        {
          std::map<std::string,Eigen::Vector3d> rescaleOsim = osimMarkersOrigin;
          // let's try with pure placement
          rescaleCloudsWithAssociation2(optiMarkers, rescaleOsim, osMod,corresNameID, articulation_transform, body_scaling, globalPublish);
        }

        else if (type_calib == 0)
        {
          // association is now good. Lets try to get estimate of CoR
          
          opNameMarkers.allCorresIDName.clear();
          opNameMarkers.corresNameID.clear();
          opNameMarkers.D_mark_all_dist.clear();
          opNameMarkers.opensimMarkersPos.clear();
          opNameMarkers.maxGapVariation.clear();

          for (std::map<std::string,Eigen::Vector3d>::iterator it = osimMarkers.begin(); it!= osimMarkers.end(); it++)
          {
            opNameMarkers.opensimMarkersPos[it->first] = optiMarkers[corresNameID[it->first]];
            opNameMarkers.maxGapVariation[it->first] = opNameMarkers.maximum_residual;
          }
          opNameMarkers.D_mark_all_dist = fosim::getMarkerDistancesInBody(osMod,opNameMarkers.opensimMarkersPos);

          for (std::map<std::string, std::map<std::string,double> >::iterator itsame = opNameMarkers.D_mark_all_dist.begin(); itsame != opNameMarkers.D_mark_all_dist.end(); itsame++) 
          {
            osMod.D_mark_all_dist[itsame->first] = itsame->second;
          }
          CoREstimateManagement(osMod, opNameMarkers, body_name, markerData, corresNameID, body_scaling, ground2ParentTransform, calib_time, globalPublish);
          // normally, there : 
          // - concerned body part CoR has been estimated
          // - according to CoR, markers have been replaced on local body frame
          // - scaling factors have been estimated.

          // now, we need to reexpress markerData for children parts; such as the movements
          // of the children parts may not be influenced by the movement of the parent part 
          // (the body part currently investigated)
          if (CoRDecouplReadapt.size() == 0)
          {
            markersManagement::suppressParentInfluenceInData(osMod, body_name, markerData, corresNameID, markersWeights, ground2ParentTransform, calib_time, globalPublish);
          }

        }

        all_scaling_factors[body_name] = body_scaling;

        std::map<std::string,Eigen::Vector3d> rescaleOsim;

        for (int l = 0; l < osimMarkersNames.size(); l++)
        {
          
          rescaleOsim[osimMarkersNames[l]] = osMod.D_mark_pos[osimMarkersNames[l]];
        }

        if (opNameMarkers.calibration_verbose)
        {
          globalPublish.publishOpensim(calib_time, rescaleOsim);
          globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
          globalPublish.markerPublisher(calib_time, optitrackPoints, globalPublish.optitrack_robust_publisher);
          globalPublish.markerPublisher(calib_time, osimMarkersIndex, globalPublish.opensim_theorical_publisher);
          globalPublish.publishJointState(calib_time, osMod.D_joints_values);

          std::cout << "debug purpose : scaling result." << std::endl;
          std::cin >> bla;
        }


        // IK with already treated optiMarkers (+ new markers). All other weights at 0.0.

        osMod.IK(adapted_opensim_positions,markersWeights,1,calib_time.toSec());

        std::map<std::string,Eigen::Vector3d> osimChecked;
        for (std::map<std::string,Eigen::Vector3d>::iterator it = osMod.D_mark_pos.begin(); it!= osMod.D_mark_pos.end(); it++)
        {
          if (std::find(already_treated_markers.begin(), already_treated_markers.end(), it->first) != already_treated_markers.end())
          {
            osimChecked[it->first] = it->second;
          }
        }

        if (opNameMarkers.calibration_verbose)
        {
          globalPublish.publishOpensim(calib_time, osimChecked);
          globalPublish.publishJointState(calib_time, osMod.D_joints_values);
          fosim::printJointsValues(osMod.osimModel,osMod.osimState);
          std::cout << "debug purpose : end" << std::endl;
          std::cin >> bla;
        }

      }
    }
    else
    {
      i = body_in_multiorder.size();
    }

  }
  
  return(manual_choice);


}


/**
 * @brief Once a global idea of correspondences has been found (= each theorical body part is associated with optitrack markers that fits the most with the
 * theoretical model ), the goal of this method is to adjust this 
 * 
 * @return int 
 */
int OpensimManagement::enhanceRobustnessByICP_DoublePart(markersManagement::managementParameters & manParams,
            optitrack_full::OptitrackNameMarkers & opNameMarkers,
            std::vector<std::map<int,Eigen::Vector3d> > markerData,
            std::map<int,std::map<std::string, Eigen::Matrix4f> > & ground2ParentTransform,
            std::map<int, Eigen::Vector3d > optitrack_pos_calib,
            fosim::OsimModel & osMod,
            std::vector<double> & adapted_q_calib,
            std::map<std::string,int> & corresNameID,
            std::map<std::string,Eigen::Vector3d> & adapted_opensim_positions,
            std::map<std::string,std::vector<double> > & all_scaling_factors,
            ros::Time calib_time,
            markersManagement::globalPublisher globalPublish )
{

  std::map<int, Eigen::Vector3d > optitrackPoints = optitrack_pos_calib;
  int manual_choice = 1;
  std::map<std::string, std::vector<std::string> > D_body_markers = osMod.D_body_markers;
   std::map<int, Eigen::Vector3d> osimMarkersIndexAdapted;
  std::vector<std::string> already_treated_markers;

  std::map<std::string,double> markersWeights;
  std::map<std::string,Eigen::Vector3d> D_mark_pos = osMod.D_mark_pos;

  for (std::map<std::string,Eigen::Vector3d>::iterator it = D_mark_pos.begin(); it!= D_mark_pos.end(); it++)
  {
    markersWeights[it->first] = 0.0;
  }

  std::vector< std::string > body_in_multiorder = osMod.markers_bodies_multiorder;

  for (int i = 0; i < body_in_multiorder.size(); i++)
  {
    std::cout << "body in multi : " << body_in_multiorder[i] << std::endl;
  }

  std::printf("------ Joint value origin ------ \n");
  fosim::printJointsValues(osMod.osimModel,osMod.osimState);
  globalPublish.publishJointState(calib_time, osMod.D_joints_values);


  // for (int i = 0; i < body_in_multiorder.size(); i++)
  // {
    
  //   // first, assign each body part + get distances between body parts

  //   std::string body_name= body_in_multiorder[i];
  //   std::vector<std::string> osimMarkersNames = D_body_markers[body_name];
  //   std::map<std::string, Eigen::Vector3d> osimMarkers;
  //   std::map<int,Eigen::Vector3d> optiMarkers;
  //   for (int l = 0; l < osimMarkersNames.size(); l++)
  //   {
  //     std::string name = osimMarkersNames[l];
  //     int opti_index = corresNameID[name];
  //     osimMarkers[name] = osMod.D_mark_pos[name];
  //     optiMarkers[opti_index] = optitrackPoints[opti_index];
  //     already_treated_markers.push_back(name);
  //   }

  //   Eigen::Matrix4f articulation_transform;
  //   std::vector<double> body_scaling = all_scaling_factors[body_name];

  //   std::cout << "ready calib specific for : " << body_name << std::endl;

  //   std::map<std::string, Eigen::Vector3d> osimMarkersOrigin = osimMarkers;
  //   globalPublish.publishJointState(calib_time, osMod.D_joints_values);
  //   globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
  //   globalPublish.publishOpensim(calib_time, osimMarkersOrigin);
    
  //   std::string bla13;
  //   std::cout << "debug purpose : initial situation" << std::endl;
  //   std::cin >> bla13;

  //   int result = calibrationByICP(manParams, osimMarkers, optiMarkers, osMod, adapted_q_calib, corresNameID, articulation_transform, body_scaling);
    
  //   globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
  //   globalPublish.publishOpensim(calib_time, osimMarkers);
  //   std::string bla12;
  //   std::cout << "debug purpose : icp result" << std::endl;
  //   std::cin >> bla12;

  //   //std::map<std::string, Eigen::Vector3d> osimMarkersRescaled;
  //   std::map<int, Eigen::Vector3d> osimMarkersIndex;

  //   // must be corrected in order to be rescaled into referential of body part

  //   int j = 0;

  //   for (std::map<std::string,Eigen::Vector3d>::iterator it = osimMarkers.begin(); it!=osimMarkers.end(); it++)
  //   {
  //     osimMarkersIndex[j] = osimMarkersOrigin[it->first];
  //     j++;
  //   }

  //   if (result < 1)
  //   {


  //     globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
  //     globalPublish.markerPublisher(calib_time, optitrackPoints, globalPublish.optitrack_robust_publisher);
  //     globalPublish.markerPublisher(calib_time, osimMarkersIndex, globalPublish.opensim_theorical_publisher);

  //     Eigen::Matrix4f O; O.setZero();
  //     std::printf("Necessary correction for body part : %s \n", body_name.c_str());
  //     manual_choice = startManualCorrection(corresNameID, optiMarkers, osMod, globalPublish, O);

  //   }

  // }

  // now, each marker of each body part is attributed, for the calibration pose
  // let's begin the recognition of the positions of the CoR


    //  std::vector<std::map<std::string,Eigen::Vector3f> > allLocalX;
    //  std::vector<std::map<std::string,Eigen::Vector3f> > allLocalY;

  // for (int i = 1; i < body_in_multiorder.size(); i++)
  // {
    
  //   // first, assign each body part + get distances between body parts

  //     opNameMarkers.allCorresIDName.clear();
  //     opNameMarkers.corresNameID.clear();
  //     opNameMarkers.D_mark_all_dist.clear();
  //     opNameMarkers.opensimMarkersPos.clear();
  //     opNameMarkers.maxGapVariation.clear();

  //   std::string body_name_Y= body_in_multiorder[i];
  //   std::string body_name_X = osMod.D_child_parent[body_name_Y];
  //   std::vector<std::string> osimMarkersNamesX = D_body_markers[body_name_X];
  //   std::vector<std::string> osimMarkersNamesY = D_body_markers[body_name_Y];
  //   std::vector<std::string> osimMarkersNames = osimMarkersNamesX;
  //   osimMarkersNames.insert(osimMarkersNamesX.end(), osimMarkersNamesY.begin(), osimMarkersNamesY.end() );
  //   
  //
  //   std::map<int,Eigen::Vector3d> optiMarkers;
  //   for (int l = 0; l < osimMarkersNames.size(); l++)
  //   {
  //     std::string name = osimMarkersNames[l];
  //     int opti_index = corresNameID[name];
  //     opNameMarkers.opensimMarkersPos[name] =  optitrackPoints[opti_index];
  //     opNameMarkers.maxGapVariation[name] = opNameMarkers.maximum_residual;
  //   }

  //     // association is now good. Lets try to get estimate of CoR
      
  //     opNameMarkers.D_mark_all_dist = fosim::getMarkerDistancesInBody(osMod,opNameMarkers.opensimMarkersPos);

  //     for (std::map<std::string, std::map<std::string,double> >::iterator itsame = opNameMarkers.D_mark_all_dist.begin(); itsame != opNameMarkers.D_mark_all_dist.end(); itsame++) 
  //     {
  //       osMod.D_mark_all_dist[itsame->first] = itsame->second;
  //     }

  //    CoREstimateManagement_DoublePart(osMod, opNameMarkers, body_name_X, body_name_Y, markerData, 
  //    allLocalX, allLocalY, corresNameID, calib_time, globalPublish);

  //     // here : 
  //     // - concerned body part CoR has been estimated
  //     // - according to CoR, markers have been replaced on local body frame
  //     // - scaling factors have been estimated.

  //     all_scaling_factors[body_name] = body_scaling;

  //     std::map<std::string,Eigen::Vector3d> rescaleOsim;

  //     for (int l = 0; l < osimMarkersNames.size(); l++)
  //     {
        
  //       rescaleOsim[osimMarkersNames[l]] = osMod.D_mark_pos[osimMarkersNames[l]];
  //     }


  //     globalPublish.publishOpensim(calib_time, rescaleOsim);
  //     globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
  //     globalPublish.markerPublisher(calib_time, optitrackPoints, globalPublish.optitrack_robust_publisher);
  //     globalPublish.markerPublisher(calib_time, osimMarkersIndex, globalPublish.opensim_theorical_publisher);
  //     globalPublish.publishJointState(calib_time, osMod.D_joints_values);

  //       std::string bla;
  //       std::cout << "debug purpose : scaling result." << std::endl;
  //     std::cin >> bla;

  //     // normally, there : 
  //     // - concerned body part CoR has been estimated
  //     // - according to CoR, markers have been replaced on local body frame
  //     // - scaling factors have been estimated.

  //     // now, we need to reexpress markerData for children parts; such as the movements
  //     // of the children parts may not be influenced by the movement of the parent part 
  //     // (the body part currently investigated)


  //     markersManagement::suppressParentInfluenceInData(osMod, body_name, markerData, corresNameID, markersWeights, ground2ParentTransform, calib_time, globalPublish);


  //     //    - IK with already treated optiMarkers (+ new markers). All other weights at 0.0.
  //     for (std::map<std::string,Eigen::Vector3d>::iterator it = osimMarkers.begin(); it!=osimMarkers.end(); it++ )
  //     {
  //         adapted_opensim_positions[it->first] = optiMarkers[corresNameID[it->first]];
  //     }



  //     osMod.IK(adapted_opensim_positions,markersWeights,1,calib_time.toSec());
  //     // could be better to display joint configuration of model here.

  //     std::map<std::string,Eigen::Vector3d> osimChecked;

  //     for (std::map<std::string,Eigen::Vector3d>::iterator it = osMod.D_mark_pos.begin(); it!= osMod.D_mark_pos.end(); it++)
  //     {
  //       if (std::find(already_treated_markers.begin(), already_treated_markers.end(), it->first) != already_treated_markers.end())
  //       {
  //         osimChecked[it->first] = it->second;
  //       }
  //     }

  //     globalPublish.publishOpensim(calib_time, osimChecked);
  //     globalPublish.publishJointState(calib_time, osMod.D_joints_values);

  //     fosim::printJointsValues(osMod.osimModel,osMod.osimState);
      
  //     std::cout << "debug purpose : end" << std::endl;
  //    std::cin >> bla;
  //   }

  //   else
  //   {
  //     i = body_in_multiorder.size();
  //   }

  // }
  
  return(manual_choice);


}


/**
 * @brief Method managing the calibration process, whether it is in simulation or in reality,
 * with recorded or live data.
 * 
 */
void OpensimManagement::start_calib(optitrack_full::DataModel * dataModel, markersManagement::globalPublisher globalPublish)
{
  
  if (simulation)
  {

      Eigen::Matrix4f I; I.setIdentity();
      
      // generate false marker positions data for the calibration process
      // create nData markers. The first one is supposed to be the calibration pose.

      int nData;
      if (type_calib == 0)
      {
        nData = frameRate*duration_for_CoR_estimate;
      }
      else
      {
        // creates only the calibration pose
        nData = 1;
      }

      double pMax = M_PI;

      // first, set stretching

      double streching_noise = 0.0;

      std::uniform_real_distribution<double> unif_stretch(-streching_noise,streching_noise);
      std::random_device rd_stretch;
      std::mt19937 gen_stretch(rd_stretch());
      std::map<std::string,std::vector<double> > stretching_vector;

      std::map<std::string,std::vector<std::string> > D_body_markers = osMod_origin.D_body_markers;

      std::vector< std::string > body_in_multiorder = osMod_origin.markers_bodies_multiorder;

      for (int i = 0; i < body_in_multiorder.size(); i++)
      {
        std::string name = body_in_multiorder[i];
        // double xStretch = 1.0+unif_stretch(gen_stretch);
        // double yStretch = 1.0+unif_stretch(gen_stretch);
        // double zStretch = 1.0+unif_stretch(gen_stretch);

        double val = (streching_noise + (-2*streching_noise*i/(D_body_markers.size() -1) ) );
        
        double xStretch = 1.1+ val;
        double yStretch = 1.1 + val;
        double zStretch = 1.1+ val;


        std::vector<double> v; v.push_back(xStretch), v.push_back(yStretch), v.push_back(zStretch);

        // now, reexpress scaling into body referential. Necessary to find transform body_ref -> ground
        // to do so. 

        std::printf("Body part : %s ; scale : [%2.3f,%2.3f,%2.3f]. \n",name.c_str(), xStretch,yStretch,zStretch);

        stretching_vector[name] = v;

      }

      std::map<std::string, Eigen::Vector3d> stretchPos = osMod_origin.D_mark_pos;
      osMod_origin.applyStretching(stretchPos, stretching_vector);

      osMod_origin.setMarkersPositions(stretchPos);

      std::map<std::string,Eigen::Vector3d> localNoise;
      //double ampl_fixed_noise = std::min( dataModel->osMod.min_mark_dist/(dataModel->opNameMarkers.minima_distance_coeff), 0.1 );
      double ampl_fixed_noise = std::min( dataModel->osMod.min_mark_dist/(2), 0.1 );
                
      // generated optitrack markers
      //std::cout << "start marker generation " << std::endl;

      std::uniform_real_distribution<double> unif_f(-ampl_fixed_noise,ampl_fixed_noise);
      std::random_device rd_f;
      std::mt19937 gen_f(rd_f());

      for ( std::map<std::string, Eigen::Vector3d >::iterator it=osMod_origin.D_mark_pos.begin(); it!=osMod_origin.D_mark_pos.end(); it++ )
      {

          double rxf = unif_f(gen_f);
          double ryf = unif_f(gen_f);
          double rzf = unif_f(gen_f);  

          // then, apply noise on position
          Eigen::Vector3d vecf(rxf,ryf,rzf);
          localNoise[it->first] = vecf+osMod_origin.D_mark_pos_local[it->first];
      }

      osMod_origin.setMarkersPositions(localNoise,"local");

      // then, create data
      std::map<std::string, int> simuCNI = dataModel->opNameMarkers.simu_gest.hiddenCorresNameID;

      markersManagement::printAssociation(simuCNI);

      double add = 0.1;

      for (int i = 0; i < nData; i++)
      {

        std::map<int,Eigen::Vector3d> markerFrame;


        // noise on position of the model (compared to calibration pose)
        std::vector<double> qv = origin_q_calib;
        double ampl_qv_noise = M_PI;

        std::uniform_real_distribution<double> unif_qv(-ampl_qv_noise,ampl_qv_noise);
        std::random_device rd_qv;
        std::mt19937 gen_qv(rd_qv());

        if (i == 0)
        {
          if (type_calib != 2)
          {
            for (int l = 6; l < qv.size(); l++)
            {
              // if (qv[l] > M_PI)
              // {
              //   add = -0.1;
              // }
              // else if (qv[l] < -M_PI)
              // {
              //   add = 0.1;
              // }
              // qv[l] += add;

              qv[l] += 0.1;
            }
          }
          osMod_origin.setQ(qv);
        }

        else
        {
          for (int l = 6; l < qv.size(); l++)
          {
            // if (qv[l] > M_PI)
            // {
            //   add = -0.1;
            // }
            // else if (qv[l] < -M_PI)
            // {
            //   add = 0.1;
            // }
            // qv[l] += add;
              qv[l]= unif_qv(gen_qv);
          }

          // std::map<std::string,double> Djv = osMod_origin.D_joints_values;
          // Djv["arm_flex_l"] = unif_qv(gen_qv);
          // osMod_origin.setQ(Djv);
          osMod_origin.setQ(qv);

        }
        


        // transformation to apply

        double x = 1.0;
        double y = 1.0;//+i*pMax/nData; //1.0
        double z = 1.0;
        double roll, pitch, yaw;
        if (i > frameRate*4)
        {
          roll = 0.0;
          pitch = 0.0; //0.2
          yaw = 0.0;
        }

        else if (i > frameRate*3)
        {
          roll = 0.0;
          pitch = 0.2; //0.2
          yaw = 0.0+ 2*0.2*pMax - i*0.2*pMax/(4*frameRate);          
        }

        else if (i > frameRate*2)
        {
          roll = 2*0.2*pMax - i*0.2*pMax/(3*frameRate);
          pitch = 0.2; //0.2
          yaw = 0.0+ i*0.2*pMax/(3*frameRate);          
        }

        else if (i > frameRate*1)
        {
          roll = 0.0 + i*0.2*pMax/(2*frameRate);
          pitch = 0.2+2*pMax - i*pMax/(2*frameRate); //0.2
          yaw = 0.0;          
        }

        else
        {
          roll = 0.0;
          pitch = 0.2+i*pMax/(1*frameRate); //0.2
          yaw = 0.0;
        }


        // else if (i > frameRate*1.5)
        // {
        //   roll = 2*0.2*pMax - i*0.2*pMax/(1.5*frameRate);
        //   pitch = 0.2+2*pMax - i*pMax/(1.5*frameRate); //0.2
        //   yaw = 0.0+2*0.2*pMax - i*0.2*pMax/(1.5*frameRate);          
        // }



        // else
        // {
        //   roll = i*0.2*pMax/(1.5*frameRate);
        //   pitch = 0.2+i*pMax/(1.5*frameRate); //0.2
        //   yaw = 0.0+i*0.2*pMax/(1.5*frameRate);
        // }

        Eigen::AngleAxisf rollAngle(roll, Eigen::Vector3f::UnitX());
        Eigen::AngleAxisf pitchAngle(pitch, Eigen::Vector3f::UnitY());
        Eigen::AngleAxisf yawAngle(yaw, Eigen::Vector3f::UnitZ());
        Eigen::Quaternion<float> q = rollAngle * pitchAngle * yawAngle;

        Eigen::Matrix3f rotationMatrix = q.matrix();
        Eigen::Vector3f translationMatrix (x, y, z);

        Eigen::Matrix4f transf;
        transf.setIdentity();
        transf.block<3,3>(0,0) = rotationMatrix;
        transf.block<3,1>(0,3) = translationMatrix;

        osMod_origin.setGroundJointOnly(transf);

        if (i == 0)
        {
          printf("----------- Calibration simulated pose -----------\n");
          fosim::printJointsValues(osMod_origin.osimModel,osMod_origin.osimState);

        }

        //fosim::printJointsValues(osMod_origin.osimModel,osMod_origin.osimState);

        // stretching to apply (for each body part)

        //std::cout << "start stretching" << std::endl;

        std::map<std::string, Eigen::Vector3d> markers_pos = osMod_origin.D_mark_pos;

        // std::cin>> bla;

        // noise to apply
        double ampl_noise = dataModel->opNameMarkers.simu_gest.noise; //std::min( dataModel->osMod.min_mark_dist/(dataModel->opNameMarkers.minima_distance_coeff), 0.1 );
          
        // generated optitrack markers
        //std::cout << "start marker generation " << std::endl;

        std::uniform_real_distribution<double> unif(-ampl_noise,ampl_noise);
        std::random_device rd;
        std::mt19937 gen(rd());

        for ( std::map<std::string, Eigen::Vector3d >::iterator it=markers_pos.begin(); it!=markers_pos.end(); it++ )
        {

            double rx = unif(gen);
            double ry = unif(gen);
            double rz = unif(gen);  
            

            // then, apply noise on position
            Eigen::Vector3d vec2(it->second[0]+rx,it->second[1]+ry,it->second[2]+rz);
            int index = simuCNI[it->first];
            markerFrame[index] = vec2;
        }

        calibMarkerData.push_back(markerFrame);
        std::map<std::string,Eigen::Matrix4f> frameTransform;
        for (std::map<std::string, std::vector<std::string> >::iterator it= osMod_origin.D_body_markers.begin(); it!=osMod_origin.D_body_markers.end(); it++)
        {
          frameTransform[it->first] = I;
        }
        ground2ParentTransform[i] = (frameTransform);

      }

    optitrack_pos_calib = calibMarkerData[0];

    osMod_origin.fromScratch();

    std::cout << "markerdata well simulated" << std::endl;


  }

  if (optitrack_pos_calib.size() ==0)
  {
    ROS_INFO("Error : no frame with the good number of markers recorded. Please retry calibration.");
  }

  else
  {
    // start the calibration process
    ROS_INFO("Ready for calibration.");

    Eigen::Matrix4f result_transf;
    std::vector<double> scaling_factors;

    // execute the calibration process : tries to determine the 
    // correspondence between the optitrack markers used for calibration and
    // the opensim markers.
    osMod_origin.setQ(origin_q_calib);


    int result = markersManagement::calibrationByICP(dataModel->opNameMarkers.manParams,
                                            osMod_origin.D_mark_pos,
                                            optitrack_pos_calib,
                                            osMod_origin,
                                            origin_q_calib,
                                            dataModel->opNameMarkers.corresNameID,
                                            result_transf,
                                            scaling_factors,false,
                                            dataModel->doPCScaling);

    // display the opensim - optitrack markers coupled, and the theorical
    // opensim model ( best situation : mostly pitch, perfect q_calib )

    fosim::OsimModel rescale_osMod(osMod_origin.model_path,osMod_origin.geometry_path, osMod_origin.setup_ik_file);
    
    rescale_osMod.setQ(origin_q_calib);

    std::map<std::string, Eigen::Vector3d> rescaleMarkers;
    
    if (dataModel->doPCScaling)
    {
      rescaleMarkers = markersManagement::applyScalingToMarkers( rescale_osMod.D_mark_pos,scaling_factors);
      rescale_osMod.setMarkersPositions(rescaleMarkers,"ground");

    }
    else
    {
      rescaleMarkers = osMod_origin.D_mark_pos_local;
      rescale_osMod.setMarkersPositions(rescaleMarkers,"local");      
    }



    rescale_osMod.setGroundJointOnly(result_transf);

    globalPublish.publishOpensimOptitrack(optitrack_pos_calib_time,optitrack_pos_calib, rescale_osMod, result_transf);

    // if the calibration process by ICP noticed an error on the opensim / optitrack correspondance,
    // activates a manual correction step for safety sake.
    int manual_choice;

    std::map<std::string,std::vector<double> > all_scaling_factors;

    

    if (result < 2 || dataModel->doPCScaling)
    {
        // if (result < 2)
        // {
        std::vector<int> v_res = markersManagement::tryToCorrectCalibPose(
                        dataModel->opNameMarkers.manParams,
                        dataModel->opNameMarkers.corresNameID,
                        osMod_origin,
                        optitrack_pos_calib,
                        origin_q_calib,
                        scaling_factors,
                        optitrack_pos_calib_time,
                        rescale_osMod,
                        globalPublish,
                        result_transf,
                        dataModel->doPCScaling
        );


        manual_choice = v_res[0];
        result = v_res[1];
      //   }

      // else if (dataModel->doPCScaling)
      // {
      //   result = 2;
      //   manual_choice = 1;
      // }

      if (manual_choice > 0)
      {
        // unless the choice is to cancel the whole procedure,
        // we always check for the association in order to be sure
        // that no bugs occured after this detection
        std::printf("Now correcting markers association... \n");
        ROS_WARN("REMEMBER : HERE, YOU ONLY NEED TO ASSOCIATE POINTS SUCH AS THEY CORRESPONDS TO THE SAME BODY PARTS. An other round of correction will then take place to associate markers for each body part.");
        manual_choice = markersManagement::startManualCorrection(dataModel->opNameMarkers.corresNameID, optitrack_pos_calib, rescale_osMod, globalPublish, result_transf);

      }
    }
    // else if (!dataModel->doPCScaling)
    // {

    //     ROS_WARN("DEBUG CHECK FOR NEW FEATURE");
    //     manual_choice = markersManagement::startManualCorrection(dataModel->opNameMarkers.corresNameID, optitrack_pos_calib, rescale_osMod, globalPublish, result_transf);

    // }




    else
    {
      std::printf("No correction seems necessary. Congrats! \n");
      for (std::map<std::string,std::vector<std::string> >::iterator it = osMod_origin.D_body_markers.begin(); it!= osMod_origin.D_body_markers.end(); it++)
      {
        all_scaling_factors[it->first] = scaling_factors;
      }
      std::map<std::string,double> D_j_v = rescale_osMod.D_joints_values;
      std::map<std::string,double>::iterator itjv;

      for (int k =0; k < D_j_v.size(); k++)
      {
        itjv = D_j_v.begin();
        std::advance(itjv,k);
        int mult_index = rescale_osMod.D_alph_multi[k];
        dataModel->opNameMarkers.q_calib[mult_index] = itjv->second;

      }
      std::map<std::string,Eigen::Vector3d> currentMarkersLocation;
      std::map<std::string,int> corresNameID = dataModel->opNameMarkers.corresNameID;
                
      for (std::map<std::string,int>::iterator it = corresNameID.begin(); it!=corresNameID.end(); it++ )
      {
        currentMarkersLocation[it->first] = optitrack_pos_calib[it->second];
      }           

      // let's suppose the calibration pose is perfect
      if (type_calib == 2)
      {
        manual_choice = 5;
        dataModel->osMod.setQ(dataModel->opNameMarkers.q_calib);
        dataModel->osMod.setMarkersPositions(currentMarkersLocation);
      }

    }

    if (manual_choice != 0 && manual_choice != 5)
    {   
      rescale_osMod.fromScratch();
      std::map<std::string,Eigen::Vector3d> currentMarkersLocation;
      std::map<std::string,int> corresNameID = dataModel->opNameMarkers.corresNameID;
              
      for (std::map<std::string,int>::iterator it = corresNameID.begin(); it!=corresNameID.end(); it++ )
      {
        currentMarkersLocation[it->first] = optitrack_pos_calib[it->second];
      }           
        std::map<std::string, std::vector<std::string> > D_body_markers = osMod_origin.D_body_markers;

        for (std::map<std::string,std::vector<std::string> >::iterator it = D_body_markers.begin(); it != D_body_markers.end(); it++)
        {
          std::vector<double> v;
          all_scaling_factors[it->first] = v;

        }
        rescale_osMod.setQ(origin_q_calib);
        rescale_osMod.setGroundJointOnly(result_transf);
        std::cout << "Start robustness" << std::endl;
        // case type_calib = 1
        manual_choice = enhanceRobustnessByICP(dataModel->opNameMarkers.manParams,
                                                  opNameMarkersCoR,
                                                  calibMarkerData,
                                                  ground2ParentTransform,
                                                  optitrack_pos_calib,
                                                  rescale_osMod,
                                                  dataModel->opNameMarkers.q_calib,
                                                  dataModel->opNameMarkers.corresNameID,
                                                  currentMarkersLocation,
                                                  all_scaling_factors,
                                                  optitrack_pos_calib_time,
                                                  globalPublish
                                                  );

    }

    // if the user didn't chose to stop the program after the manual correction step,
    // save the correspondances to the concerned variables.
    if (manual_choice != 0)
    {

      std::map<std::string,Eigen::Vector3d> markersLocation;
      std::map<std::string,int> corresNameID = dataModel->opNameMarkers.corresNameID;
              

      dataModel->opNameMarkers.CINCalib.clear();
      for (std::map<std::string,int>::iterator it = corresNameID.begin(); it!=corresNameID.end(); it++ )
      {
        dataModel->opNameMarkers.CINCalib[it->second] = it->first;
        markersLocation[it->first] = optitrack_pos_calib[it->second];
        markersManagement::manageACINAdding( it->first,it->second,calibFrame, dataModel->opNameMarkers.allCorresIDName, dataModel->opNameMarkers.repeat,true);

      }     

      // if the current experimentation is a record streamed by Motive,
      // search for the calibration frame at the end of the experiment
      if (dataModel->opNameMarkers.repeat)
      {
        dataModel->opNameMarkers.foundCalibFrame = false;
      }

      // if the user chose to activate scaling step, execute it.
      if (scaling)
      {

        std::map<std::string,double> D_j_v_ori = rescale_osMod.D_joints_values;
        bool success = start_scaling(rescale_osMod, markersLocation, all_scaling_factors);
        // if the scaling step was a success, activates the scaling check
        if (success)
        {

          rescale_osMod.setQ(dataModel->opNameMarkers.q_calib);
          //rescale_osMod.setGroundJointOnly(result_transf);
          // publish the position of the scaled opensim markers
          
          globalPublish.publishOpensimOptitrack(optitrack_pos_calib_time, optitrack_pos_calib, rescale_osMod );
          // check the precision of the scaling process
          int choice = markersManagement::checkScaling(rescale_osMod, D_j_v_ori, optitrack_pos_calib,markersLocation, globalPublish);
          bool confirmation;


          // if the precision of the scaling is satisfactory, save it and conclude the calibration process
          if (choice == 2)
          {
            rescale_osMod.clone3(dataModel->osMod,false,false);
            dataModel->opNameMarkers.D_mark_all_dist = dataModel->osMod.D_mark_all_dist;
            // to force to trigger assemble to new IK 
            dataModel->opNameMarkers.framesSinceAssemble = dataModel->opNameMarkers.limFramesSinceAssemble;
            confirmation = true;
          }
          // however, the scaling could be canceled ...
          else if (choice == 1)
          {
            confirmation = false;
          }
          // ... or the whole calibration process could be canceled.
          else if (choice == 0)
          {
            reboot_calib(dataModel);

            confirmation = false;

          }
          // we can also do scaling only on markers on non-scaled model
          // nb : naive version only for now
          else if (choice == 3)
          {
            fosim::printJointsValues(dataModel->osMod.osimModel,dataModel->osMod.osimState);

            dataModel->osMod.setQ(dataModel->opNameMarkers.q_calib);
            //dataModel->osMod.setGroundJointOnly pas bsn, déjà réglé
            // dans q_calib
            fosim::printJointsValues(dataModel->osMod.osimModel,dataModel->osMod.osimState);

            dataModel->osMod.setMarkersPositions(markersLocation);

            globalPublish.publishOpensimOptitrack(optitrack_pos_calib_time,optitrack_pos_calib, dataModel->osMod);

            std::cout << "debug, wait for answer ... " << std::endl;
            std::string bla;
            std::cin >> bla;


            //markersManagement::allDistanceVariationStatic(osMod_origin.D_mark_all_dist, dataModel->osMod.D_mark_all_dist, dataModel->opNameMarkers.maxGapVariation, dataModel->opNameMarkers.maximum_residual, dataModel->opNameMarkers.minima_distance_coeff);
            dataModel->osMod.setToNeutral();

            confirmation = false;

          }          
          // or do marker scaling on scaled model
          // nb : naive version only for now
          else if (choice == 4)
          {
            rescale_osMod.setQ(dataModel->opNameMarkers.q_calib);

            rescale_osMod.clone3(dataModel->osMod,true,false);
            dataModel->osMod.setMarkersPositions(markersLocation);

            globalPublish.publishOpensimOptitrack(optitrack_pos_calib_time,optitrack_pos_calib, dataModel->osMod);
            
            dataModel->osMod.setToNeutral();

            confirmation = true;
            
          }   



          // send the decision concerning the scaling process to scaling_manager.py
          std_srvs::SetBool srv;
          srv.request.data = confirmation;
          confirmation_scaling.call(srv);
          
          if (choice !=0 && simulation)
          {
            dataModel->opNameMarkers.simu_gest.calibCompleted = true;
          }

        }

        // otherwise, cancel the calibration process
        else
        {
          reboot_calib(dataModel);
        }


      }

      // otherwise, still try to adapt variation of position of markers
      else
      {
        rescale_osMod.clone3(dataModel->osMod, true, true);

        //markersManagement::allDistanceVariationStatic(osMod_origin.D_mark_all_dist, dataModel->osMod.D_mark_all_dist, dataModel->opNameMarkers.maxGapVariation, dataModel->opNameMarkers.maximum_residual, dataModel->opNameMarkers.minima_distance_coeff);
        if (simulation)
        {
          dataModel->opNameMarkers.simu_gest.calibCompleted = true;
        }

      }

    }
    // otherwise, cancel the calibration process
    else
    {
      reboot_calib(dataModel);
    }

  }
  // dataModel->osMod.setQ(dataModel->opNameMarkers.q_calib);
  // fosim::printJointsValues(dataModel->osMod.osimModel,dataModel->osMod.osimState);

  // globalPublish.publishJointState(optitrack_pos_calib_time, dataModel->osMod.D_joints_values);


  // std::cout << "debug, wait for answer ... " << std::endl;
  // std::string bla;
  // std::cin >> bla;

  // dataModel->osMod.setToNeutral();

  
}

void OpensimManagement::clean_associations(optitrack_full::DataModel * dataModel, 
std::vector<std::string> concerned_objects_parts,
bool cleanFORCE)
{

  dataModel->opNameMarkers.debugAssos = true;

  dataModel->opNameMarkers.manParams.filtering_verbose = true;

  dataModel->opNameMarkers.never_verbose = true;


  int currentFrame = dataModel->frameNumber;
  std::map<std::string,int> toAddMarkers;
  // only for debugging purpose
  std::map<int,std::string> suppressedObjectsBecauseAdding;
  int wi = 0;
  while (wi < concerned_objects_parts.size())
  {
    // manage suppression of additions, if necessary
    std::string object = concerned_objects_parts[wi];
    std::vector<std::string> concernedMarkers;
    // check if there are informations inside the object
    std::vector<std::string> checkIfAskForAssos = fosim::splitString(object,"=");
    if (checkIfAskForAssos.size() == 2 && dataModel->osMod.D_markers_body.find(checkIfAskForAssos[0]) != dataModel->osMod.D_markers_body.end())
    {
      // then, this is a demand for a new association. Let's treat it
      std::string name = checkIfAskForAssos[0];
      int optiID = std::stoi(checkIfAskForAssos[1]);
      std::string nameToSuppress = "";

      // do not forget to automatically manage the suppression of the previous association, if it existed
      if (dataModel->opNameMarkers.repeat)
      {
        int framePicked;
        bool found = markersManagement::checkACINCorres(optiID,currentFrame,dataModel->opNameMarkers.allCorresIDName, nameToSuppress,framePicked, true);
        if (found && nameToSuppress != name)
        {
          if (std::find(concerned_objects_parts.begin(), concerned_objects_parts.end(), nameToSuppress) == concerned_objects_parts.end())
          {
            concerned_objects_parts.push_back(nameToSuppress);
          }
          else
          {
            nameToSuppress = "";
          }
        }
      }

      toAddMarkers[name] = optiID;
      if (nameToSuppress != "")
      {
       suppressedObjectsBecauseAdding[optiID] = nameToSuppress;
      }

      //concerned_objects_parts.erase(concerned_objects_parts.begin()+wi);
      // we also need to be sure that no issues will occured during the suppression of this object
      concerned_objects_parts.push_back(name);
      wi++;



    }
    else
    {
      wi++;
    }
  }
  for (int i = 0; i < concerned_objects_parts.size(); i++)
  {
    std::string object = concerned_objects_parts[i];
    std::vector<std::string> concernedMarkers;
    if (dataModel->osMod.D_body_markers.find(object) != dataModel->osMod.D_body_markers.end())
    {
      concernedMarkers = dataModel->osMod.D_body_markers[object];
    }
    else if (dataModel->osMod.D_markers_body.find(object) != dataModel->osMod.D_markers_body.end())
    {
      concernedMarkers.push_back(object);
    }

    for (int j = 0; j < concernedMarkers.size(); j++)
    {
      std::string marker = concernedMarkers[j];
      std::map<std::string,int>::iterator it = dataModel->opNameMarkers.corresNameID.find(marker);
      // first, takes all optitrack ID concerned AND
      // that were already associated once AND
      // that are into calibration frame (except)
      // AND that were NOT into calibration frame (considered perfect)

      if (it != dataModel->opNameMarkers.corresNameID.end())
      {


        int optiID = it->second;
        // optiID was already associated once?
        std::map<int, std::map<int,LabelCarac> >::iterator it6 = dataModel->opNameMarkers.allCorresIDName.find(optiID);
        // optiID is in current frame?
        std::map<int, Eigen::Vector3d >::iterator it2 = dataModel->opNameMarkers.lastOptitrackMarkers.find(optiID);
        // optiID is in calibration frame?
        std::map<int, Eigen::Vector3d >::iterator it4 = optitrack_pos_calib.find(optiID);
        std::string markWithFrame = "";
        int framePicked = -2;              

        //if (it2!= dataModel->opNameMarkers.lastOptitrackMarkers.end() )
        if (it6!= dataModel->opNameMarkers.allCorresIDName.end() && optiID != -1 )
        {

          if ( it4 == optitrack_pos_calib.end() )
          {

            // suppress all elements concerned
            dataModel->opNameMarkers.corresNameID[marker] = -1;
            // added, to check
            dataModel->opNameMarkers.lastOsMarkersNotAssigned[marker] = 1;
            dataModel->opNameMarkers.lastFrameOsAssos[marker] = std::max(dataModel->frameNumber-1,0);
            dataModel->opNameMarkers.opensimMarkersPos[marker] = Eigen::Vector3d::Zero();
            // then, check if the optitrack IDs are into allCorresIDName : if it's the case AND the
            // frame returns the concerned marker name : 
            if (dataModel->opNameMarkers.repeat)
            {

              bool found = markersManagement::checkACINCorres(optiID,currentFrame,dataModel->opNameMarkers.allCorresIDName, markWithFrame,framePicked, true,true);
              if ( (found && markWithFrame == marker) || cleanFORCE )
              {
                std::map<int, std::map<int,LabelCarac> >::iterator it3 = dataModel->opNameMarkers.allCorresIDName.find(optiID);
                std::map<int,LabelCarac> temp;
                // (simpler) supppress ALL associations for this optitrack ID in allCorresIDName                
                if (cleanFORCE)
                {
                  dataModel->opNameMarkers.allCorresIDName.erase(it3);      
                  ROS_WARN("[SUCCESS-NORMAL-CLEANFORCE:%d] CleanUp all associations for optitrack index %d (associated right now with %s, body %s) ", cleanFORCE, optiID, marker.c_str(), dataModel->osMod.D_markers_body[marker].c_str());   
                }
                // (harder, but more precise) suppress only the current and the future 
                // associations for this optitrack index IF the optitrack index is into the current frame
                // (we do not want to risk to suppress the associations of a marker that is not into the current frame; it could be a bad mistake)
                // we still authorize it with a CLEANFORCE                
                else
                {
                  if (it2 == dataModel->opNameMarkers.lastOptitrackMarkers.end())
                  {
                    // it2 case
                    ROS_WARN("[FAILURE] Associated Optitrack index %d (for Opensim marker %s) not found in current frame; in order to prevent a false suppression, association is not suppressed.", optiID, marker.c_str());
                  }
                  else
                  {
                    int l = it3->second.size()-1;
                    found = false; 
                    std::map<int,LabelCarac>::iterator it9;
                    while (l > 0 && !found)
                    {
                      it9 = it3->second.begin();
                      std::advance(it9,l);
                      if (it9->first <= currentFrame)
                      {
                        found = true;
                      }
                      else
                      {
                        l--;
                      }
                  
                    }
                    int v = 0;
                    while (v <= l)
                    {
                      it9 = it3->second.begin();
                      std::advance(it9,v);
                      temp[it9->first] = it9->second;
                      v++;
                    }

                    LabelCarac lc;
                    temp[-1] = lc;
                    dataModel->opNameMarkers.allCorresIDName[optiID] = temp;

                    ROS_WARN("[SUCCESS-NORMAL-CLEANFORCE:%d] CleanUp all associations for optitrack index %d (associated right now with %s, body %s) ", cleanFORCE, optiID, marker.c_str(), dataModel->osMod.D_markers_body[marker].c_str());
                  }
                }
              }
              else
              {
                ROS_WARN("[FAILURE_NOTFOUND-NORMAL] Associated Optitrack index %d (for Opensim marker %s) not found in ACIN or not same association found compared to corresNameID (found association : %s) : weird.", optiID, marker.c_str(), markWithFrame.c_str());          
              }
            }
          }

          else
          {

            // suppress association in corresNameID
            dataModel->opNameMarkers.corresNameID[marker] = -1;
            // added, to check
            dataModel->opNameMarkers.lastOsMarkersNotAssigned[marker] = 1;
            dataModel->opNameMarkers.lastFrameOsAssos[marker] = std::max(dataModel->frameNumber-1,0);
            dataModel->opNameMarkers.opensimMarkersPos[marker] = Eigen::Vector3d::Zero();            
            
            bool needToSuppress = true;
            // special case where the marker was associated at a calibration step
            // check if there is more than one association for the concerned marker
            if (dataModel->opNameMarkers.repeat)
            {
              
              if ((it6!= dataModel->opNameMarkers.allCorresIDName.end() && it6->second.size() > 1 )  || cleanFORCE)
              {
                // in that case, only conserves the association of the calibration frame
                // TODO : add also associations made by user (unless cleanForce)
                bool found = markersManagement::checkACINCorres(optiID,calibFrame,dataModel->opNameMarkers.allCorresIDName, markWithFrame,framePicked, true);                
                if (found || cleanFORCE)
                {
                  std::map<int, std::map<int, LabelCarac> >::iterator it3 = dataModel->opNameMarkers.allCorresIDName.find(optiID);
                  std::map<int,LabelCarac> temp;
                  // (simpler) supppress ALL associations for this optitrack ID in allCorresIDName, even the calibration one
                  if (cleanFORCE)
                  {
                    dataModel->opNameMarkers.allCorresIDName.erase(it3);
                    ROS_WARN("[SUCCESS-CALIB-CLEANFORCE:%d] CleanUp all associations (except for calibration if cleanFORCE==0) for optitrack index %d (associated right now with %s, body %s)", cleanFORCE, optiID, marker.c_str(), dataModel->osMod.D_markers_body[marker].c_str());                                                 
                  }
                  // (harder, but more precise) suppress only the current and the future 
                  // associations for this optitrack index IF the optitrack index is into the current frame
                  // (we do not want to risk to suppress the associations of a marker that is not into the current frame; it could be a bad mistake)
                  // we still authorize it with a CLEANFORCE
                  else
                  {
                    if (it2 == dataModel->opNameMarkers.lastOptitrackMarkers.end())
                    {
                      // it2 case
                      ROS_WARN("[FAILURE] Associated Optitrack index %d (for Opensim marker %s) not found in current frame; in order to prevent a false suppression, association is not suppressed.", optiID, marker.c_str());
                    }
                    else
                    {
                      int l = it3->second.size()-1;
                      found = false; 
                      std::map<int,LabelCarac>::iterator it9;
                      while (l > 0 && !found)
                      {
                        it9 = it3->second.begin();
                        std::advance(it9,l);
                        if (it9->first <= currentFrame)
                        {
                          found = true;
                        }
                        else
                        {
                          l--;
                        }
                    
                      }
                      int v = 0;
                      while (v <= l)
                      {
                        it9 = it3->second.begin();
                        std::advance(it9,v);
                        temp[it9->first] = it9->second;
                        v++;
                      }
                      

                      // takes care that the association at calibration frame is still saved
                      LabelCarac lc;
                      lc.osim_name = markWithFrame;    
                      lc.byUser = true;
                      LabelCarac lc2;
                      temp[calibFrame+1] = lc;    
                      temp[-1] = lc2;
                      dataModel->opNameMarkers.allCorresIDName[optiID] = temp;


                      // // then, only re-add the association of the calibration frame
                      // // suppressed : maybe the association change was for a good reason? In that case, better suppress it
                      // std::map<int,std::string> temp;
                      // temp[calibFrame+1] = markWithFrame;     
                      // // bug otherwise
                      // temp[-1] = "";
                      // dataModel->opNameMarkers.allCorresIDName[optiID] = temp;


                      // if (!cleanFORCE)
                      // {
                      //   markersManagement::manageACINAdding(markWithFrame,optiID,calibFrame,dataModel->opNameMarkers.allCorresIDName,true);
                      // }
                      // suppress association in corresNameID
                      needToSuppress = true;
                      ROS_WARN("[SUCCESS-CALIB-CLEANFORCE:%d] CleanUp all associations (except for calibration if cleanFORCE==0) for optitrack index %d (associated right now with %s, body %s)", cleanFORCE, optiID, marker.c_str(), dataModel->osMod.D_markers_body[marker].c_str());                  
                    }
                  }
                }
                else
                {
                  ROS_WARN("[FAILURE_NOTFOUND-CALIB] Associated Optitrack index %d (for Opensim marker %s) not found in ACIN compared to corresNameID : weird", optiID, marker.c_str());                            
                }

              }
              else
              {
                ROS_WARN("[FAILURE-CALIB] Associated Optitrack index %d (for Opensim marker %s, body %s) has only one association, made in calibration frame; thus, association is not suppressed.", optiID, marker.c_str(), dataModel->osMod.D_markers_body[marker].c_str());
                needToSuppress = false;
              }

            }

            if (needToSuppress)
            {
              dataModel->opNameMarkers.corresNameID[marker] = -1;                  
            }

          }

        }
        else
        {
          // it6 case
          ROS_WARN("[FAILURE_NOASSOS-NORMAL] Associated Optitrack index %d was never associated; association is not suppressed.", optiID);          
        }
      }
    }
  }

  // now do adding, if necessary
  for (std::map<std::string,int>::iterator it = toAddMarkers.begin(); it!= toAddMarkers.end(); it++)
  {
      std::string name = it->first;
      int optiID = it->second;
      std::string nameToSuppress = "";
      if (suppressedObjectsBecauseAdding.find(optiID) != suppressedObjectsBecauseAdding.end())
      {
        nameToSuppress = suppressedObjectsBecauseAdding[optiID];
      }
      
      // do adding
      dataModel->opNameMarkers.corresNameID[name] = optiID;
      markersManagement::manageACINAdding(name,optiID,currentFrame,dataModel->opNameMarkers.allCorresIDName,true,true,false);

      ROS_WARN("[SUCCESS-ADDING-CLEANFORCE:%d] Add the new association : optitrack index %d | opensim marker %s. Suppressed previous association [%s] (blank if no suppression).", cleanFORCE, optiID, name.c_str(), nameToSuppress.c_str());
            
  }

}

/**
 * @brief Initialize the mocap_node. Inspired from initialize function of original optitrack_mocap package.
 * 
 */
void OpensimManagement::initialize_opensim(optitrack_full::DataModel * dataModel, 
bool record_trc, 
file_management::TRCManager & TRCManager,
optitrack_full::ServerDescription serverDescription,
markersManagement::globalPublisher globalPublish
)
{
  // added, to test
  osMod_origin.fromScratch();
  osMod_origin.setToNeutral();
  osMod_origin.clone3(dataModel->osMod,true,false);

  deb_tst = false;
  // calibration parameters
  calibration_asked = false;
  dataModel->opNameMarkers.foundCalibFrame= true;
  dataModel->opNameMarkers.pause_mode = false;
  calibFrame = 0;
  dataModel->opNameMarkers.q_calib = origin_q_calib;

  scale_file = serverDescription.scale_file;
  data_file = serverDescription.data_file;
  ros_package_path = serverDescription.ros_package_path;


  if (record_trc)
  {

    // dataModel->osMod
    for (std::map<std::string,std::string>::iterator it = dataModel->osMod.D_markers_body.begin(); it!=dataModel->osMod.D_markers_body.end(); it++ )
    {
      TRCManager.necessaryMarkers.push_back(it->first);
    }
  }

  frameRate = dataModel->opNameMarkers.frameRate;
  // for OptitrackNameMarkers

  dataModel->dispatching = false;
  dataModel->opNameMarkers.originTime = ros::Time::now();
  dataModel->opNameMarkers.constraintWeight = 1.0;
  dataModel->opNameMarkers.initConstraintWeight = 1.0;
  dataModel->opNameMarkers.framesSinceIK = 0;
  dataModel->opNameMarkers.oriLimFramesSinceIK = std::min( frameRate, 5 );
  dataModel->opNameMarkers.limFramesSinceIK = dataModel->opNameMarkers.oriLimFramesSinceIK;
  dataModel->opNameMarkers.limAdjustFramesSinceIK = 1.0;
  dataModel->opNameMarkers.framesSinceAssemble = 0;
  dataModel->opNameMarkers.limFramesSinceAssemble = frameRate/8;      

  dataModel->opNameMarkers.D_mark_all_dist = osMod_origin.D_mark_all_dist;

  dataModel->opNameMarkers.error_verbose = serverDescription.error_verbose;

  dataModel->opNameMarkers.calibration_verbose = serverDescription.calibration_verbose;

  // the higher, the more associations will be refused
  dataModel->opNameMarkers.minima_distance_coeff = 4*sqrt(3);  //4*sqrt(3)

  std::map<std::string,int> corresNameID;
  dataModel->opNameMarkers.corresNameID = corresNameID;

  std::map<std::string,double> D_mark_dist = osMod_origin.D_mark_dist;

  for (std::map<std::string,double>::iterator it= D_mark_dist.begin(); it!=D_mark_dist.end(); it++)
  {
    dataModel->opNameMarkers.maxGapVariation[it->first] = dataModel->opNameMarkers.maximum_residual;
    dataModel->opNameMarkers.lastOsMarkersNotAssigned[it->first] = 1;
  }


  dataModel->opNameMarkers.opensimMarkersPos = dataModel->osMod.D_mark_pos;

  // simulation parameters
  simulation = dataModel->opNameMarkers.simulation;

  if (simulation)
  {
    std::vector<int> v(osMod_origin.D_joints_values.size(), 0);
    dataModel->opNameMarkers.simu_gest.joint_state = v;
    dataModel->opNameMarkers.simu_gest.i_joint = 0;
    dataModel->opNameMarkers.simu_gest.speed = serverDescription.speed;
    dataModel->opNameMarkers.simu_gest.j_val = 0.0;
    dataModel->opNameMarkers.simu_gest.dissapear = serverDescription.dissapear;
    dataModel->opNameMarkers.simu_gest.max_index = osMod_origin.D_mark_pos.size();
    dataModel->opNameMarkers.simu_gest.noise = serverDescription.noise;
    dataModel->opNameMarkers.simu_gest.same_frames = serverDescription.same_frames;
    dataModel->opNameMarkers.simu_gest.cur_frames = serverDescription.same_frames;
    dataModel->opNameMarkers.simu_gest.simu_verbose = serverDescription.simu_verbose;
    dataModel->osMod.setToNeutral();
    dataModel->opNameMarkers.simu_gest.calibCompleted = false;
    int comp = 0;

    for (std::map<std::string,Eigen::Vector3d>::iterator it = osMod_origin.D_mark_pos.begin(); it!= osMod_origin.D_mark_pos.end(); it++)
    {
      dataModel->opNameMarkers.simu_gest.hiddenCorresNameID[it->first] = comp;
      comp++;
    }


  }

  // create body order 
  if (dataModel->opNameMarkers.bodies_label_order.size() == 0 )
  {
    // first, take all size of markers
    std::map<int,std::vector<std::string> > body_by_markers;
    std::vector<std::string> v;
    for (std::map<std::string, std::vector<std::string> >::iterator it = dataModel->osMod.D_body_markers.begin(); it != dataModel->osMod.D_body_markers.end(); it++)
    {
      int size = it->second.size();
      if (body_by_markers.find(size) == body_by_markers.end())
      {
        body_by_markers[size] = v;
      }
      body_by_markers[size].push_back(it->first);
    }
    std::vector<std::string> bodyPartsOrder = dataModel->osMod.markers_bodies_multiorder;
    std::map<int,std::vector<std::string> >::iterator it2 = body_by_markers.begin(); 
    for (int i = 0; i < body_by_markers.size(); i++)
    {
      it2 = body_by_markers.begin(); 
      std::advance(it2, body_by_markers.size()-1-i);
      std::vector<std::string> vBP = it2->second;

      std::map<int,std::string> root_name;
      std::vector<int> v_index;
      

      for (int j = 0; j < vBP.size(); j++)
      {
        int index = ( std::find(bodyPartsOrder.begin(), bodyPartsOrder.end(), vBP[j])    - bodyPartsOrder.begin());
        root_name[index] = vBP[j];
        v_index.push_back(index);
      }

      std::sort(v_index.begin(), v_index.end());
      for (int j = 0; j < v_index.size(); j++)
      {
        std::string name = root_name[v_index[j]];
        dataModel->opNameMarkers.bodies_label_order.push_back(name);
      }


    }

  }
 
  opNameMarkersCoR = dataModel->opNameMarkers;
  opNameMarkersCoR.repeat = true;
  // here, we suppose that we do not have any noise into the calibration process; 
  // so, we can be more tolerant about the markers associations during this process
  opNameMarkersCoR.minima_distance_coeff  = opNameMarkersCoR.minima_distance_coeff/4;
  //opNameMarkersCoR.association_verbose = false;


  reboot_calib(dataModel);

  ROS_INFO("Initialization complete for Opensim side.");



};

/**
 * @brief Used only with a labeled .trc
 * If doCalib = false, we suppose that :
 *  - you want to use your model (already scaled, markers already setup) without any modifications.
 *  (ex : using a model rescaled thanks to AddBiomechanics)
 * - your .trc file is fully labeled.
 * 
 * 
 * @param dataModel 
 * @param globalPublish 
 */
void OpensimManagement::opensim_trc_calibration(optitrack_full::DataModel * dataModel, 
markersManagement::globalPublisher globalPublish,
bool doCalib, 
bool isLabelized)
{

  if (isLabelized)
  {


    std::map<std::string,int> corresNameID = dataModel->opNameMarkers.corresNameID;

    fosim::OsimModel rescale_osMod(osMod_origin.model_path,osMod_origin.geometry_path, osMod_origin.setup_ik_file);

    std::map<std::string,Eigen::Vector3d> currentMarkersLocation;
    std::map<std::string,std::vector<double> > all_scaling_factors;

    if (doCalib)
    {
      rescale_osMod.setQ(dataModel->opNameMarkers.q_calib);
      Eigen::Matrix4f transf;
      markersManagement::findTransformWithAssociation(optitrack_pos_calib,rescale_osMod.D_mark_pos,corresNameID,transf);

      rescale_osMod.setGroundJointOnly(transf);

      for (std::map<std::string,int>::iterator it = corresNameID.begin(); it!=corresNameID.end(); it++ )
      {
        currentMarkersLocation[it->first] = optitrack_pos_calib[it->second];
      }     

      std::map<std::string, std::vector<std::string> > D_body_markers = rescale_osMod.D_body_markers;

      for (std::map<std::string,std::vector<std::string> >::iterator it = D_body_markers.begin(); it != D_body_markers.end(); it++)
      {
        std::vector<double> v;
        all_scaling_factors[it->first] = v;

      }

      globalPublish.publishJointState(optitrack_pos_calib_time, rescale_osMod.D_joints_values);
      globalPublish.markerPublisher(optitrack_pos_calib_time, optitrack_pos_calib, globalPublish.optitrack_publisher);
      globalPublish.publishOpensim(optitrack_pos_calib_time, rescale_osMod.D_mark_pos);
      std::string bla13;
      std::cout << "debug purpose : initial situation" << std::endl;
      std::cin >> bla13;


      std::cout << "Start robustness with TRC" << std::endl;
      // int manual_choice = enhanceRobustnessByICP(dataModel->opNameMarkers.manParams,
      //                                           opNameMarkersCoR,
      //                                           calibMarkerData,
      //                                           ground2ParentTransform,
      //                                           optitrack_pos_calib,
      //                                           rescale_osMod,
      //                                           dataModel->opNameMarkers.q_calib,
      //                                           dataModel->opNameMarkers.corresNameID,
      //                                           currentMarkersLocation,
      //                                           all_scaling_factors,
      //                                           optitrack_pos_calib_time,
      //                                           globalPublish
      //                                           );


      rescale_osMod.clone3(dataModel->osMod,true,false);

      dataModel->osMod.setMarkersPositions(currentMarkersLocation);
    }


  }





 
    // working

    // globalPublish.publishJointState(optitrack_pos_calib_time, dataModel->osMod.D_joints_values);
    // globalPublish.markerPublisher(optitrack_pos_calib_time, optitrack_pos_calib, globalPublish.optitrack_publisher);
    // globalPublish.publishOpensim(optitrack_pos_calib_time, dataModel->osMod.D_mark_pos);

    // std::cout << "debug purpose : initial situation 2" << std::endl;
    // std::cin >> bla13;
    ROS_INFO("Initialization complete for Opensim-TRC side.");



  }
/**
 * @brief Execute the publishing operations for an Optitrack message,
 * when the calibration process was called.
 * This allows to display the joint configuration of the calibration pose chosen by the user, how
 * the markers should be placed according to this calibration pose, and the current
 * positions of the streamed Optitrack markers.
 * 
 */
void OpensimManagement::calibration_opensim_run(optitrack_full::DataModel * dataModel, markersManagement::globalPublisher globalPublish, bool & calibration_asked)
{
  // publish the different messages at the time of the optitrack frame
  ros::Time time_pub = dataModel->opNameMarkers.lastOptitrackTime;
  globalPublish.publishOpensimOptitrack(time_pub, dataModel->opNameMarkers.lastOptitrackMarkers, osMod_origin);

  ros::Time time_ros = ros::Time::now();
  // if the duration of the calibration pose is not reached, wait and
  // save the last satisfactory pose for calibraton

  if (time_ros.toSec() < (time_calib.toSec()+duration_bef_calib) ) 
  {
    if ( dataModel->opNameMarkers.lastOptitrackMarkers.size() == osMod_origin.D_mark_pos.size())
    {
      optitrack_pos_calib = dataModel->opNameMarkers.lastOptitrackMarkers;
      optitrack_pos_calib_time = time_pub;
      calibFrame = dataModel->frameNumber;
    }
    else{
      std::cout << dataModel->opNameMarkers.lastOptitrackMarkers.size() << " ; " << osMod_origin.D_mark_pos.size() << std::endl;
      // std::string s;
      // std::cin >> s;
    }
  }
  else
  {
    if (!simulation)
    {
      if (time_ros.toSec() < (time_calib.toSec()+duration_bef_calib+duration_for_CoR_estimate) ) 
      {
        if (calibMarkerData.size() == 0)
        {
          ROS_INFO("Calibration pose saved. Now, please execute the appropriates movements for the estimation of the center of rotation. \n");
          ROS_INFO("Reminder: under no circumstances should the subject perform any translational movements. The more extensive the movements, the better the quality of the calibration process.\n");
          ROS_INFO("Please also take care to move on all degrees of freedom all the articulations of the subject.\n");
          ROS_INFO("The calibration process will start in : %f seconds.\n", duration_for_CoR_estimate);
          calibMarkerData.push_back(optitrack_pos_calib);
        }
        else
        {
          //if ( dataModel->opNameMarkers.lastOptitrackMarkers.size() == osMod_origin.D_mark_pos.size())
          //{
            if (CoRDecoupl.size() != 0)
            {
              std::string body_name;
              int i = -1;
              for (std::map<std::string,std::vector<int> >::iterator it = CoRDecoupl.begin(); it!= CoRDecoupl.end(); it++)
              {
                if (it->second[0] == dataModel->frameNumber)
                {
                  i = 0;
                  body_name = it->first;
                }
                else if (it->second[1] == dataModel->frameNumber)
                {
                  i = 1;
                  body_name = it->first;
                }
              
              }
              if (i != -1)
              {
                if (CoRDecouplReadapt.find(body_name) == CoRDecouplReadapt.end())
                {
                  std::vector<int> v(2,0);
                  CoRDecouplReadapt[body_name] = v;
                }
                CoRDecouplReadapt[body_name][i] = calibMarkerData.size();
              }
            }

            calibMarkerData.push_back(dataModel->opNameMarkers.lastOptitrackMarkers);
            std::map<std::string,Eigen::Matrix4f> frameTransform;
            Eigen::Matrix4f I; I.setIdentity();
            for (std::map<std::string, std::vector<std::string> >::iterator it= osMod_origin.D_body_markers.begin(); it!=osMod_origin.D_body_markers.end(); it++)
            {
              frameTransform[it->first] = I;
            }
            ground2ParentTransform[calibMarkerData.size()-1] = (frameTransform);

          //}   
        } 
      }

      // do calibration after the duration
      else
      {
        start_calib(dataModel,globalPublish);
        calibration_asked = false;
      }
    }
    else
    {
      ROS_INFO("Simulation mode. Data for the estimate of CoR are directly going to be simulated. \n");
      start_calib(dataModel,globalPublish);
      calibration_asked = false;
      dataModel->opNameMarkers.originTime = ros::Time::now();
    }

  }

};



/**
 * @brief Execute the classic publishing operations for an Optitrack message.
 * 
 */
void OpensimManagement::classic_opensim_run(optitrack_full::DataModel * dataModel, markersManagement::globalPublisher globalPublish, ros::Time time)
{
  // publish all necessary topics

  globalPublish.publishOptitrackOpensimAssociation(time,dataModel->opNameMarkers.corresNameID,  dataModel->opNameMarkers.lastOptitrackMarkers);

  if (dataModel->opNameMarkers.framesSinceIK == 0)
  {
    globalPublish.publishOpensim(time, dataModel->osMod.D_mark_pos,"", dataModel->opNameMarkers.lastOsMarkersNotAssigned);
    globalPublish.publishJointState(time, dataModel->osMod.D_joints_values);
    if (dataModel->opNameMarkers.simulation && (dataModel->opNameMarkers.corresNameID.size() > 0 && dataModel->opNameMarkers.foundCalibFrame == true) )
    {
      markersManagement::checkSimulationIKConfiguration(dataModel->osMod.D_joints_values, dataModel->opNameMarkers.simu_gest.hiddenJointsValues,dataModel->opNameMarkers.error_pub, dataModel->opNameMarkers.error_verbose);
    }
  }

  //   if (dataModel->opNameMarkers.corresNameID["LWJC"] == 2203)
  //   {
  //     dataModel->opNameMarkers.pause_mode = true;
  //     // std::cout << "BUG DETECTED" << std::endl;
  //     // std::string deb;
  //     // std::cin >> deb;
  //     // deb_tst = true;
  //   }  
  //   // if (dataModel->opNameMarkers.lastOsMarkersNotAssigned["LWJC"] == 1 && dataModel->opNameMarkers.lastOsMarkersNotAssigned["C7"] != 1 )
  //   // {
  //   //   std::cout << "BUG DETECTED - NOT ASSIGNED" << std::endl;
  //   //   //std::cout << dataModel->opNameMarkers.opensimMarkersPos["LWJC2"] << std::endl;
  //   //   std::string deb;
  //   //   std::cin >> deb;
  //   //   deb_tst = true;
  //   // }  
  //   // if (deb_tst)
  //   // {
  //   //   std::cout << "BUG DETECTED - CONTINUE" << std::endl;
  //   //   std::string deb;
  //   //   std::cin >> deb;
  //   // }

  // }


  // if the calibration just occured and the optitrack data are a record,
  // search for the return of the calibration frame to activate IK.
  if (dataModel->opNameMarkers.foundCalibFrame== false)
  {
    int ec = abs(calibFrame - dataModel->frameNumber);
    if (ec < frameRate*1.0/2)
    {
      dataModel->opNameMarkers.foundCalibFrame = true;
    }
  }

}


} // namespace
