#include "optitrack_full/trc_manager.h"

namespace file_management
{
    TRCManager::TRCManager()
    {
        
    }

    TRCManager::TRCManager(std::string filename, bool record_trc, bool brutal_filtering, std::vector<std::string> necessary_markers)
    {

        init(filename,record_trc,brutal_filtering,necessary_markers);

    }

    void TRCManager::init(std::string filename, bool record_trc, bool brutal_filtering, std::vector<std::string> necessary_markers)
    {
        firstCompleteLabelizedFrame = -1;     
        allFramesSeen = false;

        optitrackData.clear();
        tempTRCData.clear();
        trcCorresNameId.clear();
        recordedAttributedIndex.clear();
        lastRecordedIndex.clear();
        currentRecordedIndex.clear();
        necessaryMarkers.clear();        

        // start   
        numRepeat = 0;
        Nframe = -1;
        lastMaxID = 0;
        brutalFiltering = brutal_filtering;
        necessaryMarkers = necessary_markers;
        recordTRC = record_trc;
        filenamePath = filename;
        firstCompleteLabelizedFrame = -1;
        allFramesSeen = false;
        

    }

    bool TRCManager::readFileCaracs()
    {
        bool res = true;
        if (recordTRC)
        {
            try
            {
                std::vector<int> metrics;
                fosim::readTRCFile(filenamePath, tempTRCData, dataRate, numMarkers, metrics);
                std::cout << "Metrics evaluation of file "<< filenamePath << " : " << std::endl;
                
                double percentOfMissingMarkers = metrics[1]*1.0/(metrics[0]);
                percentOfMissingMarkers = percentOfMissingMarkers*100.0;

                double percentOfFramesWithAtLeastOneMarkerMissing = metrics[2]*1.0/tempTRCData.size();
                percentOfFramesWithAtLeastOneMarkerMissing= percentOfFramesWithAtLeastOneMarkerMissing*100;

                double percentOfFramesWithAtLeast3MarkerMissing = metrics[3]*1.0/tempTRCData.size();
                percentOfFramesWithAtLeast3MarkerMissing= percentOfFramesWithAtLeast3MarkerMissing*100;

                std::printf("/=/=/=  Percent of missing markers in file : %3.3f \n", percentOfMissingMarkers);

                std::printf("/=/=/=  Percent of frames with at least one missing marker in file : %3.3f \n", percentOfFramesWithAtLeastOneMarkerMissing);

                std::printf("/=/=/=  Percent of frames with at least 3 missing marker in file : %3.3f \n", percentOfFramesWithAtLeast3MarkerMissing);


                // for (int i = 0; i < metrics.size(); i++)
                // {
                //     std::cout << metrics[i] << std::endl;
                // }
                // std::cout << tempTRCData.size() << std::endl;
                                
            }
            catch(const std::exception& e)
            {
                std::cerr << e.what() << '\n';
                res = false;
            }

            ROS_INFO("Size of .trc file (name : %s) : %li", filenamePath.c_str(), tempTRCData.size());
        }
        return(res);
    }

    void TRCManager::run(bool mustBeLabelized)
    {
        double percentThreshold = 0.5;

        if (mustBeLabelized)
        {
            percentThreshold = 0.999;
        }
        // if after limFramesIfLabelCheck frames taken randomly in the entire file, we do not have found any
        // frame with at least one labelized marker, we can stop and say that the file is not labelized at all.
        int limFramesIfLabelCheck = 100;

        int iBeg = 0;

        if (recordTRC)
        {
            bool res = false;
            if (tempTRCData.size() == 0)
            {
                res = readFileCaracs();
            }
            else
            {
                res = true;
            }

            if (res)
            {

                std::map<double, std::map<std::string, Eigen::Vector3d> > TRCData = tempTRCData;
                std::string s = "";

                // first, do labelization check
                bool mayBeLabelized = false;
                int k = 0;
                std::random_device rd;  // a seed source for the random number engine
                std::mt19937 gen(rd());                     
                std::uniform_int_distribution<> distrib(0, TRCData.size()-1);     
                if (TRCData.size() < limFramesIfLabelCheck)
                {
                    limFramesIfLabelCheck = TRCData.size();
                }

                std::vector<int> alreadyTaken;
                std::map<double,std::map<std::string,Eigen::Vector3d> >::iterator itB = TRCData.begin();

                while (!mayBeLabelized && k < limFramesIfLabelCheck)
                {
                    int iF = distrib(gen);
                    if ( std::find(alreadyTaken.begin(), alreadyTaken.end(), iF) == alreadyTaken.end())
                    {
                        alreadyTaken.push_back(iF);
                        std::advance(itB,iF);
                        mayBeLabelized = isOneMarkerInFrame(itB->second);
                        k++;
                    }
                }

                bool begFrameFound = mayBeLabelized;
                if (begFrameFound)
                {
                    // check the first frames until we find a frame with all the markers
                    begFrameFound = isOneExtremeFrameComplete(TRCData, true, percentThreshold, iBeg);

                    if (begFrameFound)
                    {
                        std::cout << "First frame is : " << iBeg << std::endl;
                        firstCompleteLabelizedFrame = iBeg;              
                    }
                    else
                    {
                        s = "";
                        for (int i = 0; i < necessaryMarkers.size(); i++)
                        {
                            s+= necessaryMarkers[i] + ";";
                        }
                        ROS_INFO("No complete first frame found. Checked %i frames. We were looking for the following markers : %s", iBeg, s.c_str());                    
                    }
                }

                // check the last frames until we find a frame with all the markers                
                int iEnd = 0;
                bool endFrameFound = false;
                if (begFrameFound)
                {
                    endFrameFound = isOneExtremeFrameComplete(TRCData, false, percentThreshold, iEnd, (s.size() > 3));

                    if (endFrameFound)
                    {
                        std::cout << "Last frame is : " << TRCData.size()-(iEnd+1) << std::endl;
                    }
                    else
                    {
                        if (s.size() < 3)
                        {
                            s = "";
                            for (int i = 0; i < necessaryMarkers.size(); i++)
                            {
                                s+= necessaryMarkers[i] + ";";
                            }
                        }
                        ROS_INFO("No complete last frame found. Checked %i frames. We were looking for the following markers : %s", iEnd, s.c_str()); 
                    }                
                }

                // the number of markers remains the same throughout the file 
                bool tst1 = ( begFrameFound && endFrameFound   );

                // std::map<std::string,Eigen::Vector3d>::iterator it2 = it->second.begin();
                // std::string name = it2->first;
                // std::vector<std::string> vst = fosim::splitString(name," ");
                // the name of the first marker in the first frame is a pure string (no index).
                // (necessary in case where a .trc file is partially labelled)
                // bool tst2 = ( (vst.size() == 1) && (necessaryMarkers.size()==0) );
                // tst1 = (tst1 && tst2);
                std::cout << "tst 1 : " << tst1 << std::endl;
                // std::cout << "tst 2 : " << tst2 << std::endl;

                if (tst1)
                {
                    //// to read only complete frames. May not always be recommended.
                    //std::map<double,std::map<std::string,Eigen::Vector3d> > outTRCData;
                    // outTRCData.insert(itB,itE);
                    // TRCData.clear();
                    // TRCData = outTRCData;

                    readTRCAsLabeledOptitrackMarkers(TRCData,optitrackData, trcCorresNameId, necessaryMarkers,Nframe,iBeg);
                }
                else if (!mustBeLabelized)
                {
                    readTRCAsUnlabeledOptitrackMarkers(TRCData,optitrackData);
                }
                
            }

        }
        

    }

    bool TRCManager::isOneMarkerInFrame(std::map<std::string,Eigen::Vector3d> frame,
    bool verbose
    )
    {
        bool notAMarker = false;
        int l = 0;
        while (l < necessaryMarkers.size() && !notAMarker)
        {
            if (frame.find(necessaryMarkers[l]) != frame.end())
            {
                if (verbose)
                {
                    ROS_INFO("One marker found : %s", necessaryMarkers[l].c_str());
                }
                l = necessaryMarkers.size();
                notAMarker = true;
            }
            l++;
        }
        return(notAMarker);
    }

    bool TRCManager::isFrameComplete(std::map<std::string,Eigen::Vector3d> frame,
    bool startByBegin,
    bool verbose
    )
    {
        bool allMarkersHere = true;
        int l = 0;
        while (l < necessaryMarkers.size() && allMarkersHere)
        {
            if (frame.find(necessaryMarkers[l]) == frame.end())
            {
                if (verbose)
                {
                    if (startByBegin)
                    {
                        ROS_INFO("Missing marker (at beginning) : %s", necessaryMarkers[l].c_str());
                    }
                    else
                    {
                        ROS_INFO("Missing marker (at end) : %s", necessaryMarkers[l].c_str());
                    }
                }
                l = necessaryMarkers.size();
                allMarkersHere = false;
            }
            l++;
        }
        return(allMarkersHere);
    }

    bool TRCManager::isOneExtremeFrameComplete(std::map<double,std::map<std::string,Eigen::Vector3d> > TRCData,
    bool startByBegin,
    double percentThreshold,
    int & iFrame,
    bool verbose
    )
    {
        iFrame = 0;
        bool frameFound = false;
        std::map<double,std::map<std::string,Eigen::Vector3d> >::iterator itB = TRCData.begin();

        // std::cout << TRCData.size() << std::endl;

        while (iFrame < int(TRCData.size()*percentThreshold) && !frameFound)
        {
            bool allMarkersHere  =true;
            itB = TRCData.begin();
            if (startByBegin)
            {
                std::advance(itB,iFrame);   
            }
            else
            {
                std::advance(itB,TRCData.size()-(iFrame+1) );
            }

            frameFound = isFrameComplete(itB->second, startByBegin, verbose);
            if (!frameFound)
            {
                iFrame++;
            }
        }

        return(frameFound);
    }

    void TRCManager::readTRCAsUnlabeledOptitrackMarkers(std::map<double,std::map<std::string,Eigen::Vector3d> > TRCData,
    std::map<double,std::map<int,Eigen::Vector3d> > & optitrackData )
    {
        double timeThresMiss;
        double distThresMiss;
        int markerId = 100000;
        // Max time before the dissapearing and reappearing of a marker can still be checked
        // False positive detected at 4.0
        // (retried at 6.0)
        timeThresMiss = 10.0;
        // Max distance between a dissapearing and a reapearring marker to consider them as the same
        // false positive detected at 0.0025
        // (retried at 0.014 (diameter of a marker))
        distThresMiss = 0.014;

        // for full desactivation of recovery (for hard cases)
        //timeThresMiss = 0.0;
        //distThresMiss = 0.0;        

        // for full acceptance of all associations from Motive
        //timeThresMiss = 9999999;
        //distThresMiss = 9999999;       

        std::map<int, std::pair<double,Eigen::Vector3d> > idDisspProps;

        double lastTime = -1;

        int curFNum = 0;

        int curNumMark = 0;

        for (std::map<double,std::map<std::string,Eigen::Vector3d> >::iterator it = TRCData.begin(); it!=TRCData.end(); it++)
        {
            bool emptyFrameDetected = true;
            std::map<std::string,Eigen::Vector3d> frame = it->second;
            std::map<int,Eigen::Vector3d> frameOpti;

            // if (frame.size() == 0)
            // {
            //     emptyFrameDetected = true;
            // }




            for (std::map<std::string,Eigen::Vector3d>::iterator it2 = frame.begin(); it2!= frame.end(); it2++)
            {
                emptyFrameDetected = false;
                std::string name = it2->first;
                std::vector<std::string> vst = fosim::splitString(name," ");
                int id;
                //std::cout << "size of vst : " << vst.size() << std::endl;
                if (vst.size() == 1)
                {
                    std::vector< std::string > nums = {"1","2","3","4","5","6","7","8","9","0"};
                    try
                    {
                      int i = name.size()-1;
                      bool endNums = false;
                      std::string sNums = "";
                      while (i >=0 && !endNums)
                      {
                        std::string c(1, name[i]);
                        if (std::find(nums.begin(), nums.end(), c) != nums.end())
                        {
                            sNums = name[i] + sNums;
                        }
                        else
                        {
                            endNums = true;
                        }
                        i--;
                      }
                      id = std::stoi(sNums);
                    }
                    catch(const std::exception& e)
                    {
                        //std::cout << "bug : change markerID " << name << std::endl;
                        id = markerId;
                        markerId++;
                    }
                }
                else
                {
                    try
                    {
                      id = std::stoi(vst[vst.size() -1]);
                    }
                    catch(const std::exception& e)
                    {
                        //std::cout << "bug : change markerID " << name << std::endl;
                        id = markerId;
                        markerId++;
                    }
                    

                }
                int attributedID;
                if (brutalFiltering)
                {

                    // first time the marker appears : gives it its first attributed id
                    if ( recordedAttributedIndex.find(id) == recordedAttributedIndex.end() )
                    {   
                        //std::cout << "first detection of id " << id << " (name : " << name << ")" << std::endl;
                        attributedID = lastMaxID;
                        lastMaxID++;
                        recordedAttributedIndex[id] = attributedID;
                        curNumMark++;
                    }
                    // marker has dissapear and then reappear
                    // issue : we cannot trust Optitrack; maybe the association to the
                    // new marker is wrong
                    else if (std::find(lastRecordedIndex.begin(), lastRecordedIndex.end(), id) == lastRecordedIndex.end())
                    {
                        // check distance between lastRecorded position of id and current. If distance is enoughly small,
                        // we authorize the association. Otherwise, we refuse it.
                        std::map<int, std::pair<double,Eigen::Vector3d> >::iterator itID = idDisspProps.find(id);
                        if ( itID != idDisspProps.end() && (itID->second.second  - it2->second).norm() < distThresMiss )
                        {
                            //std::cout << "========== Even if missing, id " << recordedAttributedIndex[id] << " was recovered at time " << it->first;
                            //std::cout << " (last recorded : " << itID->second.first << " ) ===========" << std::endl;

                            attributedID = recordedAttributedIndex[id];
                        }
                        else
                        {
                            // if (itID != idDisspProps.end() )
                            // {
                            //     std::cout << "=== Distance between last occurence of missing point (id : " << recordedAttributedIndex[id]  << " ) : " << (itID->second.second  - it2->second).norm()<< std::endl;
                            //     std::cout << "Time passed : " << it->first - itID->second.first << " ; pos : " << itID->second.second;
                            //     std::cout << std::endl;
                            // }
                            attributedID = lastMaxID;
                            lastMaxID++;
                            recordedAttributedIndex[id] = attributedID;  
                            curNumMark++;
                        }
                    }
                    else
                    {
                        attributedID = recordedAttributedIndex[id];
                    }
                    // note in this object the ID of the marker
                    // in the file
                    currentRecordedIndex.push_back(id);
                }
                
                else
                {
                    attributedID = id;
                }


                frameOpti[attributedID] = it2->second;
            }
            if (brutalFiltering)
            {                
                // check which ID went missing; note its last recorded frame and its position
                if (lastTime != -1)
                {
                    std::vector<int> missingIDs;
                    for (int l = 0; l < lastRecordedIndex.size(); l++)
                    {
                        // note elements that were in lastRecorded Frame but not in currentFrame
                        if (std::find(currentRecordedIndex.begin(), currentRecordedIndex.end(), lastRecordedIndex[l]) == currentRecordedIndex.end())
                        {
                            missingIDs.push_back( lastRecordedIndex[l] );
                        }
                    }
                    if (missingIDs.size() > 0)
                    {
                        std::map<int,Eigen::Vector3d> lastFrame = optitrackData[lastTime];
                        // for (std::map<int,Eigen::Vector3d>::iterator itDebug = lastFrame.begin(); itDebug!=lastFrame.end(); itDebug++)
                        // {
                        //     std::cout << itDebug->first << " ; " << itDebug->second << std::endl;
                        // }

                        for (int l = 0; l < missingIDs.size(); l++)
                        {
                            // if (lastFrame.find( recordedAttributedIndex[ missingIDs[l]] ) == lastFrame.end())
                            // {
                            //     std::cout << "BUG FOR INDEX " <<recordedAttributedIndex[ missingIDs[l]]<< std::endl;
                            // }
                            std::pair<double,Eigen::Vector3d> pr;
                            pr.first = lastTime;                            
                            pr.second = lastFrame[recordedAttributedIndex[ missingIDs[l]]];

                            //std::cout << lastFrame[missingIDs[l]] << std::endl;
                            idDisspProps[missingIDs[l]] = pr;
                        }
                    }
                }

                if (!emptyFrameDetected)
                {
                    lastRecordedIndex = currentRecordedIndex;
                    currentRecordedIndex.clear();
                }

                // flush process for missingIDs : if superior to a certain time,
                // we consider that the association cannot be correct anymore,
                // thus we suppress it to save some space
                int z = 0;
                std::map<int, std::pair<double,Eigen::Vector3d> >::iterator it2 = idDisspProps.begin();
                while (z < idDisspProps.size())
                {
                    it2 =  idDisspProps.begin();
                    std::advance(it2,z);
                    if ( (it->first - it2->second.first) > timeThresMiss)
                    {
                        idDisspProps.erase(it2);
                    }
                    else
                    {
                        z++;
                    }
                }

            }
            optitrackData[it->first] = frameOpti;

            lastTime = it->first;

            if ( (TRCData.size() - curFNum)%1000 == 0 )
            {
                std::cout << "Frames until reading end : " << TRCData.size() - curFNum;
                std::cout << "; number of markers so far : " << curNumMark << std::endl;
            }
            curFNum +=1;

        }
        // std::string s;
        // std::cin >> s;
    }

    void TRCManager::readTRCAsLabeledOptitrackMarkers(std::map<double,std::map<std::string,Eigen::Vector3d> > TRCData,
    std::map<double,std::map<int,Eigen::Vector3d> > & optitrackData,
    std::map<std::string,int> & corresNameId,
    std::vector<std::string> necessaryMarkers,
    int & Nframe, 
    int iBeg )
    {

        ROS_INFO("READ AS LABELIZED TRC FILE");
        // check si ID change ac frame vide

        allFramesSeen = true;
        bool attributed = false;
        int i = 0;
        std::map<double,std::map<std::string,Eigen::Vector3d> >::iterator it = TRCData.begin();
        std::advance(it,iBeg);
        for (std::map<std::string,Eigen::Vector3d>::iterator itmark = it->second.begin(); itmark!=it->second.end(); itmark++)
        {

            if (necessaryMarkers.size() == 0)
            {
                corresNameId[itmark->first] = i;
                i++;
            }
            else if (std::find(necessaryMarkers.begin(), necessaryMarkers.end(), itmark->first) != necessaryMarkers.end())
            {
                corresNameId[itmark->first] = i;
                i++;
            }

        }

        //std::cout << corresNameId.size() << std::endl;

        for (std::map<double,std::map<std::string,Eigen::Vector3d> >::iterator it = TRCData.begin(); it!=TRCData.end(); it++)
        {
            bool emptyFrameDetected = true;
            std::map<std::string,Eigen::Vector3d> frame = it->second;
            std::map<int,Eigen::Vector3d> frameOpti;

            // if (necessaryMarkers.size() > frame.size())
            // {
            //     ROS_WARN("Missing markers in labeled file at time %3.4f : only %ld markers instead of %ld markers", it->first, frame.size(), necessaryMarkers.size());
            // }


            for (std::map<std::string,Eigen::Vector3d>::iterator it2 = frame.begin(); it2!= frame.end(); it2++)
            {
                emptyFrameDetected = false;
                std::string name = it2->first;
                if (corresNameId.find(name) != corresNameId.end())
                {
                    int id = corresNameId[name];
                    frameOpti[id] = it2->second;
                }
                else
                {
                    ROS_INFO("Frame at time %3.4f : unknown name : %s", it->first, name.c_str());
                }

            }
            // if (!emptyFrameDetected)
            // {

            // }
            optitrackData[it->first] = frameOpti;
            // if (frameOpti.size() != corresNameId.size() )
            // {
            //     std::cout << "Frame at " << it->first << " eliminated. Size : " << frameOpti.size()  << std::endl;
            // }
            // else
            // {
            //     optitrackData[it->first] = frameOpti;
            // }

        }
    }

    // project to check if first frame really have all markers. But should be managed.
    void TRCManager::writeLabeledTRCFile(std::map<int, std::map<int,LabelCarac> > allCorresIDName, int calibFrame, int typeSave, std::string & trcPath,
          std::vector< std::vector<Eigen::Vector3d> > forbidden_areas)
    {
        int maxMark = 0;
        std::vector<std::string> markNames;    
        int frameNumber =0;

        // rather check with all

         for (std::map<double,std::map<int,Eigen::Vector3d> >::iterator it = optitrackData.begin();it!=optitrackData.end(); it++)
         {

            if (it->second.size() > maxMark)
            {

                for (std::map<int,Eigen::Vector3d>::iterator it2 = it->second.begin();it2!=it->second.end(); it2++)
                {
                    std::string name;
                    int framePicked;
                    bool found = markersManagement::checkACINCorres(it2->first, frameNumber, allCorresIDName, name, framePicked, true);
                    if (found)
                    {
                        if ( std::find(markNames.begin(), markNames.end(), name ) == markNames.end() )
                        {
                            markNames.push_back(name);
                        }
                    }
    
                }
                maxMark = markNames.size();

            }
            frameNumber+=1;
        }


        writeLabeledTRCFile(allCorresIDName,calibFrame, typeSave,trcPath,markNames, forbidden_areas);
    }

    /**
     * @brief Thanks to the labelisation process, an unlabeled .trc file can be saved as a labeled .trc file.
     * 
     * @param allCorresIDName 
     * @param calibFrame 
     * @param trcPath : Absolute path to the .trc file that should be created. Do not put a ".trc" at the end.
     */
    void TRCManager::writeLabeledTRCFile(std::map<int, std::map<int,LabelCarac> > allCorresIDName, 
    int calibFrame, int typeSave, std::string & trcPath, std::vector<std::string> markNames, 
      std::vector< std::vector<Eigen::Vector3d> > forbidden_areas)
    {
        // metrics
        // one missing marker = a opensim marker is not labelized in frame
        // while there is an unlabelized optitrack marker in frame
        int numOfFramesWithOneMissingMarker = 0;
        int numOfFramesWithOneFLMarker = 0;        
        int numOfFramesWith3MissingMarker = 0;    
        int totalNumOfMissingMarkers = 0;

        int totalNumOfAbsoluteMissingMarkers = 0;        
        int totalNumOfMarkers = 0;

        int frameNumber = 0;


        char c = trcPath.back();
        std::string s(1,c);

        if (s != "/")
        {
            trcPath += "/";
        }

        std::string oriTRCPath = trcPath;

        if (filenamePath.size() > 0)
        {
            std::vector<std::string> v1 = fosim::splitString(filenamePath,"/");
            if (v1.size() > 0)
            {
                std::vector<std::string> v2 = fosim::splitString(v1[v1.size()-1],".trc");
                //std::cout << v2.size() << std::endl;
                if (v2.size() > 0)
                {
                    trcPath+= v2[0];
                }

            }
        }


        Eigen::Vector3d blank; 

        std::vector<Eigen::Vector3d> typeBlank;
        std::vector<std::string> allTRC;

        blank.setOnes(); 

        if (typeSave == 1)
        {
            // in order to set blank as -NaN
            typeBlank.push_back( blank*sqrt(-1));
            allTRC.push_back( trcPath+"_calib_frame_" + std::to_string(calibFrame)+ "_ts_" + std::to_string(typeSave) + ".trc" );
        }
        else if (typeSave == 0)
        {
            // in order to set blank as 0            
            typeBlank.push_back( blank*0.0);
            allTRC.push_back( trcPath+"_calib_frame_" + std::to_string(calibFrame)+ "_ts_" + std::to_string(typeSave) + ".trc" );
        }
        else if (typeSave == 2)
        {
            // set both blank values in 2 different files
            typeBlank.push_back( blank*0.0);     
            typeBlank.push_back( blank*sqrt(-1));                 
            allTRC.push_back( trcPath+"_calib_frame_" + std::to_string(calibFrame)+ "_ts_" + std::to_string(0) + ".trc" );                    
            allTRC.push_back( trcPath+"_calib_frame_" + std::to_string(calibFrame)+ "_ts_" + std::to_string(1) + ".trc" );              


        }
        else if (typeSave == 3)
        {
            // set both blank values in 2 different files
            typeBlank.push_back( blank*0.0);     
            typeBlank.push_back( blank*sqrt(-1));     
            typeBlank.push_back( blank*sqrt(-1));                 
            allTRC.push_back( trcPath+"_calib_frame_" + std::to_string(calibFrame)+ "_ts_" + std::to_string(0) + ".trc" );                    
            allTRC.push_back( trcPath+"_calib_frame_" + std::to_string(calibFrame)+ "_ts_" + std::to_string(1) + ".trc" );              
            allTRC.push_back( oriTRCPath+ "markers.trc" );              


        }

        std::vector<std::map<double,std::map<std::string,Eigen::Vector3d> > > allLabeledData;

        for (int i = 0;  i < typeBlank.size(); i++)
        {
            std::map<double,std::map<std::string,Eigen::Vector3d> > labeledData;
            allLabeledData.push_back(labeledData);
        }

        //std::cout << "BLANK : " << blank << std::endl;
        bool verbose = false;

        std::map<int,int> idLastFrame;
        
        //totalNumOfMarkers= markNames.size()*optitrackData.size();

        for (std::map<double,std::map<int,Eigen::Vector3d> >::iterator it = optitrackData.begin();it!=optitrackData.end(); it++)
        {

            std::vector< std::map<std::string,Eigen::Vector3d>  >  allLabeledFrame;

            for (int i = 0; i < typeBlank.size(); i++)
            {
                std::map<std::string,Eigen::Vector3d> lF;
                allLabeledFrame.push_back(lF);
            }

            // if (frameNumber > 100 && frameNumber < 160)
            // {
            //     verbose = true;
            // }
            // else
            // {
            //     verbose = false;
            // }
            int numMissMarkInFrame = 0;

            std::map<int,Eigen::Vector3d> controlledFrame = it->second;
            markersManagement::controlForbiddenAreas(controlledFrame,forbidden_areas);
            totalNumOfMarkers+= controlledFrame.size();
            bool couldBeOk = controlledFrame.size() >= markNames.size();
            for (std::map<int,Eigen::Vector3d>::iterator it2 = controlledFrame.begin();it2!=controlledFrame.end(); it2++)
            {


                
                std::string name;
                int framePicked;
                bool found;
                bool alreadyFound = false;
                std::map<int,int>::iterator itLF =  idLastFrame.find(it2->first);
                if (itLF != idLastFrame.end())
                {
                    if (itLF->second != -1)
                    {
                        found = (itLF->second <= frameNumber);
  
                    }
                    else
                    {
                        found = true;
                    }                   
                    if (found)
                    {
                        name = allCorresIDName[it2->first][itLF->second].osim_name;
                        if (name.size()> 0)
                        {
                            found = false;
                        }
                    }
                    alreadyFound = found;                     
                }
                if (!alreadyFound)
                {
                    // try with checkACIN    
                    found = markersManagement::checkACINCorres(it2->first, frameNumber, allCorresIDName, name, framePicked, true, verbose);
                }
                
                if (found)
                {
                    for (int l = 0; l < typeBlank.size(); l++)
                    {
                        blank = typeBlank[l];
                        // if found BUT with absurd value, considers it as
                        // not found
                        if ( it2->second.norm()  < 1e-16 )
                        {
                            allLabeledFrame[l][name] = blank;
                            if (l==0)
                            {
                                numMissMarkInFrame++;
                            }
                        }
                        else
                        {
                            allLabeledFrame[l][name] = it2->second;
                            if (!alreadyFound)
                            {
                                // saves framePicked (= the identifiant of the optitrack index in allCorresIDName)
                                idLastFrame[it2->first] = framePicked;
                            }
                        }
                    }
                }
                // prints never associated optitrack markers for a good reason; so, useless.
                // else
                // {
                //     std::cout << "Frame " << frameNumber << " : marker optitrack " << it2->first<< " not associated." << std::endl;
                //     markersManagement::checkACINCorres(it2->first, frameNumber, allCorresIDName, name, framePicked, true, true);

                // }
            }
            
            for (int l = 0; l < typeBlank.size(); l++)
            {
                blank = typeBlank[l];
                if ( allLabeledFrame[l].size() < markNames.size() )
                {
                    for (int i = 0; i < markNames.size(); i++)
                    { 
                        if (allLabeledFrame[l].find(markNames[i]) == allLabeledFrame[l].end())
                        {
                            allLabeledFrame[l][markNames[i]] = blank;
                            if (l== 0)
                            {
                                if (couldBeOk)
                                {
                                    std::cout << "Frame " << frameNumber << " : marker opensim " << markNames[i]<< " not associated." << std::endl;
                                    // if (markNames[i] == "HL2" || markNames[i] == "HL4")
                                    // {
                                    //     int framePicked;
                                    //     std::string name;
                                    //     std::vector<int> v_check = {105,107,112,115,6};
                                    //     for (int v =0; v < v_check.size(); v++)
                                    //     {
                                    //          if (controlledFrame.find(v_check[v]) != controlledFrame.end())
                                    //          {
                                    //              markersManagement::checkACINCorres(v_check[v], frameNumber, allCorresIDName, name, framePicked, true, true);

                                    //          }

                                    //     }
                                    //     // std::string ss;
                                    //     //     std::cin >> ss;

                                    // }
                                }
                                numMissMarkInFrame++;
                            }
                        }
                    }
                }
            }
            frameNumber+=1;
            if (frameNumber%100 == 0)
            {
                std::cout << "current frame : " << frameNumber << "/" << optitrackData.size() << std::endl;
            }
            // if there is enough optitrack markers in the frame for labelization

            if (numMissMarkInFrame >= 1 )
            {
                if (couldBeOk)
                {
                    numOfFramesWithOneFLMarker++;
                }
                numOfFramesWithOneMissingMarker+=1;
            }
            if (numMissMarkInFrame >= 3)
            {
                numOfFramesWith3MissingMarker +=1;
            }       
            totalNumOfMissingMarkers+= numMissMarkInFrame;     

            if (!couldBeOk)
            {
                totalNumOfAbsoluteMissingMarkers+= markNames.size()- controlledFrame.size();
            }

            for (int l = 0; l < typeBlank.size(); l++)
            {
                allLabeledData[l][it->first] = allLabeledFrame[l];
            }
        }

        for (int l = 0; l < allTRC.size(); l++)
        {
            trcPath = allTRC[l];
            std::map<double,std::map<std::string,Eigen::Vector3d> > labeledData = allLabeledData[l];
            std::cout << "Labelisation completed. File will be save at " << trcPath << "." << std::endl;
            fosim::writeTRCFile(trcPath,labeledData,dataRate);            
        }



        std::cout << "Metrics evaluation of written file : " << std::endl;
        
        double totalPercentOfMissingMarkers = totalNumOfMissingMarkers*1.0/(markNames.size()*optitrackData.size());
        totalPercentOfMissingMarkers = totalPercentOfMissingMarkers*100.0;

        double percentOfMissingMarkers = totalNumOfAbsoluteMissingMarkers*1.0/(totalNumOfMarkers);
        percentOfMissingMarkers = percentOfMissingMarkers*100.0;

        double percentOfFramesWithAtLeastOneMarkerMissing = numOfFramesWithOneMissingMarker*1.0/optitrackData.size();
        percentOfFramesWithAtLeastOneMarkerMissing= percentOfFramesWithAtLeastOneMarkerMissing*100;

        double percentOfFramesWithAtLeastOneFL = numOfFramesWithOneFLMarker*1.0/optitrackData.size();
        percentOfFramesWithAtLeastOneFL= percentOfFramesWithAtLeastOneFL*100;


        double percentOfFramesWithAtLeast3MarkerMissing = numOfFramesWith3MissingMarker*1.0/optitrackData.size();
        percentOfFramesWithAtLeast3MarkerMissing= percentOfFramesWithAtLeast3MarkerMissing*100;

        std::printf("/=/=/=  Percent of opensim missing markers (absent AND unlabelized) in file : %3.3f \n", totalPercentOfMissingMarkers);

        std::printf("/=/=/=  Percent of opensim missing markers (absent only) in file : %3.3f \n", percentOfMissingMarkers);

        std::printf("/=/=/=  Percent of frames with at least one failed labelization of an opensim marker in file : %3.3f \n", percentOfFramesWithAtLeastOneFL);
        std::printf("/=/=/=  Percent of frames with at least one missing opensim marker in file : %3.3f \n", percentOfFramesWithAtLeastOneMarkerMissing);

        std::printf("/=/=/=  Percent of frames with at least 3 failed labelization OR missing opensim markers in file : %3.3f \n", percentOfFramesWithAtLeast3MarkerMissing);

        // std::string a;
        // std::cin >> a;



    }

    // void TRCManager::writeLabeledTRCFile(std::map<int, std::map<int,std::string> > allCorresIDName, int calibFrame, std::string & trcPath)
    // {
    //     std::map<double,std::map<std::string,Eigen::Vector3d> > labeledData;

    //     Eigen::Vector3d blank; blank.setZero();

    //     for (std::map<double,std::map<int,Eigen::Vector3d> >::iterator it = optitrackData.begin();it!=optitrackData.end(); it++)
    //     {
    //         std::map<std::string,Eigen::Vector3d> labeledFrame;
    //         for (std::map<int,Eigen::Vector3d>::iterator it2 = it->second.begin();it2!=it->second.end(); it2++)
    //         {
    //             std::map<int,std::string>::iterator corres = allCorresIDName.find(it2->first);
    //             if ( corres!= allCorresIDName.end())
    //             {
    //                 labeledFrame[corres->second] = it2->second;
    //             }
    //         }
    //         labeledData[it->first] = labeledFrame;
    //     }

    //     char c = trcPath.back();
    //     std::string s(1,c);

    //     if (s != "/")
    //     {
    //         trcPath += "/";
    //     }

    //     if (filenamePath.size() > 0)
    //     {
    //         std::vector<std::string> v1 = fosim::splitString(filenamePath,"/");
    //         if (v1.size() > 0)
    //         {
    //             std::vector<std::string> v2 = fosim::splitString(v1[v1.size()-1],".trc");
    //             //std::cout << v2.size() << std::endl;
    //             if (v2.size() > 0)
    //             {
    //                 trcPath+= v2[0];
    //             }

    //         }
    //     }



    //     trcPath+="_calib_frame_" + std::to_string(calibFrame)+".trc";
    //     std::cout << "Labelisation completed. File will be save at " << trcPath << "." << std::endl;

    //     fosim::writeTRCFile(trcPath,labeledData,dataRate);



    // }


    // TODO :
    // - .motive parser
    // - according to the distance between the different markers, associate each of them.
    //     * 1e round : pour chaque marqueur non assigné : 
    //          * Considérer qu'il appartient à un rigid body
    //          * Connaissant la taille de ce rigid body, prendre tous les marqueurs proches du marqueur considéré
    //          * En utilisant la position théorique du rigid body et les marqueurs proches du marqueur choisi, reprendre procédure pour bodyMarkerRecoveryByICP

    // void TRCManager::multiRigidBodyCorrection()
    // {

    // }


}


