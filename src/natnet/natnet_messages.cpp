/* 
 * Copyright (c) 2018, Houston Mechatronics Inc., JD Yamokoski
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its 
 *    contributors may be used to endorse or promote products derived from 
 *    this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "optitrack_full/natnet/natnet_messages.h"

#include <cstring>
#include <cinttypes>

#include <ros/console.h>

#include "natnet_packet_definition.h"

namespace natnet
{

namespace utilities
{
  void seek(MessageBuffer::const_iterator& iter, size_t offset)
  {
    iter += offset;
  }

  template <typename T> 
  void read_and_seek(MessageBuffer::const_iterator& iter, T& target)
  {
    // Breaking up the steps for clarity.
    //    *iter <- points to current value (const char) in message buffer
    //    &(*iter) <- return address of this const char (char const*)
    //    (T const*) pData <- casts char const* into T const*
    //    *((T const*) pData) <- dereference typed pointer and copy it into target
    char const* pData = &(*iter);
    target = *((T const*) pData);
    // ROS_INFO("\t sizeof(%s) = %d", TypeParseTraits<T>::name, (int)sizeof(T));
    seek(iter, sizeof(T));
  }

  void decode_marker_id(int sourceID, int& pOutEntityID, int& pOutMemberID)
  {
      if (pOutEntityID)
          pOutEntityID = sourceID >> 16;

      if (pOutMemberID)
          pOutMemberID = sourceID & 0x0000ffff;
  }

  // Funtion that assigns a time code values to 5 variables passed as arguments
  // Requires an integer from the packet as the timecode and timecodeSubframe
  void decode_timecode(unsigned int inTimecode, unsigned int inTimecodeSubframe, int& hour, int& minute, int& second, int& frame, int& subframe)
  {
    hour = (inTimecode>>24)&255;
    minute = (inTimecode>>16)&255;
    second = (inTimecode>>8)&255;
    frame = inTimecode&255;
    subframe = inTimecodeSubframe;
  }

  // Takes timecode and assigns it to a string
  void stringify_timecode(unsigned int inTimecode, unsigned int inTimecodeSubframe, char *Buffer, int BufferSize)
  {
    bool bValid;
    int hour, minute, second, frame, subframe;
    decode_timecode(inTimecode, inTimecodeSubframe, hour, minute, second, frame, subframe);

    snprintf(Buffer,BufferSize,"%2d:%2d:%2d:%2d.%d",hour, minute, second, frame, subframe);
    for(unsigned int i=0; i < strlen(Buffer); i++)
    {
      if(Buffer[i]==' ')
      {
        Buffer[i]='0';
      }
    }
  }
  
  optitrack_full::OptitrackMessage blockAndSelectMessage(std::map<int,optitrack_full::OptitrackMessage> messageQueue, optitrack_full::DataModel* dataModel, int frameNumber)
  {
    optitrack_full::OptitrackMessage message = messageQueue[frameNumber];
    return(message);
  }



  void selectOptitrackMessage(std::map<int,optitrack_full::OptitrackMessage> messageQueue, optitrack_full::DataModel* dataModel)
  {

    bool verbose = dataModel->opNameMarkers.selection_verbose;
    // block from here messageQueue, too much issue otherwise.

    // 
    // init case
    if (dataModel->frameOrigin == -1 && messageQueue.size() > 0)
    {
      // select first message
      std::map<int,optitrack_full::OptitrackMessage>::iterator it = messageQueue.begin();
      optitrack_full::OptitrackMessage message = it->second;
      dataModel->frameOrigin = it->first;

    }

    //else

    // 1e crit : select si suit direct

    // si plein : select num inferieur le plus proche de dernier


    // message selected, we can now relax messageQueue


    if (dataModel->frameOrigin == -1)
    {
      dataModel->frameOrigin = dataModel->frameNumber;
    }
    else if ( (dataModel->opNameMarkers.repeat) && dataModel->frameNumber <= std::max(dataModel->lastFrame*0.1,dataModel->frameOrigin*1.0) )
    {
      dataModel->frameOrigin = dataModel->frameNumber;
      dataModel->lastFrame = dataModel->frameNumber;
      dataModel->numFramesCons = 0;
    }

    else if ( (!dataModel->opNameMarkers.repeat) && dataModel->frameNumber <= std::max(dataModel->lastFrame*0.1,dataModel->frameOrigin*1.0) )
    {
      dataModel->frameOrigin = dataModel->frameNumber;
      dataModel->lastFrame = dataModel->frameNumber;
      if (dataModel->numFramesCons > 0)
      {
        dataModel->lastNumFramesCons = dataModel->numFramesCons;
        dataModel->numFramesCons = -1;
      }
      else
      {
        dataModel->numFramesCons = dataModel->lastNumFramesCons;
      }
    }  


    if (verbose)
    {
      ROS_INFO("=== BEGIN DATA FRAME ===");
      ROS_INFO("Frame number: %d", dataModel->frameNumber);
      ROS_INFO("Frame origin: %d", dataModel->frameOrigin);
      ROS_INFO("Last Frame : %d", dataModel->lastFrame);
    }


  } 

/**
 * @brief 
 * 
 * This method supposes that D_body_markers contains only the bodies of the markers repetoried 
 * in osMarkersNotAssigned.
 * 
 * @param osMarkersNotAssigned 
 * @param D_body_markers 
 * @param numOsNotAssigned 
 * @param enoughOsAssigned : if body part has enough markers for IK
 * @param numOsByBodyPart number of Opensim markers assigned by body part.
 * @param forIKOnly : evaluate whether we should check bodyParts for IK only (= true, so must check for 2 markers only),
 * or if we want to evaluate if we should try to check for activating a recovery (= false, so must check for all markers)
 * @return true 
 * @return false 
 */
  bool checkIKPossible(std::map<std::string, int> osMarkersNotAssigned,
    std::map<std::string, std::vector<std::string> > D_body_markers,
    int & numOsNotAssigned,
    std::map<std::string,bool> & enoughOsAssigned,
    std::map<std::string,int> & numOsByBodyPart,
    bool forIKOnly )
  {


    int map_index = 0;
    std::map<std::string, std::vector<std::string> >::iterator itmap;
    bool IK_possible = true;
    while (map_index < D_body_markers.size() )
    {
      itmap = D_body_markers.begin();
      std::advance(itmap, map_index);
      std::vector<std::string> body_markers = itmap->second;
      int sum_assigned = (int) body_markers.size();
      for (int i = 0; i < body_markers.size(); i++)
      {
        std::string marker_name = body_markers[i];
        sum_assigned -= osMarkersNotAssigned[marker_name];
      }

      numOsByBodyPart[itmap->first] = sum_assigned;
      int minMark;
      if (forIKOnly)
      {      
        // at least 2 markers by body part to authorize IK
        minMark = 2;
      }
      else
      {
        // for more complete check
        minMark = 3;
      }



      if (body_markers.size() < minMark)
      {
        minMark = body_markers.size();
      }

      if (sum_assigned <  minMark )
      {
        IK_possible = false;
        enoughOsAssigned[itmap->first] = false;
      }
      else
      {
        enoughOsAssigned[itmap->first] = true;
      }

      numOsNotAssigned += (body_markers.size()- sum_assigned);

      map_index++;

    }

    return(IK_possible);

  }


  void manageTime(optitrack_full::DataModel * dataModel, double timeFrame)
  {
    if (dataModel->numFramesCons == 0)
    {
      
      if (dataModel->opNameMarkers.repeat)
      {
        dataModel->opNameMarkers.originTime.fromSec( dataModel->opNameMarkers.messageReceptionTime.toSec() - timeFrame );
      }
      else
      {

        dataModel->opNameMarkers.originTime.fromSec( dataModel->opNameMarkers.messageReceptionTime.toSec() - timeFrame );

      }

    }
    else if (dataModel->numFramesCons> 0)
    {

      if (dataModel->opNameMarkers.repeat)
      {
        double current_origin = dataModel->opNameMarkers.originTime.toSec();
        double mod_origin = (current_origin*(dataModel->numFramesCons) + ( dataModel->opNameMarkers.messageReceptionTime.toSec() - timeFrame)  )*1.0/(dataModel->numFramesCons+1) ;
        dataModel->opNameMarkers.originTime.fromSec( mod_origin );        
      }
      else
      {
        double current_origin = dataModel->opNameMarkers.originTime.toSec();
        double mod_origin = (current_origin*(dataModel->numFramesCons) + ( dataModel->opNameMarkers.messageReceptionTime.toSec() - timeFrame)  )*1.0/(dataModel->numFramesCons+1) ;
        dataModel->opNameMarkers.originTime.fromSec( mod_origin );
      }


    }

    // set lastOptitracTime
    if (dataModel->opNameMarkers.repeat)
    {
      dataModel->opNameMarkers.lastOptitrackTime.fromSec(timeFrame +  dataModel->opNameMarkers.originTime.toSec() );

    }

    else
    {
      dataModel->opNameMarkers.lastOptitrackTime.fromSec(timeFrame+  dataModel->opNameMarkers.originTime.toSec() );

    }
  }

  
/**
 * @brief Try to associate the Optitrack markers defined inside opNameMarkers with the sets of markers defined
 * inside allMarkerSets.
 * 
 * The Opensim markers that should be followed is fixed by osMarkersNotAssigned variable.
 * 
 * This function must be used BEFORE initialize_new_markerSets (with the risk that for this frame, a recovery step might
 * fail because the calibration frame is not already defined)
 * 
 * @param opNameMarkers 
 * @param osMod 
 * @param corresNameID 
 * @param osMarkersNotAssigned 
 * @param called 
 */
  void markersAssociation(  optitrack_full::OptitrackNameMarkers & opNameMarkers,
  std::vector<optitrack_full::MarkerSet> & allMarkerSets)
  {
    
    bool called = false;
    bool verbose = opNameMarkers.association_verbose;
    double mdc = 16*sqrt(3); //4*sqrt(3);
    // check if a marker is missing or not
    int i = 0;
    int numMarkNotAssigned = 0;
    bool oneMissing = false;
    std::map< int, std::vector<std::string> > rigidBodyWithMissingMarkers;
    std::map<int,Eigen::Vector3d> opMarkers = opNameMarkers.lastOptitrackMarkersNotAssigned;

    std::vector<std::string> nameMarkNotAssigned;
    if (verbose) {   std::cout << " == BEGIN ASSOCIATION PHASE == " << std::endl; }


    // first, check associations according to CorresNameID + Motive
    // bool aCINConfirmed = (opNameMarkers.repeat && opNameMarkers.allCorresIDName.find(opNameMarkers.frameNumber) != opNameMarkers.allCorresIDName.end());

    for (int i = 0; i < allMarkerSets.size(); i++)
    {
      if (allMarkerSets[i].markerMissing)
      {
        if (verbose) {std::cout << "Miss for : " << i << " (name : " << allMarkerSets[i].name << ")" << std::endl;}
        std::vector<std::string> missingMarkers;
        for (std::map<std::string,bool>::iterator it=allMarkerSets[i].markersDetected.begin(); it!= allMarkerSets[i].markersDetected.end(); it++)
        {
          if (!it->second)
          {
            
            std::map<std::string,int>::iterator itname = opNameMarkers.corresNameID.find(it->first);


            if (itname != opNameMarkers.corresNameID.end() && opMarkers.find(itname->second) != opMarkers.end() ) 
            {
              int id = itname->second;
              Eigen::Vector3d marker = opMarkers[id];
              std::map<int,Eigen::Vector3d >::iterator it2; 
              std::string name;
              int framePicked;
              bool found = markersManagement::checkACINCorres(id, opNameMarkers.frameNumber, opNameMarkers.allCorresIDName, name, framePicked, opNameMarkers.repeat);


              if (!opNameMarkers.repeat)
              {
                // marker already identified, can be added directly
                std::string osname = itname->first;
                allMarkerSets[i].markersPosition[osname] = marker;
                it2 = opMarkers.find(id);
                opMarkers.erase(it2);
                if (verbose) {printf("Classic : Associate %s with %d . \n",osname.c_str(), id); }
                allMarkerSets[i].markersDetected[osname] = true;
              }

              else if (found) 
              {
                std::string osname = name;
                allMarkerSets[i].markersPosition[osname] = marker;
                                
                // if the id was detected at repeat step, must mean that this id
                // should now be the current id associated with the Opensim marker
                if (verbose && (id != opNameMarkers.corresNameID[osname]) ) {  called = true; printf("Repeat : associate %s with %d . \n",osname.c_str(), id); }

                opNameMarkers.corresNameID[osname] = id;

                it2 = opMarkers.find(id);
                opMarkers.erase(it2);
                allMarkerSets[i].markersDetected[osname] = true;
              }

            }
            else 
            {
              missingMarkers.push_back(it->first);
              nameMarkNotAssigned.push_back(it->first);
            }
          }
        }
        //std::cout << "allfoundOK" << std::endl;
        if (missingMarkers.size()>0)
        {

          rigidBodyWithMissingMarkers[i] = missingMarkers;
          numMarkNotAssigned += missingMarkers.size();
          oneMissing = true;
          //std::cout << "add missing ok" << std::endl;
        }
        else
        {
          //std::cout << "notmissing : " << i << std::endl;
          allMarkerSets[i].markerMissing = false;
        }
      }
    }

    //std::cout << "ok " << std::endl;

    // if used, means that one marker must have been lost. Let's deal with it.
    bool recount = false;


    if (rigidBodyWithMissingMarkers.size() > 0)
    {
      markersManagement::globalPublisher globalPublish;
      ros::Time time_pub = ros::Time::now();
      for (std::map<int, std::vector<std::string> >::iterator it = rigidBodyWithMissingMarkers.begin(); it != rigidBodyWithMissingMarkers.end(); it++) 
      {
        if (allMarkerSets[it->first].markerMissing && opMarkers.size() != 0)
        {
          std::map<std::string,bool> markersDetected = allMarkerSets[it->first].markersDetected;
          std::vector<std::string> markersBodyNames = allMarkerSets[it->first].markersNames;
          std::vector<std::string> missMarkers = it->second;
          std::map<std::string,Eigen::Vector3d> originMarkersBody = allMarkerSets[it->first].markersPositionOrigin;
          std::map<std::string,Eigen::Vector3d> currentMarkersBody;

          for (std::map<std::string,Eigen::Vector3d>::iterator it2 = originMarkersBody.begin(); it2!= originMarkersBody.end(); it2++)
          {
            if (markersDetected[it2->first])
            {
              currentMarkersBody[it2->first] = allMarkerSets[it->first].markersPosition[it2->first];
            }
          }
          int numMiss = it->second.size();
          int numNotMissed = currentMarkersBody.size();  

          if (numNotMissed >= 3 && numMiss != 0 )
          {
            // case where at least 3 markers of the body were identified, but other are missing,
            // and we still have optitrack markers that are not assigned
            // the idea here is to simulate the position of the missing markers and to selected the closest optitrack markers
            // from those missing markers, in order to accelerate the association.
            if (verbose) {std::cout << "3 markers debuging procedure for : " << allMarkerSets[it->first].name << std::endl;}
            
            recoveryMarkersProcedure(currentMarkersBody, originMarkersBody, globalPublish, time_pub, false);

            int j = 0;
            int opti_index;
            while (j < missMarkers.size() && opMarkers.size() != 0)
            {                          
              std::string treatedMark = missMarkers[j];
              if (verbose) {printf("Recovery Procedure for marker : %s \n", treatedMark.c_str());}
              std::map<std::string, double > D_mark_all_dist = allMarkerSets[it->first].markersDistance[treatedMark];

              double min_osim_dist = 99999;
              bool modMin = false;
              double max_osim_dist = 0.0;

              // get min - max distance between treatedMark and the other markers of the rigid body
              for (std::map<std::string,double>::iterator it2 = D_mark_all_dist.begin(); it2 != D_mark_all_dist.end(); it2++)
              { 
                if (markersDetected[it2->first])
                {
                  if (it2->second < min_osim_dist)
                  {
                    min_osim_dist = it2->second;
                    modMin = true;
                  }
                  if (it2->second > max_osim_dist)
                  {
                    max_osim_dist = it2->second;
                  }
                }
              }

              if (!modMin)
              {
                min_osim_dist = 0.0;
              }

              double closest_diff = min_osim_dist/mdc;

              Eigen::Vector3d simulatedPos = currentMarkersBody[treatedMark];

              if (verbose) {
                std::cout << "Simulated position : " << simulatedPos << std::endl;
                std::cout << "Closest diff : " << closest_diff << std::endl;
              }
              int id_selected = -1;
              std::map<int,Eigen::Vector3d >::iterator itopti;
              opti_index = 0;
              while (opti_index < opMarkers.size())
              {

                itopti = opMarkers.begin();
                std::advance(itopti,opti_index);

                double d = (itopti->second-simulatedPos).norm();

                if (verbose) {std::printf("Candidate : %d ; distance : %f. \n", itopti->first, d);}

                if (d <= closest_diff)
                {
                  closest_diff = d;
                  id_selected = itopti->first;
                }
                opti_index++;
              }
              // an optitrack element respecting those conditions has been
              // selected, congrats!
              if (id_selected != -1)
              {
                if (verbose) {printf("Found association (after recovery procedure) : index %d with marker %s. \n", id_selected, treatedMark.c_str());}

                opNameMarkers.corresNameID[treatedMark] = id_selected;
                manageRepeatAssociation( treatedMark, id_selected, opNameMarkers);            


                allMarkerSets[i].markersPosition[treatedMark] = opMarkers[id_selected];
                opMarkers.erase( opMarkers.find(id_selected) );
                allMarkerSets[i].markersDetected[treatedMark] = true;
                std::remove(nameMarkNotAssigned.begin(),nameMarkNotAssigned.end(),treatedMark);
                recount = true;
              }
              else
              {
                if (verbose) {printf("No association found (after recovery procedure) for marker %s. \n", treatedMark.c_str());}
              }
              j++;
            }
          }

          // case where there is less than 3 markers identified in the body, but there is still enough 
          // optitrack markers not assigned to try to assign them with this body part.
          else if (opMarkers.size() >= numMiss && allMarkerSets[it->first].markersPositionOrigin.size() >= 3 )
          {
            if (verbose) {std::cout << "Less than 3 markers debuging procedure for : " << allMarkerSets[it->first].name << std::endl;}
            opNameMarkers.manParams.filtering_verbose = true;
      
            std::map<std::string,Eigen::Vector3d> localBodyPos = allMarkerSets[it->first].markersPositionOrigin;
            std::map<std::string,int> localCorresNameID;
            std::map<int, Eigen::Vector3d> localOpMarkers = opMarkers;
            std::map<std::string,std::string> D_markers_body;


            for (int i = 0; i < markersBodyNames.size(); i++)
            {
              std::string name = markersBodyNames[i];
              if ( allMarkerSets[it->first].markersDetected[name])
              {
                if (opNameMarkers.corresNameID.find(name) != opNameMarkers.corresNameID.end())
                {
                  localCorresNameID[name] = opNameMarkers.corresNameID[name];
                }
                else
                {
                  localCorresNameID[name] = -i;
                }
                localOpMarkers[localCorresNameID[name]] = currentMarkersBody[name];  
                D_markers_body[name] =  allMarkerSets[it->first].name;
                //std::cout << name << " : " << localCorresNameID[name] << std::endl;
              }
            }

            for (std::map<int,Eigen::Vector3d>::iterator it20= localOpMarkers.begin(); it20!=localOpMarkers.end(); it20++) 
            {
              std::cout << "id : " << it20->first << " value : " << it20->second << std::endl;
            }
          // for rigid bodies markerSet only
            int result = markersManagement::bodyMarkerRecoveryByICP(opNameMarkers.manParams, localBodyPos, localOpMarkers, allMarkerSets[it->first].markersDistance, 1.0, D_markers_body, localCorresNameID,0.01, opNameMarkers.debugPub); // 0.01

            opNameMarkers.manParams.filtering_verbose = false;
            if (result == 1)
            {
              for (std::map<std::string,int>::iterator it2 = localCorresNameID.begin(); it2!= localCorresNameID.end(); it2++)
              {
                if (it2->second >= 0)
                {
                  std::string treatedMark = it2->first;
                  int id_selected = it2->second;
                  opNameMarkers.corresNameID[treatedMark] = id_selected;
                  manageRepeatAssociation( treatedMark, id_selected, opNameMarkers);            

                  allMarkerSets[it->first].markersPosition[treatedMark] = opMarkers[id_selected];
                  opMarkers.erase( opMarkers.find(id_selected) );
                  allMarkerSets[i].markersDetected[treatedMark] = true;
                  std::remove(nameMarkNotAssigned.begin(),nameMarkNotAssigned.end(),treatedMark);
                  
                }
                else
                {
                  localCorresNameID.erase(it2);
                }

              }
              std::cout << "---" << std::endl;
              markersManagement::printAssociation(localCorresNameID);
              std::cout << "Debug for recovery By ICP (frameNumber :  "<< opNameMarkers.frameNumber << " )" << std::endl;
              // std::string debRec;
              // std::cin >> debRec;
              recount = true;
              // opNameMarkers.pause_mode = true;
            }
            else if (result == -1)
            {
              std::cout << "Debug for recovery By ICP - after bug" << std::endl;
              //  opNameMarkers.pause_mode = true;
              //  ros::spinOnce();      
              // std::string debRec;
              // std::cin >> debRec;                    
            }
          }
        }
      }
    }
    
    //std::cout << "retrieve ok" << std::endl;

    bool checkCalled = ( (opMarkers.size() >0 && nameMarkNotAssigned.size() > 0  ) || nameMarkNotAssigned.size() > 0 || called);

    if (verbose && (checkCalled) )
    {
      called = true;
      ROS_INFO("Number of markers not assigned : %ld. \n", nameMarkNotAssigned.size());
      for (int i = 0; i < nameMarkNotAssigned.size(); i++)
      {
        std::cout << nameMarkNotAssigned[i] << " ; ";
      }
      std::cout << std::endl;

      ROS_INFO("arkers not assigned : %ld. \n", opMarkers.size() );
      for (std::map<int,Eigen::Vector3d>::iterator it = opMarkers.begin(); it!= opMarkers.end(); it++)
      {
        std::cout << it->first << " ; ";
      }
      std::cout << std::endl;
    }
    if (verbose) {   std::cout << " == END ASSOCIATION PHASE == " << std::endl; }
  }

/**
 * @brief Try to associate the Optitrack markers defined inside opNameMarkers with the Opensim markers
 * defined inside osMod.
 * 
 * The Opensim markers that should be followed is fixed by osMarkersNotAssigned variable.
 * 
 * @param opNameMarkers 
 * @param osMod 
 * @param corresNameID 
 * @param numOsNotAssigned : number of opensim markers that are not assigned during this frame
 * @param osMarkersNotAssigned 
 * @param called 
 */
  void markersAssociationShort(  optitrack_full::OptitrackNameMarkers & opNameMarkers,
  fosim::OsimModel & osMod,
  std::map<std::string,int> & osMarkersNotAssigned,
  bool & IK_possible,
  int & numOsNotAssigned,
  bool & called)
  {

    opNameMarkers.noMoreAssosPossible = false;
    int numOsAssigned = 0;
    bool verbose = opNameMarkers.association_verbose;
    double mdc = opNameMarkers.minima_distance_coeff;


    std::map<std::string, std::vector<std::string> > current_D_body_markers;
    // update the rigid body markers to take into account only the markers in osMarkersNotAssigned
    for (std::map<std::string,int>::iterator it3 = osMarkersNotAssigned.begin(); it3!= osMarkersNotAssigned.end(); it3++)
    {
      if (current_D_body_markers.find(it3->first) == current_D_body_markers.end() )
      {
        current_D_body_markers[osMod.D_markers_body[it3->first]] = osMod.D_body_markers[osMod.D_markers_body[it3->first]];
      }
    }

    std::map<int, Eigen::Vector3d > opMarkers;
    
    opMarkers = opNameMarkers.lastOptitrackMarkersNotAssigned;
    std::map<int,Eigen::Vector3d >::iterator it2; 

    std::map<std::string,int> corresNameID = opNameMarkers.corresNameID;
    std::map<int,std::string>::iterator itname;

    // check if a marker is missing or not
    int mapindex = 0;
    std::map<int,Eigen::Vector3d >::iterator it; 
    //ROS_INFO("ASSOS");
    //markersManagement::printAssociation(corresNameID);    
    
    //if (verbose) { ROS_INFO("BEGIN ASSOS"); }
    bool start = true;


    //bool aCINConcerned = (opNameMarkers.repeat && opNameMarkers.allCorresIDName.find(opNameMarkers.frameNumber) != opNameMarkers.allCorresIDName.end() );

    while (mapindex < opMarkers.size())
    {
      if (verbose && start)
      {
        ROS_INFO("Number of markers in frame %d : %ld", opNameMarkers.frameNumber, opMarkers.size());
        start = false;
      }
      it = opMarkers.begin();
      std::advance(it,mapindex);
      int id = it->first;
      Eigen::Vector3d marker = it->second;
      //std::cout << "current ID : " << id << std::endl;
      auto itname = std::find_if(std::begin(corresNameID), std::end(corresNameID), [&](const std::pair<std::string,int> &pair)
      {
          return pair.second == id;
      });

      if (itname != corresNameID.end() ) 
      {
        // marker already identified, can be add directly
        std::string osname = itname->first;
        opNameMarkers.opensimMarkersPos[osname] = marker;
        it2 = opMarkers.find(id);
        opMarkers.erase(it2);
        if (verbose && opNameMarkers.lastOsMarkersNotAssigned[osname] == 1) {

          printf("Classic (frame %d) : Associate %s with %d . \n",opNameMarkers.frameNumber, osname.c_str(), id); 
          
        }
        osMarkersNotAssigned[osname] = 0;
        numOsAssigned +=1;
        opNameMarkers.markersWeights[osname] = 1.0*osMod.D_markers_weight[osname];
        mapindex--;

      }
      else if (opNameMarkers.repeat)
      {
        std::string osname;
        int framePicked;
        bool vb = false;//true;//(opNameMarkers.frameNumber <= 3000);
        if (vb)
        {
          printf("In association step : \n");
        }
        bool found = markersManagement::checkACINCorres(id, opNameMarkers.frameNumber, opNameMarkers.allCorresIDName, osname, framePicked, opNameMarkers.repeat,vb);

        if (found)
        { 
          opNameMarkers.opensimMarkersPos[osname] = marker;
                          
          // if the id was detected at repeat step, must mean that this id
          // should now be the current id associated with the Opensim marker
          if (verbose && (opNameMarkers.lastOsMarkersNotAssigned[osname] == 1 || id != corresNameID[osname]) ) {  called = true; printf("Repeat : associate %s with %d . \n",osname.c_str(), id); }

          opNameMarkers.corresNameID[osname] = id;

          it2 = opMarkers.find(id);
          opMarkers.erase(it2);

          osMarkersNotAssigned[osname] = 0;
          numOsAssigned +=1;
          opNameMarkers.markersWeights[osname] = 1.0*osMod.D_markers_weight[osname];

          mapindex--;
        }
      }
      mapindex++;

    }

    // if used, means that one marker must have been lost. Let's deal with it.
    verbose = opNameMarkers.association_verbose;
    if (numOsAssigned!=osMarkersNotAssigned.size() && opMarkers.size() > 0)
    {
      // new idea : for each opensim marker not assigned : 
      // * for each optitrack marker not assigned : 
      //        * get distance between this marker and the optitrack marker
      //        * compare this distance with distance with closest of opensim
      //        * if this distance is inferior to variationcoeff * distanceclosest, note % of proximity to distanceclosest
      //        * associate opensim marker and optitrack marker that have the best % of proximity

      if (verbose) {called = true; std::cout << "At least one marker not assigned, try to fix it ... " << std::endl; }
      std::map<std::string,int>::iterator itmap;
      std::map<int,Eigen::Vector3d >::iterator itopti;
      int map_index = 0;
      int opti_index = 0;

      while (map_index < osMarkersNotAssigned.size()){
        itmap = osMarkersNotAssigned.begin();
        std::advance(itmap,map_index);
        if (itmap->second!=0)
        {

          if (verbose) {std::cout << "Check missing marker : " << itmap->first << std::endl; }

          std::string osMarkName = itmap->first;
          // distance of missing opensim marker from each other opensim markers of the same body.
          std::map< std::string, double> markersAssociated = osMod.D_mark_all_dist[osMarkName];

          double min_dist_osim = 9999;
          for (std::map<std::string, double>::iterator itmin = markersAssociated.begin(); itmin!=markersAssociated.end(); itmin++)
          {
            if (itmin->second < min_dist_osim)
            {
              min_dist_osim = itmin->second;
            }
          }

          // each optitrack markers not associated are candidate for the Opensim marker not associated.
          std::map<int,Eigen::Vector3d> opti_candidate = opMarkers;
          double max_change = opNameMarkers.maxGapVariation[osMarkName];
          double distMV = 0.003;
          double thresMV = std::sqrt((distMV)*(distMV)*3);
          if (max_change > thresMV)
          {
            max_change = thresMV;
          }

          if (verbose) {std::printf("Parameters : max variation of position : %f; number candidates at first : %ld; could be check with %ld opensim markers \n", max_change, opti_candidate.size(), markersAssociated.size());}

          int mark_index = 0;
          std::map< std::string, double>::iterator itmark;
          int numAssos = 0;
          bool atLeastTwo = false;

          while (mark_index< markersAssociated.size() && opti_candidate.size() >0)
          {
            // for each opensim marker associated with the missing opensim marker
            // (= marker on same body)
            itmark = markersAssociated.begin();
            std::advance(itmark,mark_index);

            // if (osMarkersNotAssigned[itmark->first] == 0)
            // {

              // if this marker is still associated, compare its theorical distance
              // from the missing opensim marker to the distance betwwen the missing
              // opensim marker and the optitrack markers that are not associated.
              double min_reject = 9999;
              bool madeOneAssos = false;
            
              double radius = itmark->second;
              if (verbose) {std::printf("Candidate : %s ; radius : %f. \n", itmark->first.c_str(), radius);}
              Eigen::Vector3d cur_os_pos;
              if (opNameMarkers.lastOsMarkersNotAssigned.size() > 0 && opNameMarkers.lastOsMarkersNotAssigned[itmark->first] == 0)//(opNameMarkers.opensimMarkersPos.find(itmark->first) != opNameMarkers.opensimMarkersPos.end())
              {
                 //if (verbose) {std::cout << "Marker opensim chosen  by last Assos"<< std::endl;}
    
                cur_os_pos = opNameMarkers.opensimMarkersPos[itmark->first];
              }
              else
              {
                //if (verbose) {std::cout << "Marker opensim chosen  by IK guess"<< std::endl;}
                cur_os_pos = osMod.D_mark_pos[itmark->first];
              }
                if (verbose) {std::cout << "Marker opensim chosen : " << cur_os_pos << std::endl;}
    
              std::map<int,Eigen::Vector3d >::iterator itopti2;
              opti_index = 0;
              while (opti_index < opti_candidate.size())
              {
                itopti = opti_candidate.begin();
                std::advance(itopti,opti_index);
                double d = (cur_os_pos - itopti->second).norm();
                if (verbose) {std::cout << "Marker optitrack " << itopti->first << " : distance : " << d << std::endl;}
    
                // if the difference is superior to all recorded variations of distance of the 
                //  theorical marker --> cancel it.
                if (abs(d-radius) > max_change)
                {
                  if (verbose){ std::cout << "Rejected. " << std::endl;}
                  itopti2 = opti_candidate.find(itopti->first);
                  opti_candidate.erase(itopti2);
                  opti_index--;
                }
                else
                {
                  if (abs(d-radius) < min_reject)
                  {
                    //if (verbose) {std::cout << "Marker optitrack " << itopti->first << " : distance : " << d << std::endl;}
                    if (verbose){ std::cout << "Accepted. " << std::endl;}
                    min_reject = abs(d-radius);
                    if (!madeOneAssos)
                    {
                      madeOneAssos = true;
                      numAssos +=1;
                    }
                  }

                }

                opti_index++;

              }
              if (verbose)
              {
                printf("After this selection, number of candidates : %ld \n", opti_candidate.size());
                printf("Min reject for this one : %f. \n", min_reject);
              }

            //}
            mark_index++;
            
          }

           
          int size = markersAssociated.size();
          atLeastTwo = (numAssos >= std::min(2,size) );
          Eigen::Vector3d selected_pose; selected_pose.setZero();
          Eigen::Vector3d guess_pose;
          if (opNameMarkers.lastOsMarkersNotAssigned.size() > 0 && opNameMarkers.lastOsMarkersNotAssigned[osMarkName] == 0)//(opNameMarkers.opensimMarkersPos.find(itmark->first) != opNameMarkers.opensimMarkersPos.end())
          {
              if (verbose) {std::cout << "Marker opensim guess by last Assos"<< std::endl;}

            guess_pose = opNameMarkers.opensimMarkersPos[osMarkName];
          }
          else
          {
            if (verbose) {std::cout << "Marker opensim guess by IK"<< std::endl;}
            guess_pose = osMod.D_mark_pos[osMarkName];
          }

          if (opti_candidate.size() == 1 && atLeastTwo)
          {
            // one association is possible, and it was checked by at least with two Opensim markers
            // that are still associated; thus, this association might be correct.
            std::map<int,Eigen::Vector3d >::iterator itselect = opti_candidate.begin();
            selected_pose = itselect->second;
            int selected_id = itselect->first;
            if (verbose)
            {
              printf("Found association : index %d with marker %s. \n", selected_id, itmap->first.c_str());
            }
            opNameMarkers.corresNameID[osMarkName] = selected_id;
            manageRepeatAssociation(osMarkName, selected_id, opNameMarkers);                   

            opMarkers.erase( opMarkers.find(selected_id) );
            osMarkersNotAssigned[osMarkName] = 0;
            opNameMarkers.markersWeights[osMarkName] = 1.0*osMod.D_markers_weight[osMarkName];
            opNameMarkers.opensimMarkersPos[osMarkName] = selected_pose;

            map_index = -1;

          }
          else if (opti_candidate.size()>1 && atLeastTwo)
          {
            // second recovery procedure : closest to last recorded position of opensim marker
            // and then, check if this position make sense compared to theorical distances.

            // in consequence, reduce weight associated with marker
            if (verbose)
            {
              std::cout << "More than one marker selected : loading second detection ... " << std::endl;
            }

            

            double min_dist2 = INFINITY;
            std::map<int,Eigen::Vector3d >::iterator it_candidate;


            opti_index = 0;


            // now, get the closest candidate.
            while (opti_index < opti_candidate.size())
            {
              itopti = opti_candidate.begin();
              std::advance(itopti,opti_index);
              double d = (guess_pose - itopti->second).norm();
              if (d < min_dist2)
              {
                min_dist2 = d;
                it_candidate = itopti;
              }
              opti_index++;
            }

            selected_pose = it_candidate->second;
            int selected_id = it_candidate->first;

            if (verbose) {printf("Found association (after second detection) : index %d with marker %s. \n", selected_id, itmap->first.c_str());}
            opNameMarkers.corresNameID[osMarkName] = selected_id;
            manageRepeatAssociation(osMarkName, selected_id, opNameMarkers);           

            opMarkers.erase( opMarkers.find(selected_id) );
            osMarkersNotAssigned[osMarkName] = 0;
            opNameMarkers.markersWeights[osMarkName] = 1.0*osMod.D_markers_weight[osMarkName];
            opNameMarkers.opensimMarkersPos[osMarkName] = selected_pose;

          }
          else if (!atLeastTwo && opti_candidate.size() == 1)
          {
            std::map<int,Eigen::Vector3d >::iterator itselect = opti_candidate.begin();
        
            int selected_id = itselect->first;
            // try by comparing position of opensim marker to IK

           if (verbose)
            {
              std::cout << "Try to find by IK check ... " << std::endl;
              std::cout << "By IK : " << guess_pose << std::endl;
              std::cout << "Distance from selected : " << (itselect->second - guess_pose).norm()<< std::endl;
            }            

            if (( (itselect->second - guess_pose).norm() )<  max_change)
            {
              //authorize change
              selected_pose = itselect->second;    
              if (verbose) {printf("Found association (after IK check) : index %d with marker %s. \n", selected_id, itmap->first.c_str());}
              opNameMarkers.corresNameID[osMarkName] = selected_id;
              manageRepeatAssociation(osMarkName, selected_id, opNameMarkers);           

              opMarkers.erase( opMarkers.find(selected_id) );
              osMarkersNotAssigned[osMarkName] = 0;
              opNameMarkers.markersWeights[osMarkName] = 1.0*osMod.D_markers_weight[osMarkName];
              opNameMarkers.opensimMarkersPos[osMarkName] = selected_pose;
              }              

            else
            {
              // otherwise, the marker is disqualified for now
              opNameMarkers.opensimMarkersPos[osMarkName] = selected_pose;
              opNameMarkers.markersWeights[osMarkName] = 0.0;
              // raise a little bit the tolerance
              if (verbose) { printf("Raise tolerance from : %f", opNameMarkers.maxGapVariation[osMarkName]); }
              // cvheck si block ici ou non
              opNameMarkers.maxGapVariation[osMarkName] = std::max( std::min( pow(10.0,1.0/opNameMarkers.frameRate)*opNameMarkers.maxGapVariation[osMarkName], min_dist_osim/(mdc) ),  opNameMarkers.maxGapVariation[osMarkName] ); 
              if (verbose) { printf(" to : %f. \n", opNameMarkers.maxGapVariation[osMarkName]); }
            }
          }
          else if (atLeastTwo)
          {
            // otherwise, the marker is disqualified for now
            opNameMarkers.opensimMarkersPos[osMarkName] = selected_pose;
            opNameMarkers.markersWeights[osMarkName] = 0.0;
            // raise a little bit the tolerance
            if (verbose) { printf("Raise tolerance from : %f", opNameMarkers.maxGapVariation[osMarkName]); }
            // cvheck si block ici ou non
            opNameMarkers.maxGapVariation[osMarkName] = std::max( std::min( pow(10.0,1.0/opNameMarkers.frameRate)*opNameMarkers.maxGapVariation[osMarkName], min_dist_osim/(mdc) ),  opNameMarkers.maxGapVariation[osMarkName] ); 
            if (verbose) { printf(" to : %f. \n", opNameMarkers.maxGapVariation[osMarkName]); }
          }

        }
        map_index++;

      }


    }
      
    numOsNotAssigned = 0;
    std::map<std::string,bool> enoughOsAssigned;
    std::map<std::string,int> numOsByBodyPart;
    IK_possible = checkIKPossible(osMarkersNotAssigned,current_D_body_markers,numOsNotAssigned, enoughOsAssigned, numOsByBodyPart, false);



    // second recovery procedure. 
    bool recount = false;
    if (numOsNotAssigned != 0 && opMarkers.size() != 0)
    {
      markersManagement::globalPublisher globalPublish;
      ros::Time time_pub = ros::Time::now();
      // try other way : from parent to child
      std::vector<std::string> bodyPartsOrder = osMod.markers_bodies_multiorder;

      for (int i = 0; i < bodyPartsOrder.size(); i++)
      {
        std::map<std::string,bool>::iterator it = enoughOsAssigned.find(bodyPartsOrder[i]);
        if (it!= enoughOsAssigned.end())
        {
          
        //   }
        // }

        // for (std::map<std::string,bool>::iterator it = enoughOsAssigned.begin(); it!= enoughOsAssigned.end(); it++)
        // {
          double tolICP = 0.0021;
          bool useICP = false;
          // case where at least 3 markers of the body were identified, but other are missing,
          // and we still have optitrack markers that are not assigned
          // the idea here is to simulate the position of the missing markers and to selected the closest optitrack markers
          // from those missing markers, in order to accelerate the association.
          int numMiss = osMod.D_body_markers[it->first].size()- numOsByBodyPart[it->first];
          int numMarkInBody = osMod.D_body_markers[it->first].size();
          int numMarkAssos = (numMarkInBody - numMiss);              
          if (it->second && numMiss !=0 && opMarkers.size() != 0 )
          // test : try to always ignore this step, as next step is more robust now
          // (even if more costly)
          //if (false)
          {
            if (verbose) {std::cout << "3 markers debuging procedure for : " << it->first << std::endl;}
            std::vector<std::string> markersBodyNames = osMod.D_body_markers[it->first];
            std::map<std::string,Eigen::Vector3d> currentMarkersBody;
            std::vector<std::string> missMarkers;
            std::map<std::string,Eigen::Vector3d> originMarkersBody;

            for (int l= 0; l < markersBodyNames.size(); l++)
            {
              if (osMarkersNotAssigned[markersBodyNames[l] ] == 0)
              {
                currentMarkersBody[markersBodyNames[l]] = opNameMarkers.opensimMarkersPos[markersBodyNames[l]];
              }
              else
              {
                missMarkers.push_back(markersBodyNames[l]);
              }
              originMarkersBody[markersBodyNames[l]] = osMod.D_mark_pos_local[markersBodyNames[l]];

            }
            recoveryMarkersProcedure(currentMarkersBody, originMarkersBody, globalPublish, time_pub, false);

            int j = 0;
            int opti_index;
            while (j < missMarkers.size() && opMarkers.size() != 0)
            {            
              std::string treatedMark = missMarkers[j];
              if (verbose) {printf("Recovery Procedure for marker : %s \n", treatedMark.c_str());}

              double min_osim_dist = 99999;
              double max_osim_dist = 0.0;
              for (std::map<std::string,double>::iterator it2 = osMod.D_mark_all_dist[treatedMark].begin(); it2 != osMod.D_mark_all_dist[treatedMark].end(); it2++)
              {
                if (it2->second < min_osim_dist)
                {
                  min_osim_dist = it2->second;
                }
                if (it2->second > max_osim_dist)
                {
                  max_osim_dist = it2->second;
                }
              }
              double closest_diff = min_osim_dist/mdc;
              if (opNameMarkers.lastOsMarkersNotAssigned[treatedMark] == 1 && numOsNotAssigned == 1 && opMarkers.size() == 1)
              {
                // we hugely increase tolerance in this obvious case
                closest_diff = max_osim_dist;
              }

              Eigen::Vector3d simulatedPos = currentMarkersBody[treatedMark];

              if (verbose) {
                std::cout << "Simulated position : " << simulatedPos << std::endl;
                std::cout << "Closest diff : " << closest_diff << std::endl;
              }
              int id_selected = -1;
              std::map<int,Eigen::Vector3d >::iterator itopti;
              opti_index = 0;
              while (opti_index < opMarkers.size())
              {

                itopti = opMarkers.begin();
                std::advance(itopti,opti_index);

                double d = (itopti->second-simulatedPos).norm();

                if (verbose) {std::printf("Candidate : %d ; distance : %f. \n", itopti->first, d);}

                if (d <= closest_diff)
                {
                  closest_diff = d;
                  id_selected = itopti->first;
                }
                opti_index++;
              }
              // an optitrack element respecting those conditions has been
              // selected, congrats!
              if (id_selected != -1)
              {
                if (verbose) {printf("Found association (after recovery procedure) : index %d with marker %s. \n", id_selected, treatedMark.c_str());}
                opNameMarkers.corresNameID[treatedMark] = id_selected;
                manageRepeatAssociation( treatedMark, id_selected, opNameMarkers);           

                opNameMarkers.opensimMarkersPos[treatedMark] = opMarkers[id_selected];
                opMarkers.erase( opMarkers.find(id_selected) );
                osMarkersNotAssigned[treatedMark] = 0;
                opNameMarkers.markersWeights[treatedMark] = 1.0*osMod.D_markers_weight[treatedMark];
                // numOsNotAssigned -=1;
                // numOsByBodyPart[it->first] +=1;

                recount = true;

              }
              else
              {
                if (verbose) {printf("No association found (after recovery procedure) for marker %s. \n", treatedMark.c_str());}
                if (opNameMarkers.lastOsMarkersNotAssigned[treatedMark] == 1)
                {
                  // case looks more obvious; thus, use ICP now for this step, but with very small tolerance
                  tolICP = 0.0015;
                  useICP = true;
                  if (verbose)
                  {
                  std::cout << "part : " << it->first << " : 3 markers, but nothing found, so use ICP" << std::endl;
                  }
                
                }
                // else
                // {
                //   std::cout << it->first << " : Not ICP because :" << std::endl;
                //   std::cout << opNameMarkers.lastOsMarkersNotAssigned[treatedMark] << std::endl;
                //   std::cout << numOsNotAssigned << std::endl;
                // }
              }
              j++;
            }
          }
        
          // case where there is less than 3 markers identified in the body, but there is still enough 
          // optitrack markers not assigned to try to assign them with this body part.

          // test : to force to go to this step, ignore enoughOSAssigned (previously)
        // else if (!it->second && opMarkers.size() != 0 && opMarkers.size() >= numMiss)
          // test : if there is a possibility of ICP (e.g, if the currently associated points + the not associated optitrack points
          // have a sufficent number for ICP), try it
          else if (!it->second && opMarkers.size() != 0 && ( numMarkAssos +opMarkers.size()  >= 3 && numMiss > 0) )
          //else if (opMarkers.size() >= numMiss)        
          {
            useICP = true;
          }

          if (useICP)
          {

            // test : add to tolICP maxGapVar
            double max_var_local = 0;

            opNameMarkers.manParams.filtering_verbose = true;
            if (opNameMarkers.manParams.filtering_verbose) { std::cout << "Less than 3 markers debuging procedure for : " << it->first << std::endl; }
            std::vector<std::string> markersBodyNames = osMod.D_body_markers[it->first];          
            std::map<std::string,Eigen::Vector3d> localBodyPos;
            std::map<std::string,int> localCorresNameID;

            for (int i = 0; i < markersBodyNames.size(); i++)
            {
              std::string name = markersBodyNames[i];
              localBodyPos[name] = osMod.D_mark_pos_local[name];
              localCorresNameID[name] = opNameMarkers.corresNameID[name];
              if (osMarkersNotAssigned[name] == 0)
              {
                //if (verbose) { std::cout << "Add position of element " << name << "as it is still association with optitrack" << std::endl;}
                opMarkers[opNameMarkers.corresNameID[name] ] =  opNameMarkers.opensimMarkersPos[name];
              }
              if (opNameMarkers.maxGapVariation[name] > max_var_local)
              {
                max_var_local = opNameMarkers.maxGapVariation[name];
              }

            }
            tolICP+= max_var_local;
            if (opNameMarkers.manParams.filtering_verbose) { std::cout << "Chosen tol ICP : " << tolICP << std::endl;}

            // if there is parent and parent has enough markers for IK, authorize to try to associate 
            // markers by ICP even if we do not have enough optitrack markers

            // for all rigid bodies
            // TODO : check bug where after cleanUp by user and reassos, ACIN[-1] is not reset at good value
            // activer debugICP ap cleanUp par user, parce que trop bizarre
            
            int result = markersManagement::bodyMarkerRecoveryByICP(opNameMarkers.manParams, localBodyPos, opMarkers, osMod, localCorresNameID,tolICP, opNameMarkers.debugPub, opNameMarkers.CINCalib); // 0.01

            opNameMarkers.manParams.filtering_verbose = false;
            if (result >= 1 )
            {
              for (std::map<std::string,int>::iterator it = localCorresNameID.begin(); it!= localCorresNameID.end(); it++)
              {
                std::string treatedMark = it->first;
                int id_selected = it->second;

                std::map<int,Eigen::Vector3d>::iterator itn = opMarkers.find(id_selected);

                if (itn != opMarkers.end())
                {
                  opNameMarkers.corresNameID[treatedMark] = id_selected;
                  if (result == 1 || result == 2)
                  {
                    // new information, need to add it
                    // result == 2 : correct old association : so NEED to communicate about it
                    if (result == 2)
                    {
                      ROS_INFO("== Correct old association : verbose activated for this iteration");
                    }

                    manageRepeatAssociation( treatedMark, id_selected, opNameMarkers, (result == 2));    
                  }

                  opNameMarkers.opensimMarkersPos[treatedMark] = opMarkers[id_selected];
                  opMarkers.erase( itn );
                  osMarkersNotAssigned[treatedMark] = 0;
                  opNameMarkers.markersWeights[treatedMark] = 1.0*osMod.D_markers_weight[treatedMark];
                }

              }
              std::cout << "--- After ICP check ---" << std::endl;
              markersManagement::printAssociation(localCorresNameID);
              std::cout << "Debug for recovery By ICP (frameNumber :  "<< opNameMarkers.frameNumber << " )" << std::endl;

              if (opNameMarkers.debugAssos)
              {

    
                std::string debRec;
              std::cin >> debRec;
            //  opNameMarkers.pause_mode = true;

              }
              recount = true;

            }
            else if (result == -1)
            {
              std::cout << "Debug for recovery By ICP - after bug" << std::endl;
              //  opNameMarkers.pause_mode = true;
              //  ros::spinOnce();      
              // std::string debRec;
              // std::cin >> debRec;                    
            }

              // cleanUp previous associations made
              // (to be sure that already associated elements will not stay
              // in opMarkers)
              for (int i = 0; i < markersBodyNames.size(); i++)
              {
                std::string name = markersBodyNames[i];
                
                std::map<int,Eigen::Vector3d>::iterator it_assos = opMarkers.find(opNameMarkers.corresNameID[name]);
                if (osMarkersNotAssigned[name] == 0 && it_assos != opMarkers.end())
                {
                  
                  opMarkers.erase(it_assos);
                }
              }
            }
        }
      }
    }

    if (opNameMarkers.simulation)
    {

      markersManagement::checkSimulationAssociation(opNameMarkers.corresNameID,opNameMarkers.simu_gest.hiddenCorresNameID, opNameMarkers.error_pub,opNameMarkers.error_verbose);
      
    }
    
    if (recount)
    {
      numOsNotAssigned = 0;
      enoughOsAssigned.clear();
      numOsByBodyPart.clear();
      IK_possible = checkIKPossible(osMarkersNotAssigned,current_D_body_markers,numOsNotAssigned, enoughOsAssigned, numOsByBodyPart);
    }

    bool checkCalled = ( (opMarkers.size() >0 && numOsNotAssigned>0  ) || called);

    if (verbose || (checkCalled) )
    {
      if (!opNameMarkers.never_verbose)
      {
        called = true;
        ROS_INFO("Frame : %d", opNameMarkers.frameNumber);
        ROS_INFO("Number of Opensim markers not assigned : %d. \n", numOsNotAssigned);
        markersManagement::checkAssignedMarkers(osMarkersNotAssigned, osMod.D_markers_body, true);
        ROS_INFO("Number of Optitrack markers not assigned : %ld. \n", opMarkers.size() );
        for (std::map<int,Eigen::Vector3d>::iterator it = opMarkers.begin(); it!= opMarkers.end(); it++)
        {
          std::cout << it->first << " ; ";
        }
        std::cout << std::endl;
        std::cout << "IK possible : " << IK_possible << std::endl;

      }
    }
    if (recount && IK_possible)
    {
      opNameMarkers.framesSinceAssemble = opNameMarkers.limFramesSinceAssemble;
    }

    if (numOsNotAssigned == 0 || opMarkers.size() == 0)
    {
      // for this frame, no more associations are possible
      opNameMarkers.noMoreAssosPossible = true;
    }

    // D_mark_all_dist = D_mark_all_dist_assigned = opensim markers pos : actual position, corresponds to position of attributed Optitrack markers    

    // D_mark_all_dist_ori : previous distance in position of markers.

          // // if IK was possible without issue, use it to get infos about the different markers
          // std::map<std::string, std::map<std::string, double> > D_mark_all_dist = fosim::getMarkerDistancesInBody(osMod,opNameMarkers.opensimMarkersPos);
          // // use position of markers obtained by IK instead (RMQ : TOO MANY BAD ASSOCIATIONS WITH THAT)
          // //std::map<std::string, std::map<std::string, double> > D_mark_all_dist = fosim::getMarkerDistancesInBody(osMod,osMod.D_mark_pos);

          // std::map<std::string, std::map<std::string, double> > D_mark_all_dist_assigned = D_mark_all_dist;
          // std::map<std::string, std::map<std::string, double> > D_mark_all_dist_origin = opNameMarkers.D_mark_all_dist;
          // std::map<std::string, std::map<std::string, double> >::iterator it_map_dist;
          // std::map<std::string, std::map<std::string, double> >::iterator it_map_ori;
          // int index_deb = 0;
          // int comp = 0;
          // while (comp < D_mark_all_dist_assigned.size() )
          // {
          //   it_map_dist = D_mark_all_dist_assigned.begin();

          //   std::advance(it_map_dist,comp);
          //   // std::cout << "bmld" << std::endl;
          //   // std::cout << comp << std::endl;
          //   // std::cout << enoughOsAssigned[osMod.D_markers_body[it_map_dist->first]] << std::endl;

          //   // suppress tolerance check for body part where IK impossible
          //   // (i.e not enough markers)
          //   // also suppress tolerance check for marker not assigned
          //   //if (!enoughOsAssigned[osMod.D_markers_body[it_map_dist->first]] || osMarkersNotAssigned[it_map_dist->first] == 1 )
          //   //  do not use enoughOsAssigned : could be false right now
          //   if ( osMarkersNotAssigned[it_map_dist->first] == 1 )
          //   {
          //     it_map_ori = D_mark_all_dist_origin.begin();     
        
          //     std::advance(it_map_ori,comp);        

          //     D_mark_all_dist_assigned.erase(it_map_dist);

          //     D_mark_all_dist_origin.erase(it_map_ori);

          //     comp--;
              
          //   }
          //   // maj of position for attributed markers
          //   else
          //   {
          //     opNameMarkers.D_mark_all_dist[it_map_dist->first] = it_map_dist->second;
          //   }


          //   // index_deb ++;
          //   // if (index_deb%100 == 0)
          //   // {
          //   //   std::cout << "D_mark_all_dist_assigned bug" << std::endl;
          //   // }
          //   comp++;


          // if (verbose)
          // {

          //   std::printf("---- Resume before ---- \n"); 
          //   for (std::map<std::string,double>::iterator it = opNameMarkers.maxGapVariation.begin(); it!= opNameMarkers.maxGapVariation.end(); it++)
          //   {
          //     std::printf(" Value for treated : %s : %f \n", it->first.c_str(), opNameMarkers.maxGapVariation[it->first]);
          //   }

          // }

          //std::cout << D_mark_all_dist_origin.size() << std::endl;
          //std::cout << D_mark_all_dist_assigned.size() << std::endl;
          //fosim::printMarkerDistancesInBody(osMod.osimModel,osMod.osimState);
          // markersManagement::allDistanceVariationDynamic(D_mark_all_dist_origin,D_mark_all_dist_assigned,osMod.D_mark_all_dist,opNameMarkers.maxGapVariation, opNameMarkers.maximum_residual, 1.0/opNameMarkers.frameRate, mdc, false);


          // markersManagement::publishToleranceValue(opNameMarkers.maxGapVariation,opNameMarkers.error_pub, opNameMarkers.error_verbose);




      //  }




    // note which markers are associated

    if (opNameMarkers.lastOsMarkersNotAssigned.size() > 0)
    {

      for (std::map<std::string,int>::iterator it = osMarkersNotAssigned.begin(); it!= osMarkersNotAssigned.end(); it++)
      {
        if ( (opNameMarkers.lastOsMarkersNotAssigned[it->first] == 0) && (osMarkersNotAssigned[it->first] == 1) )
        {
          opNameMarkers.lastFrameOsAssos[it->first] = std::max(opNameMarkers.frameNumber-1,0);
        }

      }
    }

    // also suppress object that are now associated whereas they weren't in past
    int l = 0;
    std::map<std::string,int>::iterator itc = opNameMarkers.lastFrameOsAssos.begin();
    while (l < opNameMarkers.lastFrameOsAssos.size())
    {
      itc = opNameMarkers.lastFrameOsAssos.begin();
      std::advance(itc,l);
      if ( (opNameMarkers.lastOsMarkersNotAssigned[itc->first] == 1) && (osMarkersNotAssigned[itc->first] == 0) )
      {
        opNameMarkers.lastFrameOsAssos.erase(itc);
      }
      else
      {
        l++;
      }
    }
  

    opNameMarkers.lastOsMarkersNotAssigned = osMarkersNotAssigned;
  // if (verbose)
  // {

  //   std::printf("---- Resume out 2 ---- \n"); 
  //   for (std::map<std::string,double>::iterator it = opNameMarkers.maxGapVariation.begin(); it!= opNameMarkers.maxGapVariation.end(); it++)
  //   {
  //     std::printf(" Value for treated : %s : %f \n", it->first.c_str(), opNameMarkers.maxGapVariation[it->first]);
  //   }

  // }
  

  }

/**
 * @brief Try to associate the Optitrack markers defined inside opNameMarkers with the Opensim markers
 * defined inside osMod.
 * 
 * The Opensim markers that should be followed is fixed by osMarkersNotAssigned variable.
 * - suppress check ac distance :
    - Dès deb, pr ts assos, faire IK 
    - Ensuite, pr chqe partie crps : 
        * ICP (UNIQUEMENT)
        * IK
    - Enfin, pr chqe partie crps (où pas d'assos) :  
        * Check distance, faire assos selon distance
        * A chaque réassos de bodyPart, faire IK
    - IK finale envoyée

 * @param opNameMarkers 
 * @param osMod 
 * @param corresNameID 
 * @param numOsNotAssigned : number of opensim markers that are not assigned during this frame
 * @param osMarkersNotAssigned 
 * @param called 
 */
  void markersAssociation(  optitrack_full::OptitrackNameMarkers & opNameMarkers,
  fosim::OsimModel & osMod,
  std::map<std::string,int> & osMarkersNotAssigned,
  bool & IK_possible,
  int & numOsNotAssigned,
  bool & called)
  {

    opNameMarkers.noMoreAssosPossible = false;
    int numOsAssigned = 0;
    bool verbose = opNameMarkers.association_verbose;
    double mdc = opNameMarkers.minima_distance_coeff;


    std::map<std::string, std::vector<std::string> > current_D_body_markers;
    // update the rigid body markers to take into account only the markers in osMarkersNotAssigned
    for (std::map<std::string,int>::iterator it3 = osMarkersNotAssigned.begin(); it3!= osMarkersNotAssigned.end(); it3++)
    {
      if (current_D_body_markers.find(it3->first) == current_D_body_markers.end() )
      {
        current_D_body_markers[osMod.D_markers_body[it3->first]] = osMod.D_body_markers[osMod.D_markers_body[it3->first]];
      }
    }

    std::map<int, Eigen::Vector3d > opMarkers;
    
    opMarkers = opNameMarkers.lastOptitrackMarkersNotAssigned;
    std::map<int,Eigen::Vector3d >::iterator it2; 

    std::map<std::string,int> corresNameID = opNameMarkers.corresNameID;
    std::map<int,std::string>::iterator itname;

    // check if a marker is missing or not
    int mapindex = 0;
    std::map<int,Eigen::Vector3d >::iterator it; 
    //ROS_INFO("ASSOS");
    //markersManagement::printAssociation(corresNameID);    
    
    //if (verbose) { ROS_INFO("BEGIN ASSOS"); }


    bool start = true;

  


    //bool aCINConcerned = (opNameMarkers.repeat && opNameMarkers.allCorresIDName.find(opNameMarkers.frameNumber) != opNameMarkers.allCorresIDName.end() );

    // --- first marker association

    while (mapindex < opMarkers.size())
    {
      if (verbose && start)
      {
        ROS_INFO("Number of markers in frame %d : %ld", opNameMarkers.frameNumber, opMarkers.size());
        start = false;
      }
      it = opMarkers.begin();
      std::advance(it,mapindex);
      int id = it->first;
      Eigen::Vector3d marker = it->second;
      //std::cout << "current ID : " << id << std::endl;
      auto itname = std::find_if(std::begin(corresNameID), std::end(corresNameID), [&](const std::pair<std::string,int> &pair)
      {
          return pair.second == id;
      });

      if (itname != corresNameID.end() ) 
      {
        // marker already identified, can be add directly
        std::string osname = itname->first;
        opNameMarkers.opensimMarkersPos[osname] = marker;
        it2 = opMarkers.find(id);
        opMarkers.erase(it2);
        if (verbose && opNameMarkers.lastOsMarkersNotAssigned[osname] == 1) {

          printf("Classic (frame %d) : Associate %s with %d . \n",opNameMarkers.frameNumber, osname.c_str(), id); 
          
        }
        osMarkersNotAssigned[osname] = 0;
        numOsAssigned +=1;
        opNameMarkers.markersWeights[osname] = 1.0*osMod.D_markers_weight[osname];
        mapindex--;

      }
      else if (opNameMarkers.repeat)
      {
        std::string osname;
        int framePicked;
        bool vb = false;//true;//(opNameMarkers.frameNumber <= 3000);
        if (vb)
        {
          printf("In association step : \n");
        }
        bool found = markersManagement::checkACINCorres(id, opNameMarkers.frameNumber, opNameMarkers.allCorresIDName, osname, framePicked, opNameMarkers.repeat, vb);

        if (found)
        { 
          opNameMarkers.opensimMarkersPos[osname] = marker;
                          
          // if the id was detected at repeat step, must mean that this id
          // should now be the current id associated with the Opensim marker
          if (verbose && (opNameMarkers.lastOsMarkersNotAssigned[osname] == 1 || id != corresNameID[osname]) ) {  called = true; printf("Repeat : associate %s with %d . \n",osname.c_str(), id); }

          opNameMarkers.corresNameID[osname] = id;

          it2 = opMarkers.find(id);
          opMarkers.erase(it2);

          osMarkersNotAssigned[osname] = 0;
          numOsAssigned +=1;
          opNameMarkers.markersWeights[osname] = 1.0*osMod.D_markers_weight[osname];

          mapindex--;
        }
      }
      mapindex++;

    }



    // if used, means that one marker must have been lost. Let's deal with it.
    verbose = opNameMarkers.association_verbose;

    numOsNotAssigned = 0;
    std::map<std::string,bool> enoughOsAssigned;
    std::map<std::string,int> numOsByBodyPart;
    IK_possible = checkIKPossible(osMarkersNotAssigned,current_D_body_markers,numOsNotAssigned, enoughOsAssigned, numOsByBodyPart, false);

    // first recovery procedure : try with ICP
    bool recount = false;

    bool retryIK = true;
    if (numOsAssigned != osMarkersNotAssigned.size() && opMarkers.size() != 0)
    {

      std::map<int,std::string> fixedAssos = opNameMarkers.CINCalib;
      for (std::map<std::string,int>::iterator it =  opNameMarkers.corresNameID.begin();
      it!= opNameMarkers.corresNameID.end();
      it++ )
      {
        int id = it->second;
        if (fixedAssos.find(id) == fixedAssos.end())
        {
          std::string name;
          int framePicked;
          bool found = markersManagement::checkACINCorres(id,opNameMarkers.frameNumber,
          opNameMarkers.allCorresIDName, name, framePicked);
          if (found)
          {
            LabelCarac lc = opNameMarkers.allCorresIDName[id][framePicked];
            if (lc.byUser)
            {
              // add as fixed association the name<->id set by users.
              fixedAssos[id] = name;
            }
          }
        }
      }



      opNameMarkers.constraintWeight = opNameMarkers.initConstraintWeight;

      //std::vector<std::string> followedMarkers={"HL1","HL2","HL3","HL4","LRS","LUS","LWJC","LWJC2","LLHE","LMHE","LEJC","LEJC2"};

      for (std::map<std::string,double>::iterator  it = opNameMarkers.markersWeights.begin(); it !=opNameMarkers.markersWeights.end(); it++)
      {
        if (osMarkersNotAssigned[it->first] == 0)
        {
          opNameMarkers.markersWeights[it->first] = it->second*opNameMarkers.constraintWeight;
        }
        else
        {
        opNameMarkers.markersWeights[it->first] = 0.0;
        }
      }   

      markersManagement::globalPublisher globalPublish;
      ros::Time time_pub = ros::Time::now();
      // try other way : from parent to child (= root to children)
      //std::vector<std::string> bodyPartsOrder = osMod.markers_bodies_multiorder;
      // try otherwise : 
      //  * first, body parts with the most markers at start
      //  * among those body parts, do from parent to child
      std::vector<std::string> bodyPartsOrder = opNameMarkers.bodies_label_order;

      for (int i = 0; i < bodyPartsOrder.size(); i++)
      {
        if (retryIK)
        {
       
          osMod.IK(opNameMarkers.opensimMarkersPos,opNameMarkers.markersWeights, 1,opNameMarkers.lastOptitrackTime.toSec()-opNameMarkers.originTime.toSec());
          retryIK = false;
        }

        std::map<std::string,bool>::iterator it = enoughOsAssigned.find(bodyPartsOrder[i]);
        if (it!= enoughOsAssigned.end())
        {
          

          
        //   }
        // }

        // for (std::map<std::string,bool>::iterator it = enoughOsAssigned.begin(); it!= enoughOsAssigned.end(); it++)
        // {
          double tolICP = 0.0021;
          bool useICP = false;
          // case where at least 3 markers of the body were identified, but other are missing,
          // and we still have optitrack markers that are not assigned
          // the idea here is to simulate the position of the missing markers and to selected the closest optitrack markers
          // from those missing markers, in order to accelerate the association.
          int numMiss = osMod.D_body_markers[it->first].size()- numOsByBodyPart[it->first];
          int numMarkInBody = osMod.D_body_markers[it->first].size();
          int numMarkAssos = (numMarkInBody - numMiss);              
          if (it->second && numMiss !=0 && opMarkers.size() != 0 )
          // test : try to always ignore this step, as next step is more robust now
          // (even if more costly)
          //if (false)
          {
            if (verbose) {std::cout << "3 markers debuging procedure for : " << it->first << std::endl;}
            std::vector<std::string> markersBodyNames = osMod.D_body_markers[it->first];
            std::map<std::string,Eigen::Vector3d> currentMarkersBody;
            std::vector<std::string> missMarkers;
            std::map<std::string,Eigen::Vector3d> originMarkersBody;

            for (int l= 0; l < markersBodyNames.size(); l++)
            {
              if (osMarkersNotAssigned[markersBodyNames[l] ] == 0)
              {
                currentMarkersBody[markersBodyNames[l]] = opNameMarkers.opensimMarkersPos[markersBodyNames[l]];
              }
              else
              {
                missMarkers.push_back(markersBodyNames[l]);
              }
              originMarkersBody[markersBodyNames[l]] = osMod.D_mark_pos_local[markersBodyNames[l]];

            }
            recoveryMarkersProcedure(currentMarkersBody, originMarkersBody, globalPublish, time_pub, false);

            int j = 0;
            int opti_index;
            while (j < missMarkers.size() && opMarkers.size() != 0)
            {            
              std::string treatedMark = missMarkers[j];
              if (verbose) {printf("Recovery Procedure for marker : %s \n", treatedMark.c_str());}

              double min_osim_dist = 99999;
              double max_osim_dist = 0.0;
              for (std::map<std::string,double>::iterator it2 = osMod.D_mark_all_dist[treatedMark].begin(); it2 != osMod.D_mark_all_dist[treatedMark].end(); it2++)
              {
                if (it2->second < min_osim_dist)
                {
                  min_osim_dist = it2->second;
                }
                if (it2->second > max_osim_dist)
                {
                  max_osim_dist = it2->second;
                }
              }
              double closest_diff = min_osim_dist/mdc;
              if (opNameMarkers.lastOsMarkersNotAssigned[treatedMark] == 1 && numOsNotAssigned == 1 && opMarkers.size() == 1)
              {
                // we hugely increase tolerance in this obvious case
                closest_diff = max_osim_dist;
              }

              Eigen::Vector3d simulatedPos = currentMarkersBody[treatedMark];

              if (verbose) {
                std::cout << "Simulated position : " << simulatedPos << std::endl;
                std::cout << "Closest diff : " << closest_diff << std::endl;
              }
              int id_selected = -1;
              std::map<int,Eigen::Vector3d >::iterator itopti;
              opti_index = 0;
              while (opti_index < opMarkers.size())
              {

                itopti = opMarkers.begin();
                std::advance(itopti,opti_index);

                double d = (itopti->second-simulatedPos).norm();

                if (verbose) {std::printf("Candidate : %d ; distance : %f. \n", itopti->first, d);}

                if (d <= closest_diff)
                {
                  closest_diff = d;
                  id_selected = itopti->first;
                }
                opti_index++;
              }
              // an optitrack element respecting those conditions has been
              // selected, congrats!
              if (id_selected != -1)
              {
                if (verbose) {printf("Found association (after recovery procedure) : index %d with marker %s. \n", id_selected, treatedMark.c_str());}
                opNameMarkers.corresNameID[treatedMark] = id_selected;
                manageRepeatAssociation( treatedMark, id_selected, opNameMarkers);           

                opNameMarkers.opensimMarkersPos[treatedMark] = opMarkers[id_selected];
                opMarkers.erase( opMarkers.find(id_selected) );
                osMarkersNotAssigned[treatedMark] = 0;
                opNameMarkers.markersWeights[treatedMark] = 1.0*osMod.D_markers_weight[treatedMark]*opNameMarkers.constraintWeight;
                // numOsNotAssigned -=1;
                // numOsByBodyPart[it->first] +=1;

                retryIK = true;

                recount = true;

              }
              else
              {
                if (verbose) {printf("No association found (after recovery procedure) for marker %s. \n", treatedMark.c_str());}
                if (opNameMarkers.lastOsMarkersNotAssigned[treatedMark] == 1)
                {
                  // case looks more obvious; thus, use ICP now for this step, but with very small tolerance
                  tolICP = 0.0015;
                  useICP = true;
                  if (verbose)
                  {
                  std::cout << "part : " << it->first << " : use ICP" << std::endl;
                  }
                }
                // else
                // {
                //   std::cout << it->first << " : Not ICP because :" << std::endl;
                //   std::cout << opNameMarkers.lastOsMarkersNotAssigned[treatedMark] << std::endl;
                //   std::cout << numOsNotAssigned << std::endl;
                // }
              }
              j++;
            }
          }
        
          // case where there is less than 3 markers identified in the body, but there is still enough 
          // optitrack markers not assigned to try to assign them with this body part.

          // test : to force to go to this step, ignore enoughOSAssigned (previously)
        // else if (!it->second && opMarkers.size() != 0 && opMarkers.size() >= numMiss)
          // test : if there is a possibility of ICP (e.g, if the currently associated points + the not associated optitrack points
          // have a sufficent number for ICP), try it
          else if (!it->second && opMarkers.size() != 0 && ( numMarkAssos +opMarkers.size()  >= 3 && numMiss > 0) )
          //else if (opMarkers.size() >= numMiss)        
          {
            useICP = true;
          }

          if (useICP)
          {

            // test : add to tolICP maxGapVar
            double max_var_local = 0;

            //opNameMarkers.manParams.filtering_verbose = true;
            if (opNameMarkers.manParams.filtering_verbose) { std::cout << "Less than 3 markers debuging procedure for : " << it->first << std::endl; }
            std::vector<std::string> markersBodyNames = osMod.D_body_markers[it->first];          
            std::map<std::string,Eigen::Vector3d> localBodyPos;
            std::map<std::string,int> localCorresNameID;

            for (int i = 0; i < markersBodyNames.size(); i++)
            {
              std::string name = markersBodyNames[i];
              localBodyPos[name] = osMod.D_mark_pos_local[name];
              localCorresNameID[name] = opNameMarkers.corresNameID[name];
              if (osMarkersNotAssigned[name] == 0)
              {
                //if (verbose) { std::cout << "Add position of element " << name << "as it is still association with optitrack" << std::endl;}
                opMarkers[opNameMarkers.corresNameID[name] ] =  opNameMarkers.opensimMarkersPos[name];
              }
              if (opNameMarkers.maxGapVariation[name] > max_var_local)
              {
                max_var_local = opNameMarkers.maxGapVariation[name];
              }

            }
            tolICP+= max_var_local;
            if (opNameMarkers.manParams.filtering_verbose) { std::cout << "Chosen tol ICP : " << tolICP << std::endl;}

            // if there is parent and parent has enough markers for IK, authorize to try to associate 
            // markers by ICP even if we do not have enough optitrack markers

            // for all rigid bodies
            // TODO : check bug where after cleanUp by user and reassos, ACIN[-1] is not reset at good value
            // activer debugICP ap cleanUp par user, parce que trop bizarre
            int result = markersManagement::bodyMarkerRecoveryByICP(opNameMarkers.manParams, localBodyPos, opMarkers, osMod, localCorresNameID,tolICP, opNameMarkers.debugPub, fixedAssos); // 0.01

            //opNameMarkers.manParams.filtering_verbose = false;
            if (result >= 1 )
            {
              for (std::map<std::string,int>::iterator it = localCorresNameID.begin(); it!= localCorresNameID.end(); it++)
              {
                std::string treatedMark = it->first;
                int id_selected = it->second;

                std::map<int,Eigen::Vector3d>::iterator itn = opMarkers.find(id_selected);

                if (itn != opMarkers.end())
                {
                  opNameMarkers.corresNameID[treatedMark] = id_selected;
                  if (result == 1 || result == 2)
                  {
                    // new information, need to add it

                    manageRepeatAssociation( treatedMark, id_selected, opNameMarkers, (result == 2));    
                  }

                  opNameMarkers.opensimMarkersPos[treatedMark] = opMarkers[id_selected];
                  opMarkers.erase( itn );
                  osMarkersNotAssigned[treatedMark] = 0;
                  opNameMarkers.markersWeights[treatedMark] = 1.0*osMod.D_markers_weight[treatedMark]*opNameMarkers.constraintWeight;
                }

              }
              retryIK = true;
              if  (opNameMarkers.manParams.filtering_verbose) 
              {
              std::cout << "--- After ICP check ---" << std::endl;
              markersManagement::printAssociation(localCorresNameID);
              std::cout << "Debug for recovery By ICP (frameNumber :  "<< opNameMarkers.frameNumber << " )" << std::endl;
              }
              if (opNameMarkers.debugAssos)
              {

    
                std::string debRec;
              std::cin >> debRec;
            //  opNameMarkers.pause_mode = true;

              }
              recount = true;

            }
            else if (result == -1)
            {
              std::cout << "Debug for recovery By ICP - after bug" << std::endl;
              //  opNameMarkers.pause_mode = true;
              //  ros::spinOnce();      
              // std::string debRec;
              // std::cin >> debRec;                    
            }

              // cleanUp previous associations made
              // (to be sure that already associated elements will not stay
              // in opMarkers)
              for (int i = 0; i < markersBodyNames.size(); i++)
              {
                std::string name = markersBodyNames[i];
                
                std::map<int,Eigen::Vector3d>::iterator it_assos = opMarkers.find(opNameMarkers.corresNameID[name]);
                if (osMarkersNotAssigned[name] == 0 && it_assos != opMarkers.end())
                {
                  
                  opMarkers.erase(it_assos);
                }
              }
            }
        }
      }
    }


    numOsNotAssigned = 0;
    enoughOsAssigned.clear();
    numOsByBodyPart.clear();
    IK_possible = checkIKPossible(osMarkersNotAssigned,current_D_body_markers,numOsNotAssigned, enoughOsAssigned, numOsByBodyPart, false);
    // second recovery procedure : try with distance

    if (numOsNotAssigned != 0 && opMarkers.size() != 0)
    {
      retryIK = true;
      if (retryIK)
      {
      
        osMod.IK(opNameMarkers.opensimMarkersPos,opNameMarkers.markersWeights, 1,opNameMarkers.lastOptitrackTime.toSec()-opNameMarkers.originTime.toSec());
        retryIK = false;
      }



      if (verbose) {called = true; std::cout << "At least one marker not assigned, try to fix it ... " << std::endl; }
      std::map<std::string,int>::iterator itmap;
      std::map<int,Eigen::Vector3d >::iterator itopti;
      int map_index = 0;
      int opti_index = 0;

      while (map_index < osMarkersNotAssigned.size()){
        itmap = osMarkersNotAssigned.begin();
        std::advance(itmap,map_index);
        if (itmap->second!=0)
        {

          if (verbose) {std::cout << "Check missing marker : " << itmap->first << std::endl; }

          std::string osMarkName = itmap->first;
          // distance of missing opensim marker from each other opensim markers of the same body.
          std::map< std::string, double> markersAssociated = osMod.D_mark_all_dist[osMarkName];

          double min_dist_osim = 9999;
          for (std::map<std::string, double>::iterator itmin = markersAssociated.begin(); itmin!=markersAssociated.end(); itmin++)
          {
            if (itmin->second < min_dist_osim)
            {
              min_dist_osim = itmin->second;
            }
          }

          // each optitrack markers not associated are candidate for the Opensim marker not associated.
          std::map<int,Eigen::Vector3d> opti_candidate = opMarkers;
          double max_change = opNameMarkers.maxGapVariation[osMarkName];
          double distMV = 0.003;
          double thresMV = std::sqrt((distMV)*(distMV)*3);
          if (max_change > thresMV)
          {
            max_change = thresMV;
          }

          if (verbose) {std::printf("Parameters : max variation of position : %f; number candidates at first : %ld; could be check with %ld opensim markers \n", max_change, opti_candidate.size(), markersAssociated.size());}

          int mark_index = 0;
          std::map< std::string, double>::iterator itmark;
          int numAssos = 0;
          bool atLeastTwo = false;

          while (mark_index< markersAssociated.size() && opti_candidate.size() >0)
          {
            // for each opensim marker associated with the missing opensim marker
            // (= marker on same body)
            itmark = markersAssociated.begin();
            std::advance(itmark,mark_index);

            // if (osMarkersNotAssigned[itmark->first] == 0)
            // {

              // if this marker is still associated, compare its theorical distance
              // from the missing opensim marker to the distance betwwen the missing
              // opensim marker and the optitrack markers that are not associated.
              double min_reject = 9999;
              bool madeOneAssos = false;
            
              double radius = itmark->second;
              if (verbose) {std::printf("Candidate : %s ; radius : %f. \n", itmark->first.c_str(), radius);}
              Eigen::Vector3d cur_os_pos;
              if (opNameMarkers.lastOsMarkersNotAssigned.size() > 0 && opNameMarkers.lastOsMarkersNotAssigned[itmark->first] == 0)//(opNameMarkers.opensimMarkersPos.find(itmark->first) != opNameMarkers.opensimMarkersPos.end())
              {
                 //if (verbose) {std::cout << "Marker opensim chosen  by last Assos"<< std::endl;}
    
                cur_os_pos = opNameMarkers.opensimMarkersPos[itmark->first];
              }
              else
              {
                //if (verbose) {std::cout << "Marker opensim chosen  by IK guess"<< std::endl;}
                cur_os_pos = osMod.D_mark_pos[itmark->first];
              }
                if (verbose) {std::cout << "Marker opensim chosen : " << cur_os_pos << std::endl;}
    
              std::map<int,Eigen::Vector3d >::iterator itopti2;
              opti_index = 0;
              while (opti_index < opti_candidate.size())
              {
                itopti = opti_candidate.begin();
                std::advance(itopti,opti_index);
                double d = (cur_os_pos - itopti->second).norm();
                if (verbose) {std::cout << "Marker optitrack " << itopti->first << " : distance : " << d << std::endl;}
    
                // if the difference is superior to all recorded variations of distance of the 
                //  theorical marker --> cancel it.
                if (abs(d-radius) > max_change)
                {
                  if (verbose){ std::cout << "Rejected. " << std::endl;}
                  itopti2 = opti_candidate.find(itopti->first);
                  opti_candidate.erase(itopti2);
                  opti_index--;
                }
                else
                {
                  if (abs(d-radius) < min_reject)
                  {
                    //if (verbose) {std::cout << "Marker optitrack " << itopti->first << " : distance : " << d << std::endl;}
                    if (verbose){ std::cout << "Accepted. " << std::endl;}
                    min_reject = abs(d-radius);
                    if (!madeOneAssos)
                    {
                      madeOneAssos = true;
                      numAssos +=1;
                    }
                  }

                }

                opti_index++;

              }
              if (verbose)
              {
                printf("After this selection, number of candidates : %ld \n", opti_candidate.size());
                printf("Min reject for this one : %f. \n", min_reject);
              }

            //}
            mark_index++;
            
          }

           
          int size = markersAssociated.size();
          atLeastTwo = (numAssos >= std::min(2,size) );
          Eigen::Vector3d selected_pose; selected_pose.setZero();
          Eigen::Vector3d guess_pose;
          if (opNameMarkers.lastOsMarkersNotAssigned.size() > 0 && opNameMarkers.lastOsMarkersNotAssigned[osMarkName] == 0)//(opNameMarkers.opensimMarkersPos.find(itmark->first) != opNameMarkers.opensimMarkersPos.end())
          {
              if (verbose) {std::cout << "Marker opensim guess by last Assos"<< std::endl;}

            guess_pose = opNameMarkers.opensimMarkersPos[osMarkName];
          }
          else
          {
            if (verbose) {std::cout << "Marker opensim guess by IK"<< std::endl;}
            guess_pose = osMod.D_mark_pos[osMarkName];
          }

          if (opti_candidate.size() == 1 && atLeastTwo)
          {
            // one association is possible, and it was checked by at least with two Opensim markers
            // that are still associated; thus, this association might be correct.
            std::map<int,Eigen::Vector3d >::iterator itselect = opti_candidate.begin();
            selected_pose = itselect->second;
            int selected_id = itselect->first;
            if (verbose)
            {
              printf("Found association : index %d with marker %s. \n", selected_id, itmap->first.c_str());
            }
            opNameMarkers.corresNameID[osMarkName] = selected_id;
            manageRepeatAssociation(osMarkName, selected_id, opNameMarkers);                   

            opMarkers.erase( opMarkers.find(selected_id) );
            osMarkersNotAssigned[osMarkName] = 0;
            opNameMarkers.markersWeights[osMarkName] = 1.0*osMod.D_markers_weight[osMarkName]*opNameMarkers.constraintWeight;
            opNameMarkers.opensimMarkersPos[osMarkName] = selected_pose;

            retryIK = true;

            map_index = -1;

          }
          else if (opti_candidate.size()>1 && atLeastTwo)
          {
            // second recovery procedure : closest to last recorded position of opensim marker
            // and then, check if this position make sense compared to theorical distances.

            // in consequence, reduce weight associated with marker
            if (verbose)
            {
              std::cout << "More than one marker selected : loading second detection ... " << std::endl;
            }

            

            double min_dist2 = INFINITY;
            std::map<int,Eigen::Vector3d >::iterator it_candidate;


            opti_index = 0;


            // now, get the closest candidate.
            while (opti_index < opti_candidate.size())
            {
              itopti = opti_candidate.begin();
              std::advance(itopti,opti_index);
              double d = (guess_pose - itopti->second).norm();
              if (d < min_dist2)
              {
                min_dist2 = d;
                it_candidate = itopti;
              }
              opti_index++;
            }

            selected_pose = it_candidate->second;
            int selected_id = it_candidate->first;

            if (verbose) {printf("Found association (after second detection) : index %d with marker %s. \n", selected_id, itmap->first.c_str());}
            opNameMarkers.corresNameID[osMarkName] = selected_id;
            manageRepeatAssociation(osMarkName, selected_id, opNameMarkers);           

            opMarkers.erase( opMarkers.find(selected_id) );
            osMarkersNotAssigned[osMarkName] = 0;
            opNameMarkers.markersWeights[osMarkName] = 1.0*osMod.D_markers_weight[osMarkName]*opNameMarkers.constraintWeight;
            opNameMarkers.opensimMarkersPos[osMarkName] = selected_pose;

            retryIK = true;

          }
          else if (!atLeastTwo && opti_candidate.size() == 1)
          {
            std::map<int,Eigen::Vector3d >::iterator itselect = opti_candidate.begin();
        
            int selected_id = itselect->first;
            // try by comparing position of opensim marker to IK

           if (verbose)
            {
              std::cout << "Try to find by IK check ... " << std::endl;
              std::cout << "By IK : " << guess_pose << std::endl;
              std::cout << "Distance from selected : " << (itselect->second - guess_pose).norm()<< std::endl;
            }            

            if (( (itselect->second - guess_pose).norm() )<  max_change)
            {
              //authorize change
              selected_pose = itselect->second;    
              if (verbose) {printf("Found association (after IK check) : index %d with marker %s. \n", selected_id, itmap->first.c_str());}
              opNameMarkers.corresNameID[osMarkName] = selected_id;
              manageRepeatAssociation(osMarkName, selected_id, opNameMarkers);           

              opMarkers.erase( opMarkers.find(selected_id) );
              osMarkersNotAssigned[osMarkName] = 0;
              opNameMarkers.markersWeights[osMarkName] = 1.0*osMod.D_markers_weight[osMarkName]*opNameMarkers.constraintWeight;
              opNameMarkers.opensimMarkersPos[osMarkName] = selected_pose;

              retryIK = true;
            
            }              

            else
            {
              // otherwise, the marker is disqualified for now
              opNameMarkers.opensimMarkersPos[osMarkName] = selected_pose;
              opNameMarkers.markersWeights[osMarkName] = 0.0;
              // raise a little bit the tolerance
              if (verbose) { printf("Raise tolerance from : %f", opNameMarkers.maxGapVariation[osMarkName]); }
              // cvheck si block ici ou non
              opNameMarkers.maxGapVariation[osMarkName] = std::max( std::min( pow(10.0,1.0/opNameMarkers.frameRate)*opNameMarkers.maxGapVariation[osMarkName], min_dist_osim/(mdc) ),  opNameMarkers.maxGapVariation[osMarkName] ); 
              if (verbose) { printf(" to : %f. \n", opNameMarkers.maxGapVariation[osMarkName]); }
            }
          }
          else if (atLeastTwo)
          {
            // otherwise, the marker is disqualified for now
            opNameMarkers.opensimMarkersPos[osMarkName] = selected_pose;
            opNameMarkers.markersWeights[osMarkName] = 0.0;
            // raise a little bit the tolerance
            if (verbose) { printf("Raise tolerance from : %f", opNameMarkers.maxGapVariation[osMarkName]); }
            // cvheck si block ici ou non
            opNameMarkers.maxGapVariation[osMarkName] = std::max( std::min( pow(10.0,1.0/opNameMarkers.frameRate)*opNameMarkers.maxGapVariation[osMarkName], min_dist_osim/(mdc) ),  opNameMarkers.maxGapVariation[osMarkName] ); 
            if (verbose) { printf(" to : %f. \n", opNameMarkers.maxGapVariation[osMarkName]); }
          }

        }
        map_index++;

      }


    }
      


    if (opNameMarkers.simulation)
    {

      markersManagement::checkSimulationAssociation(opNameMarkers.corresNameID,opNameMarkers.simu_gest.hiddenCorresNameID, opNameMarkers.error_pub,opNameMarkers.error_verbose);
      
    }
    
    if (recount)
    {
      numOsNotAssigned = 0;
      enoughOsAssigned.clear();
      numOsByBodyPart.clear();
      IK_possible = checkIKPossible(osMarkersNotAssigned,current_D_body_markers,numOsNotAssigned, enoughOsAssigned, numOsByBodyPart);
    }

    bool checkCalled = ( (opMarkers.size() >0 && numOsNotAssigned>0  ) || called);

    if (verbose || (checkCalled) )
    {
      if (!opNameMarkers.never_verbose)
      {
        called = true;
        ROS_INFO("Frame : %d", opNameMarkers.frameNumber);
        ROS_INFO("Number of Opensim markers not assigned : %d. \n", numOsNotAssigned);
        markersManagement::checkAssignedMarkers(osMarkersNotAssigned, osMod.D_markers_body, true);
        ROS_INFO("Number of Optitrack markers not assigned : %ld. \n", opMarkers.size() );
        for (std::map<int,Eigen::Vector3d>::iterator it = opMarkers.begin(); it!= opMarkers.end(); it++)
        {
          std::cout << it->first << " ; ";
        }
        std::cout << std::endl;
        std::cout << "IK possible : " << IK_possible << std::endl;

      }
    }
    if (recount && IK_possible)
    {
      opNameMarkers.framesSinceAssemble = opNameMarkers.limFramesSinceAssemble;
    }

    if (numOsNotAssigned == 0 || opMarkers.size() == 0)
    {
      // for this frame, no more associations are possible
      opNameMarkers.noMoreAssosPossible = true;
    }

    // D_mark_all_dist = D_mark_all_dist_assigned = opensim markers pos : actual position, corresponds to position of attributed Optitrack markers    

    // D_mark_all_dist_ori : previous distance in position of markers.

          // // if IK was possible without issue, use it to get infos about the different markers
          // std::map<std::string, std::map<std::string, double> > D_mark_all_dist = fosim::getMarkerDistancesInBody(osMod,opNameMarkers.opensimMarkersPos);
          // // use position of markers obtained by IK instead (RMQ : TOO MANY BAD ASSOCIATIONS WITH THAT)
          // //std::map<std::string, std::map<std::string, double> > D_mark_all_dist = fosim::getMarkerDistancesInBody(osMod,osMod.D_mark_pos);

          // std::map<std::string, std::map<std::string, double> > D_mark_all_dist_assigned = D_mark_all_dist;
          // std::map<std::string, std::map<std::string, double> > D_mark_all_dist_origin = opNameMarkers.D_mark_all_dist;
          // std::map<std::string, std::map<std::string, double> >::iterator it_map_dist;
          // std::map<std::string, std::map<std::string, double> >::iterator it_map_ori;
          // int index_deb = 0;
          // int comp = 0;
          // while (comp < D_mark_all_dist_assigned.size() )
          // {
          //   it_map_dist = D_mark_all_dist_assigned.begin();

          //   std::advance(it_map_dist,comp);
          //   // std::cout << "bmld" << std::endl;
          //   // std::cout << comp << std::endl;
          //   // std::cout << enoughOsAssigned[osMod.D_markers_body[it_map_dist->first]] << std::endl;

          //   // suppress tolerance check for body part where IK impossible
          //   // (i.e not enough markers)
          //   // also suppress tolerance check for marker not assigned
          //   //if (!enoughOsAssigned[osMod.D_markers_body[it_map_dist->first]] || osMarkersNotAssigned[it_map_dist->first] == 1 )
          //   //  do not use enoughOsAssigned : could be false right now
          //   if ( osMarkersNotAssigned[it_map_dist->first] == 1 )
          //   {
          //     it_map_ori = D_mark_all_dist_origin.begin();     
        
          //     std::advance(it_map_ori,comp);        

          //     D_mark_all_dist_assigned.erase(it_map_dist);

          //     D_mark_all_dist_origin.erase(it_map_ori);

          //     comp--;
              
          //   }
          //   // maj of position for attributed markers
          //   else
          //   {
          //     opNameMarkers.D_mark_all_dist[it_map_dist->first] = it_map_dist->second;
          //   }


          //   // index_deb ++;
          //   // if (index_deb%100 == 0)
          //   // {
          //   //   std::cout << "D_mark_all_dist_assigned bug" << std::endl;
          //   // }
          //   comp++;


          // if (verbose)
          // {

          //   std::printf("---- Resume before ---- \n"); 
          //   for (std::map<std::string,double>::iterator it = opNameMarkers.maxGapVariation.begin(); it!= opNameMarkers.maxGapVariation.end(); it++)
          //   {
          //     std::printf(" Value for treated : %s : %f \n", it->first.c_str(), opNameMarkers.maxGapVariation[it->first]);
          //   }

          // }

          //std::cout << D_mark_all_dist_origin.size() << std::endl;
          //std::cout << D_mark_all_dist_assigned.size() << std::endl;
          //fosim::printMarkerDistancesInBody(osMod.osimModel,osMod.osimState);
          // markersManagement::allDistanceVariationDynamic(D_mark_all_dist_origin,D_mark_all_dist_assigned,osMod.D_mark_all_dist,opNameMarkers.maxGapVariation, opNameMarkers.maximum_residual, 1.0/opNameMarkers.frameRate, mdc, false);


          // markersManagement::publishToleranceValue(opNameMarkers.maxGapVariation,opNameMarkers.error_pub, opNameMarkers.error_verbose);




      //  }




    // note which markers are associated

    if (opNameMarkers.lastOsMarkersNotAssigned.size() > 0)
    {

      for (std::map<std::string,int>::iterator it = osMarkersNotAssigned.begin(); it!= osMarkersNotAssigned.end(); it++)
      {
        if ( (opNameMarkers.lastOsMarkersNotAssigned[it->first] == 0) && (osMarkersNotAssigned[it->first] == 1) )
        {
          opNameMarkers.lastFrameOsAssos[it->first] = std::max(opNameMarkers.frameNumber-1,0);
        }

      }
    }

    // also suppress object that are now associated whereas they weren't in past
    int l = 0;
    std::map<std::string,int>::iterator itc = opNameMarkers.lastFrameOsAssos.begin();
    while (l < opNameMarkers.lastFrameOsAssos.size())
    {
      itc = opNameMarkers.lastFrameOsAssos.begin();
      std::advance(itc,l);
      if ( (opNameMarkers.lastOsMarkersNotAssigned[itc->first] == 1) && (osMarkersNotAssigned[itc->first] == 0) )
      {
        opNameMarkers.lastFrameOsAssos.erase(itc);
      }
      else
      {
        l++;
      }
    }
  

    opNameMarkers.lastOsMarkersNotAssigned = osMarkersNotAssigned;
  // if (verbose)
  // {

  //   std::printf("---- Resume out 2 ---- \n"); 
  //   for (std::map<std::string,double>::iterator it = opNameMarkers.maxGapVariation.begin(); it!= opNameMarkers.maxGapVariation.end(); it++)
  //   {
  //     std::printf(" Value for treated : %s : %f \n", it->first.c_str(), opNameMarkers.maxGapVariation[it->first]);
  //   }

  // }
  

  }



  bool isIDConcernedByInversion( optitrack_full::OptitrackNameMarkers& opNameMarkers,
  int optiID,
  std::pair<std::string,int> & markCorrection)
  {
    bool inversionTrig = false;
    if (opNameMarkers.repeat) 
    {
      int frameNumber = opNameMarkers.frameNumber;
      std::map< int, std::map< int,std::pair<std::string,int> > >::iterator it = opNameMarkers.inverseIDs.find(frameNumber);
      if (it != opNameMarkers.inverseIDs.end())
      {
         std::map< int,std::pair<std::string,int> >::iterator it2 = it->second.find(optiID);
         if (it2 != it->second.end())
         {
           
          markCorrection.first = it2->second.first;
          markCorrection.second = it2->second.second;

          inversionTrig = true;
         }
      }    
    }
    return(inversionTrig);

  }



  void manageRepeatAssociation( std::string treatedMark, int id_selected,
  optitrack_full::OptitrackNameMarkers& opNameMarkers,
  bool canClean
  )
  {
    bool vb = canClean;
    int frame = opNameMarkers.frameNumber;
    std::map<std::string,int>::iterator it = opNameMarkers.lastFrameOsAssos.find(treatedMark);
    if ( it != opNameMarkers.lastFrameOsAssos.end())
    {
      // changes selected frame to start association at the first time
      // where opensim marker was not assigned anymore
      frame = it->second+1;
      opNameMarkers.lastFrameOsAssos.erase(it);
    }

    markersManagement::manageACINAdding(treatedMark,id_selected, frame, opNameMarkers.allCorresIDName,  opNameMarkers.repeat,false,vb, canClean);
    // if (opNameMarkers.repeat) 
    // {
    //   std::map<int, std::map<int,std::string> >::iterator it = opNameMarkers.allCorresIDName.find(opNameMarkers.frameNumber);

    //   if (it == opNameMarkers.allCorresIDName.end())
    //   {
    //     std::map<int,std::string> mp;
    //     opNameMarkers.allCorresIDName[opNameMarkers.frameNumber] = mp;

    //   }

    //     std::map<int,std::string>::iterator it2 = opNameMarkers.allCorresIDName[opNameMarkers.frameNumber].find(id_selected);
    //     // first poss : this is the 1st time we have this association; thus, we add it
    //     if ( it2 == opNameMarkers.allCorresIDName[opNameMarkers.frameNumber].end() )
    //     {
    //       opNameMarkers.allCorresIDName[opNameMarkers.frameNumber][id_selected] = treatedMark;

    //     }

    // }


  }   


  void markersRecognitionAndIK(  optitrack_full::OptitrackNameMarkers& opNameMarkers,
  fosim::OsimModel & osMod)
  {   
  
  if (opNameMarkers.corresNameID.size() > 0 && opNameMarkers.foundCalibFrame == true)
  {
    //ROS_INFO("HEHRE");
    bool verbose = opNameMarkers.manParams.ik_verbose;
    bool called = false;
    std::map<std::string,int> osMarkersNotAssigned;
    bool tryRecalib;
    bool IK_possible;
    std::map<std::string, Eigen::Vector3d >::iterator ite;

    double mdc = opNameMarkers.minima_distance_coeff;

    //std::vector<std::string> osMarkNames;

    for ( ite =osMod.D_mark_pos.begin(); ite != osMod.D_mark_pos.end(); ite++)
    {
      osMarkersNotAssigned[ite->first] = 1;
      //osMarkNames.push_back(ite->first);
    }

    int numOsNotAssigned = 0;
    markersAssociation(opNameMarkers,osMod,osMarkersNotAssigned,IK_possible, numOsNotAssigned, called);

    // let's deal with IK

    tryRecalib = false;
    int result = -1;

    if (numOsNotAssigned > 0)
    {
      opNameMarkers.framesSinceAssemble += 1;
    }

    if ( IK_possible)
    {

      
      // all markers associated, let's do IK
      opNameMarkers.constraintWeight = opNameMarkers.initConstraintWeight;

      //std::vector<std::string> followedMarkers={"HL1","HL2","HL3","HL4","LRS","LUS","LWJC","LWJC2","LLHE","LMHE","LEJC","LEJC2"};

      for (std::map<std::string,double>::iterator  it = opNameMarkers.markersWeights.begin(); it !=opNameMarkers.markersWeights.end(); it++)
      {
        if (osMarkersNotAssigned[it->first] == 0)
        {
          opNameMarkers.markersWeights[it->first] = it->second*opNameMarkers.constraintWeight;
        }
        else
        {
         opNameMarkers.markersWeights[it->first] = 0.0;
        }
      }
      // tracking by default
      int mode = 0;
      if (opNameMarkers.framesSinceAssemble >= opNameMarkers.limFramesSinceAssemble)
      {
        // assemble activated
        mode = 1;
        opNameMarkers.framesSinceAssemble = 0;
      }
      if (called && verbose){ 
        std::cout << "IK mode (1 : from scratch; 0 : from coordinates) : " << mode << std::endl;
        fosim::printMarkersWeights(opNameMarkers.markersWeights);
        markersManagement::printAssociationByBody(opNameMarkers.corresNameID, osMod);
      }

      

      result = osMod.IK(opNameMarkers.opensimMarkersPos,opNameMarkers.markersWeights, mode,opNameMarkers.lastOptitrackTime.toSec()-opNameMarkers.originTime.toSec());
      
      //opNameMarkers.opensimMarkersPos = opNameMarkers.opensimMarkers;

      if (result >= 0)
      {

          markersManagement::computeMarkersErrors(opNameMarkers.opensimMarkersPos, osMod.D_mark_pos, opNameMarkers.markersWeights, opNameMarkers.error_pub, opNameMarkers.error_verbose);

        // if (called)
        // {
        //   fosim::printJointsValues(osMod.osimModel,osMod.osimState);
        // }
        opNameMarkers.framesSinceIK = 0;
        opNameMarkers.framesSinceAssemble += 1;
        opNameMarkers.constraintWeight = opNameMarkers.initConstraintWeight;
        if (numOsNotAssigned == 0)
        {
          tryRecalib = true;
        }
      // opNameMarkers.limAdjustFramesSinceIK = std::max( opNameMarkers.limAdjustFramesSinceIK*0.5, 0.1);
      // int lim = opNameMarkers.limAdjustFramesSinceIK/1;
      // opNameMarkers.limFramesSinceIK = std::min(opNameMarkers.oriLimFramesSinceIK + lim, opNameMarkers.frameRate/5);  


          

        //   // if (verbose)
        //   // {

        //   //   std::printf("---- Resume before ---- \n"); 
        //   //   for (std::map<std::string,double>::iterator it = opNameMarkers.maxGapVariation.begin(); it!= opNameMarkers.maxGapVariation.end(); it++)
        //   //   {
        //   //     std::printf(" Value for treated : %s : %f \n", it->first.c_str(), opNameMarkers.maxGapVariation[it->first]);
        //   //   }

        //   // }

        //   //std::cout << D_mark_all_dist_origin.size() << std::endl;
        //   //std::cout << D_mark_all_dist_assigned.size() << std::endl;
        //   //fosim::printMarkerDistancesInBody(osMod.osimModel,osMod.osimState);
        //   markersManagement::allDistanceVariationDynamic(D_mark_all_dist_origin,D_mark_all_dist_assigned,osMod.D_mark_all_dist,opNameMarkers.maxGapVariation, opNameMarkers.maximum_residual, 1.0/opNameMarkers.frameRate, mdc, false);


        //   markersManagement::publishToleranceValue(opNameMarkers.maxGapVariation,opNameMarkers.error_pub, opNameMarkers.error_verbose);




        // }

      
      }
      else
      {

        if (verbose) {ROS_WARN("IK failed for this frame");}
        opNameMarkers.framesSinceAssemble =opNameMarkers.limFramesSinceAssemble ;
        opNameMarkers.framesSinceIK += 1;

      }

      // IK function with opensimMarkersPos + markersWeights, track
    }

    else
    {
      // some Opensim markers are missing, let's deal with it
      if (opNameMarkers.framesSinceIK < opNameMarkers.limFramesSinceIK )
      {
        // skip the frame
        if (verbose) {ROS_INFO("Let's wait (%i/%i).", opNameMarkers.framesSinceIK, opNameMarkers.limFramesSinceIK);}
        opNameMarkers.framesSinceIK +=1;

        // this means that it just that a optitrack marker has dissapeared; in order to adapt
        // the value of limFramesSinceIK (and increase fluidity), adapt limFramesSinceIK
        // if (opMarkers.size() < numOsNotAssigned )
        // {
        //   opNameMarkers.limAdjustFramesSinceIK *= 1+ 1.0/(numOsNotAssigned-opMarkers.size());
        //   int lim = opNameMarkers.limAdjustFramesSinceIK/1;
        //   opNameMarkers.limFramesSinceIK = std::max(opNameMarkers.oriLimFramesSinceIK - lim, 0);
        // }
      }

      else
      {
        // let's try to find an approximation of the position of the Opensim markers by IK on the markers found.
        if (verbose) {std::cout << "Let's try IK nevertheless." << std::endl;}
        opNameMarkers.constraintWeight *= 0.5;
        for (std::map<std::string,double>::iterator  it = opNameMarkers.markersWeights.begin(); it !=opNameMarkers.markersWeights.end(); it++)
        {
          opNameMarkers.markersWeights[it->first] = it->second*opNameMarkers.constraintWeight;
        }
        // do with assemble
        int mode = 1;
        opNameMarkers.framesSinceAssemble = 0;


        result = osMod.IK(opNameMarkers.opensimMarkersPos,opNameMarkers.markersWeights,mode,opNameMarkers.lastOptitrackTime.toSec()- opNameMarkers.originTime.toSec());

        markersManagement::computeMarkersErrors(opNameMarkers.opensimMarkersPos, osMod.D_mark_pos, opNameMarkers.markersWeights, opNameMarkers.error_pub, opNameMarkers.error_verbose);

        opNameMarkers.framesSinceIK = 0;

        //tryRecalib = true;

      }


    }

        // if (result == 1 && opNameMarkers.framesSinceAssemble == 0)
        // {
        //   // as computation time is REALLLY big for this step, try to reduce it by activating it
        //   // only after assemble
        //   // if IK was possible without issue, use it to get infos about the different markers
        //   std::map<std::string, std::map<std::string, double> > D_mark_all_dist = fosim::getMarkerDistancesInBody(osMod,opNameMarkers.opensimMarkersPos);
        //   // use position of markers obtained by IK instead (no sense, because with this step we want to
        //   // get the variation of distances between markers of a same body; if we use local markers from 
        //   // osMod, as they do not change, no variation will be recorded)
        //   //std::map<std::string, std::map<std::string, double> > D_mark_all_dist = fosim::getMarkerDistancesInBody(osMod,osMod.D_mark_pos);

        //   std::map<std::string, std::map<std::string, double> > D_mark_all_dist_assigned = D_mark_all_dist;
        //   std::map<std::string, std::map<std::string, double> > D_mark_all_dist_origin = opNameMarkers.D_mark_all_dist;
        //   std::map<std::string, std::map<std::string, double> >::iterator it_map_dist;
        //   std::map<std::string, std::map<std::string, double> >::iterator it_map_ori;
        //   int index_deb = 0;
        //   int comp = 0;
        //   while (comp < D_mark_all_dist_assigned.size() )
        //   {
        //     it_map_dist = D_mark_all_dist_assigned.begin();

        //     std::advance(it_map_dist,comp);
        //     // std::cout << "bmld" << std::endl;
        //     // std::cout << comp << std::endl;
        //     // std::cout << enoughOsAssigned[osMod.D_markers_body[it_map_dist->first]] << std::endl;

        //     // suppress tolerance check for body part where IK impossible
        //     // (i.e not enough markers)
        //     // also suppress tolerance check for marker not assigned
        //     //if (!enoughOsAssigned[osMod.D_markers_body[it_map_dist->first]] || osMarkersNotAssigned[it_map_dist->first] == 1 )
        //     //  do not use enoughOsAssigned : could be false right now
        //     if ( osMarkersNotAssigned[it_map_dist->first] == 1 )
        //     {
        //       it_map_ori = D_mark_all_dist_origin.begin();     
        
        //       std::advance(it_map_ori,comp);        

        //       D_mark_all_dist_assigned.erase(it_map_dist);

        //       D_mark_all_dist_origin.erase(it_map_ori);

        //       comp--;
              
        //     }
        //     // maj of position for attributed markers
        //     else
        //     {
        //       opNameMarkers.D_mark_all_dist[it_map_dist->first] = it_map_dist->second;
        //     }


        //     // index_deb ++;
        //     // if (index_deb%100 == 0)
        //     // {
        //     //   std::cout << "D_mark_all_dist_assigned bug" << std::endl;
        //     // }
        //     comp++;
        //   }

        //   markersManagement::allDistanceVariationDynamic(D_mark_all_dist_origin,D_mark_all_dist_assigned,osMod.D_mark_all_dist,opNameMarkers.maxGapVariation, opNameMarkers.maximum_residual, 1.0/opNameMarkers.frameRate, mdc, false);


        //   markersManagement::publishToleranceValue(opNameMarkers.maxGapVariation,opNameMarkers.error_pub, opNameMarkers.error_verbose);


        // }

  if (result == 0 && opNameMarkers.framesSinceAssemble == 0)
  {
    // sometimes, this could be linked with a bad IK result; to try to correct this, 
    // try to reboot osMod
    std::cout << "TOO MANY BUGS IN IK, TRY REBOOT" << std::endl;

    // only saves local position of markers
    std::map<std::string, Eigen::Vector3d> D_mark_pos_local = osMod.D_mark_pos_local;
    // ...and saves joint configuration (can be changed)
    std::map<std::string, double> D_joints_values = osMod.D_joints_values;

    // then, reset it 
    osMod.fromScratch();

    osMod.setMarkersPositions(D_mark_pos_local,"local");
    //osMod.setQ(D_joints_values);
    osMod.setToNeutral();

    // ... and retry it
    result = osMod.IK(opNameMarkers.opensimMarkersPos,opNameMarkers.markersWeights,1,opNameMarkers.lastOptitrackTime.toSec()- opNameMarkers.originTime.toSec());

    if (result !=1)
    {
      // if it still fails, just reset previous joint values
      osMod.setQ(D_joints_values);
    }

    else
    {
      markersManagement::computeMarkersErrors(opNameMarkers.opensimMarkersPos, osMod.D_mark_pos, opNameMarkers.markersWeights, opNameMarkers.error_pub, opNameMarkers.error_verbose);

      opNameMarkers.framesSinceIK = 0;          
    }

  }


  if (verbose && called) {std::cout << "----- End of association for frame "<< opNameMarkers.frameNumber << " -------" << std::endl; }

  //fosim::printJointsValues(osMod.osimModel,osMod.osimState);
  
  // commented for now, maj is provoking some issues with the 
  // true distances (check why)

  int index = 0;
  std::map<std::string,Eigen::Vector3d>::iterator it3;
  while (index < opNameMarkers.opensimMarkersPos.size() )
  {
    it3 = opNameMarkers.opensimMarkersPos.begin();
    std::advance(it3, index);
    if (opNameMarkers.markersWeights[it3->first] < 1e-6) 
    {
      opNameMarkers.opensimMarkersPos.erase(it3);
      index--;
    }
    index++;
  }


  // if (tryRecalib)
  // {
  // opNameMarkers.D_mark_all_dist = D_mark_all_dist;
  // }

  // if (tryRecalib)
  // {
  //   std::map<std::string, std::map<std::string, double> > D_mark_all_dist = fosim::getMarkerDistancesInBody(osMod,opNameMarkers.opensimMarkersPos);
  //   markersManagement::allDistanceVariationDynamic(opNameMarkers.D_mark_all_dist,D_mark_all_dist,opNameMarkers.maxGapVariation, opNameMarkers.maximum_residual, 1.0/opNameMarkers.frameRate, mdc);
  //   opNameMarkers.D_mark_all_dist = D_mark_all_dist;
  // } 

  }



  }


} // namespace utilities


void ConnectionRequestMessage::serialize(
  MessageBuffer& msgBuffer, 
  optitrack_full::DataModel const*)
{
  natnet::Packet pkt;
  pkt.messageId = natnet::MessageType::Connect;
  pkt.numDataBytes = 0;

  // Resize the message buffer and copy contents
  msgBuffer.resize(4); // 2 bytes for messageId and 2 for numDataBtyes
  char *pBuffer = &msgBuffer[0];
  memcpy(pBuffer, &pkt, msgBuffer.size());
}


void ServerInfoMessage::deserialize(
  MessageBuffer const& msgBuffer, 
  optitrack_full::DataModel* dataModel)
{
  char const* pBuffer = &msgBuffer[0];
  natnet::Packet const* packet = (natnet::Packet const*)pBuffer;

  int nver[4] = {0, 0, 0, 0};
  int sver[4] = {0, 0, 0, 0};
  for(int i=0;i<4;++i) {
    nver[i] = (int)(packet->data.sender.natNetVersion[i]);
    sver[i] = (int)(packet->data.sender.version[i]);
  }

  dataModel->highResClockFrequency = packet->data.senderServer.HighResClockFrequency ;

  //std::cout << "freq : " << packet->data.senderServer.HighResClockFrequency << std::endl;
  //std::cout << "dataport : " << packet->data.senderServer.DataPort << std::endl;

  dataModel->setVersions(nver, sver);
}
void DataFrameMessage::MarkerSetsPart::deserialize(
  MessageBuffer::const_iterator& msgBufferIter, 
  std::vector<optitrack_full::MarkerSet> & markerSets,
  bool reception_verbose,
  bool & newMarkerSet
)
  {

      bool verbose = reception_verbose;
      int numMarkerSets = 0;
      utilities::read_and_seek(msgBufferIter, numMarkerSets); // seek only?      
      if (verbose)
      {
      ROS_INFO("*** MARKER SETS ***");
      ROS_INFO("Marker set count: %d", numMarkerSets);
      }
      //dataFrame->markerSets.resize(numMarkerSets);

      // Loop through number of marker sets and get name and data
      // TODO: Whether this correctly parses marker sets has not been tested

      // for (auto& markerSet : dataFrame->markerSets)
      // {
      for (int i = 0; i < numMarkerSets; i++)
      {
        int markerSetIndex = -1;
        //char[256] chNameSet;
        //std::string chNameSet;
        char chNameSet[256];

        // Markerset name
        strcpy(chNameSet, &(*msgBufferIter));
        utilities::seek(msgBufferIter, strlen(chNameSet) + 1);
        // 
        std::string nameSet = chNameSet;        
        if (verbose)
        {
        ROS_INFO("  Marker set %d: %s", i, nameSet.c_str());
        }

        if (nameSet != "all")
        {

          int j = 0;
          while (j < markerSets.size() && (markerSets[j].name != nameSet)  )
          {
            j++;
          }
          optitrack_full::MarkerSet markerSetChecked;
          // we found an ancient markerSet
          if (j < markerSets.size())
          {
            //std::cout << "found : " << markerSets[j].name << std::endl;
            markerSetIndex = j;
            markerSetChecked = markerSets[markerSetIndex];
          }
          // new markerSet detected
          else
          {
            markerSetChecked.name = nameSet;
            markerSetChecked.objectInitialized = false;
            markerSetChecked.currentMotivePose.setIdentity();
            newMarkerSet = true;
            //std::cout << "add : " << markerSetChecked.name << std::endl;
          }
          markerSetChecked.markerMissing = false;

          // Read number of markers that belong to the model
          int numMarkers = 0;
          utilities::read_and_seek(msgBufferIter, numMarkers);
          if (verbose)
          {
              ROS_INFO("  Number of markers: %d", numMarkers);
          }
          // Loop through the markers that belongs to the model
          for (int j = 0; j < numMarkers; j++)
          {
            optitrack_full::Marker marker;
            utilities::read_and_seek(msgBufferIter, marker);
            if (verbose)
            {
                  ROS_INFO("    Marker %d: [x=%3.2f,y=%3.2f,z=%3.2f]", 
            j+1, marker.x, marker.y, marker.z);
            }
            std::string markerName = nameSet + "_Marker" + std::to_string(j+1);
            Eigen::Vector3d mark; mark << marker.x, marker.y, marker.z;
            markerSetChecked.markersPosition[markerName] = mark;
            if (mark.norm() == 0.0 )
            {
              markerSetChecked.markersDetected[markerName] = false;
              markerSetChecked.markerMissing = true;
            }
            else
            {
              markerSetChecked.markersDetected[markerName] = true;
            }
          }

          // now, get distance between markers.
          for (std::map<std::string, Eigen::Vector3d>::iterator it = markerSetChecked.markersPosition.begin(); it != markerSetChecked.markersPosition.end(); it++)
          {          
              if (markerSetChecked.markersDistance.find(it->first) == markerSetChecked.markersDistance.end() )
              {
                std::map<std::string,double> markerDistances;
                markerSetChecked.markersDistance[it->first] = markerDistances;
              }
          }

          int k = 0;
          for (std::map<std::string, Eigen::Vector3d>::iterator it = markerSetChecked.markersPosition.begin(); it != markerSetChecked.markersPosition.end(); it++)
          {
            if (markerSetChecked.markersDetected[it->first])
            {
              if (  markerSetChecked.markersDistance[it->first].size() < markerSetChecked.markersPosition.size() -1)
              {
                int l = k;
                while (l < markerSetChecked.markersPosition.size() )
                {
                  std::map<std::string, Eigen::Vector3d>::iterator it2 = markerSetChecked.markersPosition.begin();
                  std::advance(it2,l);
                  if (markerSetChecked.markersDetected[it2->first])
                  {
                    double dist = (it->second - it2->second).norm();
                    markerSetChecked.markersDistance[it->first][it2->first] = dist;
                    markerSetChecked.markersDistance[it2->first][it->first] = dist;
                  }
                  l++;
                }
              }
            }
            k++;
          }

          if (markerSetIndex == -1)
          {
            markerSets.push_back(markerSetChecked);
          }
          else
          {
            markerSets[markerSetIndex] = markerSetChecked;
          }

        }

        else
        {
          if (verbose)
          {
            ROS_INFO("Not checked as the informations inside are redundant.");
          }
          int numMarkers = 0;
          utilities::read_and_seek(msgBufferIter, numMarkers);
        // if (verbose)
        // {
        //     ROS_INFO("  Number of markers: %d", numMarkers);
        // }            
          // Loop through the markers that belongs to the model
          for (int j = 0; j < numMarkers; j++)
          {
            optitrack_full::Marker marker;
            utilities::read_and_seek(msgBufferIter, marker); 
          // if (verbose)
          // {
          //       ROS_INFO("    Marker %d: [x=%3.2f,y=%3.2f,z=%3.2f]", 
          // j, marker.x, marker.y, marker.z);
          // }                         
          }
        }
      }
  }


/**
 * @brief 
 * 
 */
void DataFrameMessage::LabeledMarkersMessagePart::deserialize(
  MessageBuffer::const_iterator& msgBufferIter, 
  std::map<int,Eigen::Vector3d> & opMarkers,
  std::map<int,Eigen::Vector3d> & opMarkersNotSolved,
  std::map<int, std::map<int,Eigen::Vector3d> > & opMarkRigidBodiesGroup,
  bool reception_verbose,
  optitrack_full::Version const& NatNetVersion)
  {
      int lastID = -1;
      int minLastID = 1e6;

      std::map<int,Eigen::Vector3d> tempOpMarkersNotSolved;

      bool verbose = reception_verbose;
      //ROS_INFO("*** LABELED MARKERS ***");
      int numLabeledMarkers = 0;
      utilities::read_and_seek(msgBufferIter, numLabeledMarkers);
      //ROS_INFO("Labeled marker count: %d", numLabeledMarkers);

      // Loop through labeled markers
      for (int j=0; j < numLabeledMarkers; j++)
      {
        int id = 0; 
        utilities::read_and_seek(msgBufferIter, id);


        int modelId, markerId;
        utilities::decode_marker_id(id, modelId, markerId);

        optitrack_full::Marker marker;
        utilities::read_and_seek(msgBufferIter, marker);
        
        float size;
        utilities::read_and_seek(msgBufferIter, size);


        short params = 0;

        // marker was not visible (occluded) in this frame
        bool bOccluded;
        // position provided by point cloud solve     
        bool bPCSolved;
        // position provided by model solve
        bool bModelSolved;  
        // marker has an associated model
        bool bHasModel;
        // marker is an unlabeled marker
        bool bUnlabeled;  

        if (NatNetVersion >= optitrack_full::Version("2.6"))
        {
          // marker params
          utilities::read_and_seek(msgBufferIter, params);
          // marker was not visible (occluded) in this frame
          bOccluded = (params & 0x01) != 0;
          // position provided by point cloud solve     
          bPCSolved = (params & 0x02) != 0;
          // position provided by model solve
          bModelSolved = (params & 0x04) != 0;  
          if (NatNetVersion >= optitrack_full::Version("3.0"))
          {
            // marker has an associated model
            bHasModel = (params & 0x08) != 0;
            // marker is an unlabeled marker
            bUnlabeled = (params & 0x10) != 0;   
            // marker is an active marker 
            bool bActiveMarker = (params & 0x20) != 0;
          }
        }

        Eigen::Vector3d mark; mark <<marker.x,marker.y,marker.z ;

        // detect if a new group has been reported
        if ( abs(id - lastID) > 1000 && lastID != -1)
        {
          // the not labelized group is always the one with the smaller indexes.
          if (lastID < minLastID)
          {
            minLastID = lastID;
            opMarkersNotSolved = tempOpMarkersNotSolved;
          }
          opMarkRigidBodiesGroup[lastID] = tempOpMarkersNotSolved;
          tempOpMarkersNotSolved.clear();
        }

        tempOpMarkersNotSolved[id] = mark;
        opMarkers[id] = mark;
        lastID = id;

      if (verbose) {//ROS_INFO("  MarkerID: %d, ModelID: %d", markerId, modelId);
       ROS_INFO("  Recorded ID : %d", id);      
        ROS_INFO("    Pos: [%3.2f,%3.2f,%3.2f]", 
          marker.x, marker.y, marker.z); }

        //std::cout << "    Params value : " << params << std::endl;

        std::string sOccluded = bOccluded ? "true" : "false";
        std::string sPCSolved = bPCSolved ? "true" : "false";
        std::string sModelSolved = bModelSolved ? "true" : "false";
        std::string sHasModel = bHasModel ? "true" : "false";
        std::string sUnlabeled = bUnlabeled ? "true" : "false";
        // if (verbose)
        // {
        //   ROS_DEBUG("    Booleans : Occluded : %s; PCSolved : %s; ModelSolved : %s; HasModel : %s; Unlabeled : %s", sOccluded.c_str(), sPCSolved.c_str(), sModelSolved.c_str(), sHasModel.c_str(), sUnlabeled.c_str());
        // }
        // NatNet version 3.0 and later
        if (NatNetVersion >= optitrack_full::Version("3.0"))
        {
          // Marker residual
          float residual = 0.0f;
          utilities::read_and_seek(msgBufferIter, residual);
          ROS_DEBUG("    Residual:  %3.2f", residual);
        }
      }

      opMarkRigidBodiesGroup[lastID] = tempOpMarkersNotSolved;
      if (lastID < minLastID)
        {
          minLastID = lastID;
          opMarkersNotSolved = tempOpMarkersNotSolved;         
          tempOpMarkersNotSolved.clear();
        }

      if (verbose)
      {
        std::printf("From message : number of Optitrack markers not assigned : %ld. \n", opMarkersNotSolved.size() );
        for (std::map<int,Eigen::Vector3d>::iterator it = opMarkersNotSolved.begin(); it!= opMarkersNotSolved.end(); it++)
        {
          std::cout << it->first << " ; ";
        }
      }
}


void DataFrameMessage::RigidBodyMessagePart::deserialize(
  MessageBuffer::const_iterator& msgBufferIter, 
  optitrack_full::RigidBody& rigidBody,
  optitrack_full::Version const& natNetVersion,
  bool verbose)
{
  // Read id, position and orientation of each rigid body
  utilities::read_and_seek(msgBufferIter, rigidBody.bodyId);
  utilities::read_and_seek(msgBufferIter, rigidBody.pose);

  if (verbose)
  {
  ROS_INFO("  Rigid body ID: %d", rigidBody.bodyId);
  ROS_INFO("    Pos: [%3.2f,%3.2f,%3.2f], Ori: [%3.2f,%3.2f,%3.2f,%3.2f]",
           rigidBody.pose.position.x,
           rigidBody.pose.position.y,
           rigidBody.pose.position.z,
           rigidBody.pose.orientation.x,
           rigidBody.pose.orientation.y,
           rigidBody.pose.orientation.z,
           rigidBody.pose.orientation.w);
  }
  // NatNet version 2.0 and later
  if (natNetVersion >= optitrack_full::Version("2.0"))
  {
    // Mean marker error
    utilities::read_and_seek(msgBufferIter, rigidBody.meanMarkerError);
    if (verbose) {ROS_INFO("    Mean marker error: %3.2f", rigidBody.meanMarkerError);}
  }

  // NatNet version 2.6 and later
  if (natNetVersion >= optitrack_full::Version("2.6"))
  {
    // params
    short params = 0; 
    utilities::read_and_seek(msgBufferIter, params);
    rigidBody.isTrackingValid = params & 0x01; // 0x01 : rigid body was successfully tracked in this frame
    if (verbose) {ROS_INFO("    Successfully tracked in this frame: %s", 
      (rigidBody.isTrackingValid ? "YES" : "NO"));}
  }
}


void DataFrameMessage::deserialize(
  MessageBuffer const& msgBuffer, 
  optitrack_full::DataModel* dataModel)
{
  // Get iterator to beginning of buffer and skip the header

  MessageBuffer::const_iterator msgBufferIter = msgBuffer.begin();
  utilities::seek(msgBufferIter, 4); // Skip the header (4 bytes)

  // Next 4 bytes is the frame number
  utilities::read_and_seek(msgBufferIter, dataModel->frameNumber);
    ROS_INFO("=== BEGIN DATA FRAME ===");
    ROS_INFO("Frame number: %d", dataModel->frameNumber);


  // Here on out its conveinent to get a pointer directly
  // to the ModelFrame object as well as the NatNetVersion
  optitrack_full::ModelFrame* dataFrame = &(dataModel->dataFrame);
  optitrack_full::Version const& NatNetVersion = dataModel->getNatNetVersion();

  // Next 4 bytes is the number of data sets (markersets, rigidbodies, etc)
  int numMarkerSets = 0;
  utilities::read_and_seek(msgBufferIter, numMarkerSets);
  ROS_DEBUG("*** MARKER SETS ***");
  ROS_DEBUG("Marker set count: %d", numMarkerSets);
  dataFrame->markerSets.resize(numMarkerSets);

  // Loop through number of marker sets and get name and data
  // TODO: Whether this correctly parses marker sets has not been tested
  int icnt = 0;
  for (auto& markerSet : dataFrame->markerSets)
  {
    // Markerset name
    strcpy(markerSet.nameCh, &(*msgBufferIter));
    utilities::seek(msgBufferIter, strlen(markerSet.nameCh) + 1);
    ROS_DEBUG("  Marker set %d: %s", icnt++, markerSet.nameCh);

    // Read number of markers that belong to the model
    int numMarkers = 0;
    utilities::read_and_seek(msgBufferIter, numMarkers);
    markerSet.markers.resize(numMarkers);
    ROS_DEBUG("  Number of markers: %d", numMarkers);

    int jcnt = 0;
    for (auto& marker : markerSet.markers)
    {
      // read marker positions
      utilities::read_and_seek(msgBufferIter, marker);
      ROS_DEBUG("    Marker %d: [x=%3.2f,y=%3.2f,z=%3.2f]", 
        jcnt++, marker.x, marker.y, marker.z);
    }
  }

  // Loop through unlabeled markers
  ROS_DEBUG("*** UNLABELED MARKERS (Deprecated) ***");
  int numUnlabeledMarkers = 0;
  utilities::read_and_seek(msgBufferIter, numUnlabeledMarkers);
  dataFrame->otherMarkers.resize(numUnlabeledMarkers);
  ROS_DEBUG("Unlabled marker count: %d", numUnlabeledMarkers);

  // Loop over unlabled markers
  icnt = 0;
  for (auto& marker : dataFrame->otherMarkers)
  {
    // read positions of 'other' markers
    utilities::read_and_seek(msgBufferIter, marker);
    ROS_DEBUG("  Marker %d: [x=%3.2f,y=%3.2f,z=%3.2f]", 
        icnt++, marker.x, marker.y, marker.z);
    // Deprecated
  }

  // Loop through rigidbodies
  ROS_DEBUG("*** RIGID BODIES ***");
  int numRigidBodies = 0;
  utilities::read_and_seek(msgBufferIter, numRigidBodies);
  dataFrame->rigidBodies.resize(numRigidBodies);
  ROS_DEBUG("Rigid count: %d", numRigidBodies);

  // Loop over rigid bodies
  for (auto& rigidBody : dataFrame->rigidBodies)
  {
    DataFrameMessage::RigidBodyMessagePart rigidBodyMessagePart;
    rigidBodyMessagePart.deserialize(msgBufferIter, rigidBody, dataModel->getNatNetVersion());
  }

  // Skeletons (NatNet version 2.1 and later)
  // TODO: skeletons not currently included in data model. Parsing
  //       is happening.. but need to copy into a model.
  if (NatNetVersion >= optitrack_full::Version("2.1"))
  {
    ROS_DEBUG("*** SKELETONS ***");
    int numSkeletons = 0;
    utilities::read_and_seek(msgBufferIter, numSkeletons);
    ROS_DEBUG("Skeleton count: %d", numSkeletons);

    // Loop through skeletons
    for (int j=0; j < numSkeletons; j++)
    {
      // skeleton id
      int skeletonId = 0;
      utilities::read_and_seek(msgBufferIter, skeletonId);
      ROS_DEBUG("Skeleton ID: %d", skeletonId);

      // Number of rigid bodies (bones) in skeleton
      int numRigidBodies = 0;
      utilities::read_and_seek(msgBufferIter, numRigidBodies);
      ROS_DEBUG("Rigid body count: %d", numRigidBodies);

      // Loop through rigid bodies (bones) in skeleton
      for (int j=0; j < numRigidBodies; j++)
      {
        optitrack_full::RigidBody rigidBody;
        DataFrameMessage::RigidBodyMessagePart rigidBodyMessagePart;
        rigidBodyMessagePart.deserialize(msgBufferIter, rigidBody, NatNetVersion);
      } // next rigid body
    } // next skeleton
  }

  // Labeled markers (NatNet version 2.3 and later)
  // TODO: like skeletons, labeled markers are not accounted for
  //       in the data model. They are being parsed but not recorded.
  if (NatNetVersion >= optitrack_full::Version("2.3"))
  {

    DataFrameMessage::LabeledMarkersMessagePart labeledMarkersMessagePart;
    std::map<int,Eigen::Vector3d> opMarkers;
    std::map<int,Eigen::Vector3d> opMarkersNotLabel;    
    std::map<int,std::map<int,Eigen::Vector3d> > opMarkRigidBodies;    
    bool reception_verbose = dataModel->opNameMarkers.reception_verbose;
    labeledMarkersMessagePart.deserialize(msgBufferIter,opMarkers, opMarkersNotLabel, opMarkRigidBodies, reception_verbose, NatNetVersion);
  
  }

  // Force Plate data (NatNet version 2.9 and later)
  // TODO: This is definitely not in the data model..
  if (NatNetVersion >= optitrack_full::Version("2.9"))
  {
    ROS_DEBUG("*** FORCE PLATES ***");
    int numForcePlates;
    utilities::read_and_seek(msgBufferIter, numForcePlates);
    ROS_DEBUG("Force plate count: %d", numForcePlates);
    for (int iForcePlate = 0; iForcePlate < numForcePlates; iForcePlate++)
    {
        // ID
        int forcePlateId = 0;
        utilities::read_and_seek(msgBufferIter, forcePlateId);
        ROS_DEBUG("Force plate ID: %d", forcePlateId);

        // Channel Count
        int numChannels = 0; 
        utilities::read_and_seek(msgBufferIter, numChannels);
        ROS_DEBUG("  Number of channels: %d", numChannels);

        // Channel Data
        for (int i = 0; i < numChannels; i++)
        {
            ROS_DEBUG("    Channel %d: ", i);
            int numFrames = 0;
            utilities::read_and_seek(msgBufferIter, numFrames);
            for (int j = 0; j < numFrames; j++)
            {
                float val = 0.0f;  
                utilities::read_and_seek(msgBufferIter, val);
                ROS_DEBUG("      Frame %d: %3.2f", j, val);
            }
        }
    }
  }

  // Device data (NatNet version 3.0 and later)
  // TODO: Also not in the data model..
  if (NatNetVersion >= optitrack_full::Version("3.0"))
  {
    ROS_INFO("*** DEVICE DATA ***");
    int numDevices;
    utilities::read_and_seek(msgBufferIter, numDevices);
    ROS_INFO("Device count: %d", numDevices);

    for (int iDevice = 0; iDevice < numDevices; iDevice++)
    {
      // ID
      int deviceId = 0;
      utilities::read_and_seek(msgBufferIter, deviceId);
      ROS_INFO("  Device ID: %d", deviceId);

      // Channel Count
      int numChannels = 0;
      utilities::read_and_seek(msgBufferIter, numChannels);

      // Channel Data
      for (int i = 0; i < numChannels; i++)
      {
        ROS_INFO("    Channel %d: ", i);
        int nFrames = 0; 
        utilities::read_and_seek(msgBufferIter, nFrames);
        for (int j = 0; j < nFrames; j++)
        {
            float val = 0.0f;
            utilities::read_and_seek(msgBufferIter, val);
            ROS_INFO("      Frame %d: %3.2f", j, val);
        }
      }
    }
  }

  // software latency (removed in version 3.0)
  ROS_DEBUG("*** DIAGNOSTICS ***");
  if (NatNetVersion < optitrack_full::Version("3.0"))
  {
    utilities::read_and_seek(msgBufferIter, dataFrame->latency);
    ROS_DEBUG("Software latency : %3.3f", dataFrame->latency);
  }

  // timecode
  unsigned int timecode = 0;
  utilities::read_and_seek(msgBufferIter, timecode);

  unsigned int timecodeSub = 0;
  utilities::read_and_seek(msgBufferIter, timecodeSub);

  char szTimecode[128] = "";
  utilities::stringify_timecode(timecode, timecodeSub, szTimecode, 128);

  // timestamp
  double timestamp = 0.0f;

  // NatNet version 2.7 and later - increased from single to double precision
  if (NatNetVersion >= optitrack_full::Version("2.7"))
  {
    utilities::read_and_seek(msgBufferIter, timestamp);
  }
  else
  {
    float fTimestamp = 0.0f;
    utilities::read_and_seek(msgBufferIter, fTimestamp);
    timestamp = (double)fTimestamp;
  }
  ROS_INFO("Timestamp: %3.3f", timestamp);

  // high res timestamps (version 3.0 and later)
  if (NatNetVersion >= optitrack_full::Version("3.0"))
  {
    uint64_t cameraMidExposureTimestamp = 0;
    utilities::read_and_seek(msgBufferIter, cameraMidExposureTimestamp);
    ROS_INFO("Mid-exposure timestamp: %" PRIu64 "", cameraMidExposureTimestamp);

    uint64_t cameraDataReceivedTimestamp = 0;
    utilities::read_and_seek(msgBufferIter, cameraDataReceivedTimestamp);
    ROS_INFO("Camera data received timestamp: %" PRIu64 "", cameraDataReceivedTimestamp);

    uint64_t transmitTimestamp = 0;
    utilities::read_and_seek(msgBufferIter, transmitTimestamp);
    ROS_INFO("Transmit timestamp: %" PRIu64 "", transmitTimestamp);
  }

  // frame params
  short params = 0;  
  utilities::read_and_seek(msgBufferIter, params);
  // 0x01 Motive is recording
  bool bIsRecording = (params & 0x01) != 0;
  // 0x02 Actively tracked model list has changed
  bool bTrackedModelsChanged = (params & 0x02) != 0;

  // end of data tag
  int eod = 0; 
  utilities::read_and_seek(msgBufferIter, eod);
  ROS_INFO("=== END DATA FRAME ===");
}


void DataFrameMessage::receptionMessage(
  MessageBuffer const& msgBuffer, 
  optitrack_full::DataModel* dataModel)
{

  bool verbose = dataModel->opNameMarkers.reception_verbose;

  // Get iterator to beginning of buffer and skip the header
  MessageBuffer::const_iterator msgBufferIter = msgBuffer.begin();
  utilities::seek(msgBufferIter, 4); // Skip the header (4 bytes)

  // Next 4 bytes is the frame number
  int frameNumber;

  utilities::read_and_seek(msgBufferIter, frameNumber);

  if (dataModel->queueMessage.find(frameNumber) == dataModel->queueMessage.end() )
  {

    bool stream_markers_only = dataModel->streamMarkersOnly;

    optitrack_full::OptitrackMessage currentMessage;
    // Here on out its conveinent to get a pointer directly
    // to the ModelFrame object as well as the NatNetVersion
    
    optitrack_full::ModelFrame* dataFrame = &(dataModel->dataFrame);
    optitrack_full::Version const& NatNetVersion = dataModel->getNatNetVersion();

    // Next 4 bytes is the number of data sets (markersets, rigidbodies, etc)
    if (stream_markers_only ==false)
    {
      int numMarkerSets = 0;
      utilities::read_and_seek(msgBufferIter, numMarkerSets); // seek only?
//  ROS_INFO("*** MARKER SETS ***");
// ROS_INFO("Marker set count: %d", numMarkerSets);
      dataFrame->markerSets.resize(numMarkerSets);

      // Loop through number of marker sets and get name and data
      // TODO: Whether this correctly parses marker sets has not been tested
      int icnt = 0;
      for (auto& markerSet : dataFrame->markerSets)
      {
        // Markerset name
        strcpy(markerSet.nameCh, &(*msgBufferIter));
        utilities::seek(msgBufferIter, strlen(markerSet.nameCh) + 1);
       // ROS_INFO("  Marker set %d: %s", icnt++, markerSet.name);

        // Read number of markers that belong to the model
        int numMarkers = 0;
        utilities::read_and_seek(msgBufferIter, numMarkers);
        markerSet.markers.resize(numMarkers);
        //    ROS_INFO("  Number of markers: %d", numMarkers);
          int jcnt = 0;
        for (auto& marker : markerSet.markers)
        {
          // read marker positions
          utilities::read_and_seek(msgBufferIter, marker);
              //  ROS_INFO("    Marker %d: [x=%3.2f,y=%3.2f,z=%3.2f]", 
       // jcnt++, marker.x, marker.y, marker.z);
        }
      }

      // Loop through unlabeled markers
  //ROS_INFO("*** UNLABELED MARKERS (Deprecated) ***");      
      int numUnlabeledMarkers = 0;
      utilities::read_and_seek(msgBufferIter, numUnlabeledMarkers);
      dataFrame->otherMarkers.resize(numUnlabeledMarkers);
        //ROS_INFO("Unlabled marker count: %d", numUnlabeledMarkers);

      // Loop over unlabled markers
      icnt = 0;
      for (auto& marker : dataFrame->otherMarkers)
      {
        // read positions of 'other' markers
        utilities::read_and_seek(msgBufferIter, marker);
            //ROS_INFO("  Marker %d: [x=%3.2f,y=%3.2f,z=%3.2f]", 
        //icnt++, marker.x, marker.y, marker.z);
        // Deprecated
      }

      // Loop through rigidbodies
      //ROS_INFO("*** RIGID BODIES ***");
      int numRigidBodies = 0;
      utilities::read_and_seek(msgBufferIter, numRigidBodies);
      dataFrame->rigidBodies.resize(numRigidBodies);
      //ROS_INFO("Rigid count: %d", numRigidBodies);
      // Loop over rigid bodies
      for (auto& rigidBody : dataFrame->rigidBodies)
      {
        DataFrameMessage::RigidBodyMessagePart rigidBodyMessagePart;
        rigidBodyMessagePart.deserialize(msgBufferIter, rigidBody, dataModel->getNatNetVersion());
      }

      // Skeletons (NatNet version 2.1 and later)
      // TODO: skeletons not currently included in data model. Parsing
      //       is happening.. but need to copy into a model.
      if (NatNetVersion >= optitrack_full::Version("2.1"))
      {
        //ROS_INFO("*** SKELETONS ***");
        int numSkeletons = 0;
        utilities::read_and_seek(msgBufferIter, numSkeletons);
        //ROS_INFO("Skeleton count: %d", numSkeletons);
        // Loop through skeletons
        for (int j=0; j < numSkeletons; j++)
        {

          // skeleton id
          int skeletonId = 0;
          utilities::read_and_seek(msgBufferIter, skeletonId);
          //ROS_INFO("Skeleton ID: %d", skeletonId);

          // Number of rigid bodies (bones) in skeleton
          int numRigidBodies = 0;
          utilities::read_and_seek(msgBufferIter, numRigidBodies);
          //ROS_INFO("Rigid body count: %d", numRigidBodies);

          // Loop through rigid bodies (bones) in skeleton
          for (int j=0; j < numRigidBodies; j++)
          {
            optitrack_full::RigidBody rigidBody;
            DataFrameMessage::RigidBodyMessagePart rigidBodyMessagePart;
            rigidBodyMessagePart.deserialize(msgBufferIter, rigidBody, NatNetVersion);
          } // next rigid body
        } // next skeleton
      }

    }


    else
    {
      int numMarkerSets = 0;
      utilities::read_and_seek(msgBufferIter, numMarkerSets); // seek only?

      int numUnlabeledMarkers = 0;
      utilities::read_and_seek(msgBufferIter, numUnlabeledMarkers);


      // Loop through rigidbodies
      //ROS_INFO("*** RIGID BODIES ***");
      int numRigidBodies = 0;
      utilities::read_and_seek(msgBufferIter, numRigidBodies);

      if (NatNetVersion >= optitrack_full::Version("2.1"))
      {
        int numSkeletons = 0;
        utilities::read_and_seek(msgBufferIter, numSkeletons);

      }

    }


    // Labeled markers (NatNet version 2.3 and later)
    // TODO: like skeletons, labeled markers are not accounted for
    //       in the data model. They are being parsed but not recorded.
    if (NatNetVersion >= optitrack_full::Version("2.3"))
    {

      DataFrameMessage::LabeledMarkersMessagePart labeledMarkersMessagePart;

      bool reception_verbose = dataModel->opNameMarkers.reception_verbose;
      labeledMarkersMessagePart.deserialize(msgBufferIter, currentMessage.optitrackMarkers, currentMessage.optitrackMarkersNotAssigned, currentMessage.opMarkRigidBodies, reception_verbose, NatNetVersion);

    }

    if (stream_markers_only == false)
    {
      if (NatNetVersion >= optitrack_full::Version("2.9"))
        {
          //ROS_DEBUG("*** FORCE PLATES ***");
          int numForcePlates;
          utilities::read_and_seek(msgBufferIter, numForcePlates);
          //ROS_DEBUG("Force plate count: %d", numForcePlates);
          for (int iForcePlate = 0; iForcePlate < numForcePlates; iForcePlate++)
          {
              // ID
              int forcePlateId = 0;
              utilities::read_and_seek(msgBufferIter, forcePlateId);
              //ROS_DEBUG("Force plate ID: %d", forcePlateId);

              // Channel Count
              int numChannels = 0; 
              utilities::read_and_seek(msgBufferIter, numChannels);
              //ROS_DEBUG("  Number of channels: %d", numChannels);

              // Channel Data
              for (int i = 0; i < numChannels; i++)
              {
                  //ROS_DEBUG("    Channel %d: ", i);
                  int numFrames = 0;
                  utilities::read_and_seek(msgBufferIter, numFrames);
                  for (int j = 0; j < numFrames; j++)
                  {
                      float val = 0.0f;  
                      utilities::read_and_seek(msgBufferIter, val);
                      //ROS_DEBUG("      Frame %d: %3.2f", j, val);
                  }
              }
          }
        }

        // Device data (NatNet version 3.0 and later)
        // TODO: Also not in the data model..
        if (NatNetVersion >= optitrack_full::Version("3.0"))
        {
          //ROS_INFO("*** DEVICE DATA ***");
          int numDevices;
          utilities::read_and_seek(msgBufferIter, numDevices);
          //ROS_INFO("Device count: %d", numDevices);

          for (int iDevice = 0; iDevice < numDevices; iDevice++)
          {
            // ID
            int deviceId = 0;
            utilities::read_and_seek(msgBufferIter, deviceId);
            //ROS_INFO("  Device ID: %d", deviceId);

            // Channel Count
            int numChannels = 0;
            utilities::read_and_seek(msgBufferIter, numChannels);

            // Channel Data
            for (int i = 0; i < numChannels; i++)
            {
              //ROS_INFO("    Channel %d: ", i);
              int nFrames = 0; 
              utilities::read_and_seek(msgBufferIter, nFrames);
              for (int j = 0; j < nFrames; j++)
              {
                  float val = 0.0f;
                  utilities::read_and_seek(msgBufferIter, val);
                  //ROS_INFO("      Frame %d: %3.2f", j, val);
              }
            }
          }
        }

        // software latency (removed in version 3.0)
        //ROS_DEBUG("*** DIAGNOSTICS ***");
        if (NatNetVersion < optitrack_full::Version("3.0"))
        {
          utilities::read_and_seek(msgBufferIter, dataFrame->latency);
          //ROS_DEBUG("Software latency : %3.3f", dataFrame->latency);
        }

    }


    else
    {
      if (NatNetVersion >= optitrack_full::Version("2.9"))
        {
          //ROS_DEBUG("*** FORCE PLATES ***");
          int numForcePlates;
          utilities::read_and_seek(msgBufferIter, numForcePlates);
        }
        // Device data (NatNet version 3.0 and later)
        // TODO: Also not in the data model..
        if (NatNetVersion >= optitrack_full::Version("3.0"))
        {
          //ROS_INFO("*** DEVICE DATA ***");
          int numDevices;
          utilities::read_and_seek(msgBufferIter, numDevices);
          //ROS_INFO("Device count: %d", numDevices);
        }
        // software latency (removed in version 3.0)
        //ROS_DEBUG("*** DIAGNOSTICS ***");
        if (NatNetVersion < optitrack_full::Version("3.0"))
        {
          utilities::read_and_seek(msgBufferIter, dataFrame->latency);
          //ROS_DEBUG("Software latency : %3.3f", dataFrame->latency);
        }
    }


  // timecode
  unsigned int timecode = 0;
  utilities::read_and_seek(msgBufferIter, timecode);
  //std::cout << "timecode : " << timecode << std::endl;
  unsigned int timecodeSub = 0;
  utilities::read_and_seek(msgBufferIter, timecodeSub);
  //std::cout << "timecodesub : " << timecodeSub << std::endl;
  char szTimecode[128] = "";
  utilities::stringify_timecode(timecode, timecodeSub, szTimecode, 128);

        // timestamp
    double timestamp = 0.0f;

    // NatNet version 2.7 and later - increased from single to double precision
    if (NatNetVersion >= optitrack_full::Version("2.7"))
    {
      utilities::read_and_seek(msgBufferIter, timestamp);
    }
    else
    {
      float fTimestamp = 0.0f;
      utilities::read_and_seek(msgBufferIter, fTimestamp);
       timestamp = (double)fTimestamp;
    }

    //ROS_INFO("Timestamp: %3.3f", timestamp);
    //ROS_INFO("To string : %s", szTimecode);

    // high res timestamps (version 3.0 and later)
    double high_res_time = 0.0;
    if (NatNetVersion >= optitrack_full::Version("3.0"))
    {
      uint64_t cameraMidExposureTimestamp = 0;
      utilities::read_and_seek(msgBufferIter, cameraMidExposureTimestamp);


      uint64_t cameraDataReceivedTimestamp = 0;
      utilities::read_and_seek(msgBufferIter, cameraDataReceivedTimestamp);

      uint64_t transmitTimestamp = 0;
      utilities::read_and_seek(msgBufferIter, transmitTimestamp);

      if (verbose)
      {
        ROS_INFO("Mid-exposure timestamp: %" PRIu64 "", cameraMidExposureTimestamp);
        ROS_INFO("Camera data received timestamp: %" PRIu64 "", cameraDataReceivedTimestamp);
        ROS_INFO("Transmit timestamp: %" PRIu64 "", transmitTimestamp);
      }
      
      high_res_time = (transmitTimestamp - std::max(cameraMidExposureTimestamp,cameraDataReceivedTimestamp ) )*1.0/(dataModel->highResClockFrequency);
    
    }

    double true_timestamp = timestamp -high_res_time;
    currentMessage.trueTimestamp = true_timestamp;

    ROS_INFO("Message timestamp reception : %3.4f", true_timestamp);
    ROS_INFO("Frame number: %d", frameNumber);
    //utilities::markersRecognitionAndIK(dataModel->opNameMarkers,dataModel->osMod);
    dataModel->queueMessage[frameNumber] = currentMessage;

  }

  if (verbose)
  {
    ROS_INFO("=== END DATA FRAME ===");
  }
}





void DataFrameMessage::deserializeFast(
  MessageBuffer const& msgBuffer, 
  optitrack_full::DataModel* dataModel)
{

  bool verbose = dataModel->opNameMarkers.reception_verbose;
  bool stream_markers_only = dataModel->streamMarkersOnly;

  // Get iterator to beginning of buffer and skip the header
  MessageBuffer::const_iterator msgBufferIter = msgBuffer.begin();
  utilities::seek(msgBufferIter, 4); // Skip the header (4 bytes)

  // Next 4 bytes is the frame number
  utilities::read_and_seek(msgBufferIter, dataModel->frameNumber);

  if (dataModel->frameOrigin == -1)
  {
    dataModel->frameOrigin = dataModel->frameNumber;
  }

  else if ( (dataModel->opNameMarkers.repeat) && dataModel->frameNumber <= std::max(dataModel->lastFrame*0.1,dataModel->frameOrigin*1.0) )
  {
    dataModel->frameOrigin = dataModel->frameNumber;
    dataModel->lastFrame = dataModel->frameNumber;
    dataModel->numFramesCons = 0;
  }

  else if ( (!dataModel->opNameMarkers.repeat) && dataModel->frameNumber <= std::max(dataModel->lastFrame*0.1,dataModel->frameOrigin*1.0) )
  {
    dataModel->frameOrigin = dataModel->frameNumber;
    dataModel->lastFrame = dataModel->frameNumber;
    if (dataModel->numFramesCons > 0)
    {
      dataModel->lastNumFramesCons = dataModel->numFramesCons;
      dataModel->numFramesCons = -1;
    }
    else
    {
      dataModel->numFramesCons = dataModel->lastNumFramesCons;
    }
  }  

  if (verbose)
  {
    ROS_INFO("=== BEGIN DATA FRAME ===");
    ROS_INFO("Frame number: %d", dataModel->frameNumber);
    ROS_INFO("Frame origin: %d", dataModel->frameOrigin);
    ROS_INFO("Last Frame : %d", dataModel->lastFrame);
  }


  if (dataModel->frameNumber>= dataModel->lastFrame)
  {

    // Here on out its conveinent to get a pointer directly
    // to the ModelFrame object as well as the NatNetVersion
    
    optitrack_full::ModelFrame* dataFrame = &(dataModel->dataFrame);
    optitrack_full::Version const& NatNetVersion = dataModel->getNatNetVersion();

    // Next 4 bytes is the number of data sets (markersets, rigidbodies, etc)
    if (stream_markers_only ==false)
    {
      DataFrameMessage::MarkerSetsPart markerSetsMessagePart;
      dataModel->newMarkerSet = false;
      markerSetsMessagePart.deserialize(msgBufferIter, dataModel->allMarkerSets, verbose, dataModel->newMarkerSet);

    // labelized markers
    // Loop through labeled markers (again, redundant)
    //ROS_INFO("*** LABELED MARKERS (Deprecated) ***");
    int numUnlabeledMarkers = 0;
    utilities::read_and_seek(msgBufferIter, numUnlabeledMarkers);
    dataFrame->otherMarkers.resize(numUnlabeledMarkers);
    //ROS_INFO("Unlabled marker count: %d", numUnlabeledMarkers);

    // Loop over unlabled markers
    int icnt = 0;
    for (auto& marker : dataFrame->otherMarkers)
    {
      // read positions of 'other' markers
      utilities::read_and_seek(msgBufferIter, marker);
      //ROS_INFO("  Marker %d: [x=%3.2f,y=%3.2f,z=%3.2f]", 
      //    icnt++, marker.x, marker.y, marker.z);
      // Deprecated
    }


      // Loop through rigidbodies
      int numRigidBodies = 0;
      utilities::read_and_seek(msgBufferIter, numRigidBodies);
      dataFrame->rigidBodies.resize(numRigidBodies);
      if (verbose)
      {
      ROS_INFO("*** RIGID BODIES ***");      
      ROS_INFO("Rigid count: %d", numRigidBodies);
      }
      // Loop over rigid bodies
      for (auto& rigidBody : dataFrame->rigidBodies)
      {
        DataFrameMessage::RigidBodyMessagePart rigidBodyMessagePart;
        rigidBodyMessagePart.deserialize(msgBufferIter, rigidBody, dataModel->getNatNetVersion(), verbose);
      }

      // Skeletons (NatNet version 2.1 and later)
      // TODO: skeletons not currently included in data model. Parsing
      //       is happening.. but need to copy into a model.
      if (NatNetVersion >= optitrack_full::Version("2.1"))
      {
        //ROS_INFO("*** SKELETONS ***");
        int numSkeletons = 0;
        utilities::read_and_seek(msgBufferIter, numSkeletons);
        //ROS_INFO("Skeleton count: %d", numSkeletons);
        // Loop through skeletons
        for (int j=0; j < numSkeletons; j++)
        {

          // skeleton id
          int skeletonId = 0;
          utilities::read_and_seek(msgBufferIter, skeletonId);
          //ROS_INFO("Skeleton ID: %d", skeletonId);

          // Number of rigid bodies (bones) in skeleton
          int numRigidBodies = 0;
          utilities::read_and_seek(msgBufferIter, numRigidBodies);
          if (verbose)
          {
            ROS_INFO("Rigid body count: %d", numRigidBodies);
          }
          // Loop through rigid bodies (bones) in skeleton
          for (int j=0; j < numRigidBodies; j++)
          {
            optitrack_full::RigidBody rigidBody;                                                                     
            DataFrameMessage::RigidBodyMessagePart rigidBodyMessagePart;
            rigidBodyMessagePart.deserialize(msgBufferIter, rigidBody, NatNetVersion);

          } // next rigid body
        } // next skeleton
      }

    }


    else
    {
      int numMarkerSets = 0;
      utilities::read_and_seek(msgBufferIter, numMarkerSets); // seek only?

      int numUnlabeledMarkers = 0;
      utilities::read_and_seek(msgBufferIter, numUnlabeledMarkers);


      // Loop through rigidbodies
      //ROS_INFO("*** RIGID BODIES ***");
      int numRigidBodies = 0;
      utilities::read_and_seek(msgBufferIter, numRigidBodies);

      if (NatNetVersion >= optitrack_full::Version("2.1"))
      {
        int numSkeletons = 0;
        utilities::read_and_seek(msgBufferIter, numSkeletons);

      }

    }


    // Labeled markers (NatNet version 2.3 and later)
    // TODO: like skeletons, labeled markers are not accounted for
    //       in the data model. They are being parsed but not recorded.
    if (NatNetVersion >= optitrack_full::Version("2.3"))
    {

      DataFrameMessage::LabeledMarkersMessagePart labeledMarkersMessagePart;
      bool reception_verbose = dataModel->opNameMarkers.reception_verbose;
      dataModel->opNameMarkers.lastOptitrackMarkers.clear();
      dataModel->opNameMarkers.lastOptitrackMarkersNotAssigned.clear();
      dataModel->opNameMarkers.opMarkRigidBodies.clear();
      labeledMarkersMessagePart.deserialize(msgBufferIter,dataModel->opNameMarkers.lastOptitrackMarkers, dataModel->opNameMarkers.lastOptitrackMarkersNotAssigned, dataModel->opNameMarkers.opMarkRigidBodies, reception_verbose, NatNetVersion);
      if (verbose)
      {
        ROS_INFO("*** ALL MARKERS ***");   
        for (std::map<int, Eigen::Vector3d >::iterator it = dataModel->opNameMarkers.lastOptitrackMarkers.begin(); it != dataModel->opNameMarkers.lastOptitrackMarkers.end(); it++)
        {
            ROS_INFO("  Marker %d: [x=%3.2f,y=%3.2f,z=%3.2f]", it->first, it->second[0], it->second[1], it->second[2]);
        }

      }
    
    }

    if (stream_markers_only == false)
    {
      if (NatNetVersion >= optitrack_full::Version("2.9"))
        {
          //ROS_DEBUG("*** FORCE PLATES ***");
          int numForcePlates;
          utilities::read_and_seek(msgBufferIter, numForcePlates);
          //ROS_DEBUG("Force plate count: %d", numForcePlates);
          for (int iForcePlate = 0; iForcePlate < numForcePlates; iForcePlate++)
          {
              // ID
              int forcePlateId = 0;
              utilities::read_and_seek(msgBufferIter, forcePlateId);
              //ROS_DEBUG("Force plate ID: %d", forcePlateId);

              // Channel Count
              int numChannels = 0; 
              utilities::read_and_seek(msgBufferIter, numChannels);
              //ROS_DEBUG("  Number of channels: %d", numChannels);

              // Channel Data
              for (int i = 0; i < numChannels; i++)
              {
                  //ROS_DEBUG("    Channel %d: ", i);
                  int numFrames = 0;
                  utilities::read_and_seek(msgBufferIter, numFrames);
                  for (int j = 0; j < numFrames; j++)
                  {
                      float val = 0.0f;  
                      utilities::read_and_seek(msgBufferIter, val);
                      //ROS_DEBUG("      Frame %d: %3.2f", j, val);
                  }
              }
          }
        }

        // Device data (NatNet version 3.0 and later)
        // TODO: Also not in the data model..
        if (NatNetVersion >= optitrack_full::Version("3.0"))
        {
          //ROS_INFO("*** DEVICE DATA ***");
          int numDevices;
          utilities::read_and_seek(msgBufferIter, numDevices);
          //ROS_INFO("Device count: %d", numDevices);

          for (int iDevice = 0; iDevice < numDevices; iDevice++)
          {
            // ID
            int deviceId = 0;
            utilities::read_and_seek(msgBufferIter, deviceId);
            //ROS_INFO("  Device ID: %d", deviceId);

            // Channel Count
            int numChannels = 0;
            utilities::read_and_seek(msgBufferIter, numChannels);

            // Channel Data
            for (int i = 0; i < numChannels; i++)
            {
              //ROS_INFO("    Channel %d: ", i);
              int nFrames = 0; 
              utilities::read_and_seek(msgBufferIter, nFrames);
              for (int j = 0; j < nFrames; j++)
              {
                  float val = 0.0f;
                  utilities::read_and_seek(msgBufferIter, val);
                  //ROS_INFO("      Frame %d: %3.2f", j, val);
              }
            }
          }
        }

        // software latency (removed in version 3.0)
        //ROS_DEBUG("*** DIAGNOSTICS ***");
        if (NatNetVersion < optitrack_full::Version("3.0"))
        {
          utilities::read_and_seek(msgBufferIter, dataFrame->latency);
          //ROS_DEBUG("Software latency : %3.3f", dataFrame->latency);
        }

    }


    else
    {
      if (NatNetVersion >= optitrack_full::Version("2.9"))
        {
          //ROS_DEBUG("*** FORCE PLATES ***");
          int numForcePlates;
          utilities::read_and_seek(msgBufferIter, numForcePlates);
        }
        // Device data (NatNet version 3.0 and later)
        // TODO: Also not in the data model..
        if (NatNetVersion >= optitrack_full::Version("3.0"))
        {
          //ROS_INFO("*** DEVICE DATA ***");
          int numDevices;
          utilities::read_and_seek(msgBufferIter, numDevices);
          //ROS_INFO("Device count: %d", numDevices);
        }
        // software latency (removed in version 3.0)
        //ROS_DEBUG("*** DIAGNOSTICS ***");
        if (NatNetVersion < optitrack_full::Version("3.0"))
        {
          utilities::read_and_seek(msgBufferIter, dataFrame->latency);
          //ROS_DEBUG("Software latency : %3.3f", dataFrame->latency);
        }
    }


  // timecode
  unsigned int timecode = 0;
  utilities::read_and_seek(msgBufferIter, timecode);
  //std::cout << "timecode : " << timecode << std::endl;
  unsigned int timecodeSub = 0;
  utilities::read_and_seek(msgBufferIter, timecodeSub);
  //std::cout << "timecodesub : " << timecodeSub << std::endl;
  char szTimecode[128] = "";
  utilities::stringify_timecode(timecode, timecodeSub, szTimecode, 128);

        // timestamp
    double timestamp = 0.0f;

    // NatNet version 2.7 and later - increased from single to double precision
    if (NatNetVersion >= optitrack_full::Version("2.7"))
    {
      utilities::read_and_seek(msgBufferIter, timestamp);
    }
    else
    {
      float fTimestamp = 0.0f;
      utilities::read_and_seek(msgBufferIter, fTimestamp);
       timestamp = (double)fTimestamp;
    }

    //ROS_INFO("Timestamp: %3.3f", timestamp);
    //ROS_INFO("To string : %s", szTimecode);

    // high res timestamps (version 3.0 and later)
    double high_res_time = 0.0;
    if (NatNetVersion >= optitrack_full::Version("3.0"))
    {
      uint64_t cameraMidExposureTimestamp = 0;
      utilities::read_and_seek(msgBufferIter, cameraMidExposureTimestamp);


      uint64_t cameraDataReceivedTimestamp = 0;
      utilities::read_and_seek(msgBufferIter, cameraDataReceivedTimestamp);

      uint64_t transmitTimestamp = 0;
      utilities::read_and_seek(msgBufferIter, transmitTimestamp);

      if (verbose)
      {
        ROS_INFO("Mid-exposure timestamp: %" PRIu64 "", cameraMidExposureTimestamp);
        ROS_INFO("Camera data received timestamp: %" PRIu64 "", cameraDataReceivedTimestamp);
        ROS_INFO("Transmit timestamp: %" PRIu64 "", transmitTimestamp);
      }
      
      high_res_time = (transmitTimestamp - std::max(cameraMidExposureTimestamp,cameraDataReceivedTimestamp ) )*1.0/(dataModel->highResClockFrequency);
    
    }

    double true_timestamp = timestamp -high_res_time;


    // maj origin time
    utilities::manageTime(dataModel,true_timestamp);


    // std::cout << "time : " << high_res_time <<std::endl;
    // std::cout << "messagetime : " << dataModel->opNameMarkers.messageReceptionTime << std::endl;
    // std::cout << "minus : " << dataModel->opNameMarkers.messageReceptionTime.toSec() - high_res_time << std::endl;

    
    //dataModel->opNameMarkers.lastOptitrackTime.fromSec(dataModel->opNameMarkers.messageReceptionTime.toSec() - high_res_time);

    // TODO : mod time pr que OK

    // now that time is fixed, let's associate marker and do IK (time necessary for it)
    //ROS_INFO("Last optitrack time : %3.4f", dataModel->opNameMarkers.lastOptitrackTime.toSec());
    //ROS_INFO("Frame number: %d", dataModel->frameNumber);

    // PUT ELSWHERE TO ALLOW A BETTER GESTION
    //utilities::markersRecognitionAndIK(dataModel->opNameMarkers,dataModel->osMod);

    dataModel->lastFrame = dataModel->frameNumber;
    if (dataModel->numFramesCons >=0)
    {
      dataModel->numFramesCons += 1;
    }

  }

  if (verbose)
  {
    ROS_INFO("=== END DATA FRAME ===");
  }
}

void MessageDispatcher::dispatch(
  MessageBuffer const& msgBuffer, 
  optitrack_full::DataModel* dataModel)
{

  bool verbose = dataModel->opNameMarkers.reception_verbose;
  
  // Grab message ID by casting to a natnet packet type
  char const* pMsgBuffer = &msgBuffer[0];
  natnet::Packet const* packet = (natnet::Packet const*)(pMsgBuffer);
  if (verbose)
  {
    ROS_INFO("Message ID: %d", packet->messageId);
    ROS_INFO("Byte count : %d", (int)msgBuffer.size());
  }

  if (packet->messageId == natnet::MessageType::ModelDef ||
      packet->messageId == natnet::MessageType::FrameOfData)
  {
    if (dataModel->hasServerInfo())
    {
      dataModel->dispatching = true;
      DataFrameMessage msg;
      //msg.deserialize(msgBuffer, dataModel);
      msg.deserializeFast(msgBuffer, dataModel);
      //msg.receptionMessage(msgBuffer, dataModel);

      dataModel->opNameMarkers.frameNumber = dataModel->frameNumber;      
      dataModel->dispatching = false;
    }
    else
    {
      ROS_WARN("Client has not received server info request. Parsing data message aborted.");
    }
    return;
  }

  if (packet->messageId == natnet::MessageType::ServerInfo)
  {
    natnet::ServerInfoMessage msg;
    msg.deserialize(msgBuffer, dataModel);
    ROS_INFO_ONCE("NATNet Version : %s", 
      dataModel->getNatNetVersion().getVersionString().c_str());
    ROS_INFO_ONCE("Server Version : %s", 
      dataModel->getServerVersion().getVersionString().c_str());
    return;
  }

  if (packet->messageId == natnet::MessageType::UnrecognizedRequest)
  {
    ROS_WARN("Received unrecognized request");
  }

}
} // namespace