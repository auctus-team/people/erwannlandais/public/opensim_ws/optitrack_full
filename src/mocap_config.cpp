/* 
 * Copyright (c) 2018, Houston Mechatronics Inc., JD Yamokoski
 * Copyright (c) 2012, Clearpath Robotics, Inc., Alex Bencz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its 
 *    contributors may be used to endorse or promote products derived from 
 *    this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "optitrack_full/mocap_config.h"

#include <XmlRpcValue.h>

namespace optitrack_full
{

namespace impl
{
  template<typename T>
  bool check_and_get_param(
    XmlRpc::XmlRpcValue& config_node, 
    std::string const& key, 
    T& value)
  {
    return false;
  }

  template<>
  bool check_and_get_param<std::string>(
    XmlRpc::XmlRpcValue& config_node, 
    std::string const& key, 
    std::string& value)
  {
    if (config_node[key].getType() == XmlRpc::XmlRpcValue::TypeString)
    {
      value = (std::string&)config_node[key];
      return true;
    }

    return false;
  }
} // namespace impl

// Server description defaults
const int ServerDescription::Default::CommandPort = 1510;
const int ServerDescription::Default::DataPort   = 9000;
const int ServerDescription::Default::FrameRate   = 100;
const std::string ServerDescription::Default::MulticastIpAddress = "224.0.0.1";
const bool ServerDescription::Default::simulation = false;
const double ServerDescription::Default::speed = M_PI_2/3;
const int ServerDescription::Default::dissapear   = 0;
const int ServerDescription::Default::same_frames   = 0;
const double ServerDescription::Default::noise   = 0;
const double ServerDescription::Default::maximum_residual  = 0.01;


const bool ServerDescription::Default::simu_verbose  = false;
const bool ServerDescription::Default::reception_verbose  = false;
const bool ServerDescription::Default::association_verbose  = false;
const bool ServerDescription::Default::icp_verbose = false;
const bool ServerDescription::Default::guess_verbose = false;
const bool ServerDescription::Default::filtering_verbose = false;
const bool ServerDescription::Default::calibration_verbose = false;
const bool ServerDescription::Default::error_verbose = false;
const bool ServerDescription::Default::ik_verbose = false;
const bool ServerDescription::Default::never_verbose = false;

const bool ServerDescription::Default::stream_markers_only  = false;
const bool ServerDescription::Default::repeat  = false;

const std::string ServerDescription::Default::ros_package_path = "/home/auctus/catkin_ws/src/human_control";
const std::string ServerDescription::Default::scale_file = "/home/auctus/ospi2urdf/utilis_files/scaling/scaleTool.xml";
const std::string ServerDescription::Default::data_file = "/home/auctus/ospi2urdf/utilis_files/data.txt";


const std::string ServerDescription::Default::trc_file = "";
const std::string ServerDescription::Default::type_record = "none";
const bool ServerDescription::Default::record_trc = false;

const std::string ServerDescription::Default::forbidden_areas_file = "";

const bool ServerDescription::Default::fast_trc_read = false;

// Param keys
namespace rosparam
{
  namespace keys
  {
    const std::string MulticastIpAddress = "optitrack_config/multicast_address";
    const std::string CommandPort = "optitrack_config/command_port";
    const std::string DataPort = "optitrack_config/data_port";
    const std::string FrameRate = "optitrack_config/frame_rate";
    const std::string Version = "optitrack_config/version";
    const std::string MaxResidual = "optitrack_config/maximum_residual";


    const std::string RigidBodies = "rigid_bodies";

    const std::string RigidBodyTFManagement = "tf_properties";
    const std::string PoseTopicName = "pose";
    const std::string Pose2dTopicName = "pose2d";
    const std::string ChildFrameId = "child_frame_id";
    const std::string ParentFrameId = "parent_frame_id";

    const std::string RigidBodyMarkersManagement = "rigid_body_properties";
    const std::string RigidBodyAxisByMarkers = "referential_markers";
    const std::string RigidBodyMarkersNames = "markers_names";    

    const std::string OpensimConfig = "opensim_config";
    const std::string QCalib = "opensim_config/q_calib";
    const std::string OsimPath = "opensim_config/osim_path";
    const std::string GeomPath = "opensim_config/geometry_path";
    const std::string IKPath = "opensim_config/setup_ik_path";
     const std::string Qtopic = "opensim_config/topic_q";

    const::std::string Simu = "optitrack_config/simulation";
    const::std::string Speed = "opensim_config/simu_speed";
    const::std::string Dissapear = "opensim_config/simu_dissapear";
    const::std::string SameFrames = "opensim_config/simu_same_frames";
    const::std::string Noise = "opensim_config/simu_noise";
    const::std::string Verbose = "opensim_config/simu_verbose";
    const::std::string ICPVerbose = "opensim_config/icp_verbose";
    const::std::string GuessVerbose = "opensim_config/guess_verbose";
    const::std::string FilteringVerbose = "opensim_config/filtering_verbose";
    const::std::string IKVerbose = "opensim_config/ik_verbose";
    const::std::string NeverVerbose = "opensim_config/never_verbose";

    const::std::string ReceptionVerbose = "opensim_config/reception_verbose";
    const::std::string AssociationVerbose = "opensim_config/association_verbose";    
    const::std::string CalibrationVerbose = "opensim_config/calibration_verbose";    

    const::std::string ErrorVerbose = "opensim_config/error_verbose"; 

    const::std::string RecordTrc = "optitrack_config/record_trc"; 
    const::std::string TrcFile = "optitrack_config/trc_file"; 
    const::std::string TypeRecord = "optitrack_config/type_record"; 
    const::std::string FTRead = "optitrack_config/fast_trc_read"; 



    const::std::string MarkerStreamOnly = "optitrack_config/stream_markers_only";
    const::std::string Repeat = "optitrack_config/repeat";


    const::std::string ScaleFile = "optitrack_config/scale_file";
    const::std::string DataFile = "optitrack_config/data_file";
    const::std::string RosPackagePath = "optitrack_config/ros_package_path";

    const::std::string FAFile = "optitrack_config/forbidden_areas_path";

  }
}

ServerDescription::ServerDescription() :
  commandPort(ServerDescription::Default::CommandPort),
  dataPort(ServerDescription::Default::DataPort),
  multicastIpAddress(ServerDescription::Default::MulticastIpAddress),
  frameRate(ServerDescription::Default::FrameRate),
  simulation(ServerDescription::Default::simulation),
  speed(ServerDescription::Default::speed),
  dissapear(ServerDescription::Default::dissapear),
  same_frames(ServerDescription::Default::same_frames),
  noise(ServerDescription::Default::noise),
  maximum_residual(ServerDescription::Default::maximum_residual),
  simu_verbose(ServerDescription::Default::simu_verbose),
  reception_verbose(ServerDescription::Default::reception_verbose),
  association_verbose(ServerDescription::Default::association_verbose),
  icp_verbose(ServerDescription::Default::icp_verbose),
  guess_verbose(ServerDescription::Default::guess_verbose),
  filtering_verbose(ServerDescription::Default::filtering_verbose),
  ik_verbose(ServerDescription::Default::ik_verbose),  
  never_verbose(ServerDescription::Default::never_verbose),  
  error_verbose(ServerDescription::Default::error_verbose),
  stream_markers_only(ServerDescription::Default::stream_markers_only),
  repeat(ServerDescription::Default::repeat),
  ros_package_path(ServerDescription::Default::ros_package_path),
  scale_file(ServerDescription::Default::scale_file),
  data_file(ServerDescription::Default::data_file),
  trc_file(ServerDescription::Default::trc_file),
  record_trc(ServerDescription::Default::record_trc),
  forbidden_areas_file(ServerDescription::Default::forbidden_areas_file),
  fast_trc_read(ServerDescription::Default::fast_trc_read)
{}

void NodeConfiguration::fromRosParam(
  ros::NodeHandle& nh,
  ServerDescription& serverDescription, 
  PublisherConfigurations& pubConfigs,
  ModelConfiguration & modelConfig)
{
  // Get server cconfiguration from ROS parameter server
  if (nh.hasParam(rosparam::keys::MulticastIpAddress) )
  {
    nh.getParam(rosparam::keys::MulticastIpAddress, serverDescription.multicastIpAddress);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get multicast address, using default: " << 
      serverDescription.multicastIpAddress);
  }

  if (nh.hasParam(rosparam::keys::CommandPort) )
  {
    nh.getParam(rosparam::keys::CommandPort, serverDescription.commandPort);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get command port, using default: " << 
      serverDescription.commandPort);
  }

  if (nh.hasParam(rosparam::keys::DataPort) )
  {
    nh.getParam(rosparam::keys::DataPort, serverDescription.dataPort);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get data port, using default: " << 
      serverDescription.dataPort);
  }

  if (nh.hasParam(rosparam::keys::FrameRate) )
  {
    nh.getParam(rosparam::keys::FrameRate, serverDescription.frameRate);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get frame rate, using default: " << 
      serverDescription.frameRate);
  }

  if (nh.hasParam(rosparam::keys::Simu) )
  {
    nh.getParam(rosparam::keys::Simu, serverDescription.simulation);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get simulation parameter, using default: " << 
      serverDescription.simulation);
  }  

  if (nh.hasParam(rosparam::keys::Speed) )
  {
    nh.getParam(rosparam::keys::Speed, serverDescription.speed);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get simu_speed parameter, using default: " << 
      serverDescription.speed);
  }  


  if (nh.hasParam(rosparam::keys::Dissapear) )
  {
    nh.getParam(rosparam::keys::Dissapear, serverDescription.dissapear);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get simu_dissapear parameter, using default: " << 
      serverDescription.dissapear);
  }  

  if (nh.hasParam(rosparam::keys::Noise) )
  {
    nh.getParam(rosparam::keys::Noise, serverDescription.noise);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get simu_noise parameter, using default: " << 
      serverDescription.noise);
  }  


  if (nh.hasParam(rosparam::keys::SameFrames) )
  {
    nh.getParam(rosparam::keys::SameFrames, serverDescription.same_frames);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get simu_same_frames parameter, using default: " << 
      serverDescription.same_frames);
  }  


  if (nh.hasParam(rosparam::keys::MaxResidual) )
  {
    nh.getParam(rosparam::keys::MaxResidual, serverDescription.maximum_residual);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get maximum_residual parameter, using default: " << 
      serverDescription.maximum_residual);
  }  


  if (nh.hasParam(rosparam::keys::Verbose) )
  {
    nh.getParam(rosparam::keys::Verbose, serverDescription.simu_verbose);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get simu_verbose parameter, using default: " << 
      serverDescription.simu_verbose);
  }  

  if (nh.hasParam(rosparam::keys::ReceptionVerbose) )
  {
    nh.getParam(rosparam::keys::ReceptionVerbose, serverDescription.reception_verbose);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get reception_verbose parameter, using default: " << 
      serverDescription.reception_verbose);
  }  

  if (nh.hasParam(rosparam::keys::AssociationVerbose) )
  {
    nh.getParam(rosparam::keys::AssociationVerbose, serverDescription.association_verbose);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get association_verbose parameter, using default: " << 
      serverDescription.association_verbose);
  }  

  if (nh.hasParam(rosparam::keys::FilteringVerbose) )
  {
    nh.getParam(rosparam::keys::FilteringVerbose, serverDescription.filtering_verbose);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get filtering_verbose parameter, using default: " << 
      serverDescription.filtering_verbose);
  } 

  
  if (nh.hasParam(rosparam::keys::IKVerbose) )
  {
    nh.getParam(rosparam::keys::IKVerbose, serverDescription.ik_verbose);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get ik_verbose parameter, using default: " << 
      serverDescription.ik_verbose);
  }  

  if (nh.hasParam(rosparam::keys::NeverVerbose) )
  {
    nh.getParam(rosparam::keys::NeverVerbose, serverDescription.never_verbose);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get never_verbose parameter, using default: " << 
      serverDescription.never_verbose);
  }  


  if (nh.hasParam(rosparam::keys::GuessVerbose) )
  {
    nh.getParam(rosparam::keys::GuessVerbose, serverDescription.guess_verbose);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get guess_verbose parameter, using default: " << 
      serverDescription.guess_verbose);
  }  

  if (nh.hasParam(rosparam::keys::ICPVerbose) )
  {
    nh.getParam(rosparam::keys::ICPVerbose, serverDescription.icp_verbose);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get icp_verbose parameter, using default: " << 
      serverDescription.icp_verbose);
  }  


  if (nh.hasParam(rosparam::keys::MarkerStreamOnly) )
  {
    nh.getParam(rosparam::keys::MarkerStreamOnly, serverDescription.stream_markers_only);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get stream_markers_only parameter, using default: " << 
      serverDescription.stream_markers_only);
  }  

  if (nh.hasParam(rosparam::keys::Repeat) )
  {
    nh.getParam(rosparam::keys::Repeat, serverDescription.repeat);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get parameter " << rosparam::keys::Repeat);
    ROS_WARN_STREAM("Using default: " << serverDescription.repeat);
  }  

  if (nh.hasParam(rosparam::keys::ErrorVerbose) )
  {
    nh.getParam(rosparam::keys::ErrorVerbose, serverDescription.error_verbose);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get parameter " << rosparam::keys::ErrorVerbose);
    ROS_WARN_STREAM("Using default: " << serverDescription.error_verbose);
  }  

  if (nh.hasParam(rosparam::keys::CalibrationVerbose) )
  {
    nh.getParam(rosparam::keys::CalibrationVerbose, serverDescription.calibration_verbose);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get parameter " << rosparam::keys::CalibrationVerbose);
    ROS_WARN_STREAM("Using default: " << serverDescription.calibration_verbose);
  }  



  if (nh.hasParam(rosparam::keys::TrcFile) )
  {
    nh.getParam(rosparam::keys::TrcFile, serverDescription.trc_file);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get parameter " << rosparam::keys::TrcFile);
    ROS_WARN_STREAM("Using default: " << serverDescription.trc_file);
  }  
  modelConfig.trc_file = serverDescription.trc_file;

  // if (nh.hasParam(rosparam::keys::RecordTrc) )
  // {
  //   nh.getParam(rosparam::keys::RecordTrc, serverDescription.record_trc);
  // }
  // else 
  // {
  //   ROS_WARN_STREAM("Could not get parameter " << rosparam::keys::RecordTrc);
  //   ROS_WARN_STREAM("Using default: " << modelConfig.record_trc);
  // }  
  modelConfig.record_trc = serverDescription.record_trc;

  if (nh.hasParam(rosparam::keys::TypeRecord) )
  {
    nh.getParam(rosparam::keys::TypeRecord, serverDescription.type_record);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get parameter " << rosparam::keys::TypeRecord);
    ROS_WARN_STREAM("Using default: " << serverDescription.type_record);
  }  

  if (nh.hasParam(rosparam::keys::FTRead) )
  {
    nh.getParam(rosparam::keys::FTRead, serverDescription.fast_trc_read);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get parameter " << rosparam::keys::FTRead);
    ROS_WARN_STREAM("Using default: " << serverDescription.fast_trc_read);
  }    

  if (nh.hasParam(rosparam::keys::ScaleFile) )
  {
    nh.getParam(rosparam::keys::ScaleFile, serverDescription.scale_file);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get scale_file parameter, using default: " << 
      serverDescription.scale_file);
  }    

  if (nh.hasParam(rosparam::keys::DataFile) )
  {
    nh.getParam(rosparam::keys::DataFile, serverDescription.data_file);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get data_file parameter, using default: " << 
      serverDescription.data_file);
  }    



  if (nh.hasParam(rosparam::keys::RosPackagePath) )
  {
    nh.getParam(rosparam::keys::RosPackagePath, serverDescription.ros_package_path);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get ros_package_path parameter, using default: " << 
      serverDescription.ros_package_path);
  }    

    if (nh.hasParam(rosparam::keys::FAFile) )
  {
    nh.getParam(rosparam::keys::FAFile, serverDescription.forbidden_areas_file);
  }
  else 
  {
    ROS_WARN_STREAM("Could not get ros_package_path parameter, using default: " << 
      serverDescription.forbidden_areas_file);
  }    

  if (nh.hasParam(rosparam::keys::Version) )
  {
    nh.getParam(rosparam::keys::Version, serverDescription.version);
  }
  else
  {
    ROS_WARN_STREAM("Could not get server version, using auto");
  }  

  // Parse rigid bodies section
  if (nh.hasParam(rosparam::keys::RigidBodies))
  {
    XmlRpc::XmlRpcValue bodyList;
    nh.getParam(rosparam::keys::RigidBodies, bodyList);

    if (bodyList.getType() == XmlRpc::XmlRpcValue::TypeStruct && bodyList.size() > 0)
    {
      XmlRpc::XmlRpcValue::iterator iter;
      //for (iter = bodyList.begin(); iter != bodyList.end(); ++iter) {
      for (auto const& iter : bodyList)
      {
        std::string strBodyId = iter.first;
        XmlRpc::XmlRpcValue bodyParameters = iter.second;

        if (bodyParameters.getType() == XmlRpc::XmlRpcValue::TypeStruct) 
        {
          // check the TF parameters of the rigid body (as definied by Motive)
          if (bodyParameters[rosparam::keys::RigidBodyTFManagement].getType() == XmlRpc::XmlRpcValue::TypeStruct)
          {
            XmlRpc::XmlRpcValue TFParams = bodyParameters[rosparam::keys::RigidBodyTFManagement];
            // Load configuration for this rigid body from ROS
            PublisherConfiguration publisherConfig;
            std::sscanf(strBodyId.c_str(), "%d", &publisherConfig.rigidBodyId);
            
            bool readPoseTopicName = impl::check_and_get_param(TFParams, 
              rosparam::keys::PoseTopicName, publisherConfig.poseTopicName);

            if (!readPoseTopicName)
            {
              ROS_WARN_STREAM("Failed to parse " << rosparam::keys::PoseTopicName << 
                " for body `" << publisherConfig.rigidBodyId << "`. Pose publishing disabled.");
              publisherConfig.publishPose = false;
            }
            else
            {
              publisherConfig.publishPose = true;
            }

            bool readPose2dTopicName = impl::check_and_get_param(TFParams, 
              rosparam::keys::Pose2dTopicName, publisherConfig.pose2dTopicName);

            if (!readPose2dTopicName)
            {
              ROS_WARN_STREAM("Failed to parse " << rosparam::keys::Pose2dTopicName << 
                " for body `" << publisherConfig.rigidBodyId << "`. Pose publishing disabled.");
              publisherConfig.publishPose2d = false;
            }
            else
            {
              publisherConfig.publishPose2d = true;
            }

            bool readChildFrameId = impl::check_and_get_param(TFParams,
              rosparam::keys::ChildFrameId, publisherConfig.childFrameId);

            bool readParentFrameId = impl::check_and_get_param(TFParams,
              rosparam::keys::ParentFrameId, publisherConfig.parentFrameId);          

            if (!readChildFrameId || !readParentFrameId)
            {
              if (!readChildFrameId)
                ROS_WARN_STREAM("Failed to parse " << rosparam::keys::ChildFrameId << 
                  " for body `" << publisherConfig.rigidBodyId << "`. TF publishing disabled.");

              if (!readParentFrameId)
                ROS_WARN_STREAM("Failed to parse " << rosparam::keys::ParentFrameId << 
                  " for body `" << publisherConfig.rigidBodyId << "`. TF publishing disabled.");

              publisherConfig.publishTf = false;
            }
            else
            {
              publisherConfig.publishTf = true;
            }

            pubConfigs.push_back(publisherConfig);
          }

          // check the marker parameters of the rigid body
          if (bodyParameters[rosparam::keys::RigidBodyMarkersManagement].getType() == XmlRpc::XmlRpcValue::TypeStruct)
          {
            XmlRpc::XmlRpcValue bodyMarkParams = bodyParameters[rosparam::keys::RigidBodyMarkersManagement];

            if (bodyMarkParams[rosparam::keys::RigidBodyMarkersNames].getType() == XmlRpc::XmlRpcValue::TypeStruct)
            {
              XmlRpc::XmlRpcValue markerList = bodyParameters[rosparam::keys::RigidBodyMarkersNames];              
              std::vector<std::string> markerNames;
              for (int i=0; i < markerList.size(); i++)
              {
                auto strName =  markerList[i];
                std::string markName = strBodyId + "_" + (std::string&)strName;
                markerNames.push_back( markName);
              }
              serverDescription.allRigidBodiesMarkersNames[strBodyId] = markerNames;              
            }
            XmlRpc::XmlRpcValue initList = bodyMarkParams[rosparam::keys::RigidBodyAxisByMarkers];
            if (initList.size() > 0)
            {
              std::map<std::string,int> markerNamesAxis;
              XmlRpc::XmlRpcValue::iterator iter;
              for (auto const& iter : initList)
              {
                std::string markName = strBodyId + "_" + iter.first;
                markerNamesAxis[markName] = iter.second;
              }      
              serverDescription.allRigidBodiesMarkersAxis[strBodyId] = markerNamesAxis;
            }            
          }
        }
      }
    }
  }


  if (nh.hasParam(rosparam::keys::OpensimConfig))
  {
    // first, get q_calib
    XmlRpc::XmlRpcValue calibList;
    nh.getParam(rosparam::keys::QCalib, calibList);
    if (calibList.getType() == XmlRpc::XmlRpcValue::TypeStruct && calibList.size() > 0)
    {
      XmlRpc::XmlRpcValue::iterator iter;
      int size;
      std::map< int, double> map_cal;

      for (auto const& iter : calibList)
      {
        std::string key = iter.first;
        
        if (key == "size")
        {
          size = iter.second;
        }
        else
        {
          int index = std::stoi(key);

          double val;

          if (iter.second.getType() == XmlRpc::XmlRpcValue::TypeString)
          {
            std::string value = iter.second;
            if (value.find("M_PI") !=std::string::npos  )
            {
              if (value.find("M_PI_") !=std::string::npos  )
              {
                int i = std::stoi( value.substr( value.size() - 1,1 ) );
                if (i == 2)
                {
                  val = M_PI_2;
                }
                else if (i ==4)
                {
                  val = M_PI_4;
                }
              }

              else
              {
                val = M_PI;
              }

              if (value.find("-") !=std::string::npos)
              {
                val = -val;
              }

            }
          }

          else if (iter.second.getType() == XmlRpc::XmlRpcValue::TypeDouble)
          {
            val = (iter.second);
          }

          map_cal[index] = val;
        }


        
      }


      std::vector<double> q_calib(size,0.0);
      for (std::map<int,double>::iterator it = map_cal.begin(); it !=map_cal.end(); it++)
      {
        q_calib[it->first] = it->second;
      }
      modelConfig.q_calib = q_calib;
      
    
    }

    if (nh.hasParam(rosparam::keys::OsimPath) )
    {
      nh.getParam(rosparam::keys::OsimPath, modelConfig.osim_path);
    }
    else 
    {
      ROS_WARN_STREAM("Could not get osim_path, error. Opensim will be disactivated.");
      modelConfig.osim_path="";
    }


    if (nh.hasParam(rosparam::keys::GeomPath) )
    {
      nh.getParam(rosparam::keys::GeomPath, modelConfig.geometry_path);
    }
    else 
    {
      ROS_WARN_STREAM("Could not get geometry_path, error");
    }


    if (nh.hasParam(rosparam::keys::IKPath) )
    {
      nh.getParam(rosparam::keys::IKPath, modelConfig.setup_ik_path);
    }
    else 
    {
      ROS_WARN_STREAM("Could not get setup_ik_path, error");
    }


    if (nh.hasParam(rosparam::keys::Qtopic) )
    {
      nh.getParam(rosparam::keys::Qtopic, modelConfig.topic_q);
    }
    else 
    {
      ROS_WARN_STREAM("Could not get topic_q name, error");
    }


  }



};




}