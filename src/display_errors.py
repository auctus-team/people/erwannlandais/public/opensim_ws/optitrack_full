#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 14:50:30 2021

@author: auctus
"""

import rospy
import rospkg
import numpy as np

import os

import scipy.signal

from datetime import datetime

import matplotlib.pyplot as plt

import sys

from std_srvs.srv import SetBool
from optitrack_full.msg import all_errors
from sensor_msgs.msg import JointState

from optitrack_full.srv import listString, saveTxt

from scipy import integrate

class displayError:
    """!
    Convenient class that can display (with service calls) the errors of the
    current experimentation
    """    
    def __init__(self,rate):
        """!
        
        Constructor of the displayError class. 

        @param rate : float.
            Rate associated with the node.

        @return None.

        """

        rospy.init_node("displayError", anonymous=False, log_level=rospy.INFO)

        self.service_de = rospy.Service('display_errors',listString, self.display_errors)

        self.service_dps_spe = rospy.Service('display_position_speed_acceleration_specific',listString, self.display_pos_speed_acc_specific)

        self.service_stxt_spe =  rospy.Service('save_txt_specific',saveTxt, self.save_txt_specific)

        self.call_pause_continue = rospy.ServiceProxy('/pause_continue', SetBool)

        

        self.service_reboot = rospy.Service('reboot',SetBool, self.reboot_service)

        self.reboot()

        self.rate = rospy.Rate(rate)
        
        rospy.spin()
    
    def save_times_and_stop(self):
        
        if (len(self.L_time) > 0) :
            self.L_all_time.append(self.L_time)
            self.L_time = []
        
        if (len(self.L_time_ps) > 0):
            self.L_all_time_ps.append(self.L_time_ps)
            self.L_time_ps = []

        v = 0
        
        resp1 = 0
        while (not rospy.is_shutdown() ) and (resp1 == 0):
            try:
                ret = self.call_pause_continue(True)
                resp1 = 1
            except rospy.ServiceException as e :
                v+=1
                if (v%100) == 0:
                    print("Still try to pause...")
                if v> 1000:
                    print("Error. Please retry.")
                    resp1 = -1
        return(resp1)

    def save_txt_specific(self,msg):
        self.save_data = False
        resp1 = self.save_times_and_stop()

        Lstr = msg.data
        path = msg.pathToFolder

        if path[-1] == "/":
            path = path[:-1]

        if resp1 > 0:
            self.save_txt_data(Lstr,path)
            # stop call for stop     
        v = 0                    
        resp1 = False
        while (not rospy.is_shutdown() ) and (not resp1):
            try:
                ret = self.call_pause_continue(False)
                resp1 = True
            except rospy.ServiceException as e :
                v+=1
                if (v%100) == 0:
                    print("Still try to restart...")
                if v> 1000:
                    print("Error. Please try to restart manually.")   
                    resp1 = True                 
        print("Back to package...")
        self.save_data = True     
        return(True,"ok")       


    def display_pos_speed_acc_specific(self,msg):
        self.save_data = False
        resp1 = self.save_times_and_stop()

        Lstr = msg.data

        if resp1 > 0:
            self.display_joints_pos_speed_acc(Lstr)
            plt.show()
    
            # stop call for stop     
        v = 0                    
        resp1 = False
        while (not rospy.is_shutdown() ) and (not resp1):
            try:
                ret = self.call_pause_continue(False)
                resp1 = True
            except rospy.ServiceException as e :
                v+=1
                if (v%100) == 0:
                    print("Still try to restart...")
                if v> 1000:
                    print("Error. Please try to restart manually.")   
                    resp1 = True                 
        print("Back to package...")
        self.save_data = True     
        return(True,"ok")       

   
    
    def display_errors(self,msg):
        self.save_data = False
        resp1 = self.save_times_and_stop()
        # call for stop
        
        # next
        #try:
        if resp1 > 0:
            print("Start of displaying...")
            for k in range(len(msg.data)):
                if msg.data[k] == "all":
                    self.display_markers_pose_errors()
                    self.display_joints_errors()
                    self.display_tolerance_values()
                    self.display_markers_association_errors()
                    break
                elif msg.data[k] == "markers_pose_errors":
                    self.display_markers_pose_errors()
                elif msg.data[k] == "joints_errors":
                    self.display_joints_errors()
                elif msg.data[k] == "tolerance_values":
                    self.display_tolerance_values()   
                elif msg.data[k] == "markers_association_errors":
                    self.display_markers_association_errors() 
            # except Exception as e:
            #     print(e)
            plt.show()
    
            # stop call for stop     
        v = 0                    
        resp1 = False
        while (not rospy.is_shutdown() ) and (not resp1):
            try:
                ret = self.call_pause_continue(False)
                resp1 = True
            except rospy.ServiceException as e :
                v+=1
                if (v%100) == 0:
                    print("Still try to restart...")
                if v> 1000:
                    print("Error. Please try to restart manually.")                    
        print("Back to package...")
        self.save_data = True
        return(True)

    def reboot_service(self,msg):
        self.error_topic.unregister()
        self.position_speed_acc_topic.unregister()
        self.reboot()

    def reboot(self):
        self.L_time = []
        self.L_all_time = []
        self.D_joints_errors = {}
        self.markers_pose_errors = {}
        self.tolerance_values = {}
        # [ [markers_errors_at_i], ... ]
        self.L_markers_errors = []
        self.save_data = True

        self.L_time_ps = []
        self.L_all_time_ps = []
        self.D_joints_speeds={}
        self.D_joints_accs={}
        self.D_joints_pos= {}

        self.position_speed_acc_topic = rospy.Subscriber("/publish_position_speed_acceleration", JointState, self.callback_state)
    
        self.error_topic = rospy.Subscriber("/publish_errors", all_errors, self.callback_errors)
          
    def callback_state(self,msg):
        """!
        By convention, all joints are with 1 DOF only.
        """
        if (self.save_data):

            self.L_time_ps.append(msg.header.stamp.to_sec())

            names = msg.name

            for i in range(len(names)):

                if names[i] not in self.D_joints_pos:
                    self.D_joints_pos[names[i]] = []
                self.D_joints_pos[names[i]].append(msg.position[i])

                if names[i] not in self.D_joints_speeds:
                    self.D_joints_speeds[names[i]] = []
                self.D_joints_speeds[names[i]].append(msg.velocity[i])

                if names[i] not in self.D_joints_accs:
                    self.D_joints_accs[names[i]] = []
                self.D_joints_accs[names[i]].append(msg.effort[i])


    def callback_errors(self,msg):
        if len(msg.associated_markers_names) != 0 and self.save_data:
            self.L_time.append(msg.stamp.to_sec())            
            for i in range(len(msg.joint_names)):
                if msg.joint_names[i] not in self.D_joints_errors :
                    self.D_joints_errors[msg.joint_names[i]] = []
                self.D_joints_errors[msg.joint_names[i]].append(msg.articulation_errors[i])
            
            for i in range(len(msg.all_markers_names) ):
                if msg.all_markers_names[i] not in self.tolerance_values :
                    self.tolerance_values[msg.all_markers_names[i]] = []
                self.tolerance_values[msg.all_markers_names[i]].append(msg.association_tolerance_value[i])

                if msg.all_markers_names[i] not in self.markers_pose_errors :
                    self.markers_pose_errors[msg.all_markers_names[i]] = []

                if (msg.all_markers_names[i] not in msg.associated_markers_names):
                    self.markers_pose_errors[msg.all_markers_names[i] ].append( -1 )
    
            for i in range(len(msg.associated_markers_names)):
                name = msg.associated_markers_names[i]
                self.markers_pose_errors[name].append(msg.IK_markers_pose_errors[i])
            
            L = []
            for i in range(len(msg.markers_error)):
                L+= [msg.markers_error[i]]
            self.L_markers_errors.append(L)

    def str_for_txt(self,L_name_joint,time_list):
        """!
        
        The .txt file is organized as follows, for each line : 

        [ time values_of_all_joints speeds_of_all_joints acceleration_of_all_joints]
        
        """

        L_all_pose_list = []
        L_all_speed_list = []
        L_all_acc_list = []        
        min_speed = np.inf
        min_pos = np.inf
        min_acc = np.inf

        precision = 4

        for name_joint in L_name_joint :
            min_speed = min(min_speed, len(self.D_joints_speeds[name_joint]) )
            min_pos = min(min_pos, len(self.D_joints_pos[name_joint]) )
            min_acc = min(min_acc, len(self.D_joints_accs[name_joint]) )

            L_all_pose_list.append( self.D_joints_pos[name_joint] )
            L_all_speed_list.append( self.D_joints_speeds[name_joint] )
            L_all_acc_list.append( self.D_joints_accs[name_joint] )
        str_txt = ""

        min_all = min( min(min(min_speed,min_pos),min_acc),len(time_list) )

        for k in range(min_all):
            str_txt+="{:.4f}".format(time_list[k])
            str_pos = " "
            str_speed = ""
            str_acc = ""
            for j in range(len(L_all_pose_list)):
                str_pos+="{:.4f} ".format(L_all_pose_list[j][k])
                # # str(L_all_pose_list[j][k]) + " "  #
                # if abs(L_all_speed_list[j][k]) > 2:
                #     print("Speed of element %d : "%j, L_all_speed_list[j][k])
                str_speed+= "{:.4f} ".format(L_all_speed_list[j][k])
                #  str(L_all_speed_list[j][k]) + " " #
                str_acc+= "{:.4f} ".format(L_all_acc_list[j][k])
                # str(L_all_acc_list[j][k]) + " " #

            str_acc = str_acc[:-1] + "\n"
            #print(str_pos+str_speed+str_acc)
            str_txt= str_txt + (str_pos+str_speed+str_acc)
        str_txt = str_txt[:-1]
        return(str_txt)

    def manage_uniform_time(self):
        used_L_time = [self.L_all_time_ps[0][k]- self.L_all_time_ps[0][0] for k in range(len(self.L_all_time_ps[0]))]
        numBugOri = -1
        numBug = 0

        ecc = 0.0

        # suppress any jump superior to 1.0 s into the first component of used_L_time

        while (numBugOri != numBug ):
            used_L_time3 = [used_L_time[0], used_L_time[1]]
            if (numBug != 0):                
                numBugOri = numBug
            
            diff = 0.0 # used_L_time3[1]- used_L_time3[0]  
            ecc = max(used_L_time3[1]- used_L_time3[0],0.01)
            for i in range(2,len(used_L_time)):
                if (used_L_time[i] - used_L_time[i-1] ) > 2*ecc:
                    numBug += 1
                    supposed = used_L_time[i-1] + (used_L_time[i-1] - used_L_time[i-2])
                    diff = used_L_time[i] - supposed
                    print("One detected : before : %3.4f ; after : %3.4f"%(used_L_time[i-1], used_L_time[i]) )
                    print("With correction, new : ", used_L_time[i] - (diff))
            
                used_L_time3.append(used_L_time[i] - (diff) )

                ecc = (ecc + (used_L_time3[-1]- used_L_time3[-2]) )*(len(used_L_time3)-1)/(len(used_L_time3))
                
            
            if (numBug == 0):
                numBugOri = numBug
            used_L_time = used_L_time3        
            
        # suppress any jump superior to 1.0 s into the other component of used_L_time

        for l in range(1,len(self.L_all_time_ps)):

            curr_num = len(used_L_time)

            L_time = self.L_all_time_ps[l]
       
            supposed = used_L_time[-1] + (used_L_time[-1]  - used_L_time[-2] )

            diff = L_time[0] - supposed
            
            used_L_time2 = [L_time[k]- diff for k in range(len(L_time))]
   
            numBugOri = -1
            numBug = 0

            while (numBugOri != numBug ):
                used_L_time3 = [used_L_time2[0], used_L_time2[1]]
                if (numBug != 0):                
                    numBugOri = numBug
                diff = used_L_time3[1]- used_L_time3[0] 
                                    
                for i in range(2,len(used_L_time2)):
                    if (used_L_time2[i] - used_L_time2[i-1] ) > 2*ecc:
                        numBug += 1
                        supposed = used_L_time2[i-1] + (used_L_time2[i-1] - used_L_time2[i-2])
                        diff = used_L_time2[i] - supposed
                        print("One detected : before : %3.4f ; after : %3.4f"%(used_L_time2[i-1], used_L_time2[i]) )
                        print("With correction, new : ", used_L_time2[i] - (diff))
                    used_L_time3.append(used_L_time2[i] - (diff) )

                    ecc = (ecc + (used_L_time3[-1]- used_L_time3[-2]) )*(len(used_L_time3)+curr_num-1)/(len(used_L_time3)+curr_num)
                                
                if (numBug == 0):
                    numBugOri = numBug
                used_L_time2 = used_L_time3

            #print("aft trans : ", used_L_time2 )
            
            # then, add the components without any jump to used_L_time
            used_L_time += used_L_time2


        # for l in range(1,len(self.L_all_time_ps)):
        #     L_time = self.L_all_time_ps[l]
        #     last = used_L_time[-1]

        #     used_L_time2 = [L_time[k]- (L_time[0]-last) for k in range(len(L_time))]
   
        #     numBugOri = -1
        #     numBug = 0

        #     while (numBugOri != numBug ):
        #         used_L_time3 = []
        #         if (numBug != 0):                
        #             numBugOri = numBug
        #         last = used_L_time2[0]
        #         current = used_L_time2[0]                    
        #         for i in range(1,len(used_L_time2)):
        #             if (used_L_time2[i] - used_L_time2[i-1] ) > 1.0:
        #                 numBug += 1


        #                 # last = used_L_time2[i-1]
        #                 # current = used_L_time2[i]
        #                 # print("One detected : before : %3.4f ; after : %3.4f"%(last, current) )
        #                 # print("With correction, new : ", used_L_time2[i] - (current-last))
        #             used_L_time3.append(used_L_time2[i] - (current-last) )
                
        #         if (numBug == 0):
        #             numBugOri = numBug
        #         used_L_time2 = used_L_time3

        #     #print("aft trans : ", used_L_time2 )
            
        #     # then, add the components without any jump to used_L_time
        #     used_L_time += used_L_time2

        used_L_time = [used_L_time[k]- used_L_time[0] for k in range(len(used_L_time))]

        return(used_L_time)




    def save_txt_data(self,list_string,path):
        """!

        Create folder with current time at path.
        Into folder, save for each name in list_string a .txt file with
        time, position, speed, and acceleration. 


        """
        print("Saving txt files...")

        now = datetime.now()

        dt_string = now.strftime("%d_%m_%Y_%H_%M_%S")


        # try:
        #     os.mkdir(path+"/"+dt_string)
        # except OSError:
        #     print("Error; impossible to create the folder "+path+"/"+dt_string)
        #     return(False)

        used_L_time = self.manage_uniform_time()

        i = 0
        L_names = []
        str_names = ""
        for nameLS in list_string:
            if nameLS == "all":
                for name in self.D_joints_pos:
                    L_names.append(name)
                    str_names+= name+"-"
            elif nameLS in self.D_joints_pos:
                L_names.append(nameLS)
                str_names+= nameLS+"-"
        str_names = str_names[:-1]

        str_txt = self.str_for_txt(L_names,used_L_time)

        with open(path+"/"+dt_string+"-"+str_names+".txt","w+") as f:
            f.write(str_txt)

        return(True)    

    def display_joints_pos_speed_acc(self,list_string):
        print("Displaying position / speed / acceleration values...")
        # taken from : https://matplotlib.org/3.1.0/gallery/color/named_colors.html
        colors = ['red','green','blue','orange','black','peru','pink','purple',
                    "magenta","cyan","crimson","royalblue","slategrey","firebrick","chocolate","steelblue",
                    "navy","olive","deepskyblue","darkred", "aqua","darkkhaki","gold","peru",
                    "silver","darkorange","seagreen","coral","blueviolet","cadetblue","lawngreen","blueviolet"]
        #fig = plt.figure()
        #axs = fig.add_subplot(2,1,1)
        fig,axs = plt.subplots(3, 1)

        used_L_time = self.manage_uniform_time()

        i = 0

        integrate_check = False
        if "integrate_check" in list_string:
            integrate_check = True
        
        if "all" in list_string:
            for name in self.D_joints_pos:
                axs[0,].plot(used_L_time, self.D_joints_pos[name][:len(used_L_time)], label=name, color = colors[i+1])
                axs[1,].plot(used_L_time, self.D_joints_speeds[name][:len(used_L_time)], label=name, color = colors[i+1])
                axs[2,].plot(used_L_time, self.D_joints_accs[name][:len(used_L_time)], label=name, color = colors[i+1])

                if integrate_check:
                    n= 0
                    t = used_L_time[n:]
                    xdd =  self.D_joints_accs[name][:len(t)]
                    xd = integrate.cumtrapz(xdd, t, initial=self.D_joints_speeds[name][n])
                    x =  integrate.cumtrapz(xd, t, initial=self.D_joints_pos[name][n])
                    axs[0,].plot(t, x, label=name+"_integ", color = colors[i+1], linestyle="dashed")
                    axs[1,].plot(t, xd, label=name+"_integ", color = colors[i+1], linestyle="dashed")
                i+=1
            
        else:
            for nameLS in list_string:
                if nameLS in self.D_joints_pos:
                    name = nameLS
                    axs[0,].plot(used_L_time, self.D_joints_pos[name][:len(used_L_time)], label=name, color = colors[i+1])
                    axs[1,].plot(used_L_time, self.D_joints_speeds[name][:len(used_L_time)], label=name, color = colors[i+1])
                    axs[2,].plot(used_L_time, self.D_joints_accs[name][:len(used_L_time)], label=name, color = colors[i+1])

                    if integrate_check:
                        n= 0
                        t = used_L_time[n:]
                        xdd =  self.D_joints_accs[name][:len(t)]
                        xd = integrate.cumtrapz(xdd, t) #, initial= self.D_joints_speeds[name][n])
                        # print(xd.shape)
                        # print(len(t))
                        nd = len(t)-len(xd)
                        diffd = xd[0]-self.D_joints_speeds[name][nd]
                        xd = [xd[k]-diffd for k in range(len(xd))]
                        # print(xd[0:5])
                        # print(self.D_joints_speeds[name][nd:5+nd])                        
                        x =  integrate.cumtrapz(xd, t[nd:]) #, initial=self.D_joints_pos[name][n])
                        np = len(t)-len(x)
                        diff = x[0]-self.D_joints_pos[name][np]
                        x = [x[k]-diff for k in range(len(x))]

                        # print("---")
                        axs[0,].plot(t[np:], x, label=name+"_integ", color = colors[i+1], linestyle="dashed")
                        axs[1,].plot(t[nd:], xd, label=name+"_integ", color = colors[i+1], linestyle="dashed")                    
                i+=1

        axs[0,].set_xlabel("Time (s)")
        axs[0,].set_ylabel("Position (rad)")
        axs[0,].set_title("Value of all joints in time")
        axs[0,].legend()
        axs[1,].set_xlabel("Time (s)")
        axs[1,].set_ylabel("Speed (rad/s)")
        axs[1,].set_title("Speed of all joints in time")
        axs[1,].legend()
        axs[2,].set_xlabel("Time (s)")
        axs[2,].set_ylabel("Acceleration (rad/s²)")
        axs[2,].set_title("Acceleration of all joints in time")
        axs[2,].legend()        

        return(True)    



    def display_tolerance_values(self):
        print("Displaying tolerance values...")
        # taken from : https://matplotlib.org/3.1.0/gallery/color/named_colors.html
        colors = ['red','green','blue','orange','black','peru','pink','purple',
                    "magenta","cyan","crimson","royalblue","slategrey","firebrick","chocolate","steelblue",
                    "navy","olive","deepskyblue","darkred", "aqua","darkkhaki","gold","peru",
                    "silver","darkorange","seagreen","coral","blueviolet","cadetblue","lawngreen","blueviolet"]
        #fig = plt.figure()
        #axs = fig.add_subplot(2,1,1)
        fig,axs = plt.subplots(2, 1)

        used_L_time = self.manage_uniform_time()
            # print(last)

            # Lint = [L_time[k]- (L_time[0]) for k in range(len(L_time))]
            # print(Lint[0])
            # print(" ")
            # print(Lint[1])
            # used_L_time += [Lint[k] + last for k in range(len(Lint))]
            # print(Lint[0]+last)
            # print(" ")
            # print(Lint[1]+last)
        L_all_errs = [[] for k in range(len(self.tolerance_values))]
        L_all_means = [ ]
        for k in range(len(used_L_time)):
            
            mean = 0
            i = 0
            for name in self.tolerance_values:

                mean += self.tolerance_values[name][k]

                L_all_errs[i].append(self.tolerance_values[name][k])
                i+=1
            mean /= i
            L_all_means.append(mean)
            
        i = 0
        for name in self.tolerance_values:
            axs[1,].plot(used_L_time,L_all_errs[i], label=name, color = colors[i+1])
            i+=1
        
        axs[0,].plot(used_L_time,L_all_means,label="Mean tolerance on all markers", color = colors[0])

        axs[0,].set_xlabel("Time (s)")
        axs[0,].set_ylabel("Tolerance value (m)")
        axs[0,].set_title("Global mean marker tolerance value in time")
        axs[0,].legend()
        axs[1,].set_xlabel("Time (s)")
        axs[1,].set_ylabel("Tolerance value (m)")
        axs[1,].set_title("Specific marker tolerance value in time")
        axs[1,].legend()

        return(True)


    def display_joints_errors(self):
        print("Displaying joints errors...")
        # taken from : https://matplotlib.org/3.1.0/gallery/color/named_colors.html
        colors = ['red','green','blue','orange','black','peru','pink','purple',
                    "magenta","cyan","crimson","royalblue","slategrey","firebrick","chocolate","steelblue",
                    "navy","olive","deepskyblue","darkred", "aqua","darkkhaki","gold","peru",
                    "silver","darkorange","seagreen","coral","blueviolet","cadetblue","lawngreen","blueviolet"]
        #fig = plt.figure()
        #axs = fig.add_subplot(2,1,1)
        fig,axs = plt.subplots(2, 1)

        used_L_time = self.manage_uniform_time()


        L_all_errs = [[] for k in range(len(self.D_joints_errors))]
        L_all_means = [ ]
        for k in range(len(used_L_time)):
            
            mean = 0
            i = 0
            for name in self.D_joints_errors:

                mean += self.D_joints_errors[name][k]

                L_all_errs[i].append(self.D_joints_errors[name][k])
                i+=1
            mean /= i
            L_all_means.append(mean)
            
        i = 0
        for name in self.D_joints_errors:
            axs[1,].plot(used_L_time,L_all_errs[i], label=name, color = colors[i+1])
            i+=1
        
        axs[0,].plot(used_L_time,L_all_means,label="Mean Error on all articulations", color = colors[0])

        axs[0,].set_xlabel("Time (s)")
        axs[0,].set_ylabel("Difference between IK and reality")
        axs[0,].set_title("Global mean joint articulation error in time")
        axs[0,].legend()
        axs[1,].set_xlabel("Time (s)")
        axs[1,].set_ylabel("Difference between IK and reality")
        axs[1,].set_title("Specific joint articulation errors in time")
        axs[1,].legend()

        return(True)

    def display_markers_pose_errors(self):
        print("Displaying markers pose errors...")
        # taken from : https://matplotlib.org/3.1.0/gallery/color/named_colors.html
        colors = ['red','green','blue','orange','black','peru','pink','purple',
                    "magenta","cyan","crimson","royalblue","slategrey","firebrick","chocolate","steelblue",
                    "navy","olive","deepskyblue","darkred", "aqua","darkkhaki","gold","peru",
                    "silver","darkorange","seagreen","coral","blueviolet","cadetblue","lawngreen","blueviolet"]
        fig = plt.figure()
        ax0 = fig.add_subplot(2,1,1)
        ax1 = fig.add_subplot(2,1,2)
        #fig,axs = plt.subplots(2, 1)
        
        used_L_time = self.manage_uniform_time()

        L_all_errs = [[] for k in range(len(self.markers_pose_errors))]
        L_all_means = [ ]

        for k in range(len(used_L_time)):
            
            mean = 0
            i = 0
            l = 0

            for name in self.markers_pose_errors:

                if (self.markers_pose_errors[name][k] != -1):
                    mean += self.markers_pose_errors[name][k]
                    L_all_errs[i].append(self.markers_pose_errors[name][k])
                    l+=1
                else:
                    L_all_errs[i].append(0)
                i+=1
            mean /= l
            L_all_means.append(mean)
            
        i = 0
        for name in self.markers_pose_errors:
            #print(len(L_all_errs[i]))
            ax1.plot(used_L_time,L_all_errs[i], label=name, color = colors[i+1])
            i+=1
        
        ax0.plot(used_L_time,L_all_means,label="Mean Error on all markers pose", color = colors[0])

        ax0.set_xlabel("Time (s)")
        ax0.set_ylabel("Difference between IK and experimental (m)")
        ax0.set_title("Global mean marker pose error in time")
        ax0.legend()
        ax1.set_xlabel("Time (s)")
        ax1.set_ylabel("Difference between IK and experimental (m)")
        ax1.set_title("Specific marker pose errors in time")
        ax1.legend()

        return(True)        


    def display_markers_association_errors(self):
        print("Displaying markers association errors...")
        # taken from : https://matplotlib.org/3.1.0/gallery/color/named_colors.html
        colors = ['red','green','blue','orange','black','peru','pink','purple',
                    "magenta","cyan","crimson","royalblue","slategrey","firebrick","chocolate","steelblue",
                    "navy","olive","deepskyblue","darkred", "aqua","darkkhaki","gold","peru",
                    "silver","darkorange","seagreen","coral","blueviolet","cadetblue","lawngreen","blueviolet"]
        #fig = plt.figure()
        #axs = fig.add_subplot(2,1,1)
        fig,axs = plt.subplots(2, 1)

        used_L_time = self.manage_uniform_time()


        L_all_errs = [[] for k in range(len(self.tolerance_values))]
        L_tot = [ ]
        for k in range(len(used_L_time)):
            
            tot = 0
            i = 0

            for name in self.tolerance_values:
                if (name in self.L_markers_errors[k]):
                    tot += 1
                    L_all_errs[i].append(1)

                else:
                    L_all_errs[i].append(0)
                i+=1
            L_tot.append(tot)
            
        i = 0
        for name in self.tolerance_values:
            axs[1,].plot(used_L_time,L_all_errs[i], label=name, color = colors[i+1])
            i+=1
        
        axs[0,].plot(used_L_time,L_tot,label="Total missing associations", color = colors[0])

        axs[0,].set_xlabel("Time (s)")
        axs[0,].set_ylabel("Number of associations errors")
        axs[0,].set_title("Total number of associations errors in time")
        axs[0,].legend()
        axs[1,].set_xlabel("Time (s)")
        axs[1,].set_ylabel("Number of associations errors")
        axs[1,].set_title("Specific number of associations errors in time")
        axs[1,].legend()

        return(True)                


if __name__ == '__main__':

    ## scalingMananger object.
    sc = displayError(1)