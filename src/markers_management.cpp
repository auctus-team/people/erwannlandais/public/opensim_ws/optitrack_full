#include "optitrack_full/markers_management.h"

namespace markersManagement
{

    managementParameters::managementParameters()
    {
      transformation_epsilon = 1e-40;
      euclidean_fitness_epsilon = 1e-40;
      icp_max_iter = 1000;

      maximum_translation = 2.0;
      maximum_rotation = 5*M_PI/180;

    }

  /**
   * @brief Basic constructor for globalPublisher::globalPublisher object
   * 
   */
   globalPublisher::globalPublisher()
   {

   }

  /**
   * @brief Construct a new globalPublisher::globalPublisher object, and initialize all the 
   * variables of this object.
   * 
   * @param nh : ROS NodeHandler, used for the node.
   */
  globalPublisher::globalPublisher(ros::NodeHandle& nh, std::string topic_q)
  {
      // NB : for some kind of weird reason, it is really hard to make the rgb values match the 
      // colors that are displayed on Rviz (unless by expressing the colors only on one or two channels). 
      // That may be because Rviz express colors as rgba, whereas pcl only accept rgb colors. 

      // Optitrack publisher. Markers are displayed in orange.
      optitrack_publisher.markerPub = nh.advertise<sensor_msgs::PointCloud2>("optitrack_markers",1);
      optitrack_publisher.rgb[0] = 255; 
      optitrack_publisher.rgb[1] = 140; 
      optitrack_publisher.rgb[2] = 0; 

      optitrack_associated_publisher.markerPub = nh.advertise<optitrack_full::markers_names_indexes>("optitrack_associated_markers",1);
      optitrack_associated_publisher.rgb[0] = 255; 
      optitrack_associated_publisher.rgb[1] = 140; 
      optitrack_associated_publisher.rgb[2] = 255; 

      optitrack_robust_publisher.markerPub = nh.advertise<sensor_msgs::PointCloud2>("optitrack_robust_markers",1);
      optitrack_robust_publisher.rgb[0] = 100; 
      optitrack_robust_publisher.rgb[1] = 100; 
      optitrack_robust_publisher.rgb[2] = 100; 

      // Opensim publisher. Markers are displayed in green.
      opensim_publisher.markerPub = nh.advertise<sensor_msgs::PointCloud2>("osim_markers",1);
      opensim_publisher.rgb[0] = 0;
      opensim_publisher.rgb[1] = 255;
      opensim_publisher.rgb[2] = 140;                

      // Opensim theorical publisher (to display calibration pose only). Markers are displayed in purple.
      opensim_theorical_publisher.markerPub = nh.advertise<sensor_msgs::PointCloud2>("osim_theory_markers",1);
      opensim_theorical_publisher.rgb[0] = 140;
      opensim_theorical_publisher.rgb[1] = 0;
      opensim_theorical_publisher.rgb[2] = 255;               


      opBody.markerPub = nh.advertise<sensor_msgs::PointCloud2>("opti_body",1);
      opBody.rgb[0] = 255;
      opBody.rgb[1] = 0;
      opBody.rgb[2] = 0;      

      osBody.markerPub = nh.advertise<sensor_msgs::PointCloud2>("osim_body",1);
      osBody.rgb[0] = 0;
      osBody.rgb[1] = 140;
      osBody.rgb[2] = 255;      


      ArrowsOs.markerPub = nh.advertise<visualization_msgs::MarkerArray>("osim_tf",1);
      ArrowsOs.rgb[0] = 255;
      ArrowsOs.rgb[1] = 0;
      ArrowsOs.rgb[2] = 0;       

      ArrowsOp.markerPub = nh.advertise<visualization_msgs::MarkerArray>("opti_tf",1);
      ArrowsOp.rgb[0] = 0;
      ArrowsOp.rgb[1] = 255;
      ArrowsOp.rgb[2] = 0;   

      js_pub = nh.advertise<sensor_msgs::JointState>(topic_q, 1);

      // The Opensim to URDF referential transformation is a
      // Pi/2 rotation on X axis, then a Pi/2 rotation on Z axis.
      Eigen::AngleAxisd x_rot(M_PI_2, Eigen::Vector3d::UnitX ());
      Eigen::AngleAxisd z_rot(M_PI_2, Eigen::Vector3d::UnitZ ());
      Eigen::Quaternion<double> q = z_rot * x_rot;

      osMpi = q.matrix();

      Eigen::Quaternion<float> qf(q.w() , q.x() ,q.y(), q.z() );

      osMpi_f = qf.matrix();      

      publish_errors = nh.advertise<optitrack_full::all_errors>("/publish_errors",1);

  }

  /**
   * @brief Publish the association between the Opensim and Optitrack markers, and the position
   * of the associated Optitrack markers.
   * 
   * @param time 
   * @param corresNameID 
   * @param opti_markers 
   */

  void globalPublisher::publishOptitrackOpensimAssociation(ros::Time time, std::map<std::string,int> corresNameID,std::map<int,Eigen::Vector3d> opti_markers)
  {

    optitrack_full::markers_names_indexes msg;
    std::vector<std::string> names;
    std::vector<int> indexes;


    pcl::PointCloud<pcl::PointXYZRGBL>::Ptr PC(new pcl::PointCloud<pcl::PointXYZRGBL>);

    // create the pointcloud from the map and the caracteristics of the markerPubInfo object
    for (std::map<std::string,int>::iterator it = corresNameID.begin(); it!=corresNameID.end(); it++ )
    {

      if (opti_markers.find(it->second)!=opti_markers.end())
      {
        names.push_back(it->first);
        indexes.push_back(it->second);
        Eigen::Vector3d pos = osMpi*(opti_markers[it->second]);
        double x = pos[0];
        double y = pos[1];
        double z = pos[2];
        pcl::PointXYZRGBL pt(optitrack_associated_publisher.rgb[0], optitrack_associated_publisher.rgb[1], optitrack_associated_publisher.rgb[2], 
                              it->second);
        pt.x = x;
        pt.y = y;
        pt.z = z;
        (*PC).push_back(pt);
      }
    }



    // send the pointcloud with ROS
    sensor_msgs::PointCloud2 cloud;

    pcl::toROSMsg(*PC,cloud);

    cloud.header.frame_id = "ground";
    cloud.header.stamp = time;    

    msg.markers = cloud;
    msg.stamp = time;
    msg.names = names;
    msg.indexes = indexes;

    optitrack_associated_publisher.markerPub.publish(msg);








  }


  /**
   * @brief Display the axes corresponding to the referential expressed in transf. Those axes are displayed as Arrows,
   * where the red arrow corresponds to the first column of transf, the green arrow to the second column, and the blue arrow
   * to the third column.
   * Warning : transf should be expressed into the Opensim global referential (YXZ), as it will then be published into the classical
   * global referential (XYZ)
   * 
   * @param time 
   * @param transf 
   * @param displayObject 
   */
  void globalPublisher::displayArrows(ros::Time time, Eigen::Matrix4f transf, markerPubInfo displayObject, std::string frameID)
  {
    Eigen::Matrix4f T; T.setIdentity();
    T.block<3,3>(0,0) = osMpi_f;

    transf = T*transf;

    visualization_msgs::MarkerArray msg_delete;

    visualization_msgs::Marker marker_delete;

    marker_delete.header.frame_id = frameID;
    marker_delete.header.stamp = time;
    marker_delete.id = 20;
    marker_delete.type = visualization_msgs::Marker::ARROW;      
    marker_delete.action = visualization_msgs::Marker::DELETEALL;   

    msg_delete.markers.push_back(marker_delete); 

    displayObject.markerPub.publish(msg_delete);      

    visualization_msgs::MarkerArray msg;


    for (int i = 0; i < 3; i++)
    {
      
      visualization_msgs::Marker marker;

      marker.header.frame_id = "ground";
      marker.header.stamp = time;
      marker.id = i;
      marker.type = visualization_msgs::Marker::ARROW;      
      marker.action = visualization_msgs::Marker::ADD;

      geometry_msgs::Point pt;
  
      pt.x =  transf(0,3);
      pt.y =  transf(1,3);     
      pt.z =  transf(2,3);  
      marker.points.push_back(pt);

      pt.x =  0.1*transf(0,i) + transf(0,3);
      pt.y =  0.1*transf(1,i) + transf(1,3);
      pt.z =  0.1*transf(2,i) + transf(2,3);      
      marker.points.push_back(pt);
      if (i == 0)
      {
        marker.color.r = 1.0;
        marker.color.g = 0.0;
        marker.color.b = 0.0;        
      }
      else if (i == 1)
      {
        marker.color.r = 0.0;
        marker.color.g = 1.0;
        marker.color.b = 0.0;        
      }
      else
      {
        marker.color.r = 0.0;
        marker.color.g = 0.0;
        marker.color.b = 1.0;        
      }            
      marker.color.a = 1.0;
      marker.scale.x = 0.01;
      marker.scale.y = 0.02;
      marker.scale.z = 0.1;

      msg.markers.push_back(marker);

    }

    displayObject.markerPub.publish(msg);
  }


  /**
   * @brief Publish all the errors :
   * - Articulation errors : difference between the true joint values (given by simulation) and the
   * computed joint values.
   * - IK markers pose error : difference between the true position of the markers (given by simulation) and the
   * computed markers positions.
   * - Association Tolerance Value : authorized displacement computed for the position of each of the markers.
   */
  void globalPublisher::publishErrors(ros::Time time, ErrorsPublicationGestion & error_pub)
  {
    optitrack_full::all_errors msg;

    msg.stamp = time;
    msg.joint_names = error_pub.joint_names;
    msg.articulation_errors = error_pub.articulation_errors;
    msg.associated_markers_names = error_pub.associated_markers_names;
    msg.IK_markers_pose_errors = error_pub.IK_markers_pose_errors;
    msg.all_markers_names = error_pub.all_markers_names;
    msg.association_tolerance_value = error_pub.association_tolerance_value;
    msg.markers_error = error_pub.markers_error;

    for (int i = 0; i < error_pub.id_errors.size(); i++)
    {
      msg.id_errors.push_back(error_pub.id_errors[i]);
      msg.true_id.push_back(error_pub.true_id[i]);
    }


    publish_errors.publish(msg);

    clearErrorPublicationGestion(error_pub);

  }

  /**
   * @brief 
   * 
   * @param time 
   * @param markers 
   * @param markPub 
   * @param optitrackRef true : Optitrack : operation to cancel rotation at next method
   * false : Opensim : we don't need to correct this
   */
  void globalPublisher::markerPublisher(ros::Time time,std::map<std::string,Eigen::Vector3d> markers, markerPubInfo markPub, bool optitrackRef,  std::map<int, std::vector<int> > colors )
  {
    int i = 0;
    Eigen::Matrix3d correcMat = Eigen::Matrix3d::Identity();
    if (optitrackRef)
    {
      correcMat = osMpi.inverse().eval();
    }
    std::map<int,Eigen::Vector3d> IDMarkers;
    for( std::map<std::string,Eigen::Vector3d>::iterator it = markers.begin(); it!= markers.end(); it++)
    {
      IDMarkers[i] = correcMat*it->second;
      i++;
    }
    markerPublisher(time,IDMarkers,markPub, colors);

  }


  void globalPublisher::markerPublisher(ros::Time time,std::map<int,Eigen::Vector3d> markers, markerPubInfo markPub, std::map<int, std::vector<int> > colors )
  {
  pcl::PointCloud<pcl::PointXYZRGBL>::Ptr PC(new pcl::PointCloud<pcl::PointXYZRGBL>);

  // create the pointcloud from the map and the caracteristics of the markerPubInfo object
  for (std::map<int,Eigen::Vector3d>::iterator it = markers.begin(); it!=markers.end(); it++ )
  {
    std::vector<int> color;
    std::map<int, std::vector<int> >::iterator it2 = colors.find(it->first);
    if (it2 == colors.end())
    {
      color.push_back(markPub.rgb[0]);
      color.push_back(markPub.rgb[1]);
      color.push_back(markPub.rgb[2]);            
    }
    else
    {
      color = it2->second;
    }
    Eigen::Vector3d pos = osMpi*(it->second);
    double x = pos[0];
    double y = pos[1];
    double z = pos[2];
    pcl::PointXYZRGBL pt(color[0],color[1],color[2],
                          it->first );
    pt.x = x;
    pt.y = y;
    pt.z = z;
    (*PC).push_back(pt);
  }

  // send the pointcloud with ROS
  sensor_msgs::PointCloud2 cloud;

  pcl::toROSMsg(*PC,cloud);

  cloud.header.frame_id = "ground";
  cloud.header.stamp = time;
  markPub.markerPub.publish(cloud);

}



/**
 * @brief Convenient method to publish the Opensim markers as pointcloud.
 * 
 * @param time : ros publication time of the pointcloud message.
 * @param osim_pos : map object name_of_marker:position_of_marker of the opensim markers.
 */
void globalPublisher::publishOpensim(ros::Time time, std::map< std::string, Eigen::Vector3d > osim_pos, std::string name, std::map<std::string,int> lastOsNotAssigned)
{

  std::map< int, Eigen::Vector3d > osim_mark;

  std::map<int,std::vector<int> > colors;
  std::vector<int> notAssignedColor;

  if (name == "")
  {
    notAssignedColor.push_back( int(opensim_publisher.rgb[0]/2) );
    notAssignedColor.push_back( int(opensim_publisher.rgb[1]/2) );
    notAssignedColor.push_back( int(opensim_publisher.rgb[2]/2) );        
  }
  else if (name == "theorical")
  {
    notAssignedColor.push_back( int(opensim_theorical_publisher.rgb[0]/2) );
    notAssignedColor.push_back( int(opensim_theorical_publisher.rgb[1]/2) );
    notAssignedColor.push_back( int(opensim_theorical_publisher.rgb[2]/2) );        
  }


  int i=0;
  for (std::map<std::string,Eigen::Vector3d>::iterator it = osim_pos.begin(); it!=osim_pos.end(); it++ )
  {
    osim_mark[i] = it->second;

    std::map<std::string,int>::iterator it2 = lastOsNotAssigned.find(it->first);
    if (it2 != lastOsNotAssigned.end() && it2->second == 1)
    {
      colors[i] = notAssignedColor;
    }
    i++;
  }

  if (name == "")
  {
    markerPublisher(time, osim_mark, opensim_publisher, colors);
  }
  else if (name == "theorical")
  {
    markerPublisher(time, osim_mark, opensim_theorical_publisher, colors);
  }


}

/**
 * @brief Convenient method to publish at the same time opensim and optitrack markers.
 * It can also publish the theorical perfect pose that should have the opensim markers (which means, 
 * without an excessive rotation of the basis of the model on X and Z axis in Opensim referential), if the transformation 
 * between the world referential and the basis of the model is given.
 * The correction of the rotation matrix between Opensim and Optitrack referentiel is due to the fact this method is
 * used for the calibration process (in which the angle on X and Z axis should be close to 0).
 * 
 * @param time : ros publication time of the messages.
 * @param optitrackMarkers : map object index:position of the optitrack markers
 * @param osMod : OsimModel object of the Opensim model. Contains the position of the markers.
 * @param result_transf : Transformation between the world referential and the basis of the model. 
 * The default value desactivates the publishing of the theorical opensim markers.
 */
void globalPublisher::publishOpensimOptitrack(ros::Time time, std::map<int,Eigen::Vector3d> optitrackMarkers, fosim::OsimModel & osMod, Eigen::Matrix4f result_transf)
{

  // first, publish the opensim and optitrack markers.
  std::map< std::string, Eigen::Vector3d > D_mark_pos = osMod.D_mark_pos;
  publishOpensim(time,D_mark_pos);
  markerPublisher(time, optitrackMarkers, optitrack_publisher);

  // check if it is necessary to publish the theorical opensim pose
  Eigen::Matrix4f O; O.setZero();
  bool display_perfect = (!result_transf.isApprox(O) );

  // if this is the case, do the operation to publish this theorical pose
  if (display_perfect)
  {
    // filter the rotation matrix
    Eigen::Matrix4f transf = result_transf;
    transformFiltering(result_transf, 2.0, 10*M_PI/180);
    osMod.setGroundJointOnly(transf);

    // publish the position of the opensim markers
    std::map< std::string, Eigen::Vector3d > D_mark_pos_tho = osMod.D_mark_pos;

    std::map< int, Eigen::Vector3d > osim_mark_tho;
    int i=0;
    for (std::map<std::string,Eigen::Vector3d>::iterator it = D_mark_pos_tho.begin(); it!=D_mark_pos_tho.end(); it++ )
    {
      osim_mark_tho[i] = it->second;
      i++;
    }

    markerPublisher(time, osim_mark_tho, opensim_theorical_publisher);
    osMod.setGroundJointOnly(result_transf);

  }

}

/**
 * @brief Publish the joint configuration of the model.
 * 
 * @param time : publication time of the joint configuration.
 * @param D_joints_values : Joint configuration of the model, expressed as a coordinate_name:coordinate_value map.
 */
void globalPublisher::publishJointState(ros::Time time, std::map<std::string, double> D_joints_values)
{

    sensor_msgs::JointState js;

    js.header.stamp = time;
    js.header.frame_id = "ground";

    for(std::map<std::string, double>::iterator it = D_joints_values.begin(); it!= D_joints_values.end(); it++)
    {
      js.name.push_back(it->first);
      js.position.push_back(it->second);

    }

    js_pub.publish(js);
}


/**
 * @brief Convenient method to apply scaling to Opensim markers.
 * 
 * @param markersPos : map object name_of_marker:position_of_marker of the opensim markers.
 * @param scaling_factors : vector of scaling parameters, organized as < x_scaling, y_scaling, z_scaling >.
 * @return std::map<std::string,Eigen::Vector3d> : map object name_of_marker:position_of_marker of the opensim markers, scaled.
 */

std::map<std::string,Eigen::Vector3d> applyScalingToMarkers(std::map<std::string, Eigen::Vector3d> markersPos, std::vector<double> scaling_factors)
{
  double xS = scaling_factors[0];
  double yS = scaling_factors[1];
  double zS = scaling_factors[2];
  std::map<std::string,Eigen::Vector3d> rescaleMarkers;

  for (std::map<std::string,Eigen::Vector3d>::iterator it = markersPos.begin(); it!= markersPos.end(); it++)
  {
    Eigen::Vector3d pos = it->second;
    pos[0] = pos[0]*xS;
    pos[1] = pos[1]*yS;
    pos[2] = pos[2]*zS;
    rescaleMarkers[it->first] = pos;

  }

  return(rescaleMarkers);

}

/**
 * @brief Given a pointcloud, computes the index of the two elements that are the most distant and opposite of the 
 * centroid of the pointcloud (on the X-Z plane). It also returns how many points are on the same side than the vector (farthest_point_from_centroid,centroid)
 * (= number_pos, for number of positive points) and how many points are on the opposite side (= number_neg, for number of negative points).
 * 
 * First, get the farthest point from the centroid of the model (on the X-Z plane).
 * Thanks to this point, 
 * Once this point F is obtained, computes the vector u from the centroid and F; and computes
 * the angle between this vector and 
 * 
 * 
 * @param centroid : Position of the centroid of the point cloud (express with homogeneous coordinates).
 * @param index_side_1 : Index of the farthest point from the centroid
   @param index_side_2 : Index of the 2nd farthest point from the centroid
   @param number_pos : Number of points whose vector (centroid->point) have the same direction than the farthest point
   @param number_neg : Number of points whose vector (centroid->point) have the opposite direction than the farthest point
 * @param PC : PointCloud.
 * @return int : index of the farthest point of the input Point cloud from the Point cloud centroid.
 */
int getFarthestUppestFromCentroid(Eigen::Vector4f centroid,
int & index_side_1,
int & index_side_2,
int & number_pos,
int & number_neg,
pcl::PointCloud<pcl::PointXYZ>::Ptr PC)
{
  //std::printf("In theory \n");
  double max_dist = 0.0;
  double dist;
  int index = -1;
  // First, get the farthest point from the centroid of the model (on the X-Z plane).
  for (int i = 0; i < (*PC).size(); i++)
  {
    pcl::PointXYZ pt = (*PC)[i];
    //std::printf("Index : %i ; position (z,x) : [%f,%f] \n", i, pt.z,pt.x);
    dist = sqrt( std::pow(pt.x - centroid[0],2.0) + std::pow(pt.z - centroid[2],2.0)  );
    if (dist > max_dist)
    {
      max_dist = dist;
      index = i;
    }
  }
  index_side_1 = index;
  // Once this point F is obtained, computes the vector u from the centroid and F; and computes
  // the angle between this vector and the referential in which the pointcloud is expressed.
  double zvec, xvec;
  zvec = (*PC)[index].z-centroid[2];
  xvec = (*PC)[index].x-centroid[0];
  
  Eigen::Matrix3f T; 
  T.setIdentity();

  double angle = atan2(xvec,zvec);
  T(0,0) = cos(angle);
  T(0,1) = -sin(angle);
  T(1,0) = sin(angle);
  T(1,1) = cos(angle);
  T(0,2) = centroid[2];
  T(1,2) = centroid[0];

  double max_dist_2 = 0.0;
  number_neg = 0;
  number_pos = 1;

  // Then, computes the second farthest point from the centroid (and on the opposite direction of
  // the u vector), and the number of positive / negative points.
  for (int i = 0; i < (*PC).size(); i++)
  {
    if (i != index)
    {
      pcl::PointXYZ pt = (*PC)[i];
      Eigen::Vector3f pt_e(pt.z,pt.x,1);
      Eigen::Vector3f pt_e2 = T.inverse()*pt_e;
      //std::printf("Index : %i ; position (z,x) : [%f,%f] \n", i, pt_e2[0],pt_e2[1]);
      dist = sqrt( std::pow(pt_e2[0],2.0) + std::pow(pt_e2[1],2.0)  );
      if (dist > max_dist_2 && pt_e2[0]< 0)
      {
        max_dist_2 = dist;
        index_side_2 = i;
      }
      if (pt_e2[0] < 0)
      {
        number_neg +=1;
      }
      else if (pt_e2[0] > 0)
      {
        number_pos +=1;
      }
    }


  }
  // std::cout << "index_side_1 : " << index_side_1 << std::endl;
  // std::cout << "index_side_2 : " << index_side_2 << std::endl;
  // std::cout << "number_pos : " << number_pos << std::endl;
  // std::cout << "number_neg : " << number_neg << std::endl;  
  // std::printf("Difference : %f \n", abs(max_dist_2-max_dist) );

  if (abs(max_dist_2-max_dist) > 0.01)
  {
    number_pos = -1;
    number_neg = -1;
  }

  return(index);

}


/**
 * @brief Check the variation of the distance of the Opensim markers of a same body between two frames, without taking into account
 * the time between the two frames.
 * To do so, get the maximum variation of the markers of the body between the two frames. Then, bound it by half of the minimum distance between
 * the markers of the body for the upper limit and by the maximum residual parameter for the lower limit. Finally, apply this variation to all markers of the body.
 * 
 * @param D_mark_all_dist_ori : map object containing, for each opensim marker, the distances of the other markers of its body and their names. Thoses distances are
 * recorded for a first frame.
 * @param D_mark_all_dist : map object containing, for each opensim marker, the distances of the other markers of its body and their names. Thoses distances are
 * recorded for a second frame.
 * @param variation : map object name_of_marker:double, containing for each opensim marker an estimation of the maximum variation of its distance towards the other 
 * markers of its body.
 * @param maximum_residual : maximum residual value obtained from the calibration of the
 *  Optitrack system (in m). 
 */
void allDistanceVariationStatic( std::map< std::string, std::map<std::string,double> > D_mark_all_dist_ori, std::map< std::string,  std::map< std::string, double> > D_mark_all_dist, std::map<std::string, double>& variation , double maximum_residual, double mdc)
{
  std::vector<std::string> already_treated;

  for (std::map<std::string, std::map<std::string, double > >::iterator it = D_mark_all_dist.begin(); it!=D_mark_all_dist.end(); it++ )
  {

    // select a marker from the second frame, record it as an already treated marker
    // (as we're going to get all the variations of the distances of this marker to the other markers,
    // we won't need to compute its variation when we're going to deal with the other markers of its body).
    already_treated.push_back(it->first);

    // get the distance from this marker to the other markers of its body in the first and second frame
    std::map< std::string, double> mark_dist = it->second;
    std::map< std::string, double> mark_dist_ori = D_mark_all_dist_ori[it->first];
    double max_var = maximum_residual;
    double min_dist = 9999;

    std::vector<std::string> currently_treated;

    // for all markers that were not already treated, get the maximum variation of the distance of all 
    // those markers between the two frames.
    // Also get the minimum distance between those markers.
    for (std::map<std::string,double>::iterator it2 = mark_dist.begin(); it2!=mark_dist.end(); it2++ )
    {
      if (std::find(already_treated.begin(), already_treated.end(), it2->first) == already_treated.end())
      {
        double d = it2->second;
        double d_ori = mark_dist_ori[it2->first];
        max_var= std::max( max_var, abs( (d_ori-d) ) );
        currently_treated.push_back(it2->first);
        if (d < min_dist)
        {
          min_dist = d;
        }

      }

    }

    // Apply this variation to all markers that were not already treated. This variation is bounded by the previous variation parameter
    // for the lower limit and by half the distance between this markers and the first marker for the upper limit.
    if (currently_treated.size() >0 )
    {
      for (int i =0; i < currently_treated.size(); i++)
      {
        std::string name = currently_treated[i];
        double half_mark_dist = std::max(mark_dist[name], mark_dist_ori[name])/(mdc);
        variation[name] = std::min( std::max(variation[name],max_var), half_mark_dist );
        
      }
      // Finally, apply this variation to the first marker. 
      variation[it->first] = std::min( std::max(variation[it->first],max_var), min_dist/(mdc) );
     
    }
  }

}

/**
 * @brief Check the variation of the distance of the Opensim markers of a same body between two frames, and takes into account
 * the time between the two frames.
 * To do so, get the maximum variation of the markers of the body between the two frames. Then, bound it by half of the minimum distance between
 * the markers of the body for the upper limit and by the maximum residual parameter for the lower limit. Once this variation is obtained, compare
 * this variation to the previous variation, while taking into account the time between the two frames.
 * 
 * @param D_mark_all_dist_ori : map object containing, for each opensim marker, the distances of the other markers of its body and their names. Thoses distances are
 * recorded for a first frame.
 * @param D_mark_all_dist : map object containing, for each opensim marker, the distances of the other markers of its body and their names. Thoses distances are
 * recorded for a second frame.
 * @param variation : map object name_of_marker:double, containing for each opensim marker an estimation of the maximum variation of its distance towards the other 
 * markers of its body.
 * @param maximum_residual : maximum residual value obtained from the calibration of the
 *  Optitrack system (in m). 
 * @param dt : time between the two frames.
 * @param verbose : boolean displaying the main computational step of the method, for debug purpose.
 */
void allDistanceVariationDynamic2( std::map< std::string, std::map<std::string,double> > D_mark_all_dist_ori, std::map< std::string,  std::map< std::string, double> > D_mark_all_dist, std::map<std::string, double>& variation , double maximum_residual, double dt, bool verbose)
{
  std::vector<std::string> already_treated;

  for (std::map<std::string, std::map<std::string, double > >::iterator it = D_mark_all_dist.begin(); it!=D_mark_all_dist.end(); it++ )
  {
    verbose = (it->first == "LRS");
    if (verbose) {std::cout << "Go with : " << it->first << std::endl;}

    // select a marker from the second frame, record it as an already treated marker
    // (as we're going to get all the variations of the distances of this marker to the other markers,
    // we won't need to compute its variation when we're going to deal with the other markers of its body).    
    already_treated.push_back(it->first);

    // get the distance from this marker to the other markers of its body in the first and second frame
    std::map< std::string, double> mark_dist = it->second;
    std::map< std::string, double> mark_dist_ori = D_mark_all_dist_ori[it->first];
    double max_var = maximum_residual;
    double min_dist = 9999;

    std::vector<std::string> currently_treated;

    // for all markers that were not already treated, get the maximum variation of the distance of all 
    // those markers between the two frames.
    // Also get the minimum distance between those markers.
    for (std::map<std::string,double>::iterator it2 = mark_dist.begin(); it2!=mark_dist.end(); it2++ )
    {
      if (std::find(already_treated.begin(), already_treated.end(), it2->first) == already_treated.end())
      {
        double d = it2->second;
        double d_ori = mark_dist_ori[it2->first];
        max_var= std::max( max_var, abs( (d_ori-d) ) );
        currently_treated.push_back(it2->first);
        if (d < min_dist)
        {
          min_dist = d;
        }

      }

    }

    if (verbose) {std::cout << "Max_var computed : " << max_var << std::endl;}

    // Compute this candidate variation to all markers that were not already treated. This variation is bounded by the previous variation parameter
    // for the lower limit and by half the distance between this markers and the first marker for the upper limit.
    // Then, apply this candidate variation by comparing it with the previous variation and the time dt between the two frames.
    // In order to prevent this variation from diverging to too large values, it is also subtracted and bounded by the parameter maximum_residual.
    if (currently_treated.size() >0 )
    {
      double candidate_var;
      double wta;
      std::string name;
      for (int i =0; i < currently_treated.size(); i++)
      {
        name = currently_treated[i];
        double half_mark_dist = std::max(mark_dist[name], mark_dist_ori[name])/(2*sqrt(3));
        candidate_var = std::min( std::max(variation[name],max_var), half_mark_dist );
        
        wta = (candidate_var-variation[name])*dt;
        if (abs(wta)/dt <1e-6)
        {
          wta = -maximum_residual*dt;
        }
        //variation[name] = std::max(variation[name] + wta - maximum_residual*dt,maximum_residual );
        variation[name] = std::max(variation[name] + wta,maximum_residual );
        if (verbose) {std::printf("  Maj variation for : %s : %f \n", name.c_str(), variation[name]); }
      }
      // Finally, apply this operation to the first marker. 
      name = it->first;      
      candidate_var = std::min( std::max(variation[it->first],max_var), min_dist/(2*sqrt(3)) );
      wta = (candidate_var-variation[it->first] )*dt;
        if (abs(wta)/dt <1e-6)
        {
          wta = -maximum_residual*dt;
        }      
      if (verbose) {
        std::printf("  Candidate var for : %s : %f \n", name.c_str(), candidate_var);
        std::printf("  Variation : %f ; wta : %f \n", variation[name], wta);
        }        

      //variation[it->first] = std::max(variation[it->first] + wta - maximum_residual*dt ,maximum_residual );
      variation[name] = std::max(variation[name] + wta,maximum_residual );
      if (verbose) {std::printf(" Value for treated : %s : %f \n", it->first.c_str(), variation[it->first]);}
    }

  }

  if (verbose)
  {
    std::printf("---- Resume ---- \n"); 
    for (std::map<std::string,double>::iterator it = variation.begin(); it!= variation.end(); it++)
    {
      std::printf(" Value for treated : %s : %f \n", it->first.c_str(), variation[it->first]);
    }

  }


  if (verbose) {std::printf("--------\n"); }

}


/**
 * @brief Check the variation of the distance of the Opensim markers of a same body between two frames, and takes into account
 * the time between the two frames.
 * To do so, get the maximum variation of the markers of the body between the two frames. Then, bound it by half of the minimum distance between
 * the markers of the body for the upper limit and by the maximum residual parameter for the lower limit. Once this variation is obtained, compare
 * this variation to the previous variation, while taking into account the time between the two frames.
 * 
 * @param D_mark_all_dist_ori : map object containing, for each opensim marker, the distances of the other markers of its body and their names. Thoses distances are
 * recorded for a first frame.
 * @param D_mark_all_dist : map object containing, for each opensim marker, the distances of the other markers of its body and their names. Thoses distances are
 * recorded for a second frame.
 * @param D_mark_all_dist_theorical : distance according to Opensim model. 
 * @param variation : map object name_of_marker:double, containing for each opensim marker an estimation of the maximum variation of its distance towards the other 
 * markers of its body.
 * @param maximum_residual : maximum residual value obtained from the calibration of the
 *  Optitrack system (in m). 
 * @param dt : time between the two frames.
 * @param verbose : boolean displaying the main computational step of the method, for debug purpose.
 PBME : MINDISTLIM PASSE SON TEMPS A CHANGER !!! A MOD ASAP
 Check si pas le cas plus haut


 */
void allDistanceVariationDynamic( std::map< std::string, std::map<std::string,double> > D_mark_all_dist_ori, 
std::map< std::string,  std::map< std::string, double> > D_mark_all_dist, 
std::map<std::string, std::map<std::string,double> > D_mark_all_dist_theorical,
std::map<std::string, double>& variation ,
 double maximum_residual, double dt, double mdc, 
 bool verbose)
{
  std::vector<std::string> already_treated;

  for (std::map<std::string, std::map<std::string, double > >::iterator it = D_mark_all_dist.begin(); it!=D_mark_all_dist.end(); it++ )
  {
    //verbose = (it->first == "LRS");
    if (verbose) {std::cout << "Go with : " << it->first << std::endl;}

    // select a marker from the second frame, record it as an already treated marker
    // (as we're going to get all the variations of the distances of this marker to the other markers,
    // we won't need to compute its variation when we're going to deal with the other markers of its body).    
    already_treated.push_back(it->first);

    // get the distance from this marker to the other markers of its body in the first and second frame
    std::map< std::string, double> mark_dist = it->second;
    std::map< std::string, double> mark_dist_ori = D_mark_all_dist_ori[it->first];
    std::map< std::string, double> mark_dist_theorical = D_mark_all_dist_theorical[it->first];
    
    double max_var = 0.0;
    double min_dist = 9999;
    std::string name_min;

    std::vector<std::string> currently_treated;

    // for all markers that were not already treated, get the maximum variation of the distance of all 
    // those markers between the two frames.
    // Also get the minimum distance between those markers.
    for (std::map<std::string,double>::iterator it2 = mark_dist.begin(); it2!=mark_dist.end(); it2++ )
    {
      if (D_mark_all_dist.find(it2->first) != D_mark_all_dist.end())
      {
        double d_tho = mark_dist_theorical[it2->first];
        if (d_tho < min_dist)
        {
          //std::cout << d_tho << std::endl;
          min_dist = d_tho;
          //name_min = it->first;
        }

        if (std::find(already_treated.begin(), already_treated.end(), it2->first) == already_treated.end())
        {
          double d = it2->second;
          double d_ori = mark_dist_ori[it2->first];
          
          max_var= std::max( max_var, abs( (d_ori-d) ) );
          currently_treated.push_back(it2->first);
          // if (d < min_dist)
          // {
          //   min_dist = d;
          // }

        }
      }

    }

    if (verbose) {std::cout << "Max_var computed : " << max_var << std::endl;}

    // Compute this candidate variation to all markers that were not already treated. This variation is bounded by the previous variation parameter
    // for the lower limit and by half the distance between this markers and the first marker for the upper limit.
    // Then, apply this candidate variation by comparing it with the previous variation and the time dt between the two frames.
    // In order to prevent this variation from diverging to too large values, it is also subtracted and bounded by the parameter maximum_residual.
    if (currently_treated.size() >0 )
    {
      double candidate_var;
      double wta;
      std::string name;
      for (int i =0; i < currently_treated.size(); i++)
      {
        name = currently_treated[i];
        double half_mark_dist = mark_dist_theorical[name]/(mdc);  //std::max(mark_dist[name], mark_dist_origin[name])/(mdc);
        //candidate_var = std::min( std::max(variation[name],max_var), half_mark_dist );
        candidate_var = std::min( std::max(0.0,max_var), half_mark_dist );
        wta = (candidate_var-variation[name])*dt;
        if (abs(wta)/dt <1e-6)
        {
          wta = -maximum_residual*dt;
        }
        //variation[name] = std::max(variation[name] + wta - maximum_residual*dt,maximum_residual );
        variation[name] = std::max(variation[name] + wta,maximum_residual );
        //if (verbose) {std::printf("  Maj variation for : %s : %f \n", name.c_str(), variation[name]); }
      }
      // Finally, apply this operation to the first marker. 
      name = it->first;      
      //candidate_var = std::min( std::max(variation[it->first],max_var), min_dist/(mdc) );
      candidate_var = std::min( std::max(0.0,max_var), min_dist/(mdc) );
      wta = (candidate_var-variation[it->first] )*dt;
      if (abs(wta)/dt <1e-6)
      {
        wta = -maximum_residual*dt;
      }      
      if (verbose) {std::printf("  Candidate var for : %s : %f \n", name.c_str(), candidate_var);
      std::printf("  Min_dist_lim : %f ; Variation : %f ; wta : %f \n", min_dist/(mdc), variation[name], wta);
      }        

      //variation[it->first] = std::max(variation[it->first] + wta - maximum_residual*dt ,maximum_residual );
      variation[name] = std::max(variation[name] + wta,maximum_residual );
      if (verbose) {std::printf(" Value for treated : %s : %f \n", it->first.c_str(), variation[it->first]);}
    }

  }

  if (verbose)
  {
    std::printf("---- Resume ---- \n"); 
    for (std::map<std::string,double>::iterator it = variation.begin(); it!= variation.end(); it++)
    {
      std::printf(" Value for treated : %s : %f \n", it->first.c_str(), variation[it->first]);
    }

  }


  if (verbose) {std::printf("--------\n"); }

}

/**
 * @brief Get a guess about the angle (around the Y axis) between the point clouds corresponding to theorical opensim and optitrack markers.
 * 
 * @param PC : PointCloud corresponding to optitrack markers.
 * @param farthest_osim : farthest theorical opensim marker from the centroid of the opensim model, when
 * the model got the calibration configuration.
 * @param centroid_opti : position of the centroid of the optitrack point cloud. This centroid will be computed into this method.
 * @param angle : angle on the y axis between the opensim theorical markers and the recorded optitrack markers. This angle will be computed into this method.
 * @param manParams : managementParameters object, containing all necessary static parameters for this method.
 */
void getGuess(pcl::PointCloud<pcl::PointXYZ>::Ptr PC, 
pcl::PointXYZ farthest_osim, 
int side,
Eigen::Vector4f & centroid_opti, 
double & angle, 
managementParameters manParams)
{
  // First, get farthest point from centroid and centroid of optitrack 3d point cloud

  int tst2 = pcl::compute3DCentroid( (*PC), centroid_opti);

  int index1, index2;
  int number_pos;
  int number_neg;

  int index = getFarthestUppestFromCentroid(centroid_opti, index1,index2, number_pos, number_neg,PC);
  if (side == 0)
  {
    index = index1;
  }
  else if (side == 1)
  {
    index = index2;
  }

  pcl::PointXYZ farthest_opti = (*PC)[index];

  // Decompose optitrack point cloud on x-z components.

  pcl::PointCloud<pcl::PointXYZ>::Ptr PC_xz(new pcl::PointCloud<pcl::PointXYZ>);

  for (int i = 0; i < (*PC).size(); i++)
  {
    pcl::PointXYZ pt = (*PC)[i];
    pt.y = 0.0;
    (*PC_xz).push_back(pt);
    if (manParams.guess_verbose)
    {
      std::printf("Index : %i ; position (z,x) : [%f,%f] \n", i, pt.z,pt.x);
    }
  }

  // for each point cloud, compute the direction vector from the centroid point and
  // the farthest point from the centroid. 
  // As the calibration pose should not be symetrical, we then have a global idea of
  // the direction of each point cloud. 
  // Also gets the sign of each component of those directions.

  double opti_dir_z = farthest_opti.z- centroid_opti[2];
  double opti_dir_x = farthest_opti.x- centroid_opti[0];

  double osim_dir_z = farthest_osim.z;
  double osim_dir_x = farthest_osim.x;

  int sign_opti_x =  -2*signbit(opti_dir_x)+1 ;
  int sign_opti_z =  -2*signbit(opti_dir_z)+1 ;  

  int sign_osim_x = -2*signbit(farthest_osim.x) + 1;
  int sign_osim_z = -2*signbit(farthest_osim.z)+1;

  if (manParams.guess_verbose)
  {
    std::printf("osim_far (z,x)  : [%f, %f] \n", osim_dir_z,osim_dir_x);
    std::printf("opti vec approx (z,x)  : [%f, %f] \n", opti_dir_z, opti_dir_x );
  }  

  // In order to get a correct approximation of the position of the optitrack direction,
  // we must take into account the angle between the opensim direction and the z axis.
  // According to the vectorial product, we can know if this angle should be substracted
  // to the optitrack direction or if it should be added.

  double vectorial_product = opti_dir_z*osim_dir_x - opti_dir_x*osim_dir_z;
  int sign_vec = -2*signbit(vectorial_product)+1;

  // special case at -PI, PI
  if (sign_osim_z*(  sign_opti_z ) < 0 && abs(vectorial_product)<0.01)
  {
    if (manParams.guess_verbose) { std::printf("Change sign vec prod \n");}
    sign_vec *= -1;
  }

  if (manParams.guess_verbose)
  {
    printf("Vectorial product value : %f \n", vectorial_product);
    printf("Vectorial product : %d \n", sign_vec);
  }  

  double angle_osim_dir = atan2( (farthest_osim.x)/farthest_osim.z ,1);

  if ( -sign_vec*angle_osim_dir < 0 )
  {
    angle_osim_dir *= -1;
  }

  // Finally, take into account the opensim angle for the optitrack angle.

  double ori_dir_x = opti_dir_x;
  double ori_dir_z = opti_dir_z;  

  opti_dir_z = cos(angle_osim_dir)*ori_dir_z - sin(angle_osim_dir)*ori_dir_x;
  opti_dir_x = sin(angle_osim_dir)*ori_dir_z + cos(angle_osim_dir)*ori_dir_x;

  sign_opti_z =  -2*signbit(opti_dir_z)+1 ;
  sign_opti_x =  -2*signbit(opti_dir_x)+1 ;

  int same_side = sign_osim_z*(  sign_opti_z );

  // As the rotation on the Y axis could only be on [-Pi, Pi], 
  // the next operation is meant to check if, compared to the original opensim 
  // pointcloud direction, the optitrack pointcloud direction has moved 
  // anticlockwise or clockwise.
  // WARNING : this check is only correct if the two directions are not on the same
  // part of the x-axis (z < 0 or z > 0).
  // rotation sense > 0 : + PI : anticlockwise direction
  // rotation sense < 0 : - PI : clockwise direction
  int rotation_sense = (sign_osim_z*sign_opti_x );  

  if (manParams.guess_verbose)
  {
    std::printf("Angle osim dir : %f \n", angle_osim_dir );
    std::printf("new opti vec approx (z,x)  : [%f, %f] \n",opti_dir_z,opti_dir_x);
  }

  // use PCA analysis to get a more precise estimation of the angle between 
  // the optitrack point cloud and the z axis.

  pcl::PCA<pcl::PointXYZ> pcaGolden;
  pcaGolden.setInputCloud(PC_xz);
  Eigen::Matrix3f goldenEVs_Dir = pcaGolden.getEigenVectors();    

  double used2_m = (goldenEVs_Dir(0,0) ) / (goldenEVs_Dir(2,0) );

  double angle2 = atan2(used2_m,1);

  if (manParams.guess_verbose)
  {
      std::cout << "PCA Angle ori : " << angle2 << std::endl;
  }

  // As atan is only defined on [-PI/2, PI/2], check if the optitrack direction and the
  // opensim direction are on the same part x-axis (z < 0 or z> 0).
  // If they are not, the next operation must be applied.
  if (  same_side < 0)
  {
    angle2 = M_PI-abs(angle2);
    if (manParams.guess_verbose)
    {
      std::cout << "Not same side of x plane ; PCA angle : " << angle2 << std::endl;
    }

  }

  // In addition, check if compared to the original opensim pointcloud direction, the optitrack
  // pointcloud direction has moved anticlockwise. If it is the case, the next operation must be done.
  if ( same_side < 0 &&  rotation_sense <0 ) 
    {
      angle2 = -angle2;
      if (manParams.guess_verbose)
      {
          std::cout << "Turn to - PI; PCA angle : " << angle2<< std::endl;  
      }
    }
 
  angle = angle2;

}

std::string printAssociation(std::map< std::string, int > corresNameID)
{
  std::stringstream s;
  int i =0;
  for (std::map<std::string, int>::iterator it = corresNameID.begin(); it!=corresNameID.end(); it++ )
    {
      //std::printf("Marker name : %s (%d) ; optitrack associated : %d \n ", it->first.c_str(),i, it-> second);
      s << "Marker name : " <<  it->first << " (" << i <<") ; optitrack associated : " << it->second << std::endl;      
      i++;
    }
  std::cout << s.str() << std::endl;

  return(s.str());
}

std::string printAssociationByBody(std::map< std::string, int > corresNameID, fosim::OsimModel & osMod)
{
  std::stringstream s;
  int i =0;
  std::map<std::string,int> map_name_ind;
  for (std::map<std::string, int>::iterator it = corresNameID.begin(); it!=corresNameID.end(); it++ )
    {
      map_name_ind[it->first] = i;
      //std::printf("Marker name : %s (%d) ; optitrack associated : %d \n ", it->first.c_str(),i, it-> second);
      i++;
    }

  for (std::map<std::string, std::vector<std::string> >::iterator it = osMod.D_body_markers.begin(); it!= osMod.D_body_markers.end(); it++ )
  {
    std::vector<std::string> markers_of_body = it->second;
    s << "Body name concerned : "<< it->first << std::endl;
    //std::printf("Body name concerned : %s \n",it->first.c_str());
    for (int i = 0; i < markers_of_body.size(); i++)
    {
      std::string markName = markers_of_body[i];
      s << "---- Marker name :" <<  markName << " (" << map_name_ind[markName] << ") ; optitrack associated : "<< corresNameID[markName] << std::endl;
      //std::printf("---- Marker name : %s (%d) ; optitrack associated : %d \n", markName.c_str(), map_name_ind[markName], corresNameID[markName]);
    }
  }

  std::cout << s.str() << std::endl;
  return(s.str());

}


int startBodyFiltering(  managementParameters & manParams,
                    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp,
                    fosim::OsimModel & osMod,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr osimPC,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr optiPC,
                      std::vector<std::string> osim_name,
                      std::vector<int> opti_indexes,
                      std::map<int, Eigen::Vector3d > optitrackPoints,
                      Eigen::Matrix4f result_transf ,
                      std::map< std::string, int > & corresNameID,
                      std::map<std::string,double> & distOsimOpti,
                      bool verbose)
{
  std::map<std::string, std::string> D_markers_body = osMod.D_markers_body;
  return( startBodyFiltering(
                                    manParams,
                                    icp,
                                    D_markers_body,
                                    osimPC,
                                    optiPC,
                                    osim_name,
                                    opti_indexes,
                                    optitrackPoints,
                                    result_transf,
                                    corresNameID,
                                    distOsimOpti,
                                    false
   ) );
}

/**
 * @brief Method called just after ICP calibration, only for one specific body part. Once an estimate of the correspondences have been computed,
 * this method checks that those correspondences are correct (= every Opensim marker is associated with a unique and different
 * Optitrack marker). If this last rule is not correct, the method uses the computed transform result_transf to find those correspondences.
 * 
 * WHAT CAN BE MODIFIED : 
 *  - corresNameID
 *  - distOsimOpti
 * 
 * @param manParams managementParameters object. 
 * @param icp ICP object, containing the correspondences between Opensim and Optitrack pointclouds.
 * @param osMod : OsimModel object, corresponding to the theorical Opensim model. Contains the position of the markers.
 * @param osimPC PointCloud corresponding to the position of the theoretical Opensim markers.
 * @param optiPC PointCloud corresponding to the position of the Optitrack markers
 * @param osim_name : name of the opensim markers, on the same order as the index of the elements of osimPC.
 * @param optitrackPoints 
 * @param result_transf 
 * @param corresNameID 
 * @return int Int indicating whether the filtering operation was a success or not.
 * 0 : no bug occured
1 : a bug occured, but we may have fixed it
2 : a bug occured and recovery failed
 */
int startBodyFiltering(  managementParameters & manParams,
                    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp,
                    std::map<std::string,std::string> D_markers_body,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr osimPC,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr optiPC,
                      std::vector<std::string> osim_name,
                      std::vector<int> opti_indexes,
                      std::map<int, Eigen::Vector3d > optitrackPoints,
                      Eigen::Matrix4f result_transf ,
                      std::map< std::string, int > & corresNameID,
                      std::map<std::string,double> & distOsimOpti,
                      bool verbose)
{
        bool body_part_error = false;
        std::vector<int> pc_opti_indexes;
        std::vector<int> pc_osim_indexes;

        std::map<int, std::string > mark_body_parts;

        int num_osim = osimPC->size();
        pcl::CorrespondencesPtr corres = icp.correspondences_;
        double sum = 0;

        int check = 0;

        // index_match : optitrack
        // index_query : opensim
        // osim_name : name of all the opensim markers by order

        for (int i = 0; i < (*corres).size(); i++)
        {
          pcl::Correspondence cortst= (*corres)[i];
          if (manParams.filtering_verbose)
          {
            std::cout << "(In BF) : Opensim : " << osim_name[cortst.index_query] << " | Optitrack : " << opti_indexes[cortst.index_match] << " | " << cortst.distance << std::endl; 
          }          
          if ( mark_body_parts.find( opti_indexes[cortst.index_match]) != mark_body_parts.end() )
          {
            // means the optitrack marker has already been associated with another marker, which is an issue.
            // disqualify body part
            body_part_error = true;
          }
          // else if (cortst.index_match >= osim_name.size() )
          // {
          //   body_part_error = true;
          // }
          else
          {
            pc_osim_indexes.push_back(cortst.index_query);
            mark_body_parts[opti_indexes[cortst.index_match]] = D_markers_body[ osim_name[cortst.index_query]];
          }
          
       }

       if (num_osim != mark_body_parts.size())
       {
         // the number of theorical opensim markers is different from the total of optitrack marker recorded.
         // it means that at least one opensim marker was not attributed; raise error.
         body_part_error = true;

       }

      // now we know if there is an error or no. 
      

      // method : rescale osim ac scale_factors (ok)
      // osim -> optitrack transformation sur osim markers (pc)
      // closest, selon meth def plus bas
      if (!body_part_error)
      {

        pcl::PointCloud<pcl::PointXYZ>::Ptr osimMovedPC(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::transformPointCloud(*osimPC, *osimMovedPC, result_transf);

        std::map<std::string,Eigen::Vector3d> D_mark_pos;
        

        for (int i = 0; i< osim_name.size(); i++)
        {
          pcl::PointXYZ pt = (*osimMovedPC)[i];
          Eigen::Vector3d vect(pt.x,pt.y,pt.z);
          D_mark_pos[osim_name[i]] = vect;
        }

        std::vector<int> verif_opti_indexes;

        // directly associate markers
        for (int i = 0; i < (*corres).size(); i++)
        {

          pcl::Correspondence cortst= (*corres)[i];
          //std::string osim_body = osMod.D_markers_body[ osim_name[cortst.index_query]];
          // association seems ok. Let's add it.
          //verif_opti_indexes.push_back( opti_indexes[cortst.index_match] );
          //pc_osim_indexes.push_back(cortst.index_query);

          // inde

          int opti_index = opti_indexes[cortst.index_match];
          corresNameID[ osim_name[cortst.index_query] ] = opti_index;
          distOsimOpti[ osim_name[cortst.index_query] ] = cortst.distance;
        }

        if (manParams.filtering_verbose)
        {   printf("---- All good case ---- \n");
            printf("---- Opensim positions ---- \n");
            for (std::map<std::string,int>::iterator it = corresNameID.begin(); it!= corresNameID.end(); it++ )
            {
              if (optitrackPoints.find(it->second)!= optitrackPoints.end())
              {
                Eigen::Vector3d osPos = D_mark_pos[it->first];
                std::printf("Opensim name : %s ; position : [%3.4f, %3.4f, %3.4f] \n ", it->first.c_str(), osPos[0], osPos[1], osPos[2]);
                Eigen::Vector3d opPos = optitrackPoints[it->second];
                std::printf("Optitrack ID : %d ; position : [%3.4f, %3.4f, %3.4f] \n ", it->second,  opPos[0], opPos[1], opPos[2]);
              }

            }
        }        

      }
      //
      else
      {

        // apply pointcloud transformation
                if (manParams.filtering_verbose)
                {
        std::cout << "Issue detected in BF; try to fix it ... " << std::endl;
                }
        pc_osim_indexes.clear();        
        pcl::PointCloud<pcl::PointXYZ>::Ptr osimMovedPC(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::transformPointCloud(*osimPC, *osimMovedPC, result_transf);

        std::map<std::string,Eigen::Vector3d> D_mark_pos;
        
        for (int i = 0; i< osim_name.size(); i++)
        {
          pcl::PointXYZ pt = (*osimMovedPC)[i];
          Eigen::Vector3d vect(pt.x,pt.y,pt.z);
          D_mark_pos[osim_name[i]] = vect;
        }

        if (manParams.filtering_verbose)
        {
            printf("---- Opensim positions ---- \n");
            for (std::map<std::string, Eigen::Vector3d>::iterator it = D_mark_pos.begin(); it!=D_mark_pos.end(); it++ )
            {
                std::printf("Opensim name : %s ; position : [%3.4f, %3.4f, %3.4f] \n ", it->first.c_str(), it-> second[0], it->second[1], it->second[2]);
            }

            printf("---- Optitrack positions ---- \n");
            for (std::map<int, Eigen::Vector3d>::iterator it = optitrackPoints.begin(); it!=optitrackPoints.end(); it++ )
            {
                std::printf("Optitrack ID : %d ; position : [%3.4f, %3.4f, %3.4f] \n ", it->first, it-> second[0], it->second[1], it->second[2]);
            }

        }

        check = 1;
        
        // for each body part concerned, search the closest optitrack markers
        std::vector<std::string> error_markers = osim_name;

        std::vector<int> optitrack_considered;
        for (std::map<int,Eigen::Vector3d>::iterator it = optitrackPoints.begin(); it!=optitrackPoints.end(); it++)
        {
          optitrack_considered.push_back(it->first);
        }

        int num_mark = error_markers.size();

        std::vector< std::vector<double> > osim_all_dist_to_opti;
        // check distance of each opensim marker to each optitrack marker

        for (int j =0; j < num_mark; j++)
        {
          Eigen::Vector3d osim_mark = D_mark_pos[error_markers[j] ];
          std::vector<double> all_dist_to_opti;
          for (int k =0; k< optitrack_considered.size(); k++)
          {
            Eigen::Vector3d opti_pos = optitrackPoints[ optitrack_considered[k] ];
            all_dist_to_opti.push_back( (osim_mark - opti_pos).norm()   );
          }

          osim_all_dist_to_opti.push_back(all_dist_to_opti);

        }

        int opti_id = 0;

        std::vector<std::string> temp_osim_name = error_markers;

        //while (opti_id < optitrackPoints.size() )
        // continuer check
        while (num_mark > 0 && opti_id < optitrackPoints.size())
        {
          // one optitrack marker has been selected by opti_id
          // check opensim markers available
          int osim_index = -1;
          double dist_cons = 9999;
          
          for (int l = 0; l < num_mark; l++)
          {
            // for selected optitrack, check distances with all remaining opensim markers

            double dist_osim_opti = osim_all_dist_to_opti[l][opti_id];
            if (dist_osim_opti < dist_cons)
            {
              dist_cons = dist_osim_opti;
              osim_index = l;
            }

          }

          if (osim_index != -1)
          {
            // associate opensim Marker and optitrack
            std::string opensim_mark_name = temp_osim_name[osim_index] ;
            corresNameID[ opensim_mark_name  ] = optitrack_considered[opti_id];
            distOsimOpti[opensim_mark_name] = dist_cons;
            opti_indexes.push_back(optitrack_considered[opti_id]);

            // suppress opensim Marker, go to next optitrack marker
            osim_all_dist_to_opti.erase(osim_all_dist_to_opti.begin()+ osim_index);
            temp_osim_name.erase(temp_osim_name.begin()+ osim_index);
            num_mark--;
            opti_id++;
          }
          else
          {
            // error detected
            check = 2;
            opti_id++;
          }



        }
        
        if (manParams.filtering_verbose) {printAssociation(corresNameID);}

      }

    //display results
    if (verbose)
    {
      std::printf(" ---- FINAL ---- \n");
      std::map<std::string,int> bodyCorresNameID;
      for (int i = 0; i < osim_name.size(); i++)
      {
        bodyCorresNameID[osim_name[i]] = corresNameID[osim_name[i]];
      }
      printAssociation(bodyCorresNameID);
    }


    return(check);


}

/*
Return check : 
0 : no bug occured
1 : a bug occured, but we may have fixed it
2 : a bug occured and recovery failed

*/
int startGlobalFiltering(  managementParameters & manParams,
                    pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp,
                    fosim::OsimModel & osMod,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr osimPC,
                      pcl::PointCloud<pcl::PointXYZ>::Ptr optiPC,
                      std::vector<std::string> osim_name,
                      std::vector<int> opti_indexes,
                      std::vector<double> q_calib,
                      std::map<int, Eigen::Vector3d > optitrackPoints,
                      Eigen::Matrix4f result_transf ,
                      std::map< std::string, int > & corresNameID)
{

        std::vector<std::string> body_parts_error;
        std::vector<int> pc_opti_indexes;
        std::vector<int> pc_osim_indexes;

        std::map<int, std::string > mark_body_parts;

        int num_osim = osMod.D_markers_body.size();
        pcl::CorrespondencesPtr corres = icp.correspondences_;
        double sum = 0;

        int check = 0;

        // index_match : optitrack
        // index_query : opensim
        // osim_name : name of all the opensim markers by order


        for (int i = 0; i < (*corres).size(); i++)
        {
          pcl::Correspondence cortst= (*corres)[i];

          if (manParams.filtering_verbose)
          {
            std::cout << "Opensim : " << osim_name[cortst.index_query] << " | Optitrack : " << opti_indexes[cortst.index_match] << " | " << cortst.distance << std::endl; 
          }    

          if ( mark_body_parts.find( opti_indexes[cortst.index_match]) != mark_body_parts.end() )
          {
            // means the optitrack marker has already been associated with another marker, which is an issue.
            // disqualify body part
            
            std::string body_disq = osMod.D_markers_body[ osim_name[cortst.index_query]];
            if ( std::find(body_parts_error.begin(), body_parts_error.end(), body_disq) == body_parts_error.end() )
            {
              body_parts_error.push_back(body_disq);
              
            }
            // also disqualify the body part where the optitrack marker was first associated
            std::string first_body_disq = mark_body_parts[opti_indexes[cortst.index_match]];
            if ( std::find(body_parts_error.begin(), body_parts_error.end(), first_body_disq) == body_parts_error.end() )
            {
              body_parts_error.push_back(first_body_disq);
              
            }
            if (manParams.filtering_verbose)
            {
              std::cout << "Disqualify body " << body_disq << " and body " << first_body_disq;
              std::cout << " over Optitrack marker : " << cortst.index_match << std::endl;
            }

          }
          else
          {
            pc_osim_indexes.push_back(cortst.index_query);
            mark_body_parts[opti_indexes[cortst.index_match] ] = osMod.D_markers_body[ osim_name[cortst.index_query]];
          }
          
       }

       if (num_osim != mark_body_parts.size())
       {
         // the number of theorical opensim markers is different from the total of optitrack marker recorded.
         // it means that at least one opensim marker was not attributed; let's check which one, in order to 
         // add its body to the list of body parts where there is an attribution issue.
         for (int i =0; i < num_osim; i++)
         {
           if  ( std::find(pc_osim_indexes.begin(),pc_osim_indexes.end(), i) == pc_osim_indexes.end() ) 
           {
              // osim index i do not have any correspondences
              std::string body_disq = osMod.D_markers_body[ osim_name[i]];
              if ( std::find(body_parts_error.begin(), body_parts_error.end(), body_disq) == body_parts_error.end() )
              {
                body_parts_error.push_back(body_disq);
              }

           }
         }

       }

       // now we have all body parts where something is wrong.
      pc_osim_indexes.clear();
      std::vector<int> verif_opti_indexes;


        for (int i = 0; i < (*corres).size(); i++)
        {
          pcl::Correspondence cortst= (*corres)[i];
          std::string osim_body = osMod.D_markers_body[ osim_name[cortst.index_query]];
          if ( std::find(body_parts_error.begin(), body_parts_error.end(), osim_body) == body_parts_error.end() )
          {
            // association seems ok. Let's add it.
            verif_opti_indexes.push_back( opti_indexes[  cortst.index_match] );
            pc_osim_indexes.push_back(cortst.index_query);

            int opti_index = opti_indexes[cortst.index_match];
            corresNameID[ osim_name[cortst.index_query]] = opti_index;
          }

       }

        pcl::PointCloud<pcl::PointXYZ>::Ptr osimOriented(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::transformPointCloud(*osimPC, *osimOriented, result_transf);

        std::map<std::string, Eigen::Vector3d> D_osim_pos;
        for (int i = 0; i < osimOriented->points.size(); i++)
        {
          Eigen::Vector3d pos(osimOriented->points[i].x, osimOriented->points[i].y, osimOriented->points[i].z);
          D_osim_pos[osim_name[i]] = pos;
        }

        std::map<std::string, double> D_mark_dist = fosim::getClosestMarkerFromOtherBody(osMod.osimModel,D_osim_pos);

        if (manParams.filtering_verbose)
        {
            fosim::printMarkersPositions(D_osim_pos);
            printf("---- Optitrack positions ---- \n");
            for (std::map<int, Eigen::Vector3d>::iterator it = optitrackPoints.begin(); it!=optitrackPoints.end(); it++ )
            {
                std::printf("Optitrack ID : %d ; position : [%3.4f, %3.4f, %3.4f] \n ", it->first, it-> second[0], it->second[1], it->second[2]);
            }
            fosim::printJointsValues(osMod.osimModel,osMod.osimState);
        }


      if (body_parts_error.size() != 0)
      {
        check = 1;
        std::cout << "Issue detected in GF; try to fix it ... " << std::endl;
        // for each body part concerned, search the closest optitrack markers
        for (int i = 0; i< body_parts_error.size(); i++)
        {
          if (manParams.filtering_verbose)  {std::cout << "Current body part : " << body_parts_error[i] << std::endl;}
          std::vector<std::string> error_markers = osMod.D_body_markers[body_parts_error[i]];
          std::vector< Eigen::Vector3d > pos_markers;

          pcl::PointCloud<pcl::PointXYZ>::Ptr osimPC_cons(new pcl::PointCloud<pcl::PointXYZ>);
          pcl::PointCloud<pcl::PointXYZ>::Ptr optiPC_cons(new pcl::PointCloud<pcl::PointXYZ>);

          std::vector<int> optitrack_considered;
          std::vector<int> optitrack_excluded = verif_opti_indexes;
          std::vector<std::string> osim_considered;

          for (int j = 0; j < error_markers.size(); j++)
          {
            Eigen::Vector3d osim_mark = D_osim_pos[error_markers[j] ];
            osim_considered.push_back(error_markers[j]);
            pcl::PointXYZ pt_os(osim_mark[0], osim_mark[1], osim_mark[2] );
            (*osimPC_cons).push_back(pt_os);
          }

          // NB : finalement, plus malin de chercher sur opensim
          // rescaled et non sur opensim originel pour distance.
          // simplement trouver pour chaque opensim du PC son
          // optitrack le plus proche non sélectionné

          double max_dist = D_mark_dist[error_markers[0]];
          int num_mark = 0;
          int tol = 1;
          int max_tol = 2;

          // for each osim, search closest opti marker that was not already selected
          for (int j = 0; j < error_markers.size(); j++)
          {
            double min_dist_mark = max_dist;
            Eigen::Vector3d osim_mark = D_osim_pos[error_markers[j] ];
            std::map<int, Eigen::Vector3d >::iterator it_choose = optitrackPoints.end();

            // for the osim_mark, search the closest optitrack marker
            for (std::map<int, Eigen::Vector3d >::iterator it =optitrackPoints.begin(); it != optitrackPoints.end(); it++)
            {
              double dist = (it->second-osim_mark).norm();
              if ( ( dist < min_dist_mark ) && (std::find(optitrack_considered.begin(), optitrack_considered.end(), it->first) == optitrack_considered.end() ) )
              {
                if (std::find(optitrack_excluded.begin(), optitrack_excluded.end(), it->first) == optitrack_excluded.end() )
                {
                  min_dist_mark = dist;
                  it_choose = it;
                }
              }

            }

            if (it_choose != optitrackPoints.end())
            {
              if (manParams.filtering_verbose) { 
                printf("From (opensim marker) : [ %3.4f, %3.4f, %3.4f ] \n", osim_mark[0], osim_mark[1], osim_mark[2]) ; 
                printf("Add (optitrack marker) : [ %3.4f, %3.4f, %3.4f ] \n", it_choose->second[0], it_choose->second[1], it_choose->second[2]);
              }
              optitrack_considered.push_back(it_choose->first);
              num_mark++;
              // reset max_dist
              if (j < error_markers.size() -1)
              {
                max_dist = osMod.D_mark_dist[error_markers[j+1]];
              }
              tol = 1;              

            }
            // failed; let's try to add a little more tolerance
            else if (tol < max_tol)
            {  if (manParams.filtering_verbose) { 
                printf("Increase tolerance... \n");
              }
              max_dist +=  osMod.D_mark_dist[error_markers[j]];
              j--;
              tol++;
            }
            // otherwise, failed even after rising tolerance

            

          }

          if (num_mark != error_markers.size() )
          {
            printf("Error : it seems there is not enough optitrack markers. \n");
            check = 2;
          }

          // try to associate optitrack markers considered to opensim markers
          // else
          // {
            std::vector< std::vector<double> > osim_all_dist_to_opti;
            // check distance of each opensim marker to each optitrack marker
            // num_mark
            for (int j =0; j < error_markers.size(); j++)
            {
              Eigen::Vector3d osim_mark = D_osim_pos[error_markers[j] ];
              std::vector<double> all_dist_to_opti;
              for (int k =0; k< optitrack_considered.size(); k++)
              {
                Eigen::Vector3d opti_pos = optitrackPoints[ optitrack_considered[k] ];
                all_dist_to_opti.push_back( (osim_mark - opti_pos).norm() );
              }

              osim_all_dist_to_opti.push_back(all_dist_to_opti);

            }

            // select opti_index
            int opti_id = 0;

            num_mark = osim_considered.size();
            std::vector<std::string> temp_osim_name = osim_considered;

            //while (num_mark > 0)
            while (opti_id < optitrack_considered.size())
            {
              // select opensim to associate
              int osim_index = -1;
              double dist_cons = 9999;
              
              for (int l = 0; l < num_mark; l++)
              {
                // check distance with all opensim markers

                double dist_osim_opti = osim_all_dist_to_opti[l][0];
                if (dist_osim_opti < dist_cons)
                {
                  dist_cons = dist_osim_opti;
                  osim_index = l;
                }

              }

              if (osim_index !=-1)
              {
                // associate opensim Marker and optitrack
                std::string opensim_mark_name = temp_osim_name[osim_index];
                corresNameID[ opensim_mark_name  ] = optitrack_considered[opti_id];
                verif_opti_indexes.push_back(optitrack_considered[opti_id]);

                // suppress opensim Marker, go to next optitrack marker
                osim_all_dist_to_opti.erase(osim_all_dist_to_opti.begin()+ osim_index);
                temp_osim_name.erase(temp_osim_name.begin()+ osim_index);
                num_mark--;

              }
              opti_id++;


           }
          
          if (manParams.filtering_verbose) {printAssociation(corresNameID);}


         // }
      
      
      }
    }


    if (check == 2)
    {
      // Some body parts couldn't be associated. Still add them into corresNameID with an absurd optitrack ID.
      for (int i = 0; i < osim_name.size(); i++)
      {
        if (corresNameID.find(osim_name[i]) == corresNameID.end() )
        {
          corresNameID[osim_name[i] ] = -1;
        }
      }
    }


    //display results
    std::printf(" ---- FINAL ---- \n");
    printAssociation(corresNameID);

    return(check);


}

// void rotationFiltering(Eigen::Matrix4f & result_transf)
// {
//   Eigen::Matrix3f rot = result_transf.block<3,3>(0,0);

//   Eigen::Vector3f ea = (rot).eulerAngles(2,0,1);

//   double cor_roll = 0.0;
//   double cor_yaw = 0.0;
//   double cor_pitch = 0.0;

//   // ea[0] in [0, PI]
//   // https://eigen.tuxfamily.org/dox/group__Geometry__Module.html#title20
//   if (ea[0] > M_PI-0.14)
//   {
//     cor_yaw = M_PI;
//   }
//   else if (ea[0] > 10*M_PI/180)
//   {
//     cor_yaw = 0.0;
//   }
//   if (ea[1] >M_PI-0.14)
//   {
//     cor_roll = M_PI;
//   }
//   else if (ea[1] > 10*M_PI/180)
//   {
//     cor_roll = 0.0;
//   }
//   else if (ea[1] < -M_PI+0.14)
//   {
//     cor_roll = -M_PI;
//   }
//     else if (ea[1] < -10*M_PI/180)
//   {
//     cor_roll = 0.0;
//   }

//   Eigen::AngleAxisf rollAngle(cor_roll, Eigen::Vector3f::UnitX());
//   Eigen::AngleAxisf pitchAngle(ea[2], Eigen::Vector3f::UnitY());
//   Eigen::AngleAxisf yawAngle(cor_yaw, Eigen::Vector3f::UnitZ());
//   Eigen::Quaternion<float> q = rollAngle * pitchAngle * yawAngle;

//   Eigen::Matrix3f rotationMatrix = q.matrix();
//   result_transf.block<3,3>(0,0) = rotationMatrix;


// }

void transformFiltering(Eigen::Matrix4f & result_transf, double maximum_translation, double maximum_rotation)
{
  Eigen::Matrix3f rot = result_transf.block<3,3>(0,0);

  Eigen::Vector3f ea = (rot).eulerAngles(2,0,1);

  double cor_roll = 0.0;
  double cor_yaw = 0.0;
  double cor_pitch = 0.0;

  // ea[0] in [0, PI]
  // https://eigen.tuxfamily.org/dox/group__Geometry__Module.html#title20
  if (ea[0] > M_PI-maximum_rotation)
  {
    cor_yaw = M_PI;
  }
  else if (ea[0] > maximum_rotation)
  {
    cor_yaw = 0.0;
  }

  if (ea[1] >M_PI-maximum_rotation)
  {
    cor_roll = M_PI;
  }
  else if (ea[1] > maximum_rotation)
  {
    cor_roll = 0.0;
  }
  else if (ea[1] < -M_PI+maximum_rotation)
  {
    cor_roll = -M_PI;
  }
    else if (ea[1] < -maximum_rotation)
  {
    cor_roll = 0.0;
  }

  if (ea[2] >M_PI-maximum_rotation)
  {
    cor_pitch = M_PI;
  }
  else if (ea[2] > maximum_rotation)
  {
    cor_pitch = 0.0;
  }
  else if (ea[2] < -M_PI+maximum_rotation)
  {
    cor_pitch = -M_PI;
  }
    else if (ea[2] < -maximum_rotation)
  {
    cor_pitch = 0.0;
  }  

  Eigen::AngleAxisf rollAngle(cor_roll, Eigen::Vector3f::UnitX());
  Eigen::AngleAxisf pitchAngle(ea[2], Eigen::Vector3f::UnitY());
  Eigen::AngleAxisf yawAngle(cor_yaw, Eigen::Vector3f::UnitZ());
  Eigen::Quaternion<float> q = rollAngle * pitchAngle * yawAngle;

  Eigen::Matrix3f rotationMatrix = q.matrix();
  result_transf.block<3,3>(0,0) = rotationMatrix;

  for (int i =0; i<3; i++)
  {
    double c_rt = result_transf(i,3);
    c_rt = std::max( std::min(maximum_translation, c_rt), -maximum_translation );
    float fc_rt = c_rt;
    result_transf(i,3) = fc_rt;
  }



}


/**
 * @brief Taken from https://stackoverflow.com/questions/59395218/pcl-scale-two-point-clouds-to-the-same-size
 * debugFlags: debugOverlay will leave both input clouds scaled and in their respective eigen orientations (allows more easy comparison). primaryAxisOnly will use only the primary axis of variation to perform scaling if true, if false, it will scale all 3 axes of variation independently.


 * 
 * @param sourceCloud : cloud which need to be rescaled; here, opensim theory points.
 * @param targetCloud : cloud whose scale is the target to match for sourceCloud; here, optitrack points recorded.
 * @param transf : transformation from opensim -> optitrack, obtained previously.
 * @param primaryAxisOnly 
 */
void rescaleCloudsNoTransform(pcl::PointCloud<pcl::PointXYZ>::Ptr& sourceCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& targetCloud, std::vector<double> & scaling, Eigen::Matrix4f & transf)
{
    //analyze source cloud
    pcl::PCA<pcl::PointXYZ> pcaSource;
    pcaSource.setInputCloud(sourceCloud);
    Eigen::Matrix3f sourceEVs_Dir = pcaSource.getEigenVectors();
    Eigen::Vector4f sourceMidPt = pcaSource.getMean();
    Eigen::Matrix4f sourceTransform = Eigen::Matrix4f::Identity();
    sourceTransform.block<3, 3>(0, 0) = sourceEVs_Dir;
    sourceTransform.block<4, 1>(0, 3) = sourceMidPt;
    pcl::PointCloud<pcl::PointXYZ>::Ptr orientedSource(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*sourceCloud, *orientedSource, sourceTransform.inverse());
    pcl::PointXYZ sourceMin, sourceMax;
    pcl::getMinMax3D(*orientedSource, sourceMin, sourceMax);

    //analyze target cloud
    pcl::PCA<pcl::PointXYZ> pcaTarget;
    pcaTarget.setInputCloud(targetCloud);
    Eigen::Matrix3f targetEVs_Dir = pcaTarget.getEigenVectors();
    Eigen::Vector4f targetMidPt = pcaTarget.getMean();
    Eigen::Matrix4f targetTransform = Eigen::Matrix4f::Identity();
    targetTransform.block<3, 3>(0, 0) = targetEVs_Dir;
    targetTransform.block<4, 1>(0, 3) = targetMidPt;
    pcl::PointCloud<pcl::PointXYZ>::Ptr orientedTarget(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*targetCloud, *orientedTarget, targetTransform.inverse());
    pcl::PointXYZ targetMin, targetMax;
    pcl::getMinMax3D(*orientedTarget, targetMin, targetMax);

    //apply scaling to oriented target cloud 
    double xScale = (targetMax.x - targetMin.x) / (sourceMax.x - sourceMin.x);
    double yScale = (targetMax.y - targetMin.y) / (sourceMax.y - sourceMin.y);
    double zScale = (targetMax.z - targetMin.z) / (sourceMax.z - sourceMin.z);

   //std::cout << "xScale: " << xScale << "; yScale: " << yScale << "; zScale: " << zScale << std::endl;

    if (isnan(xScale) || isinf(xScale) || xScale > 100 || xScale < 0.01 || abs(sourceMax.x - sourceMin.x) < 1e-6)
    {
      xScale = 1.0;
    }
    if (isnan(yScale) || isinf(yScale) || yScale > 100 || yScale < 0.01 || abs(sourceMax.y - sourceMin.y) < 1e-6)
    {
      yScale = 1.0;
    }
    if (isnan(zScale) || isinf(zScale) || zScale > 100 || zScale < 0.01 || abs(sourceMax.z - sourceMin.z) < 1e-6)
    {
      zScale = 1.0;
    }
   std::cout <<"after control : xScale: " << xScale << "; yScale: " << yScale << "; zScale: " << zScale << std::endl;

    for (int i = 0; i < orientedSource->points.size(); i++)
    {
        orientedSource->points[i].x = orientedSource->points[i].x * xScale;
        orientedSource->points[i].y = orientedSource->points[i].y * yScale;
        orientedSource->points[i].z = orientedSource->points[i].z * zScale;
    }
    // NB : check if there is any way here to get source -> target transformation
    // (not sure)
    pcl::PointCloud<pcl::PointXYZ>::Ptr sourceCloudRescaled(new pcl::PointCloud<pcl::PointXYZ>);

    pcl::transformPointCloud(*orientedSource, *sourceCloudRescaled, sourceTransform);
    


    // for (int i = 0; i < sourceCloudRescaled->points.size(); i++)
    // {
    //   std::printf("Point original : [%3.4f,%3.4f,%3.4f] \n", sourceCloud->points[i].x,  sourceCloud->points[i].y,  sourceCloud->points[i].z);
    //     std::printf("Point in Rescaled : [%3.4f,%3.4f,%3.4f] \n", sourceCloudRescaled->points[i].x,  sourceCloudRescaled->points[i].y,  sourceCloudRescaled->points[i].z);
    // }
    // now, get translation between sourceCloud and sourceCloudRescaled (we want those two to be at same position)
    Eigen::Vector4f centroid;
    pcl::compute3DCentroid( (*sourceCloud), centroid);

    Eigen::Vector4f centroidR;
    pcl::compute3DCentroid( (*sourceCloudRescaled), centroidR);

    Eigen::Vector4f transfo_trans = centroid - centroidR;

    Eigen::Matrix4f transfo;transfo.setIdentity();

    

    for (int i =0; i <3;i++)
    {
      transfo(i,3) = transfo_trans[i];
    }

    transf = transfo;

    pcl::transformPointCloud(*sourceCloudRescaled, *sourceCloudRescaled, transfo);
    
    pcl::PointXYZ sourceMinTransformed, sourceMaxTransformed;
    
    pcl::getMinMax3D(*sourceCloudRescaled, sourceMinTransformed, sourceMaxTransformed);


    double xScaleGlo = (sourceMaxTransformed.x - sourceMinTransformed.x) / (sourceMax.x - sourceMin.x);
    double yScaleGlo = (sourceMaxTransformed.y - sourceMinTransformed.y) / (sourceMax.y - sourceMin.y);
    double zScaleGlo = (sourceMaxTransformed.z - sourceMinTransformed.z) / (sourceMax.z - sourceMin.z);
    //std::cout << "After PCA : xScale: " << xScaleGlo << " ; yScale: " << yScaleGlo << "; zScale: " << zScaleGlo << std::endl;

    sourceCloud = sourceCloudRescaled;

    scaling.push_back(xScale);
    scaling.push_back(yScale);
    scaling.push_back(zScale);


}



/**
 * @brief Solves y = mx +b 
 * 
 * @param x 
 * @param y 
 * @param m 
 * @param b 
 * @param r 
 * @return int 
 */
int linreg(std::vector<double> x, std::vector<double> y, double & m, double & b, double & r){
    double bs = b;

    double   sumx = 0.0;                      /* sum of x     */
    double   sumx2 = 0.0;                     /* sum of x**2  */
    double   sumxy = 0.0;                     /* sum of x * y */
    double   sumy = 0.0;                      /* sum of y     */
    double   sumy2 = 0.0;                     /* sum of y**2  */
    int n = x.size();

     for (int i=0;i<n;i++){ 
        sumx  += x[i];       
        sumx2 += std::pow(x[i],2);  
        sumxy += x[i] * y[i];
        sumy  += y[i];      
        sumy2 +=std::pow(y[i],2); 
    } 

    double denom = (n * sumx2 - std::pow(sumx,2));
    if (denom == 0) {
        // singular matrix. can't solve the problem.
        m = 0;
        b = 0;
        r = 0;
            return 1;
    }

    m = (n * sumxy  -  sumx * sumy) / denom;
    b = (sumy * sumx2  -  sumx * sumxy) / denom;
    r = (sumxy - sumx * sumy / n) /    /* compute correlation coeff */
              sqrt((sumx2 - std::pow(sumx,2)/n) *
              (sumy2 - std::pow(sumy,2)/n));
    
    if (bs == 0)
    {
      double mxbxt = 0;
      double xxt = 0;
      for (int i=0;i<n;i++){ 
        mxbxt += (m*x[i] + b)*x[i];
        xxt += x[i]*x[i];
      }
      if (xxt !=0)
      {
        m = mxbxt/xxt;
        b = 0;

      }


    }


    return 0; 
}
/**
 * @brief Solves y = mx  
 * --> m = (x.T*x)^-1 * x.T * y
 * Here, y and x are vectors. Thus, m is a double value.
 * 
 * @param x 
 * @param y 
 * @param m 
 * @param b 
 * @param r 
 * @return int 
 */
int linreg2(std::vector<double> x, std::vector<double> y, double & m){
    double   xtx = 0;
    double xty = 0;

    for (int i = 0; i < x.size(); i++)
    {
      xtx = x[i]*x[i];
      xty = x[i]*y[i];
    }
    if (xtx != 0)
    {
      xtx = 1/xtx;
      m = xtx*xty;

    }

  
    return 0; 
}


/**
 * @brief Taken from https://stackoverflow.com/questions/59395218/pcl-scale-two-point-clouds-to-the-same-size
 * debugFlags: debugOverlay will leave both input clouds scaled and in their respective eigen orientations (allows more easy comparison). primaryAxisOnly will use only the primary axis of variation to perform scaling if true, if false, it will scale all 3 axes of variation independently.
 *
 * Input of procedure : 
 * - optitrackPoints : experimental markers expressed in ground referential
 * Expressed in targetCloud.
 * - opensimPoints : theorical opensim points, expressed in ground referential, placed accordingly to the
 * last calibration pose. 
 * Expressed in sourceCloud.
 * - opensimPointsOrigin (expressed thanks to osMod). Theorical Opensim points, expressed in their local referential
 * (body part). 
 * Expressed in sourceCloudLocal.
 * - transf : Transformation found from Opensim to Optitrack points, expressed in ground. 
 * 
 * Idea here : 
 * - Apply transform transf to opensimPoints. Now, theorical opensim points should be placed at the 
 * best position to match experimental points.
 * - Find transform from opensimPoints to opensimPointsOrigin. We now have the transform from 
 * the "best" ground position of opensimPoints (i.e matching at best optitrackPoints ) to the local referential of opensimPoints.
 * This transform is called g2l_transf.
 * - Apply g2l_transf to optitrackPoints to get PCAorient. Now, experimental optitrack points are expressed into the local referential of the
 * body part.
 * - On each of the x,y,z, compute the vectors between each pair of opensim points / optitrack points associated.
 * Express their norm on each of those components. We now have, for each of the x,y,z axis, pair of distances between
 * Optitrack and Opensim markers.
 * - For each of the axis i, solve the problem Optitrack_pts = s_i * Opensim_pts, with s_i the scale factor
 * on axis i. (here, solves with least squares problem).
 * Now, each scale factor, expressed in the local referential of the body part, has been found.
 * - Apply the scaling factors to opensimPoints. Here, osMod (theorical model) is not yet modified. 
 * 
 * 
 * @param optitrackPoints : experimental markers expressed in ground referential
 * @param opensimPoints :theorical opensim points (no scaling), expressed in ground referential, placed accordingly to the
 * last calibration pose. 
 * @param osMod : OsimModel object, corresponding to the theorical opensim model.
 * @param corresNameID : map object, containing the correspondences between Opensim names and Optitrack indexes.
 * @param transf : transformation from opensim -> optitrack, obtained previously.
 * @param scaling : scaling parameters for this body part, expressed into the local referential of the body part.
 */
void rescaleCloudsWithAssociation2(std::map<int,Eigen::Vector3d> optitrackPoints, 
std::map<std::string,Eigen::Vector3d> & opensimPoints, 
fosim::OsimModel & osMod, 
std::map<std::string,int> corresNameID, Eigen::Matrix4f transf, 
std::vector<double> & scaling,
globalPublisher globalPublish  )
{

    pcl::PointCloud<pcl::PointXYZ>::Ptr sourceCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr sourceCloudLocal(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr targetCloud(new pcl::PointCloud<pcl::PointXYZ>);
     std::vector<std::string> markName;
    //analyze source cloud
    for (std::map<std::string,Eigen::Vector3d>::iterator it = opensimPoints.begin(); it!= opensimPoints.end(); it++)
    {
      Eigen::Vector3d optiVec = optitrackPoints[corresNameID[it->first]];
      Eigen::Vector3d osimVec = it->second;
      markName.push_back(it->first);
      Eigen::Vector3d osimVecOri = osMod.D_mark_pos_local[it->first];
      pcl::PointXYZ optiPt(optiVec[0],optiVec[1], optiVec[2]);
      pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
      pcl::PointXYZ osimPtOri(osimVecOri[0],osimVecOri[1], osimVecOri[2]);
      sourceCloud->points.push_back(osimPt);
      sourceCloudLocal->points.push_back(osimPtOri);
      targetCloud->points.push_back(optiPt);

    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr orientedSource(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*sourceCloud, *orientedSource, transf);

    Eigen::Matrix4f g2l_transf;
    
    pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 
    svd.estimateRigidTransformation(*orientedSource,*sourceCloudLocal,g2l_transf);


    pcl::PointCloud<pcl::PointXYZ>::Ptr localOrientedSource(new pcl::PointCloud<pcl::PointXYZ>);
    localOrientedSource = sourceCloudLocal;

    pcl::PointCloud<pcl::PointXYZ>::Ptr localOrientedTarget(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*targetCloud, *localOrientedTarget, g2l_transf);

    std::map<std::string,Eigen::Vector3d> osimdebug;
        std::map<std::string,Eigen::Vector3d> optiassosdebug;
std::map<int,Eigen::Vector3d> optidebug;

for (int i = 0; i < localOrientedSource->points.size(); i++)
{
   Eigen::Vector3d optipt(localOrientedTarget->points[i].x, localOrientedTarget->points[i].y, localOrientedTarget->points[i].z);
  optidebug[i] = optipt;
  Eigen::Vector3d osimpt(localOrientedSource->points[i].x, localOrientedSource->points[i].y, localOrientedSource->points[i].z);
  std::string name = markName[i];
  osimdebug[name] = osimpt;
  optiassosdebug[name] = optipt;
}

globalPublish.publishOpensim(ros::Time::now(), osimdebug);
globalPublish.markerPublisher(ros::Time::now(), optidebug, globalPublish.optitrack_publisher);


    double xScale, yScale, zScale;

    int n = 0;
    
    double valX,valY,valZ;
    std::vector<int> nv(3,0);
    std::vector<double> sc(3,0.0);

    double rap;
    for (int l =0; l < 3; l++)
    {
      std::vector<double> x;
      std::vector<double> y;
      for (int i = 0; i < localOrientedSource->points.size()-1; i++)
      { 

        for (int j =i+1; j < localOrientedSource->points.size(); j++)
        // int j = 0
        {
          if (true)
          
          {

            if (l == 0)
            {
              y.push_back(abs(localOrientedTarget->points[i].x-localOrientedTarget->points[j].x));
              x.push_back(abs(localOrientedSource->points[i].x-localOrientedSource->points[j].x));
            }
            else if (l== 1)
            {
              y.push_back(abs(localOrientedTarget->points[i].y-localOrientedTarget->points[j].y));
              x.push_back(abs(localOrientedSource->points[i].y-localOrientedSource->points[j].y));
              if (x[x.size()-1] != 0)
              {
                double rap = 1.0*y[y.size()-1]/x[x.size()- 1];
                std::cout << "rapport : " << rap << std::endl;
                if (rap < 0.5)
                {
                  std::cout << "x dist : " << x[x.size()- 1] << std::endl;
                  std::cout << "y dist : " << y[y.size()- 1] << std::endl;
                  std::printf("Pos i : %3.4f , %3.4f, %3.4f \n",localOrientedTarget->points[i].x, localOrientedTarget->points[i].y, localOrientedTarget->points[i].z);
                  std::printf("Pos j : %3.4f , %3.4f, %3.4f \n",localOrientedTarget->points[j].x,localOrientedTarget->points[j].y,localOrientedTarget->points[j].z);
                }
              }
            }
            else
            {
              y.push_back(abs(localOrientedTarget->points[i].z-localOrientedTarget->points[j].z));
              x.push_back(abs(localOrientedSource->points[i].z-localOrientedSource->points[j].z));

            }


          }

          // double rap = 1.0*y[y.size()-1]/x[x.size()- 1];
          // std::cout << "rapport : " << rap << std::endl;
          // if (rap < 0.5)
          // {
          //   std::cout << "x dist : " << x[x.size()- 1] << std::endl;
          //   std::cout << "y dist : " << y[y.size()- 1] << std::endl;
          //   std::printf("Pos i : %3.4f , %3.4f, %3.4f \n",localOrientedTarget->points[i].x, localOrientedTarget->points[i].y, localOrientedTarget->points[i].z);
          //   std::printf("Pos j : %3.4f , %3.4f, %3.4f \n",localOrientedTarget->points[j].x,localOrientedTarget->points[j].y,localOrientedTarget->points[j].z);
          // }

        }

      }

      //std::printf("All dist \n");
            
      double m,r;
      double b = 1.0;
      // solves y = ax+b
      linreg(x,y,m,b,r);
      std::printf("For %d : m : %f; b : %f; r : %f. \n",l,m,b,r);
      // solves y = ax
      double m2;
      linreg2(x,y,m2);
      
      std::printf("For %d : m2 : %f. \n",l,m2);
      double m3;
      double b2 = 0.0;
      linreg(x,y,m3,b2,r);
      std::printf("For %d : m3 : %f. \n",l,m3);
      //sc[l] = m3;
      if (r > 0.9)
      {
        sc[l] = m;
      }
      else
      {
        sc[l] = m3;
      }



    }

    for (int i = 0; i <3; i++)
    {
      if (isnan(sc[i]) || isinf(sc[i]) || sc[i] < 0.01)
      {
        sc[i] = 1.0;
      }
    }


std::string bla;
std::cout << "scaling debug : placement on local ref" << std::endl;
std::cin >> bla;


bool pure_placement = true;

if (pure_placement)
{
  sc[0] = 1.0;
  sc[1] = 1.0;
  sc[2] = 1.0;
  scaling = sc;

  // Pr rescaling ds ref local

  osMod.setMarkersPositions(optiassosdebug,"local");

  for (std::map<std::string,Eigen::Vector3d>::iterator it = opensimPoints.begin(); it!= opensimPoints.end(); it++)
  {
    opensimPoints[it->first] = osMod.D_mark_pos[it->first];

  }

}

else
{

  xScale = sc[0];
  yScale = sc[1];
  zScale = sc[2];

  std::map<std::string, std::vector<double> > bsc;
  std::string body_name = osMod.D_markers_body[opensimPoints.begin()->first ];

  for (int l =0; l < 3; l++)
  {
    scaling[l] *= sc[l];
  }
  bsc[ body_name] = scaling;

  osMod.applyStretching(opensimPoints,bsc,body_name);

}

}



/**
 * @brief Taken from https://stackoverflow.com/questions/59395218/pcl-scale-two-point-clouds-to-the-same-size
 * debugFlags: debugOverlay will leave both input clouds scaled and in their respective eigen orientations (allows more easy comparison). primaryAxisOnly will use only the primary axis of variation to perform scaling if true, if false, it will scale all 3 axes of variation independently.
 *
 * Input of procedure : 
 * - optitrackPoints : experimental markers expressed in ground referential, CoR not included.
 * 
 * Expressed in targetCloud.
 * - opensimPointsOrigin (expressed thanks to osMod). Theorical Opensim points, expressed in their local referential
 * (body part). CoR included (called CoR).
 * Expressed in sourceCloudLocal.
 * 
 * Idea here : 
 * - PCA analysis on Optitrack Points. With eigenvectors+meanPoint, get transformation T1
 * - PCA analysis on Theorical Opensim points. With eigenvectors+meanPoint, get transformation T2.
 * 
 * - First, transformation T from Optitrack Points to TOP. 
 * - Apply transformation T. Now, Optitrack Points are expressed on same referential as local body.
 * - Translation to be sure that CoR coincide. Now, optitrack points are expressed on same referential as local body + referentials are really the same.
 * - On each of the x,y,z, compute the vectors between each pair of opensim points / optitrack points associated.
 * Express their norm on each of those components. We now have, for each of the x,y,z axis, pair of distances between
 * Optitrack and Opensim markers.
 * - For each of the axis i, solve the problem Optitrack_pts = s_i * Opensim_pts, with s_i the scale factor
 * on axis i. (here, solves with least squares problem).
 * Now, each scale factor, expressed in the local referential of the body part, has been found.
 * - Replace Opensim points according to Optitrack Points expressed in local body.
 * 
 * @param optitrackPoints : experimental markers expressed in ground referential
 * @param opensimPoints :theorical opensim points (no scaling), expressed in ground referential, placed accordingly to the
 * last calibration pose. 
 * @param osMod : OsimModel object, corresponding to the theorical opensim model.
 * @param corresNameID : map object, containing the correspondences between Opensim names and Optitrack indexes.
 * @param transf : transformation from opensim -> optitrack, obtained previously.
 * @param scaling : scaling parameters for this body part, expressed into the local referential of the body part.
 */
void replacePointsAndEstimateScaling(std::map<int,Eigen::Vector3d> optitrackPoints, 
Eigen::Vector3f optitrackCoR,
fosim::OsimModel & osMod, 
std::map<std::string,int> corresNameID,
std::vector<double> & scaling,
ros::Time calib_time,
globalPublisher globalPublish  )
{

    pcl::PointCloud<pcl::PointXYZ>::Ptr sourceCloudLocal(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr targetCloud(new pcl::PointCloud<pcl::PointXYZ>);
     std::vector<std::string> markName;
    //analyze source cloud
    for (std::map<int,Eigen::Vector3d>::iterator it = optitrackPoints.begin(); it!= optitrackPoints.end(); it++)
    {

      auto itID= std::find_if(std::begin(corresNameID), std::end(corresNameID), [&](const std::pair<std::string,int> &pair)
      {
              return pair.second == it->first;
      });

      Eigen::Vector3d optiVec = optitrackPoints[ itID->second ];

      markName.push_back(itID->first);
      Eigen::Vector3d osimVecOri = osMod.D_mark_pos_local[itID->first];

      pcl::PointXYZ optiPt(optiVec[0],optiVec[1], optiVec[2]);
      pcl::PointXYZ osimPtOri(osimVecOri[0],osimVecOri[1], osimVecOri[2]);

      sourceCloudLocal->points.push_back(osimPtOri);
      targetCloud->points.push_back(optiPt);

    }

   pcl::PointXYZ CoROptiPt( optitrackCoR[0], optitrackCoR[1], optitrackCoR[2] );
  targetCloud->points.push_back(CoROptiPt);
  
  // for now, CoR model -> always at (0,0,0). Might change in future.

  Eigen::Vector3f CoROsim(0,0,0);

  pcl::PointXYZ CoROsimPt(CoROsim[0],CoROsim[1],CoROsim[2]);
  sourceCloudLocal->points.push_back(CoROsimPt);

    Eigen::Matrix4f g2l_transf;
    
    pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 
    svd.estimateRigidTransformation(*targetCloud,*sourceCloudLocal,g2l_transf);

    pcl::PointCloud<pcl::PointXYZ>::Ptr localOrientedSource(new pcl::PointCloud<pcl::PointXYZ>);
    localOrientedSource = sourceCloudLocal;

    pcl::PointCloud<pcl::PointXYZ>::Ptr localOrientedTarget(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*targetCloud, *localOrientedTarget, g2l_transf);

    // coincide CoR

    Eigen::Matrix4f trans_CoR; trans_CoR.setIdentity();
    Eigen::Vector3f localCoROpti(localOrientedTarget->points[localOrientedTarget->points.size()-1].x,
                                  localOrientedTarget->points[localOrientedTarget->points.size()-1].y,
                                  localOrientedTarget->points[localOrientedTarget->points.size()-1].z);
    
    trans_CoR.block<3,1>(0,3) = CoROsim - localCoROpti;

    pcl::transformPointCloud(*localOrientedTarget, *localOrientedTarget, trans_CoR);



    std::map<int,Eigen::Vector3d> osimdebug;
        std::map<std::string,Eigen::Vector3d> optiassosdebug;
std::map<int,Eigen::Vector3d> optidebug;

for (int i = 0; i < localOrientedSource->points.size(); i++)
{
   Eigen::Vector3d optipt(localOrientedTarget->points[i].x, localOrientedTarget->points[i].y, localOrientedTarget->points[i].z);
  optidebug[i] = optipt;
  Eigen::Vector3d osimpt(localOrientedSource->points[i].x, localOrientedSource->points[i].y, localOrientedSource->points[i].z);
  osimdebug[i] = osimpt;
  if (i <  localOrientedSource->points.size() - 1)
  {
    std::string name = markName[i];
    optiassosdebug[name] = optipt;
  }

}


globalPublish.markerPublisher(calib_time, osimdebug, globalPublish.opensim_publisher);
globalPublish.markerPublisher(calib_time, optidebug, globalPublish.optitrack_publisher);


    double xScale, yScale, zScale;

    int n = 0;
    
    double valX,valY,valZ;
    std::vector<int> nv(3,0);
    std::vector<double> sc(3,0.0);

    double rap;
    for (int l =0; l < 3; l++)
    {
      std::vector<double> x;
      std::vector<double> y;
      for (int i = 0; i < localOrientedSource->points.size()-1; i++)
      { 

        for (int j =i+1; j < localOrientedSource->points.size(); j++)
        {
          if (true)
          
          {

            if (l == 0)
            {
              y.push_back(abs(localOrientedTarget->points[i].x-localOrientedTarget->points[j].x));
              x.push_back(abs(localOrientedSource->points[i].x-localOrientedSource->points[j].x));
            }
            else if (l== 1)
            {
              y.push_back(abs(localOrientedTarget->points[i].y-localOrientedTarget->points[j].y));
              x.push_back(abs(localOrientedSource->points[i].y-localOrientedSource->points[j].y));
              if (x[x.size()-1] != 0)
              {
                double rap = 1.0*y[y.size()-1]/x[x.size()- 1];
                std::cout << "rapport : " << rap << std::endl;
                if (rap < 0.5)
                {
                  std::cout << "x dist : " << x[x.size()- 1] << std::endl;
                  std::cout << "y dist : " << y[y.size()- 1] << std::endl;
                  std::printf("Pos i : %3.4f , %3.4f, %3.4f \n",localOrientedTarget->points[i].x, localOrientedTarget->points[i].y, localOrientedTarget->points[i].z);
                  std::printf("Pos j : %3.4f , %3.4f, %3.4f \n",localOrientedTarget->points[j].x,localOrientedTarget->points[j].y,localOrientedTarget->points[j].z);
                }
              }
            }
            else
            {
              y.push_back(abs(localOrientedTarget->points[i].z-localOrientedTarget->points[j].z));
              x.push_back(abs(localOrientedSource->points[i].z-localOrientedSource->points[j].z));

            }


          }

        }

      }

      //std::printf("All dist \n");
            
      double m,r;
      double b = 1.0;
      // solves y = ax+b
      linreg(x,y,m,b,r);
      std::printf("For %d : m : %f; b : %f; r : %f. \n",l,m,b,r);
      // solves y = ax
      double m2;
      linreg2(x,y,m2);
      
      std::printf("For %d : m2 : %f. \n",l,m2);
      double m3;
      double b2 = 0.0;
      linreg(x,y,m3,b2,r);
      std::printf("For %d : m3 : %f. \n",l,m3);
      //sc[l] = m3;
      if (r > 0.95)
      {
        sc[l] = m;
      }
      else
      {
        sc[l] = m3;
      }



    }

    for (int i = 0; i <3; i++)
    {
      if (isnan(sc[i]) || isinf(sc[i]) || sc[i] < 0.01)
      {
        sc[i] = 1.0;
      }
    }


std::string bla;
std::cout << "scaling debug : placement on local ref" << std::endl;
std::cin >> bla;

  // replace in local referential

  osMod.setMarkersPositions(optiassosdebug,"local");

  scaling = sc;


}


/**
 * @brief Taken from https://stackoverflow.com/questions/59395218/pcl-scale-two-point-clouds-to-the-same-size
 * debugFlags: debugOverlay will leave both input clouds scaled and in their respective eigen orientations (allows more easy comparison). primaryAxisOnly will use only the primary axis of variation to perform scaling if true, if false, it will scale all 3 axes of variation independently.


 * 
 * @param sourceCloud : cloud which need to be rescaled; here, opensim theory points.
 * @param targetCloud : cloud whose scale is the target to match for sourceCloud; here, optitrack points recorded.
 * @param transf : transformation from opensim -> optitrack, obtained previously.
 * @param primaryAxisOnly 
 */
void rescaleClouds(pcl::PointCloud<pcl::PointXYZ>::Ptr& sourceCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& targetCloud, Eigen::Matrix4f transf, std::vector<double> & scaling)
{
    //analyze source cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr orientedSource(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*sourceCloud, *orientedSource, transf);
    pcl::PointXYZ sourceMin1, sourceMax1;
    pcl::getMinMax3D(*orientedSource, sourceMin1, sourceMax1);

    pcl::PCA<pcl::PointXYZ> pcaSource;
    pcaSource.setInputCloud(sourceCloud);
    Eigen::Matrix3f sourceEVs_Dir = pcaSource.getEigenVectors();
    Eigen::Vector4f sourceMidPt = pcaSource.getMean();
    Eigen::Matrix4f sourceTransform = Eigen::Matrix4f::Identity();
    sourceTransform.block<3, 3>(0, 0) = sourceEVs_Dir;
    sourceTransform.block<4, 1>(0, 3) = sourceMidPt;
    pcl::PointCloud<pcl::PointXYZ>::Ptr PCAorientedSource(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*orientedSource, *PCAorientedSource, sourceTransform.inverse());
    pcl::PointXYZ sourceMin, sourceMax;
    pcl::getMinMax3D(*PCAorientedSource, sourceMin, sourceMax);

    //analyze targed cloud
    pcl::PCA<pcl::PointXYZ> pcaTarget;
    pcaTarget.setInputCloud(targetCloud);
    pcl::PointXYZ targetMin1, targetMax1;
    pcl::getMinMax3D(*targetCloud, targetMin1, targetMax1);

    Eigen::Matrix3f targetEVs_Dir = pcaTarget.getEigenVectors();
    Eigen::Vector4f targetMidPt = pcaTarget.getMean();
    Eigen::Matrix4f targetTransform = Eigen::Matrix4f::Identity();
    targetTransform.block<3, 3>(0, 0) = targetEVs_Dir;
    targetTransform.block<4, 1>(0, 3) = targetMidPt;
    pcl::PointCloud<pcl::PointXYZ>::Ptr orientedTarget(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*targetCloud, *orientedTarget, targetTransform.inverse());
    pcl::PointXYZ targetMin, targetMax;
    pcl::getMinMax3D(*orientedTarget, targetMin, targetMax);


    //apply scaling to oriented sample cloud 
    double xScale = (targetMax.x - targetMin.x) / (sourceMax.x - sourceMin.x);
    double yScale = (targetMax.y - targetMin.y) / (sourceMax.y - sourceMin.y);
    double zScale = (targetMax.z - targetMin.z) / (sourceMax.z - sourceMin.z);

    // std::cout << "From PCA : xScale: " << xScale << " | yScale: " << yScale << " | zScale: " << zScale << std::endl; 
 
    double xScale1 = (targetMax1.x - targetMin1.x) / (sourceMax1.x - sourceMin1.x);
    double yScale1 = (targetMax1.y - targetMin1.y) / (sourceMax1.y - sourceMin1.y);
    double zScale1 = (targetMax1.z - targetMin1.z) / (sourceMax1.z - sourceMin1.z);
     
     std::cout << "Parameters from PCA  : xScale: " << xScale1 << " | yScale: " << yScale1 << " | zScale: " << zScale1 << std::endl; 
 
    for (int i = 0; i < PCAorientedSource->points.size(); i++)
    {
        PCAorientedSource->points[i].x = PCAorientedSource->points[i].x * xScale1;
        PCAorientedSource->points[i].y = PCAorientedSource->points[i].y * yScale1;
        PCAorientedSource->points[i].z = PCAorientedSource->points[i].z * zScale1;
        
    }

pcl::PointCloud<pcl::PointXYZ>::Ptr sourceCloudRescale(new pcl::PointCloud<pcl::PointXYZ>);

pcl::transformPointCloud(*PCAorientedSource, *orientedSource, sourceTransform);

    pcl::PointXYZ sourceMin2, sourceMax2;
    pcl::getMinMax3D(*orientedSource, sourceMin2, sourceMax2);

    double xScale2 = (targetMax1.x - targetMin1.x) / (sourceMax2.x - sourceMin2.x);
    double yScale2 = (targetMax1.y - targetMin1.y) / (sourceMax2.y - sourceMin2.y);
    double zScale2 = (targetMax1.z - targetMin1.z) / (sourceMax2.z - sourceMin2.z);

     //std::cout << "From PCA then back to normal : xScale: " << xScale2 << " | yScale: " << yScale2 << " | zScale: " << zScale2 << std::endl; 
 

    
   
    for (int i = 0; i < sourceCloud->points.size(); i++)
    {
        sourceCloud->points[i].x = sourceCloud->points[i].x * xScale1;
        sourceCloud->points[i].y = sourceCloud->points[i].y * yScale1;
        sourceCloud->points[i].z = sourceCloud->points[i].z * zScale1;
        
    }

    
    scaling.push_back(xScale1);
    scaling.push_back(yScale1);
    scaling.push_back(zScale1);

}

/**
 * @brief Taken from https://stackoverflow.com/questions/59395218/pcl-scale-two-point-clouds-to-the-same-size
 * debugFlags: debugOverlay will leave both input clouds scaled and in their respective eigen orientations (allows more easy comparison). primaryAxisOnly will use only the primary axis of variation to perform scaling if true, if false, it will scale all 3 axes of variation independently.


 * 
 * @param sourceCloud : cloud which need to be rescaled; here, opensim theory points.
 * @param targetCloud : cloud whose scale is the target to match for sourceCloud; here, optitrack points recorded.
 * @param transf : transformation from opensim -> optitrack, obtained previously.
 * @param primaryAxisOnly 
 */
void rescaleClouds2(pcl::PointCloud<pcl::PointXYZ>::Ptr& sourceCloud, pcl::PointCloud<pcl::PointXYZ>::Ptr& targetCloud, Eigen::Matrix4f transf, std::vector<double> & scaling)
{
    //analyze source cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr orientedSource(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::transformPointCloud(*sourceCloud, *orientedSource, transf);
    pcl::PointXYZ sourceMin, sourceMax;
    pcl::getMinMax3D(*orientedSource, sourceMin, sourceMax);

    //analyze targed cloud
 
    pcl::PointXYZ targetMin, targetMax;
    pcl::getMinMax3D(*targetCloud, targetMin, targetMax);

    //apply scaling to oriented sample cloud 
    double xScale = (targetMax.x - targetMin.x) / (sourceMax.x - sourceMin.x);
    double yScale = (targetMax.y - targetMin.y) / (sourceMax.y - sourceMin.y);
    double zScale = (targetMax.z - targetMin.z) / (sourceMax.z - sourceMin.z);

     std::cout << "xScale: " << xScale << " | yScale: " << yScale << " | zScale: " << zScale << std::endl; 
 

/*     for (int i = 0; i < orientedSource->points.size(); i++)
    {
        orientedSource->points[i].x = orientedSource->points[i].x * xScale;
        orientedSource->points[i].y = orientedSource->points[i].y * yScale;
        orientedSource->points[i].z = orientedSource->points[i].z * zScale;
        
    }
    
    sourceCloud = orientedSource;
    
     */

    for (int i = 0; i < sourceCloud->points.size(); i++)
    {
        sourceCloud->points[i].x = sourceCloud->points[i].x * xScale;
        sourceCloud->points[i].y = sourceCloud->points[i].y * yScale;
        sourceCloud->points[i].z = sourceCloud->points[i].z * zScale;
        
    }

    
    scaling.push_back(xScale);
    scaling.push_back(yScale);
    scaling.push_back(zScale);

}

/**
 * @brief Goal : now association nearly ok, for each body part,
 * scaling; correspondance of markers; adaptation 
 * 
 * @return int 
 */
int multiEnhanceRobustnessByICP(managementParameters & manParams,
            std::map<int, Eigen::Vector3d > optitrackPoints,
            fosim::OsimModel & osMod,
            std::vector<double> & adapted_q_calib,
            std::map<std::string,int> & corresNameID,
            std::map<std::string,Eigen::Vector3d> & adapted_opensim_positions,
            std::map<std::string,std::vector<double> > & all_scaling_factors,
            ros::Time calib_time,
            globalPublisher globalPublish )
{
  int manual_choice = 1;
  std::map<std::string, std::vector<std::string> > D_body_markers = osMod.D_body_markers;
   std::map<int, Eigen::Vector3d> osimMarkersIndexAdapted;
  std::vector<std::string> already_treated_markers;

  std::map<std::string,double> markersWeights;
  std::map<std::string,Eigen::Vector3d> D_mark_pos = osMod.D_mark_pos;

  for (std::map<std::string,Eigen::Vector3d>::iterator it = D_mark_pos.begin(); it!= D_mark_pos.end(); it++)
  {
    markersWeights[it->first] = 0.0;
  }


  


  // pbme : D_body_markers currently in alphabetic_order. Pbme : should be 
  // in root --> child order.
  
  // To fix this : first, find the root body part of model (= body part directly
  // linked to ground).

  std::vector< std::string > body_in_multiorder;

  std::map<std::string,std::string> D_c_p = osMod.D_child_parent;

  std::map<std::string, std::string> D_c_p_m = osMod.D_child_parent;

  auto itground= std::find_if(std::begin(D_c_p_m), std::end(D_c_p_m), [&](const std::pair<std::string,std::string> &pair)
  {
          return pair.second == "ground";
  });

  body_in_multiorder.push_back(itground->first);

  std::map<std::string, std::vector<std::string> > D_parent_all_children;

  for (std::map<std::string,std::string>::iterator it = D_c_p.begin(); it!=D_c_p.end(); it++)
  {
    std::vector<std::string> v;
    D_parent_all_children[it->first] = v;
  }
  std::vector<std::string> v;  
  v.push_back(itground->first);
  D_parent_all_children["ground"] = v;

  // next, let's navigate from root to final children. We will
  // then add the whole chain to body_in_multiorder.

  // first, find body parts which are directly children from global_root
  std::string global_root = body_in_multiorder[0];

  bool itnameC = true;

  while (itnameC)
  {
    auto itname = std::find_if(std::begin(D_c_p_m), std::end(D_c_p_m), [&](const std::pair<std::string,std::string> &pair)
    {
            return pair.second == global_root;
    });
    itnameC = (itname != D_c_p_m.end());

    std::vector<std::string> current_link;

    std::string child; 

    if (itnameC)
    { 
      // in this case, a child directly linked to ground has been found;
      // find its kinematics chain to an end.
      child = itname->first;
      current_link.push_back(child);
      D_c_p_m.erase(itname);
      D_parent_all_children[itground->first].push_back(child);

    }

    bool itchildC = itnameC;

    while (itchildC)
    {
        // runs through the entire parent-child chain to one end
        // (= a body part with a parent but no children).

      // search the child of the current considered child. If this child has no child -> it's an end.
      auto itchild = std::find_if(std::begin(D_c_p_m), std::end(D_c_p_m), [&](const std::pair<std::string,std::string> &pair)
      {
              return pair.second == child;
      });

      itchildC = (itchild !=D_c_p_m.end() );



      if (itchildC)
      {
        if (D_body_markers.find(itchild->first)!= D_body_markers.end() )
        {        
          D_parent_all_children[itground->first].push_back(itchild->first);
          for (int i =0; i < current_link.size(); i++)
          {
            D_parent_all_children[current_link[i]].push_back(itchild->first);
          }
        }        
        // the current child has a child : select its own child
        current_link.push_back(itchild->first);
        child = itchild->first;
      }


    }

    for (int k = 0; k < current_link.size(); k++)
    {
      if (std::find(body_in_multiorder.begin(), body_in_multiorder.end(), current_link[k]) == body_in_multiorder.end())
      {
        // save only body parts that have markers (useless otherwise for algorithm)
        if (D_body_markers.find(current_link[k])!= D_body_markers.end() )
        {
          body_in_multiorder.push_back(current_link[k]);
        }
      } 
    }

  }

  // for (std::map<std::string,std::vector<std::string> >::iterator it = D_parent_all_children.begin(); it!=D_parent_all_children.end(); it++)
  // {
  //   std::printf("Considered root : %s :\n", it->first.c_str());
  //   std::cout << "  ";
  //   for (int i =0; i < it->second.size(); i++)
  //   {
  //     std::cout << it->second[i] << " ";
  //   }
  //   std::cout << std::endl;
  // }



  std::printf("------ Joint value origin ------ \n");
  fosim::printJointsValues(osMod.osimModel,osMod.osimState);


  for (int i = 0; i < body_in_multiorder.size(); i++)
  {
    
    //    - MAJ q_calib

    std::map<std::string,double> D_j_v = osMod.D_joints_values;
    std::map<std::string,double>::iterator itjv;

    for (int k =0; k < D_j_v.size(); k++)
    {
      itjv = D_j_v.begin();
      std::advance(itjv,k);
      int mult_index = osMod.D_alph_multi[k];
      adapted_q_calib[mult_index] = itjv->second;

    }

    osMod.setQ(adapted_q_calib);

    std::string body_name = body_in_multiorder[i];
    std::vector<std::string> osimMarkersNames = D_body_markers[body_name];
    std::map<std::string, Eigen::Vector3d> osimMarkers;
    std::map<std::string, Eigen::Vector3d> osimMarkers2Rescale;
    std::map<int,Eigen::Vector3d> optiMarkers;
    for (int l = 0; l < osimMarkersNames.size(); l++)
    {
      std::string name = osimMarkersNames[l];
      int opti_index = corresNameID[name];
      osimMarkers[name] = osMod.D_mark_pos[name];
      markersWeights[name] = 1.0;
      optiMarkers[opti_index] = optitrackPoints[opti_index];
      already_treated_markers.push_back(name);
      osimMarkers2Rescale[name] = osMod.D_mark_pos[name];
    }
    std::vector<std::string> allMark2Rescale = D_parent_all_children[body_name];
  
    for (int l = 0; l < allMark2Rescale.size(); l++)
    {
      std::vector<double> vd;
      //bsc[allMark2Rescale[i]] = vd;
      std::vector<std::string> bodyMarkers = D_body_markers[allMark2Rescale[l] ];
      for (int j = 0; j < bodyMarkers.size(); j++)
      {
        osimMarkers2Rescale[bodyMarkers[j]] = osMod.D_mark_pos[bodyMarkers[j]];
      }
      
    }
    

    Eigen::Matrix4f articulation_transform;
    std::vector<double> body_scaling = all_scaling_factors[body_name];

    std::cout << "ready calib specific for : " << body_name << std::endl;
    // nb : maybe add axis where transformation authorized (by articulation_transform)
    std::map<std::string, Eigen::Vector3d> osimMarkersOrigin = osimMarkers;

    int result = calibrationByICP(manParams, osimMarkers, optiMarkers, osMod, adapted_q_calib, corresNameID, articulation_transform, body_scaling);
    
    //       globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
    //    globalPublish.publishOpensim(calib_time, osimMarkers);
    //     std::string bla12;
    //   std::cout << "debug purpose : icp result" << std::endl;
    //  std::cin >> bla12;
    std::map<std::string, Eigen::Vector3d> osimMarkersRescaled;
    std::map<int, Eigen::Vector3d> osimMarkersIndex;

    // must be corrected in order to be rescaled into referential of body part

    int j = 0;

    for (std::map<std::string,Eigen::Vector3d>::iterator it = osimMarkers.begin(); it!=osimMarkers.end(); it++)
    {
      osimMarkersIndex[j] = osimMarkersOrigin[it->first];
      
      j++;
      Eigen::Vector4f v2(it->second[0]*body_scaling[0], it->second[1]*body_scaling[1], it->second[2]*body_scaling[2], 1.0);
      v2 = articulation_transform*v2;
      Eigen::Vector3d v3(v2[0],v2[1],v2[2]);
      //osimMarkersRescaled[it->first] = v3;
      osimMarkersRescaled[it->first] = it->second;
    }



    if (result > 0)
    {


      globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
      globalPublish.markerPublisher(calib_time, optitrackPoints, globalPublish.optitrack_robust_publisher);
      globalPublish.markerPublisher(calib_time, osimMarkersIndex, globalPublish.opensim_theorical_publisher);

      std::map<std::string,int> bodyCNID;
      for (std::map<std::string,Eigen::Vector3d>::iterator it = osimMarkers.begin(); it!=osimMarkers.end(); it++)
      {
        bodyCNID[it->first] = corresNameID[it->first];
      }

      Eigen::Matrix4f O; O.setZero();
      std::printf("Necessary correction for body part : %s \n", body_name.c_str());
      manual_choice = startManualCorrection(bodyCNID, optiMarkers, osMod, globalPublish, O);

      if (manual_choice != 0)
      {

        for (std::map<std::string,int>::iterator it = bodyCNID.begin(); it!=bodyCNID.end(); it++)
        {
          corresNameID[it->first] = it->second;
        }

      }

    }

    if (manual_choice != 0)
    {

    // other idea : add weight according to proximity from base
    // closest from base -> more weight

    // better : 
    // new PCA with optitrack / opensim theorical


    // thanks to associations, get a mean estimation of scaling factors
    // from all the markers

    // accordingly rescale of theorical markers
    std::map<std::string,Eigen::Vector3d> rescaleOsim = osimMarkersOrigin;

    //std::map<std::string,std::vector<double> > sc;
    //sc[body_name] = body_scaling;
    // sc[body_name][0] = 1.4;
    // sc[body_name][1] = 1.3;
    // sc[body_name][2] = 1.2;
    //osMod.applyStretching(rescaleOsim,sc);
    // = osimMarkersOrigin;

  rescaleCloudsWithAssociation2(optiMarkers, rescaleOsim, osMod,corresNameID, articulation_transform, body_scaling, globalPublish);

    std::map<std::string, std::vector<double> > bsc;  

    for (std::map<std::string, std::vector<double> >::iterator it3 = bsc.begin(); it3!= bsc.end(); it3++)
    {
      bsc[it3->first] = body_scaling;
      for (int l =0; l < 3; l++)
      {
        std::cout << all_scaling_factors[it3->first][l] << std::endl;
        all_scaling_factors[it3->first][l] *= body_scaling[l];
      }
    }
    bsc[ osMod.D_markers_body[rescaleOsim.begin()->first ]] = body_scaling;

    osMod.applyStretching(osimMarkers2Rescale,bsc,body_name);

  std::printf(" Body scaling (from association ) : %f, %f, %f \n", body_scaling[0], body_scaling[1], body_scaling[2]);
  // rescaleCloudsWithAssociation(optiMarkers, rescaleOsim,corresNameID, articulation_transform, body_scaling);

    all_scaling_factors[body_name] = body_scaling;

    // IK



    globalPublish.publishOpensim(calib_time, osimMarkers2Rescale);
    globalPublish.markerPublisher(calib_time, optiMarkers, globalPublish.optitrack_publisher);
globalPublish.markerPublisher(calib_time, optitrackPoints, globalPublish.optitrack_robust_publisher);
    globalPublish.markerPublisher(calib_time, osimMarkersIndex, globalPublish.opensim_theorical_publisher);
        globalPublish.publishJointState(calib_time, osMod.D_joints_values);


      std::string bla;
      std::cout << "debug purpose : scaling result." << std::endl;
     std::cin >> bla;


      // add step to estimate scale factor between rescaleOsim / osimMarkersOrigin,
      // in their ground referential. Will then be send during scaling step.


      osMod.setMarkersPositions(osimMarkers2Rescale);

  // globalPublish.publishOpensim(calib_time, osimMarkers2Rescale);

      //    - IK with already treated optiMarkers (+ new markers). All other weights at 0.0.
      for (std::map<std::string,Eigen::Vector3d>::iterator it = osimMarkers.begin(); it!=osimMarkers.end(); it++ )
      {
          adapted_opensim_positions[it->first] = optiMarkers[corresNameID[it->first]];
      }


      osMod.IK(adapted_opensim_positions,markersWeights,1,calib_time.toSec());
      // could be better to display joint configuration of model here.
      globalPublish.publishJointState(calib_time, osMod.D_joints_values);

      std::map<std::string,Eigen::Vector3d> osimChecked;

      for (std::map<std::string,Eigen::Vector3d>::iterator it = osMod.D_mark_pos.begin(); it!= osMod.D_mark_pos.end(); it++)
      {
        if (std::find(already_treated_markers.begin(), already_treated_markers.end(), it->first) != already_treated_markers.end())
        {
          osimChecked[it->first] = it->second;
        }
      }

    int j = 0;

    for (std::map<std::string,Eigen::Vector3d>::iterator it = osimMarkers2Rescale.begin(); it!=osimMarkers2Rescale.end(); it++)
    {
      osimMarkersIndex[j] = osMod.D_mark_pos[it->first];
      j++;
    }    
    globalPublish.markerPublisher(calib_time, osimMarkersIndex, globalPublish.opensim_theorical_publisher);
  globalPublish.publishOpensim(calib_time, osimChecked);

      fosim::printJointsValues(osMod.osimModel,osMod.osimState);
      
      std::cout << "debug purpose" << std::endl;
     std::cin >> bla;
    }

    else
    {
      i = body_in_multiorder.size();
    }

  }
  
  return(manual_choice);


}


/**
 * @brief 
 * 
 * @param manParams 
 * @param theoricalOpensimMarkers 
 * @param optitrackPoints 
 * @param osMod 
 * @param q_calib 
 * @param corresNameID 
 * @param result_transf transformation from Opensim to Optitrack position.
 * @param scaling_factors 
 * @param doPCScaling : option to desactivate the PC scaling for guessing the association in ICP. This is useful if, for example,
 * you are testing the same model and you have already calibrate once the position of the markers
 * @return int result code : 
 * -1 : association by ICP failed
 * 0 : ICP converged, but result seems to have failed
 * 1 : association by ICP succeded after recovery
 * 2 : association by ICP succeded without any issues
 * 
 * NB : maybe add filtering according to type of transform (rotation, translation at first)
 * 
 * NB : q_calib must be set to osMod before executing method.
 * 
 */
int calibrationByICP(
            managementParameters & manParams,
            std::map<std::string, Eigen::Vector3d > & theoricalOpensimMarkers,
            std::map<int, Eigen::Vector3d > optitrackPoints,
            fosim::OsimModel & osMod,
            std::vector<double> q_calib,
            std::map<std::string,int> & corresNameID,
            Eigen::Matrix4f & result_transf,
            std::vector<double> & scaling_factors,
            bool useResultTransfAsGuess,
            bool doPCScaling
             ) 
{

    bool global = (theoricalOpensimMarkers.size() == osMod.D_mark_pos.size());


    //std::map<std::string, Eigen::Vector3d > theoricalOpensimMarkers = osMod.D_mark_pos;
    pcl::PointCloud<pcl::PointXYZ>::Ptr osimPC(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr optiPC(new pcl::PointCloud<pcl::PointXYZ>);
    std::vector<int> opti_indexes;
    std::vector<std::string> osim_name;

    for ( std::map<std::string, Eigen::Vector3d >::iterator it=theoricalOpensimMarkers.begin(); it!=theoricalOpensimMarkers.end(); it++ )
    {
      Eigen::Vector3d vec = it->second;
      pcl::PointXYZ pt(vec[0],vec[1],vec[2]);
      (*osimPC).push_back(pt);
      osim_name.push_back(it->first);
    
    }

    for ( std::map<int, Eigen::Vector3d >::iterator it=optitrackPoints.begin(); it!=optitrackPoints.end(); it++ )
    {
      Eigen::Vector3d vec = it->second;
      pcl::PointXYZ pt(vec[0],vec[1],vec[2]);
      (*optiPC).push_back(pt);
      opti_indexes.push_back(it->first);
    }
    Eigen::Matrix4f transf;
    Eigen::Matrix4f init_guess;
    init_guess.setIdentity();
    Eigen::Matrix3f rotMat;
    Eigen::Vector3f init_trans;

    int result;

    if (optitrackPoints.size() < 3)
    {
      std::printf("ERROR : Impossible to do ICP (number markers < 3). Let's try to find transformation with current associations.\n");

      findTransformWithAssociation(optitrackPoints,theoricalOpensimMarkers,corresNameID,result_transf);
      std::vector<double> v{1.0,1.0,1.0};
      scaling_factors = v;
      result = -1;
    }

    else
    {

      if (global)
      {

        if (!useResultTransfAsGuess)
        {


          Eigen::Vector4f centroidOsim;
          pcl::compute3DCentroid( (*osimPC),centroidOsim);
          int index1, index2;
          int np, nn;
          int side = -1;

          int index_osim = getFarthestUppestFromCentroid(centroidOsim, index1, index2, np,nn,osimPC);

          if (np != -1)
          {
            if (np > nn)
            {
              index_osim = index1;
              side = 0;
            }
            else
            {
              index_osim = index2;
              side = 1;
            }
          }

          pcl::PointXYZ farthest_osim = (*osimPC)[index_osim];

          Eigen::Vector3f centerOsim(centroidOsim[0], centroidOsim[1], centroidOsim[2]);

          Eigen::Vector4f centroidOpti;
          double angle;

          getGuess(optiPC,farthest_osim,side,centroidOpti,angle,manParams);

          Eigen::Vector3f centerOpti(centroidOpti[0], centroidOpti[1], centroidOpti[2]);
          
          Eigen::AngleAxisf init_rotation(angle, Eigen::Vector3f::UnitY ());
          rotMat = init_rotation.matrix();

          init_trans = centerOpti -rotMat*centerOsim;

          if (manParams.icp_verbose)
          {
              std::cout << " Angle estimated : " << angle << std::endl;
              std::cout << " Translation : " << init_trans << std::endl;
          }

        }
        else
        {
          rotMat.block<3,3>(0,0) = result_transf.block<3,3>(0,0);
          init_trans.block<3,1>(0,0) = result_transf.block<3,1>(0,3);

        }
        init_guess.block<3,3>(0,0) = rotMat;
        init_guess.block<3,1>(0,3) = init_trans;  
        // try rescale with this transform

        if (doPCScaling)
        {
          rescaleClouds(osimPC,optiPC,init_guess,scaling_factors);
        }
        else
        {
          scaling_factors.push_back(1.0);
          scaling_factors.push_back(1.0);
          scaling_factors.push_back(1.0);
        }

      }
      else
      {
        rotMat.setIdentity();

        init_guess.block<3,3>(0,0) = rotMat;

        // try rescale without any transform
        

        rescaleCloudsNoTransform(osimPC,optiPC,scaling_factors,transf);
        Eigen::Vector4f centroidOsim;
        pcl::compute3DCentroid( (*osimPC),centroidOsim);

        Eigen::Vector4f centroidOpti;
        pcl::compute3DCentroid( (*optiPC),centroidOpti);
        Eigen::Vector3f centerOsim(centroidOsim[0], centroidOsim[1], centroidOsim[2]);

        Eigen::Vector3f centerOpti(centroidOpti[0], centroidOpti[1], centroidOpti[2]);
        
        init_trans = centerOpti -rotMat*centerOsim;

        init_guess.block<3,1>(0,3) = init_trans;  

        // for (int i =0; i<3;i++)
        // {
        // scaling_factors.push_back(1);
        // }
        //rescaleClouds(osimPC,optiPC,init_guess,scaling_factors);
        
      }

      double score = 10;
      int iter = 0;
      int max_iter = 10;

      pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>);
      pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;

      int max_iters = manParams.icp_max_iter;

      double euclidean_fitness_epsilon = osMod.min_mark_dist/max_iters;

      icp.setMaximumIterations(manParams.icp_max_iter);
      icp.setTransformationEpsilon(manParams.transformation_epsilon); // 1e-40
      icp.setRANSACOutlierRejectionThreshold(osMod.min_mark_dist/2);

      // source : original point cloud
      // target : pointcloud that we want to obtain
      // transform : from source to target

      icp.setInputSource(osimPC);
      icp.setInputTarget(optiPC);    
      
      Eigen::Matrix4f init_guess2;
      init_guess2.setIdentity();

      init_guess2 = init_guess;
      bool derivation = false;
      if (manParams.icp_verbose)
      {
        std::cout << "Start ICP." << std::endl;
      }

      double max_score;
      if (global)
      {
        max_score = 1e-6;
      }
      else
      {
        max_score = 1e-4;
      }

      icp.setEuclideanFitnessEpsilon(max_score*0.1);

      double best_score = INFINITY;
      Eigen::Matrix4f best_transf;

      while ( ( score > max_score) && (iter < max_iter)     )
      {

        icp.align(*transformed_cloud_ptr, init_guess2); 
        if (manParams.icp_verbose)
        {
          std::cout << "ICP has converged:" << icp.hasConverged()
                << " score: " << icp.getFitnessScore() << std::endl;
        }

        Eigen::Matrix4f final_transf =  icp.getFinalTransformation();
        Eigen::Matrix3f rot = final_transf.block<3,3>(0,0);

        Eigen::Vector3f ea = (rot).eulerAngles(2,0,1);

        if (manParams.icp_verbose)
        {
          std::cout << "Translation : " << final_transf.block<3,1>(0,3) << std::endl;

          std::cout << "Rotation (euler) : " << ea << std::endl;

        }

        // if (!global)
        // {
        //   transformFiltering(final_transf, manParams.maximum_translation, manParams.maximum_rotation);
        //   init_guess2 = final_transf;
        // }

        if (abs(score - icp.getFitnessScore()) < max_score)
        {
          if (manParams.icp_verbose)
          {
              std::cout << "No more changes. Stop icp." << std::endl;
          }
          iter = max_iter;
        }
        else
        {
          if (score - icp.getFitnessScore() < 0)
          {
            derivation = true;
          }
          else if ( icp.getFitnessScore()< best_score)
          {
            best_score = icp.getFitnessScore();
            best_transf = final_transf;
          }
          
        }

        score = icp.getFitnessScore();

        if ( (score > max_score) && (iter < max_iter)   )
        {

          if (derivation)
          {
            if (manParams.icp_verbose)
            {
                std::cout << "Derivation recorded. Try to fix." << std::endl;
            }

            if (global)
            {
              transformFiltering(final_transf, manParams.maximum_translation, manParams.maximum_rotation);
              init_guess2.block<3,3>(0,0) = final_transf.block<3,3>(0,0);
              init_guess2.block<3,1>(0,3) = init_trans; 
            }
            else
            {
              transformFiltering(final_transf, manParams.maximum_translation, manParams.maximum_rotation);
              init_guess2 = final_transf;
            }


          }
    

        }

        iter++;

      }

      // if (global)
      // {
      result_transf = best_transf;
      // }
      // else
      // {
      //   result_transf = transf;
      // }
      //rotationFiltering(result_transf);


      if (icp.hasConverged())
        {
      
          if ( global )
          { 
            int check =   startGlobalFiltering( 
                          manParams,
                          icp,
                          osMod,
                          osimPC,
                          optiPC,
                          osim_name,
                          opti_indexes,
                          q_calib,
                          optitrackPoints,
                          result_transf,
                          corresNameID);   
            if (check == 0)
            {
              // success
              result = 2;
            }
            else if (check == 2)
            {
              // warn of recovery procedure
              result = 1;
            }
            else
            {
              // convert the specification of the error as a classic error
              // msg, to be sure that we will have a reaction for calibration checking
              result = 0;
            }

          }
          else
          {
            std::map<std::string,double> distOsimOpti;
            int check =   startBodyFiltering( 
                          manParams,
                          icp,
                          osMod,
                          osimPC,
                          optiPC,
                          osim_name,
                          opti_indexes,
                          optitrackPoints,
                          result_transf,
                          corresNameID,
                          distOsimOpti,
                          true);  
            if (check == 2)
            {
              // convert the specification of the error as a classic error
              // msg, to be sure that we will have a reaction for calibration checking
              result = -1;
            }
            // else if (check != 0 && !doPCScaling)
            // {
            //   // paranoiac case : any suspicion of bug should trigger
            //   // a check by user
            //   result = 0;
            // }

            else
            {
              // otherwise, considers operation as success
              result = 1;
            }
              

          }


        }
      else
      {
        result = 1;
      }
  }

pcl::PointCloud<pcl::PointXYZ>::Ptr osimMovedPC(new pcl::PointCloud<pcl::PointXYZ>);
pcl::transformPointCloud(*osimPC, *osimMovedPC, result_transf);

  std::map<std::string, Eigen::Vector3d >::iterator itosim;
  for (int i = 0; i < osimMovedPC->points.size(); i++)
  {
      itosim = theoricalOpensimMarkers.begin();
      std::advance(itosim,i);
      Eigen::Vector3d pt(osimMovedPC->points[i].x, osimMovedPC->points[i].y, osimMovedPC->points[i].z );
      theoricalOpensimMarkers[itosim->first] = pt;
  }    

return(result);
};

/*

Output : [manual_choice, result]
manual_choice : 
  * 3 : No correction
  * 2: Specific correction asked
  * 0 : Cancel all
*/
std::vector<int> tryToCorrectCalibPose( managementParameters & manParams,
            std::map<std::string,int> & corresNameID,
 fosim::OsimModel & osMod_origin,
  std::map<int, Eigen::Vector3d >optitrack_pos_calib,
   std::vector<double> origin_q_calib,
    std::vector<double>  & scaling_factors,
ros::Time optitrack_pos_calib_time,
    fosim::OsimModel & rescale_osMod,
   globalPublisher globalPublish, 
     Eigen::Matrix4f & result_transf,
     bool doPCScaling)
{
  ROS_INFO("Calibration pose (ground==>root_joint) seems incorrect. Try to fix it...");

  std::vector<int>  v_res(2,0); 

  std::string manual_corr;
  int manual_choice = -1;
  int result = 1;

  while (manual_choice == -1)
  {
    std::printf("What kind of correction do you want? [Specific,None,Cancel]\n");
    std::cin >> manual_corr;
    if (manual_corr.substr(0,1) == "N" || manual_corr.substr(0,1) == "n")
    {
      manual_choice = 3;
    }    
    else if (manual_corr.substr(0,1) == "S" || manual_corr.substr(0,1) == "s")
    {
      manual_choice = 2;
    }
    else if (manual_corr.substr(0,1) == "C" || manual_corr.substr(0,1) == "c")
    {
      manual_choice = 0;
    }

  }


  if (manual_choice == 2)
  {
    Eigen::Matrix4f origin_transf = result_transf;
    std::printf("--- Specific Mode Activated --- \n");
    int correc_optitrack;
    std::string choice = "None";

    while (choice != "End")
    {
      bool displayCorrect = false;
      bool applyCorrect = false;
      bool displayTransf = false;
      std::printf("Please enter the type of correction that you want to add to the current pose : [A]ngular or [T]ranslation. Enter [End] to finish. ([H] for other options.) \n");
      std::cin.clear();
      std::cin >> choice;
      if (choice == "T" || choice == "t" || choice == "A" || choice == "a")
      {
        // reset rescale_osMod and result_transf
        result_transf = origin_transf;
        osMod_origin.fromScratch();
        osMod_origin.setQ(origin_q_calib);
        //rescale_osMod.setGroundJointOnly(origin_transf);
        //rescale_osMod.setMarkersPositions(osMod_origin.D_mark_pos);

        bool isRot = false;
        if (choice == "A" || choice == "a" )
        {
          isRot = true;
        }

        int axis = -1;
        while (axis == -1)
        {
          std::printf("--- Please enter on which axis this correction should be applied (according to Opensim Referential : Y-up, X-on-right) : X,Y,Z --- \n");
          choice = "";
          std::cin.clear();
          std::cin >> choice;          
          if (choice == "X" || choice == "x")
          {
            axis = 0;
          }

          else if (choice == "Y" || choice == "y")
          {
            axis = 1;
          }

          else if (choice == "Z" || choice == "z")
          {
            axis = 2;
          }
          else
          {
              std::printf("Sorry, %s wasn't a valid answer.\n", choice.c_str());       
              std::cin.clear();
          }

        }

        if (axis != -1)
        {
          std::printf("--- Please enter the value of the correction (in meters for translation, in degrees for rotation) --- \n");
          std::cin.clear();
          choice = "";
          std::cin >> choice;
          Eigen::Matrix4f correctMatrix; correctMatrix.setIdentity();
          double val = std::stod(choice);
          if (isRot)
          {
            // degrees ==> radian
            val = M_PI * val / 180.0;
            Eigen::Vector3f uAx;
            if (axis  == 0)
            {
              uAx =  Eigen::Vector3f::UnitX();
            }
            else if (axis  == 1)
            {
              uAx =  Eigen::Vector3f::UnitY();
            }
            else if (axis  == 2)
            {
              uAx =  Eigen::Vector3f::UnitZ();
            }                        
            Eigen::AngleAxisf init_rotation(val,uAx);
            Eigen::Matrix3f homoM = init_rotation.matrix();
            Eigen::Matrix3f I; I.setIdentity();
            correctMatrix.block<3,3>(0,0) = (homoM);
            correctMatrix.block<3,1>(0,3) = (I-homoM)*result_transf.block<3,1>(0,3);
            //result_transf.block<3,3>(0,0) = homoM*result_transf.block<3,3>(0,0);

          }
          else
          {
            correctMatrix(axis,3) = val;
            //result_transf = correctMatrix * result_transf;

          }
          result_transf = correctMatrix * result_transf;

          // apply correction to check
          applyCorrect = true;

        }

      }      
      else if (choice == "C" || choice == "c" )
      {
        // display transformation
        displayTransf= true;
      }   
      else if (choice == "R" || choice == "r" )
      {
        displayCorrect = true;
      }        

      else if (choice == "H" || choice == "h" )
      {
        std::printf("Options : \n");
        std::printf("Display the current transformation in Console : [C]. \n");
        std::printf("Display the current transformation in Rviz : [R]. \n");

      }
      else if (choice == "End")
      {
        displayCorrect = true;
        displayTransf = true;
      }
      else 
      {  
        std::printf("Sorry, %s wasn't a valid answer.\n", choice.c_str());       
        std::cin.clear();
       
      }
      
      if (applyCorrect)      
      {
        rescale_osMod.fromScratch();

        rescale_osMod.setQ(origin_q_calib);        
        // display transformation
        Eigen::Matrix3f M;
        Eigen::Vector3f V;
        M.block<3,3>(0,0) = result_transf.block<3,3>(0,0);
        V.block<3,1>(0,0) = result_transf.block<3,1>(0,3);
        Eigen::Quaternion<float> q(M);        
        // auto ea = q.toRotationMatrix().eulerAngles(2, 0, 1);
        auto ea = q.toRotationMatrix().eulerAngles(1, 0, 2);

        std::printf("Chosen ground guess : ");
        std::printf("Angles (Euler, roll-pitch-yaw) : [%3.4f ; %3.4f ; %3.4f ] \n",ea[0],ea[1],ea[2]);
        std::printf("Angles (Quaternion, [x,y,z,w]) : [%3.4f ; %3.4f ; %3.4f ; %3.4f ] \n",q.x() , q.y() , q.z() ,q.w() );    
        std::printf("Translation [x,y,z] : [ %3.4f ; %3.4f ; %3.4f ] \n",V(0,0),V(1,0),V(2,0));

        // rescale_osMod.setGroundJointOnly(result_transf);

        // globalPublish.publishOpensimOptitrack(optitrack_pos_calib_time,optitrack_pos_calib, rescale_osMod, result_transf);


        // std::string blk;
        // std::cin >> blk;
        rescale_osMod.setGroundJointOnly(origin_transf);

        //osMod_origin.setQ(origin_q_calib);
        result = markersManagement::calibrationByICP(manParams,
                                                osMod_origin.D_mark_pos,
                                                optitrack_pos_calib,
                                                osMod_origin,
                                                origin_q_calib,
                                                corresNameID,
                                                result_transf,
                                                scaling_factors,
                                                true,
                                                doPCScaling);

        // display the opensim - optitrack markers coupled, and the theorical
        // opensim model ( best situation : mostly pitch, perfect q_calib )

        rescale_osMod.setQ(origin_q_calib);

        std::map<std::string, Eigen::Vector3d> rescaleMarkers;

        if (doPCScaling)
        {
          rescaleMarkers = markersManagement::applyScalingToMarkers( rescale_osMod.D_mark_pos,scaling_factors);
          rescale_osMod.setMarkersPositions(rescaleMarkers,"ground");

        }
        else
        {
          rescaleMarkers = osMod_origin.D_mark_pos_local;
          rescale_osMod.setMarkersPositions(rescaleMarkers,"local");      
        }
        rescale_osMod.setGroundJointOnly(result_transf);

        displayCorrect = true;
        displayTransf = true;

      }

      if (displayCorrect)
      {

        rescale_osMod.setGroundJointOnly(result_transf);

        globalPublish.publishOpensimOptitrack(optitrack_pos_calib_time,optitrack_pos_calib, rescale_osMod, result_transf);

      }

      if (displayTransf)
      {
        Eigen::Matrix3f M;
        Eigen::Vector3f V;
        M.block<3,3>(0,0) = result_transf.block<3,3>(0,0);
        V.block<3,1>(0,0) = result_transf.block<3,1>(0,3);
        Eigen::Quaternion<float> q(M);        
        // auto ea = q.toRotationMatrix().eulerAngles(2, 0, 1);
        auto ea = q.toRotationMatrix().eulerAngles(1, 0, 2);

        std::printf("Result transf : \n");
        std::printf("Angles (Euler, roll-pitch-yaw) : [%3.4f ; %3.4f ; %3.4f ] \n",ea[0],ea[1],ea[2]);
        std::printf("Angles (Quaternion, [x,y,z,w]) : [%3.4f ; %3.4f ; %3.4f ; %3.4f ] \n",q.x() , q.y() , q.z() ,q.w() );    
        std::printf("Translation [x,y,z] : [ %3.4f ; %3.4f ; %3.4f ] \n",V(0,0),V(1,0),V(2,0));
      }

  }

  }

  v_res[0] = manual_choice;
  v_res[1] = result;

  return(v_res);


}


int startManualCorrection(std::map< std::string, int > & corresNameID,
                           std::map<int, Eigen::Vector3d > optitrackPoints,
                           fosim::OsimModel & osMod,
                           globalPublisher globalPublish, 
                           Eigen::Matrix4f result_transf
                            )
{
  std::printf("--- Start Manual Correction --- \n");
  std::string manual_corr;
  int manual_choice = -1;

  while (manual_choice == -1)
  {
    std::printf("What kind of correction do you want? [Iterative,Specific,None,Cancel]\n");
    std::cin.clear();
    std::cin >> manual_corr;
    if (manual_corr.substr(0,1) == "N" || manual_corr.substr(0,1) == "n")
    {
      manual_choice = 3;
    }    
    else if (manual_corr.substr(0,1) == "I" || manual_corr.substr(0,1) == "i")
    {
      manual_choice = 1;
    }
    else if (manual_corr.substr(0,1) == "S" || manual_corr.substr(0,1) == "s")
    {
      manual_choice = 2;
    }
    else if (manual_corr.substr(0,1) == "C" || manual_corr.substr(0,1) == "c")
    {
      manual_choice = 0;
    }



  }


  if (manual_choice == 1)
  {
    std::printf("--- Iterative Mode Activated --- \n");
    std::map<std::string, int>::iterator it;
    int index = 0;
    while (index < corresNameID.size())
  {
    it = corresNameID.begin();
    std::advance(it,index);
    std::string choice = "None";
    bool choice_made = false;
    int correc_optitrack;
    Eigen::Vector3d osim_pos = osMod.D_mark_pos[ it->first ] ;
    Eigen::Vector3d opti_pos = optitrackPoints[ it->second];

    std::printf("Marker name (%d) : %s ; optitrack associated : %d \n ", index, it->first.c_str(), it-> second);
    std::printf("Opensim Marker Position : [%3.4f, %3.4f, %3.4f] \n", osim_pos[0], osim_pos[1], osim_pos[2] );
    std::printf("Optitrack Marker Position : [%3.4f, %3.4f, %3.4f] \n", opti_pos[0], opti_pos[1], opti_pos[2] );
    while(choice_made == false)
    {
      std::printf("Do you agree with this association? ([Y/N], [H] for other options.) \n");
      std::cin.clear();
      std::cin >> choice;
      if (choice == "Y" || choice == "y")
      {
        choice_made = true;
        index++;
      }
      else if (choice == "N" || choice == "n" )
      {

        bool valid = false;
        while (!valid)
        {
          std::printf("Fine. Please enter the corrected id of the optitrack marker.\n");
          std::cin.clear();
          std::cin >> correc_optitrack;
          if (optitrackPoints.find(correc_optitrack) != optitrackPoints.end())
          {
            corresNameID[it->first] = correc_optitrack;
            choice_made = true;
            index++;
            valid = true;
          }
          else
          {
            std::printf("Sorry, no optitrack marker has %d as ID in this frame.\n", correc_optitrack);     
            std::cin.clear();
          }
        }

      }
      else if (choice == "R" || choice == "r" )
      {
        ros::Time time = ros::Time::now();
        globalPublish.publishOpensimOptitrack(time,optitrackPoints, osMod, result_transf);
        std::printf("Currently, the association studied is : \n");
        std::printf("Marker name : %s ; optitrack associated : %d \n ", it->first.c_str(), it-> second);
      }      
      else if (choice == "C" || choice == "c" )
      {
        printAssociation(corresNameID);
        std::printf("Currently, the association studied is : \n");
        std::printf("Marker name : %s ; optitrack associated : %d \n ", it->first.c_str(), it-> second);
      }            
      else if (choice == "H" || choice == "h" )
      {
        std::printf("Options : \n");
        std::printf("Display the current associations in Console : [C]. \n");
        std::printf("Display the current associations in Rviz : [R]. \n");
        std::printf("End the iterative verification : [End]. \n");

      }

      else if (choice == "End")
      {
        index = corresNameID.size();
        choice_made = true;


      }

      else
      {
          std::printf("Sorry, %s wasn't a valid answer.\n", choice.c_str());       
      }

    }
    


  }

  }
  else if (manual_choice == 2)
  {
    
    std::printf("--- Specific Mode Activated --- \n");
    int correc_optitrack;
    std::string choice = "None";

    while (choice != "End")
    {
      std::printf("Please enter the names of the Opensim Markers that should be corrected. Enter [End] to finish. ([H] for other options.) \n");
      std::cin.clear();
      std::cin >> choice;
      if (choice == "R" || choice == "r" )
      {
        ros::Time time = ros::Time::now();
        globalPublish.publishOpensimOptitrack(time,optitrackPoints, osMod, result_transf);
      }      
      else if (choice == "C" || choice == "c" )
      {
        printAssociationByBody(corresNameID,osMod);
      }            
      else if (choice == "H" || choice == "h" )
      {
        std::printf("Options : \n");
        std::printf("Display the current associations in Console : [C]. \n");
        std::printf("Display the current associations in Rviz : [R]. \n");

      }
      else if (choice == "End")
      {
        continue;
      }

      else 
      {
        std::map<std::string,int>::iterator ite = corresNameID.find(choice);
        if (ite == corresNameID.end())
        {
          std::printf("Sorry, the marker %s wasn't found in the model.\n", choice.c_str());
        }
        else
        {
          bool valid = false;
          while (!valid)
          {
            std::printf("Fine. Please enter the corrected id of the optitrack marker.\n");
            std::cin.clear();
            std::cin >> correc_optitrack;
            if (optitrackPoints.find(correc_optitrack) != optitrackPoints.end())
            {
              corresNameID[ite->first] = correc_optitrack;
              valid = true;
            }
            else
            {
              std::printf("Sorry, no optitrack marker has %d as ID in this frame.\n", correc_optitrack);     

            }
          }          
        }
        
      }

    }

  }

  return(manual_choice);

}

/**
 * @brief 
 * Scaling should have already been applied on osMod.
 * 
 * @param osMod 
 * @param D_j_v_ori 
 * @param markersLocation 
 * @param scaling_factors 
 * @return int 
 */
int checkScaling(fosim::OsimModel & osMod, 
std::map<std::string,double> D_j_v_ori,
std::map<int,Eigen::Vector3d> opti_pos_calib,
std::map<std::string,Eigen::Vector3d> markersLocation,
globalPublisher globalPublish)
{

  std::map<std::string, double> markersWeights;
  int index = 0;
  for (std::map<std::string, Eigen::Vector3d>::iterator it = markersLocation.begin(); it!=markersLocation.end(); it++)
  {
    if (index < 6)
    {
      markersWeights[it->first] = 4.0;
    }
    else
    {
      markersWeights[it->first] = 1.0;
    }
    index++;
  }
  
  int result = osMod.IK(markersLocation,markersWeights,1,0.0);
  int choice = -1;
  if (result >0)
  {

    std::map<std::string,double> D_joints_values = osMod.D_joints_values;

    double max_diff=0;
    std::map<std::string, double > part_calib;
    std::map<std::string, double > part_IK;

    for (std::map<std::string, double>::iterator it = D_joints_values.begin(); it!=D_joints_values.end(); it++)
    {
      std::printf("Part %s : calibration : %f ; IK : %f. \n", it->first.c_str(), D_j_v_ori[it->first], it->second );

      part_calib[it->first] =  D_j_v_ori[it->first];
      part_IK[it->first] = it->second;
      if ( abs(D_j_v_ori[it->first] ) > 1e-4)
      {
        double diff = abs(it->second- D_j_v_ori[it->first]);
        if (diff > max_diff)
        {
          max_diff = diff;
        }
      }
    }

    std::string rep= "";
    
    if (max_diff > 0.1)
    {
      usleep(2*1e6);
      while (choice ==-1 )
      {
        ROS_INFO("Warning : difference between parts seems a little bit too big (max diff : %f). You may want to retry calibration if you want a proper scaling.",max_diff);
        ROS_INFO("Your choice are : (R)etry calibration and scaling | (S)cale anyway and continue | (T)ry without scaling | (K)eep model but do marker scaling | (M)arkers scaling on non-scaled model.  ");
        ROS_INFO("You can display the differences with (D), display the theorical joint configuration with (J), and display the IK joint configuration with (I). ");
        std::cin>> rep;
        if (rep.substr(0,1) == "t" | rep.substr(0,1)== "T")
        {
          ROS_INFO("Ok. Let's try without scaling. ");
          choice = 1;
        }
        else if (rep.substr(0,1) == "R" | rep.substr(0,1)== "r")
        {
          ROS_INFO("Ok. Just recall the service whenever you're ready.");
          choice = 0;
        }
        else if (rep.substr(0,1) == "S" | rep.substr(0,1)== "s")
        {
          ROS_INFO("Ok. Let's try with scaling.");
          choice = 2;
        }
        else if (rep.substr(0,1) == "M" | rep.substr(0,1)== "m")
        {
          ROS_INFO("Ok. Let's try with only marker scaling on non-scaled model.");
          choice = 3;
        }
        else if (rep.substr(0,1) == "K" | rep.substr(0,1)== "k")
        {
          ROS_INFO("Ok. Let's try with marker scaling on scaled model.");
          choice = 4;
        }        

        else if (rep.substr(0,1) == "D" | rep.substr(0,1)== "d")
        {
          for(std::map<std::string, double >::iterator it = part_calib.begin(); it!= part_calib.end(); it++)
          {
            std::printf("Part %s : calibration : %f ; IK : %f. \n", it->first.c_str(),it->second, part_IK[it->first] );
          }
        }        
        else if (rep.substr(0,1) == "J" | rep.substr(0,1)== "j")
        {
          globalPublish.publishJointState(ros::Time::now(),part_calib);
          osMod.setQ(part_calib);
          globalPublish.publishOpensimOptitrack(ros::Time::now(), opti_pos_calib, osMod ); 
        }    
        else if (rep.substr(0,1) == "I" | rep.substr(0,1)== "i")
        {
          globalPublish.publishJointState(ros::Time::now(),part_IK);
          osMod.setQ(part_IK);
          globalPublish.publishOpensimOptitrack(ros::Time::now(), opti_pos_calib, osMod );      
        }                        
      


      }

    }
    else
    {
      ROS_INFO("Pose seems good. Let's do it!");
      choice = 2;
    }

  }

  else
  {
    ROS_INFO("Impossible IK. Please retry. \n");
    choice = 0;
  }
  osMod.setToNeutral();
  return(choice);

}


std::map<int, Eigen::Vector3d> simulateMarkers(fosim::OsimModel & osMod, 
int frame_rate, 
SimulationGestion & simu_gest)
{


int i_joint = simu_gest.i_joint;
std::vector<int> joint_state = simu_gest.joint_state;
double j_val = simu_gest.j_val;
double speed = simu_gest.speed;
int dissapear = simu_gest.dissapear;
int max_index = simu_gest.max_index;
std::map< std::string, int> hiddenCorresNameID = simu_gest.hiddenCorresNameID;
int same_frames = simu_gest.same_frames;
int cur_frames = simu_gest.cur_frames;
double ampl_noise = simu_gest.noise;
bool verbose = simu_gest.simu_verbose;

bool calibCompleted = simu_gest.calibCompleted;


 std::map<std::string, double> D_j_ori = osMod.D_joints_values;


  osMod.setToNeutral();

 std::map<std::string, double> D_j_neutral = osMod.D_joints_values;

  std::vector<double> joints_ori(D_j_ori.size(), 0.0);

  std::vector<double> mod_joints(D_j_ori.size(), 0.0);

  int j = 0;
  for(std::map<std::string, double>::iterator it = D_j_ori.begin(); it!= D_j_ori.end(); it++)
  {
      int l = osMod.D_alph_multi[j];
      joints_ori[l] = it->second;
      mod_joints[l] = D_j_neutral[it->first];
      j++;

  }

  double jl;
  double dt = 1.0/frame_rate;

  if (i_joint < mod_joints.size() )
  {
    if (joint_state[i_joint] == 0)
    {
      j_val = mod_joints[i_joint];
      joint_state[i_joint] = 1;
    }


    else if (joint_state[i_joint] == 1)
    {
        jl = osMod.osimModel.updCoordinateSet()[i_joint].get_range(1);
        j_val += speed*dt ;
        
        j_val = std::min(j_val,jl);
        if (j_val == jl)
        {
          joint_state[i_joint] = 2;

        };

    }

    else if (joint_state[i_joint] ==  2)
    {
        jl = osMod.osimModel.updCoordinateSet()[i_joint].get_range(0);
        j_val -= speed*dt ;
        j_val = std::max(j_val,jl);

        if (j_val == jl)
        {
          joint_state[i_joint] = 3;

        };

    }

    else if (joint_state[i_joint] ==  3)
    {
        jl = osMod.osimModel.updCoordinateSet()[i_joint].get_default_value();
        j_val += speed*dt ;
        j_val = std::min(j_val,jl);

        if (j_val == jl)
        {
          joint_state[i_joint] = 4;

        };

    };
  

  }

  else
  {
    i_joint = 0;
    j_val = 0.0;
    for (int i =0; i< joint_state.size(); i++)
    {
      joint_state[i] = 0;
    };

  }

    mod_joints[i_joint] = j_val;
    osMod.setQ(mod_joints);

    simu_gest.hiddenJointsValues = osMod.D_joints_values;

    //std::cout << "--- Current joint for marker generation ----" << std::endl;
    //fosim::printJointsValues(osMod.osimModel,osMod.osimState);
    std::map<std::string, Eigen::Vector3d >  osim_pos = osMod.D_mark_pos;
    std::map< int, Eigen::Vector3d > opti_pos;

    //std::printf("i_joint : %d ; state : %d; value : %f \n", i_joint,joint_state[i_joint], j_val);
    
    int i =-1; 

  std::uniform_real_distribution<double> unif(-ampl_noise,ampl_noise);
  std::random_device rd;
  std::mt19937 gen(rd());

  // supress markers
      for(std::map<std::string, Eigen::Vector3d>::iterator it = osim_pos.begin(); it!= osim_pos.end(); it++)
      {
        if (calibCompleted)
        {
          i = hiddenCorresNameID[it->first];
        }
        else
        {
          i++;
        }

        if (i<0 && cur_frames > same_frames)
        {
          i = -i;
          hiddenCorresNameID[it->first] = i;
          if (verbose) {printf("Mod index : %d to %d (associated previously with : %s ). \n",-i, i, it->first.c_str());}

        }

        if (i>= 0)
        {
          // add noise
          Eigen::Vector3d vec = it->second;
          double rx = unif(gen);
          double ry = unif(gen);
          double rz = unif(gen);  
          Eigen::Vector3d vec2(vec[0]+rx,vec[1]+ry,vec[2]+rz);
          opti_pos[i] = vec2;
        }
        //opti_pos[i] = it->second;

      
      }


    // now, suppress a number dissapear of markers, and change them with new markers (with new id)
    if (calibCompleted && cur_frames > same_frames)
    {
      cur_frames = -1;
      i = 0;
      int randrank;
      int del;
      
      while (i < dissapear)
      {
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::srand(seed);
        randrank = rand() % (opti_pos.size()-1 );
        del = rand()%(2);

        std::map<std::string, int>::iterator it2 = hiddenCorresNameID.begin();
        std::advance(it2, randrank);

        int id = it2->second;

        if (id >=0)
        {
          if (verbose)
          {
            std::cout << "Rank : " << randrank << std::endl;
            std::cout << "Del : " << (del>0) << std::endl;
          }
                  
          std::string name = it2->first;

          int new_id;
          if (del>0)
          {
            new_id = -max_index-i;
          }
          else
          {
            new_id = max_index+i;
          }

          if (verbose) {printf("Mod index : %d to %d (associated previously with : %s ). \n",id, new_id, name.c_str());}


          hiddenCorresNameID[name] = new_id;

          std::map<int, Eigen::Vector3d >::iterator itopti = opti_pos.find(id);
          if (del ==0)
          {
          opti_pos[new_id] = opti_pos[id];
          }
          
          opti_pos.erase(itopti);        
          i++;
        }

      }
      max_index = max_index+i;

    }
    cur_frames++;


    if (joint_state[i_joint]==4)
    {
      i_joint+=1;
      
    };
  

    osMod.setQ(joints_ori);

simu_gest.i_joint = i_joint;
simu_gest.joint_state = joint_state;
simu_gest.j_val = j_val;
simu_gest.max_index = max_index;
simu_gest.hiddenCorresNameID = hiddenCorresNameID;
simu_gest.cur_frames = cur_frames;


return(opti_pos);

}

/**
 * @brief (Can only be used in simulation). Check if the correspondences determined by the algorithm are matching the
 * true ones. Exclude the associations where the marker has dissapears (determined in trueCorresNameID by a negative id).
 * 
 * @param corresNameId Computed correspondences between Opensim markers (string) and Optitrack ID (int), by a pair string:id.
 * @param corresNameId True correspondences between Opensim markers (string) and Optitrack ID (int), by a pair string:id.
 * @param error_pub ErrorPublicationGestion object, managing the publication of all the errors that could be created by the process.
 */
void checkSimulationAssociation(std::map<std::string,int> corresNameId,
std::map<std::string,int> trueCorresNameId,
ErrorsPublicationGestion & error_pub,
bool verbose)
{
  std::vector<std::string> allNameErr;
  std::vector<int> allTrueId;
  std::vector<int> allFalseId;
  for (std::map<std::string,int>::iterator it = trueCorresNameId.begin(); it!= trueCorresNameId.end(); it++)
  {
    int trueId = it->second;
    int id = corresNameId[it->first];

    if (trueId >= 0 && id != trueId && std::find(allNameErr.begin(), allNameErr.end(), it->first) == allNameErr.end())
    {
      allNameErr.push_back(it->first);
      allTrueId.push_back(trueId);
      allFalseId.push_back(id);
      auto itname = std::find_if(std::begin(corresNameId), std::end(corresNameId), [&](const std::pair<std::string,int> &pair)
      {
              return pair.second == trueId;
      });

      if (itname != corresNameId.end())
      {
        allNameErr.push_back(itname->first);
        allTrueId.push_back(trueCorresNameId[itname->first]);
        allFalseId.push_back(itname->second);
      }
      


    }

  }

  if (allNameErr.size() != 0)
  { 
    for (int i = 0; i < allNameErr.size(); i++)
    {
      error_pub.markers_error.push_back(allNameErr[i]);
      error_pub.id_errors.push_back(allFalseId[i]);
      error_pub.true_id.push_back(allTrueId[i]);            
    }
  }

  if (allNameErr.size() != 0 && verbose)
  { std::printf("Association errors !! \n");
    for (int i = 0; i < allNameErr.size(); i++)
    {     
      std::printf("For name : %s; Id taken : %d; true Id : %d. \n",allNameErr[i].c_str(), allFalseId[i], allTrueId[i] );
    }
    usleep(allNameErr.size()*1e6);
  }
  else if (verbose)
  {
    std::printf("Check ok! \n");
  }
}

/**
 * @brief (Can only be used in Simulation). Check the difference between the true joint configuration and the computed
 * joint configuration by Inverse Kinematics. May triggers a signal if this difference is very huge for a element of the
 * q vector (> 20 degrees for now).
 * 
 * @param D_joints_values Joint configuration determined by IK.
 * @param true_D_joints_values True joint configuration of the model.
 * @param error_pub ErrorPublicationGestion object, managing the publication of all the errors that could be created by the process.
 */
void checkSimulationIKConfiguration(std::map<std::string,double> D_joints_values,
std::map<std::string,double> true_D_joints_values,
ErrorsPublicationGestion & error_pub,
bool verbose)
{
  std::vector<std::string> allNameErr;
  std::vector<double> allTrueValues;
  std::vector<double> allFalseValues;
  for (std::map<std::string,double>::iterator it = true_D_joints_values.begin(); it!= true_D_joints_values.end(); it++)
  {
    double trueValue = it->second;
    double value = D_joints_values[it->first];
    double diff = abs(trueValue- value);
    error_pub.articulation_errors.push_back(diff);
    error_pub.joint_names.push_back(it->first);

    if (  diff > 20*M_PI/180  && std::find(allNameErr.begin(), allNameErr.end(), it->first) == allNameErr.end())
    {
        allNameErr.push_back(it->first);
        allTrueValues.push_back(trueValue);
        allFalseValues.push_back(value);
    }
  }

  if (allNameErr.size() != 0 && verbose)
  { std::printf("IK errors !! \n");
    for (int i = 0; i < allNameErr.size(); i++)
    {
       std::printf("For name : %s; Value taken : %f; true value : %f. \n",allNameErr[i].c_str(), allFalseValues[i], allTrueValues[i] );
    }
    std::string deb;
    std::printf("Press any button to continue. \n");
    std::cin >> deb;
  }
  else if (verbose)
  {
    std::printf("Check IK ok! \n");
  }
}
/**
 * @brief Indicates which Opensim marker is currently not assigned.
 * 
 * @param osMarkersNotAssigned Indicates which Opensim markers were assigned (= 0) or not assignated (= 1).
 * 
 */
void checkAssignedMarkers(std::map<std::string, int> osMarkersNotAssigned,
std::map<std::string, std::string> D_marker_body, 
bool verbose)
{
  bool first = true;
  for (std::map<std::string, int>::iterator it = osMarkersNotAssigned.begin(); it!= osMarkersNotAssigned.end(); it++ )
  {
    
    if (it->second == 1)
    {
      if (verbose)
      {
        if (first) { std::printf("Not assigned markers : "); first = false;}
        std::printf("%s (in %s), ", it->first.c_str(), D_marker_body[it->first].c_str() );
      }

    }


  }
  if (!first)
  {
  std::printf(".\n");
  }
}
/**
 * @brief Compute the error in the position of each marker (between the position computed by IK and the true position).
 * 
 * @param osMarkersExperimental Experimental position of each of the Opensim markers (that were retrieved from Optitrack markers)
 * @param osMarkersTheorical Inverse Kinematics markers.
 * @param error_pub ErrorPublicationGestion object, managing the publication of all the errors that could be created by the process.
 */
void computeMarkersErrors(std::map<std::string, Eigen::Vector3d> osMarkersExperimental,
std::map<std::string, Eigen::Vector3d> osMarkersTheorical,
std::map<std::string,double> markerWeights,
ErrorsPublicationGestion & error_pub,
bool verbose)
{
  for (std::map<std::string,Eigen::Vector3d>::iterator it = osMarkersExperimental.begin(); it!=osMarkersExperimental.end(); it++)
  {
    if (markerWeights[it->first]!= 0.0)
    {
      double diff = (it->second - osMarkersTheorical[it->first]).norm();
      error_pub.associated_markers_names.push_back(it->first);
      error_pub.IK_markers_pose_errors.push_back(diff);
    }
  }
}

/**
 * @brief Save the tolerance of displacement for the position of each marker.
 * 
 * @param toleranceValue
 * @param error_pub ErrorPublicationGestion object, managing the publication of all the errors that could be created by the process.
 */
void publishToleranceValue(std::map<std::string,double> toleranceValue,
ErrorsPublicationGestion & error_pub,
bool verbose)
{
  for (std::map<std::string,double>::iterator it = toleranceValue.begin(); it!=toleranceValue.end(); it++)
  {
    error_pub.association_tolerance_value.push_back(it->second);
    error_pub.all_markers_names.push_back(it->first);
  }
}
/**
 * @brief Clear all the variables of a ErrorPublicationGestion object. Should be
 * executed for each new frame.
 * 
 * @param error_pub ErrorPublicationGestion object, managing the publication of all the errors that could be created by the process.
 */
void clearErrorPublicationGestion(ErrorsPublicationGestion & error_pub)
{
    error_pub.joint_names.clear();
    error_pub.articulation_errors.clear();
    error_pub.all_markers_names.clear();
    error_pub.associated_markers_names.clear();
    error_pub.IK_markers_pose_errors.clear();
    error_pub.markers_error.clear();
    error_pub.id_errors.clear();
    error_pub.true_id.clear();
    error_pub.association_tolerance_value.clear();
}

void findTransformWithAssociation(std::map<int,Eigen::Vector3d> optitrackPoints, 
std::map<std::string,Eigen::Vector3d> opensimPoints, 
std::map<std::string,int> corresNameID,
Eigen::Matrix4f & transf)
{

    pcl::PointCloud<pcl::PointXYZ>::Ptr sourceCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr targetCloud(new pcl::PointCloud<pcl::PointXYZ>);

    for (std::map<std::string,Eigen::Vector3d>::iterator it = opensimPoints.begin(); it!= opensimPoints.end(); it++)
    {
      Eigen::Vector3d optiVec = optitrackPoints[corresNameID[it->first]];
      Eigen::Vector3d osimVec = it->second;

      pcl::PointXYZ optiPt(optiVec[0],optiVec[1], optiVec[2]);
      pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
      
      sourceCloud->points.push_back(osimPt);
      targetCloud->points.push_back(optiPt);

    }

    pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 
    svd.estimateRigidTransformation(*sourceCloud,*targetCloud,transf);

    std::cout << transf << std::endl;
}


void mapIntEigen2PCL(std::map<int,Eigen::Vector3d> map, pcl::PointCloud<pcl::PointXYZ>::Ptr & PC)
{
  for (std::map<int,Eigen::Vector3d>::iterator it = map.begin(); it!= map.end(); it++)
  {
    Eigen::Vector3d osimVec = it->second;
    pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
    PC->points.push_back(osimPt);
  }
}

void mapStringEigen2PCL(std::map<std::string,Eigen::Vector3d> map, pcl::PointCloud<pcl::PointXYZ>::Ptr & PC, std::vector<std::string> & nameOrder)
{
  int i = 0;
  for (std::map<std::string,Eigen::Vector3d>::iterator it = map.begin(); it!= map.end(); it++)
  {
    nameOrder.push_back(it->first);
    Eigen::Vector3d osimVec = it->second;
    pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
    PC->points.push_back(osimPt);
  }
}
/**
 * @brief 
 * 
 *   // idea : 
  // - get ground to body part transform T.
  //     * first naïve solution : find it by IK
  // - apply T^(-1) to all children markers
 * 
 * @param osMod 
 * @param body_part 
 * @param markerData 
 * @param ground2ParentTransform For each time, body_name:transform_of_body_from_ground_to_parent
 */
void suppressParentInfluenceInData(fosim::OsimModel & osMod, 
std::string body_part,
std::vector<std::map<int,Eigen::Vector3d> > markerData,
std::map<std::string,int> corresNameID,
std::map<std::string,double> markersWeights,
std::map<int,std::map<std::string, Eigen::Matrix4f> > & ground2ParentTransform,
ros::Time calib_time,
globalPublisher globalPublish)
{

  std::vector<std::string> markers_body = osMod.D_body_markers[body_part];
  std::vector<std::string> body_children = osMod.D_parent_all_children[body_part];

  // std::cout << "For body part : " << body_part << std::endl;
  // for (int i = 0; i < body_children.size(); i++)
  // {
  //   std::cout << "body children : " << body_children[i] << std::endl;
  // }

  std::vector<std::string> direct_children;
  std::string root = osMod.markers_bodies_multiorder[0];

  for (int i = 0; i < body_children.size(); i++)
  {
    std::string parent = osMod.D_child_parent[body_children[i]];
    while ( parent != root && (std::find( osMod.markers_bodies_multiorder.begin(),  osMod.markers_bodies_multiorder.end(), parent) ==  osMod.markers_bodies_multiorder.end() ) )
    {
      parent = osMod.D_child_parent[parent];
    }

    if (parent == body_part)
    {
      direct_children.push_back(body_children[i]);
      
    }
    // else
    // {
    //   std::cout << "Not direct for "<< body_part << " : " << body_children[i] << " ; its direct parent is : " << osMod.D_child_parent[body_children[i]] << std::endl;
    // }
  }

  // for (int i = 0; i < direct_children.size(); i++)
  // {
  //   std::cout << "direct child : " << direct_children[i] << std::endl;
  // }

  // std::string bla;
  // std::cin >> bla;

  // get local configuration

  std::map<std::string,Eigen::Vector3d> local_placement = osMod.D_mark_pos_local;

  pcl::PointCloud<pcl::PointXYZ>::Ptr localCloud(new pcl::PointCloud<pcl::PointXYZ>);

  for (int i = 0; i < markers_body.size(); i++)
  {
    Eigen::Vector3d osimVec = local_placement[markers_body[i]];
    pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
    localCloud->points.push_back(osimPt);
  }

  pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 
  
  std::map<std::string,double> origin_joints_values = osMod.D_joints_values;

  for (int i = 0; i < markerData.size(); i++)
  {
  
    std::map<int,Eigen::Vector3d> markerFrame = markerData[i];
    std::map<std::string,Eigen::Vector3d> osimMarkerFrame;
    for (int j = 0; j < markers_body.size(); j++)
    {
      std::string marker_name = markers_body[j];
      int id = corresNameID[marker_name];

      osimMarkerFrame[marker_name] = markerFrame[id];

    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr PC(new pcl::PointCloud<pcl::PointXYZ>);

    for (int j = 0; j < markers_body.size(); j++)
    {
      Eigen::Vector3d osimVec = osimMarkerFrame[markers_body[j]];
      pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
      PC->points.push_back(osimPt);
    }

    // IK method

    // osMod.IK(osimMarkerFrame,markersWeights,0,calib_time.toSec());

    // Eigen::Matrix4f g2b_transform = fosim::getBodyTransformInGround(osMod.osimModel, osMod.osimState, body_part);

    // PC method

    Eigen::Matrix4f g2b_transform;

    svd.estimateRigidTransformation(*PC,*localCloud,g2b_transform);

    //  display for debug purpose
    // if (i%50 == 0)
    // {
    //   pcl::PointCloud<pcl::PointXYZ>::Ptr PCDisp(new pcl::PointCloud<pcl::PointXYZ>);
    //   pcl::transformPointCloud(*PC, *PCDisp, g2b_transform );
    //   std::map<int, Eigen::Vector3d> points_in_body;
    //   for (int j = 0; j < PCDisp->points.size(); j++)
    //   {
    //     pcl::PointXYZ pt = PCDisp->points[j];
    //     Eigen::Vector3d ptd(pt.x,pt.y,pt.z);
    //     points_in_body[j] = ptd;
    //   }

    //   globalPublish.publishOpensim(calib_time, osimMarkerFrame);
    //   globalPublish.markerPublisher(calib_time,points_in_body,globalPublish.optitrack_publisher);
    //   std::string gd;
    //   std::cin >> gd;

    // }


    // save transform to direct children part

    for (int j = 0; j < direct_children.size(); j++)
    {
      if (ground2ParentTransform[i].find(direct_children[j]) != ground2ParentTransform[i].end())
      {
        ground2ParentTransform[i][direct_children[j]] = g2b_transform;
      }
    }

  }

  // reset joints_values according to previous

  //osMod.setQ(origin_joints_values);





}




/**
 * @brief Find the center of rotation with Normalization Method. (from Constrained least squares optimization for robust estimation of
 * center of rotation). For now, points needs to have no translation movement for this method to work. 
 * NB : The current implementation of NM is (for unknown reason) not the same as described in the paper; from tests, it is impossible
 * to get a good result with the condition u.T*C*u = 1. 
 * This is why this condition is not respected in this implementation (the only condition is to minimize the cost function u.T*S*u).
 * 
 * 
 * @param bodyMarkerData Vector containing, for each time, the position and identifiant of Optitrack markers of a specific body part.
 * @param m Position of the center of rotation according to the referential where the markers of the body part are expressed.
 * @param calib_time  Timestamp of the selected optitrack frame for calibration.
 * @param globalPublish Global object managing the publication of point clouds.
 */
void CoREstimateWithMarkers_NM(std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerData, 
Eigen::Vector3f & m,
ros::Time calib_time,
globalPublisher globalPublish )
{
  std::map<int,Eigen::Vector3d> combinateDisplayPC;
  std::map<int,Eigen::Vector3d> centeringCombinateDisplayPC;
  std::map<int,Eigen::Vector3d> originDisplayPC;

  pcl::PointCloud<pcl::PointXYZ>::Ptr originCloud(new pcl::PointCloud<pcl::PointXYZ>);

  int i = 0;
  static const int nPoints = bodyMarkerData[0].size();

  static const Eigen::Index nP = nPoints;

  Eigen::MatrixXd D(nPoints*bodyMarkerData.size(), nPoints+4);
  D.setZero();

  //std::cout << "ready " << std::endl;

  for (std::map<std::string,Eigen::Vector3d>::iterator it = bodyMarkerData[0].begin(); it!= bodyMarkerData[0].end(); it++)
  {
    Eigen::Vector3d osimVec = it->second;
    pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
    originCloud->points.push_back(osimPt);

    int rw = i*bodyMarkerData.size();

    double w = std::pow(osimVec[0],2)+ std::pow(osimVec[1],2)+ std::pow(osimVec[2],2);
    
    D(rw,0) = w;
    D(rw,1) = osimVec[0];
    D(rw,2) = osimVec[1];
    D(rw,3) = osimVec[2];
    
    //D.block(0+i*bodyMarkerData.size(),4+i, nPoints, 1) = V;
    for (int f = 0; f < bodyMarkerData.size(); f++)
    {
      D(f+rw, 4+i) = 1;
    }

    //D.block<nP,1>(0+i*bodyMarkerData.size(),4+i) = V;

    combinateDisplayPC[i] = osimVec;
    centeringCombinateDisplayPC[i] = osimVec;
    originDisplayPC[i] = osimVec;
    i++;
  }

  pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 

  Eigen::Vector4f centroidOrigin;
  Eigen::Vector4f centroidPC;
  

  pcl::compute3DCentroid( (*originCloud),centroidOrigin);
  Eigen::Vector4f lastcentroidPC = centroidOrigin;

  Eigen::MatrixXd C(4+nPoints,4+nPoints);

  Eigen::Matrix4f transf;
    Eigen::Matrix4f transf2;


  C.setZero();
  C(1,1) = nPoints;
  C(2,2) = nPoints;
  C(3,3) = nPoints;
  for (int l = 4; l < 4+nPoints; l++)
  {
    C(0,l) = -2;
    C(l,0) = -2;
  }

  pcl::PointCloud<pcl::PointXYZ>::Ptr lastPC(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr lastOriPC(new pcl::PointCloud<pcl::PointXYZ>);
  lastPC = originCloud;
  lastOriPC = originCloud;

  Eigen::Vector4f rotPoint;
  rotPoint.setOnes();

  Eigen::Vector4f checkPt;
  checkPt.setOnes();

  for (int j = 1; j < bodyMarkerData.size(); j++)
  {
    Eigen::Vector3d cons_pt;
    int i2 = i;
    // express markers at time j into PointCloud PC
    pcl::PointCloud<pcl::PointXYZ>::Ptr PC(new pcl::PointCloud<pcl::PointXYZ>);
    for (std::map<std::string,Eigen::Vector3d>::iterator it = bodyMarkerData[j].begin(); it!= bodyMarkerData[j].end(); it++)
    {

      Eigen::Vector3d osimVec = it->second;
      pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
      if (i2 == i)
      {
        cons_pt = osimVec;
      }
      PC->points.push_back(osimPt);
      combinateDisplayPC[i2] = osimVec;
      i2++;
    }



    pcl::compute3DCentroid( (*PC),centroidPC);

    // std::cout << "oir : " << std::endl <<  centroidOrigin << std::endl;
    // std::cout << "curr : "  << std::endl << centroidPC << std::endl;

    svd.estimateRigidTransformation(*lastPC,*PC,transf);
    svd.estimateRigidTransformation(*originCloud,*PC,transf2);



    if ( rotPoint.isApprox(Eigen::Vector4f::Ones() ) )
    {
      //std::cout << "matrix to be inversed : "  << Eigen::Matrix3f::Identity() -transf.block<3,3>(0,0)  << std::endl;
      rotPoint.block<3,1>(0,0) = (Eigen::Matrix3f::Identity() -transf.block<3,3>(0,0) ).inverse()*transf.block<3,1>(0,3);
    }


    // std::cout << "transfo 0 -> t : " << std::endl;
    //  Eigen::Vector3f tst = transf2.block<3,1>(0,3);
    // // std::cout << "translation : " << tst << std::endl;
    // Eigen::Vector3f tst2 = (Eigen::Matrix3f::Identity() -transf2.block<3,3>(0,0) ).inverse()*tst;
    //  std::cout << "identical point : " << tst2 << std::endl;

    // std::cout << "transfo t -> t+1: " << std::endl;
    // Eigen::Vector3f tst1 = transf.block<3,1>(0,3);
    // Eigen::Vector3f tst22 = (Eigen::Matrix3f::Identity() -transf.block<3,3>(0,0) ).inverse()*tst1;
    // std::cout << "identical point : " << tst22 << std::endl;

    // std::cout << "should be : "  << j*M_PI/50 << std::endl;

    // Eigen::Vector4f newCentroidOri = transf2.inverse()*centroidPC;
    // std::cout << "diff centroid : " << centroidOrigin - newCentroidOri << std::endl;

    
    
    // std::cout << "rotation : " << transf.block<3,3>(0,0).eulerAngles(2,0,1)[2] << std::endl;
    // std::cout << "(should be : " << (j)*M_PI/50 << std::endl;

    Eigen::Matrix3f rot_t_tp1 = transf.block<3,3>(0,0);

    Eigen::AngleAxisf rollAngle(0.0, Eigen::Vector3f::UnitX());
    Eigen::AngleAxisf pitchAngle( M_PI/50, Eigen::Vector3f::UnitY());
    Eigen::AngleAxisf yawAngle(0.0, Eigen::Vector3f::UnitZ());
    Eigen::Quaternion<float> q = rollAngle * pitchAngle * yawAngle;

    Eigen::Matrix3f trueRotationMatrix = q.matrix();




    Eigen::Matrix4f curr_transf = Eigen::Matrix4f::Identity();
    
    curr_transf.block<3,3>(0,0) = rot_t_tp1;

    //curr_transf.block<3,3>(0,0) =trueRotationMatrix;

    Eigen::Vector4f trueCoR; trueCoR.setOnes();


   // Eigen::Vector4f trans_on_ori = trueCoR - curr_transf*trueCoR;
    
    Eigen::Vector4f trans_on_ori = rotPoint - curr_transf*rotPoint;


    curr_transf.block<4,1>(0,3) = trans_on_ori;  
    curr_transf(3,3) = 1;
    //curr_transf = transf2;

    // curr_transf.block<3,3>(0,0) = transf.block<3,3>(0,0).inverse();
    // //curr_transf.block<4,1>(0,3) = transf.block<4,1>(0,3);    
    // curr_transf.block<4,1>(0,3) = -(lastcentroidPC - centroidPC);
    // //curr_transf(0,3) = -j*M_PI/50;
    // curr_transf(3,3) = 1;

    pcl::PointCloud<pcl::PointXYZ>::Ptr PC2(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr OPC2(new pcl::PointCloud<pcl::PointXYZ>);
                
    //pcl::transformPointCloud(*PC,*PC2, curr_transf);



    for (int k = 0; k < PC->points.size(); k++)
    {
      pcl::PointXYZ ptk( PC->points[k].x - originCloud->points[k].x,
                        PC->points[k].y - originCloud->points[k].y,
                        PC->points[k].z - originCloud->points[k].z);
      PC2->points.push_back(ptk);
    }


    //pcl::transformPointCloud(*lastOriPC,*PC2, curr_transf);
    //pcl::transformPointCloud(*lastOriPC,*PC2, transf);
    // pcl::compute3DCentroid( (*PC2),centroidPC2);

    // std::cout << "difference : " << (centroidOrigin - centroidPC2) << std::endl;

    for (int l = 0; l < PC2->points.size(); l++)
    {
      
      pcl::PointXYZ osimPt = PC->points[l]; // PC2->points[l]
      Eigen::Vector3d osimVec(osimPt.x, osimPt.y, osimPt.z);

      double w = std::pow(osimVec[0],2)+ std::pow(osimVec[1],2)+ std::pow(osimVec[2],2);
      
      D(j+l*bodyMarkerData.size(),0) = w;
      D(j+l*bodyMarkerData.size(),1) = osimVec[0];
      D(j+l*bodyMarkerData.size(),2) = osimVec[1];
      D(j+l*bodyMarkerData.size(),3) = osimVec[2];

      // display purpose
      osimVec[0] = PC2->points[l].x;
      osimVec[1] = PC2->points[l].y;
      osimVec[2] = PC2->points[l].z;
      centeringCombinateDisplayPC[i] = osimVec;
      i++;      

    }

    lastPC = PC;
    lastOriPC = PC2;
    lastcentroidPC = centroidPC;

  }


  globalPublish.markerPublisher(calib_time, combinateDisplayPC, globalPublish.optitrack_publisher);

  globalPublish.markerPublisher(calib_time, originDisplayPC, globalPublish.opensim_publisher);

  globalPublish.markerPublisher(calib_time, centeringCombinateDisplayPC, globalPublish.opensim_theorical_publisher);


  Eigen::MatrixXd S(4+nPoints,4+nPoints);

  S = D.transpose() * D;

  // please check https://eigen.tuxfamily.org/dox/classEigen_1_1GeneralizedEigenSolver.html
  Eigen::GeneralizedEigenSolver<Eigen::MatrixXd> es(S,C);

  std::cout << "All eigenvalues are : " << es.eigenvalues() << std::endl;
  //std::cout << "All eigenvectors are : " << std::endl << es.eigenvectors() << std::endl;

  //double bestEigenValue = es.alphas()[0]/es.betas()[0];

  int l = 0;
  double bestEigenValue;
  double min = INFINITY;
  double check = 1;
  double eps = 0.1;

  Eigen::VectorXd bestEigenVector(nPoints+4);


  while (l < 4+nPoints)
  {
    if ( real(es.eigenvalues()[l]) != 0 )
    {
      double lambda =  real(es.eigenvalues()[l]);
      Eigen::VectorXd u(4+nPoints);
      for (int k = 0; k < nPoints+4; k++)
      {
        u[k] = real(es.eigenvectors()(k,l) );
      }
      double uSu =  u.transpose() * S * u ;
      double uCu =  u.transpose() * C * u ;

      if (uSu < min)
      {
        std::cout << "lambda accepted : " << lambda << std::endl;
        min = uSu;
        bestEigenVector = u;
        bestEigenValue = lambda;
      }
      std::cout << "uCu : " << uCu << std::endl;


    }
    l++;

  }

  if (!isinf(min))
  {
    std::cout << "Best Eigen Value is : " << bestEigenValue << std::endl;

    m[0] = -bestEigenVector[1] / (2*bestEigenVector[0]) ;
    m[1] = -bestEigenVector[2]/(2*bestEigenVector[0]);
    m[2] = -bestEigenVector[3]/(2*bestEigenVector[0]);

    std::cout << "For NM, m is then : " << m << std::endl;

    std::map<int,Eigen::Vector3d> CoR;
    Eigen::Vector3d md(m[0],m[1],m[2]);
    CoR[0] = md;

    globalPublish.markerPublisher(calib_time, CoR, globalPublish.opensim_theorical_publisher);

  }
  else
  {
    std::cout << "ERROR : NO SOLUTION FOUND" << std::endl;
  }


  std::string bla12;
  std::printf("Debug \n");
  std::cin >> bla12;



}



/**
 * @brief Find the center of rotation with Gamage method (from New least squares solutions for estimating the average centre of
 * rotation and the axis of rotation). For now, points needs to have no translation
 * movement for this method to work. 
 * NB : The current implementation of NM is (for unknown reason) not the same as described in the paper; from tests, it is impossible
 * to get a good result with the condition u.T*C*u = 1. 
 * This is why this condition is not respected in this implementation (the only condition is to minimize the cost function u.T*S*u).
 * 
 * @param bodyMarkerData Vector containing, for each time, the position and identifiant of Optitrack markers of a specific body part.
 * @param m Position of the center of rotation according to the referential where the markers of the body part are expressed.
 * @param calib_time  Timestamp of the selected optitrack frame for calibration.
 * @param globalPublish Global object managing the publication of point clouds.
 */
void CoREstimateWithMarkers_Gam(std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerData, 
Eigen::Vector3f & m,
ros::Time calib_time,
globalPublisher globalPublish )
{
  std::map<int,Eigen::Vector3d> combinateDisplayPC;
  std::map<int,Eigen::Vector3d> centeringCombinateDisplayPC;
  std::map<int,Eigen::Vector3d> originDisplayPC;

  pcl::PointCloud<pcl::PointXYZ>::Ptr originCloud(new pcl::PointCloud<pcl::PointXYZ>);

  int i = 0;
  int nPoints = bodyMarkerData[0].size();


  std::vector<Eigen::Vector3d> all_vpb;
  std::vector<double> all_vpb2;
  std::vector<Eigen::Vector3d> all_vpb3;

  std::vector<Eigen::Matrix3d> all_vpvpT;



  for (std::map<std::string,Eigen::Vector3d>::iterator it = bodyMarkerData[0].begin(); it!= bodyMarkerData[0].end(); it++)
  {
    Eigen::Vector3d osimVec = it->second;
    pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
    originCloud->points.push_back(osimPt);

    combinateDisplayPC[i] = osimVec;
    centeringCombinateDisplayPC[i] = osimVec;
    originDisplayPC[i] = osimVec;

    all_vpb.push_back(osimVec);

    double vpb2 = osimVec.transpose()*osimVec;

    all_vpb2.push_back(vpb2);

    all_vpb3.push_back(vpb2*osimVec);

    all_vpvpT.push_back( osimVec*osimVec.transpose() );

    i++;
  }

  pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 

  Eigen::Vector4f centroidOrigin;
  Eigen::Vector4f centroidPC;
  
  pcl::compute3DCentroid( (*originCloud),centroidOrigin);
  Eigen::Vector4f lastcentroidPC = centroidOrigin;

  Eigen::Matrix4f transf;
  Eigen::Matrix4f transf2;

  pcl::PointCloud<pcl::PointXYZ>::Ptr lastPC(new pcl::PointCloud<pcl::PointXYZ>);
  pcl::PointCloud<pcl::PointXYZ>::Ptr lastOriPC(new pcl::PointCloud<pcl::PointXYZ>);
  lastPC = originCloud;
  lastOriPC = originCloud;

  Eigen::Vector4f rotPoint;
  rotPoint.setOnes();

  Eigen::Vector4f checkPt;
  checkPt.setOnes();

  for (int j = 1; j < bodyMarkerData.size(); j++)
  {
    Eigen::Vector3d cons_pt;
    int i2 = i;
    // express markers at time j into PointCloud PC
    pcl::PointCloud<pcl::PointXYZ>::Ptr PC(new pcl::PointCloud<pcl::PointXYZ>);
    for (std::map<std::string,Eigen::Vector3d>::iterator it = bodyMarkerData[j].begin(); it!= bodyMarkerData[j].end(); it++)
    {

      Eigen::Vector3d osimVec = it->second;
      pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
      if (i2 == i)
      {
        cons_pt = osimVec;
      }
      PC->points.push_back(osimPt);
      combinateDisplayPC[i2] = osimVec;
      i2++;
    }



    pcl::compute3DCentroid( (*PC),centroidPC);

    // std::cout << "oir : " << std::endl <<  centroidOrigin << std::endl;
    // std::cout << "curr : "  << std::endl << centroidPC << std::endl;

    svd.estimateRigidTransformation(*lastPC,*PC,transf);
    svd.estimateRigidTransformation(*originCloud,*PC,transf2);


    if ( rotPoint.isApprox(Eigen::Vector4f::Ones() ) )
    {
      //std::cout << "matrix to be inversed : "  << Eigen::Matrix3f::Identity() -transf.block<3,3>(0,0)  << std::endl;
      rotPoint.block<3,1>(0,0) = (Eigen::Matrix3f::Identity() -transf.block<3,3>(0,0) ).inverse()*transf.block<3,1>(0,3);
    }


    // std::cout << "transfo 0 -> t : " << std::endl;
    //  Eigen::Vector3f tst = transf2.block<3,1>(0,3);
    // // std::cout << "translation : " << tst << std::endl;
    // Eigen::Vector3f tst2 = (Eigen::Matrix3f::Identity() -transf2.block<3,3>(0,0) ).inverse()*tst;
    //  std::cout << "identical point : " << tst2 << std::endl;

    // std::cout << "transfo t -> t+1: " << std::endl;
    // Eigen::Vector3f tst1 = transf.block<3,1>(0,3);
    // Eigen::Vector3f tst22 = (Eigen::Matrix3f::Identity() -transf.block<3,3>(0,0) ).inverse()*tst1;
    // std::cout << "identical point : " << tst22 << std::endl;

    // std::cout << "should be : "  << j*M_PI/50 << std::endl;

    // Eigen::Vector4f newCentroidOri = transf2.inverse()*centroidPC;
    // std::cout << "diff centroid : " << centroidOrigin - newCentroidOri << std::endl;

    
    
    // std::cout << "rotation : " << transf.block<3,3>(0,0).eulerAngles(2,0,1)[2] << std::endl;
    // std::cout << "(should be : " << (j)*M_PI/50 << std::endl;

    Eigen::Matrix3f rot_t_tp1 = transf.block<3,3>(0,0);

    Eigen::AngleAxisf rollAngle(0.0, Eigen::Vector3f::UnitX());
    Eigen::AngleAxisf pitchAngle( M_PI/50, Eigen::Vector3f::UnitY());
    Eigen::AngleAxisf yawAngle(0.0, Eigen::Vector3f::UnitZ());
    Eigen::Quaternion<float> q = rollAngle * pitchAngle * yawAngle;

    Eigen::Matrix3f trueRotationMatrix = q.matrix();


    Eigen::Matrix4f curr_transf = Eigen::Matrix4f::Identity();
    
    curr_transf.block<3,3>(0,0) = rot_t_tp1;

    //curr_transf.block<3,3>(0,0) =trueRotationMatrix;

    Eigen::Vector4f trueCoR; trueCoR.setOnes();


   // Eigen::Vector4f trans_on_ori = trueCoR - curr_transf*trueCoR;
    
    Eigen::Vector4f trans_on_ori = rotPoint - curr_transf*rotPoint;


    curr_transf.block<4,1>(0,3) = trans_on_ori;  
    curr_transf(3,3) = 1;
    //curr_transf = transf2;

    // curr_transf.block<3,3>(0,0) = transf.block<3,3>(0,0).inverse();
    // //curr_transf.block<4,1>(0,3) = transf.block<4,1>(0,3);    
    // curr_transf.block<4,1>(0,3) = -(lastcentroidPC - centroidPC);
    // //curr_transf(0,3) = -j*M_PI/50;
    // curr_transf(3,3) = 1;

    pcl::PointCloud<pcl::PointXYZ>::Ptr PC2(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr OPC2(new pcl::PointCloud<pcl::PointXYZ>);
                
    //pcl::transformPointCloud(*PC,*PC2, curr_transf);

    for (int k = 0; k < PC->points.size(); k++)
    {
      pcl::PointXYZ ptk( PC->points[k].x - originCloud->points[k].x,
                        PC->points[k].y - originCloud->points[k].y,
                        PC->points[k].z - originCloud->points[k].z);
      PC2->points.push_back(ptk);
    }

    //pcl::transformPointCloud(*lastOriPC,*PC2, curr_transf);
    //pcl::transformPointCloud(*lastOriPC,*PC2, transf);
    // pcl::compute3DCentroid( (*PC2),centroidPC2);

    // std::cout << "difference : " << (centroidOrigin - centroidPC2) << std::endl;

    for (int l = 0; l < PC2->points.size(); l++)
    {
      pcl::PointXYZ osimPt = PC->points[l]; // PC2->points[l]
      Eigen::Vector3d osimVec(osimPt.x, osimPt.y, osimPt.z);

      all_vpb[l] += osimVec;

      double vpb2 = osimVec.transpose() * osimVec;

      all_vpb2[l] += (vpb2);

      all_vpb3[l] += (vpb2*osimVec);
      
      all_vpvpT[l] +=  osimVec*osimVec.transpose() ;

      // display purpose
      osimVec[0] = PC2->points[l].x;
      osimVec[1] = PC2->points[l].y;
      osimVec[2] = PC2->points[l].z;
      centeringCombinateDisplayPC[i] = osimVec;
      i++;      

    }

    lastPC = PC;
    lastOriPC = PC2;
    lastcentroidPC = centroidPC;

  }

  globalPublish.markerPublisher(calib_time, combinateDisplayPC, globalPublish.optitrack_publisher);

  globalPublish.markerPublisher(calib_time, originDisplayPC, globalPublish.opensim_publisher);

  globalPublish.markerPublisher(calib_time, centeringCombinateDisplayPC, globalPublish.opensim_theorical_publisher);

  Eigen::Vector3d b; b.setZero();

  Eigen::Matrix3d A; A.setZero();

  for (int p = 0; p < nPoints; p++)
  {
    all_vpb[p] = all_vpb[p]/bodyMarkerData.size();
    all_vpb2[p] = all_vpb2[p]/bodyMarkerData.size();
    all_vpb3[p] = all_vpb3[p]/bodyMarkerData.size();
    all_vpvpT[p] = all_vpvpT[p]/bodyMarkerData.size();

    b += all_vpb3[p] - all_vpb[p]*all_vpb2[p];
    A += all_vpvpT[p] - all_vpb[p]*all_vpb[p].transpose();

  }
  A = 2*A;

  Eigen::Vector3d md = A.inverse()*b;

  m[0] = md[0];
  m[1] = md[1];
  m[2] = md[2];

  std::cout << "For Gamage, m is then : " << m << std::endl;

  std::map<int,Eigen::Vector3d> CoR;
  CoR[0] = md;

  globalPublish.markerPublisher(calib_time, CoR, globalPublish.opensim_theorical_publisher);



  std::string bla12;
  std::printf("Debug \n");
  std::cin >> bla12;
}

void recoveryMarkersProcedure(std::map<std::string, Eigen::Vector3d> & opensimMissingMarkers,
std::map<std::string,Eigen::Vector3d> originOpensimMarkers,
globalPublisher globalPublish,
ros::Time calib_time,
bool verbose)
{
  std::vector<std::map<std::string, Eigen::Vector3d> > vect;
  vect.push_back(opensimMissingMarkers);
  recoveryMarkersProcedure(vect,originOpensimMarkers,globalPublish,calib_time);
  opensimMissingMarkers = vect[0];
}

/**
 * @brief Idea : case where the attributed Opensim marker set has at least 3 markers. 
 * The goal of this method is to estimate the position of the other markers from those 3 markers.
 * 
 * From an reference pointcloud configuration, it could then be possible to estimate the transform
 * from the reference to the attributed Opensim marker set; the missing Opensim markers will then correspond
 * to the transformed markers of the reference configuration.
 * 
 */
void recoveryMarkersProcedure(std::vector<std::map<std::string, Eigen::Vector3d> > & opensimMissingMarkers,
std::map<std::string,Eigen::Vector3d> originOpensimMarkers,
globalPublisher globalPublish,
ros::Time calib_time,
bool verbose)
{

  std::vector<std::string> opensimMarkersNames;

  pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 

  for (std::map<std::string,Eigen::Vector3d>::iterator it = originOpensimMarkers.begin(); it!= originOpensimMarkers.end(); it++)
  {
    opensimMarkersNames.push_back(it->first);
  }

  std::map<int,Eigen::Vector3d> posOri;
  std::map<int,Eigen::Vector3d> posBef;
  std::map<int,Eigen::Vector3d> posAft;

  Eigen::Vector3d osimVecOri;
  Eigen::Vector3d osimVec;

  for (int i= 0; i < opensimMissingMarkers.size(); i++)  
  {

    if ( opensimMissingMarkers[i].size() != originOpensimMarkers.size() )
    {
      
      posOri.clear();
      posBef.clear();
      posAft.clear();

      pcl::PointCloud<pcl::PointXYZ>::Ptr sourceCloud(new pcl::PointCloud<pcl::PointXYZ>);
      pcl::PointCloud<pcl::PointXYZ>::Ptr sourceCloudTotal(new pcl::PointCloud<pcl::PointXYZ>);
      pcl::PointCloud<pcl::PointXYZ>::Ptr sourceCloud2Target(new pcl::PointCloud<pcl::PointXYZ>);
      pcl::PointCloud<pcl::PointXYZ>::Ptr targetCloud(new pcl::PointCloud<pcl::PointXYZ>);
      Eigen::Matrix4f transf;
      std::vector<std::string> missMarkName;
      std::map<std::string,Eigen::Vector3d> treatedOpensimMarkers = opensimMissingMarkers[i]; 
      int ind = 0;

      for (int j = 0; j < opensimMarkersNames.size(); j++)
      {
        std::string markerName = opensimMarkersNames[j];
        if (treatedOpensimMarkers.find(markerName) != treatedOpensimMarkers.end())
        {
          osimVecOri = originOpensimMarkers[markerName];
          pcl::PointXYZ osimPtOri(osimVecOri[0],osimVecOri[1], osimVecOri[2]);
          sourceCloud->points.push_back(osimPtOri);
          sourceCloudTotal->points.push_back(osimPtOri);

          osimVec = treatedOpensimMarkers[markerName];
          pcl::PointXYZ osimPt(osimVec[0],osimVec[1], osimVec[2]);
          targetCloud->points.push_back(osimPt);

          posOri[ind] = osimVecOri;
          posBef[ind] = osimVec;
          posAft[ind] = osimVec;

          ind++;
        }
        else
        {
          missMarkName.push_back(markerName);
        }

      }

      int ind2 = ind;

      for (int j = 0; j < missMarkName.size(); j++)
      {
          osimVecOri = originOpensimMarkers[missMarkName[j]];
          pcl::PointXYZ osimPtOri(osimVecOri[0],osimVecOri[1], osimVecOri[2]);
          sourceCloudTotal->points.push_back(osimPtOri);

          posOri[ind2] = osimVecOri;
          ind2++;
      }
      svd.estimateRigidTransformation(*sourceCloud,*targetCloud,transf);
      
      pcl::transformPointCloud(*sourceCloudTotal,*sourceCloud2Target, transf);

      int beg = targetCloud->points.size();

      for (int j = beg; j < sourceCloud2Target->points.size(); j++)
      {
        pcl::PointXYZ osimPt = sourceCloud2Target->points[j];
        osimVec[0] = osimPt.x;
        osimVec[1] = osimPt.y;
        osimVec[2] = osimPt.z;
        //std::cout << "new pos for  : " << missMarkName[j-beg] << " : " << osimVec << std::endl;
        opensimMissingMarkers[i][missMarkName[j-beg]] = osimVec;
        posAft[ind] = osimVec;
        ind++;

      }
      if (verbose)
      {
        globalPublish.markerPublisher(calib_time,posOri, globalPublish.opensim_publisher);
        globalPublish.markerPublisher(calib_time,posBef, globalPublish.optitrack_publisher);
        globalPublish.markerPublisher(calib_time,posAft, globalPublish.opensim_theorical_publisher);
      }

    }

  }

}



// changer methode : 
// determiner uniquement CoR pr pose de calibration
// ensuite, chercher a replacer marqueurs. 

// idee : 
// * partir d'extremite, pour aller vers base
// * pr une partie de corps ac 1 extremite (ex : radius ) : 
//      - pos marqueurs ds ref local def en fonction de pos de 1e CoR
// * pr partie crps ac 2 extremites (ex : humerus) : 
//      - tester placement suivant : 
//          : rotation determinee comme d'hab
//          : tracer segment des 2 CoR ds theorie / realite
//          : prendre milieu, faire en sorte que milieu confondu entre theorie / realite
//     - sinon : comme d'hab avec NM / Gam
//



/**
 * @brief Taken from https://gamedev.stackexchange.com/questions/28395/rotating-vector3-by-a-quaternion
 * 
 * @param v 
 * @param q 
 * @param vp 
 */
void rotate_vector_by_quaternion(Eigen::Vector3f v, Eigen::Quaternion<float> q, Eigen::Vector3f & vp)
{
    // Extract the vector part of the quaternion
    Eigen::Vector3f u(q.x(), q.y(), q.z() );

    // Extract the scalar part of the quaternion
    float s = q.w();

    // Do the math
    vp = 2.0f * u.dot(v) * u
          + (s*s - u.dot(u) ) * v
          + 2.0f * s * u.cross(v);
}


void getTransformationMatrixOfPointCloudByPCA(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
Eigen::Matrix4f & transformation)
{
  Eigen::Vector4f translation;
  pcl::compute3DCentroid(*cloud,translation);
  getTransformationMatrixOfPointCloudByPCA(cloud, transformation, translation); 
}

/**
 * @brief Get the Transformation Matrix of a pointCloud (= orientation and translation according to ground) of a
 * PointCloud by PCA method.
 * 
 * @param Cloud 
 * @param transformation 
 */
void getTransformationMatrixOfPointCloudByPCA(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
Eigen::Matrix4f & transformation,
Eigen::Vector4f translation)
{
  // for (int i = 0; i < cloud->points.size(); i++)
  // {
  //   std::printf("Origin position : %3.4f, %3.4f, %3.4f. \n", cloud->points[i].x,cloud->points[i].y,cloud->points[i].z);
  // }

  transformation.setIdentity();
  transformation.block<4,1>(0,3) = translation;
  pcl::PCA<pcl::PointXYZ> pcaX;
  pcaX.setInputCloud(cloud);
  Eigen::Matrix3f XEVs_Dir = pcaX.getEigenVectors();

  // std::cout << "---" << std::endl;
  // std::cout << "Eigen Vectors : " << std::endl;
  // std::cout << XEVs_Dir << std::endl;
  // std::cout << XEVs_Dir.block<3,1>(0,0).cross(XEVs_Dir.block<3,1>(0,1)) << std::endl;
  // std::cout << std::endl;
  // std::cout << XEVs_Dir.block<3,1>(0,1).cross(XEVs_Dir.block<3,1>(0,2)) << std::endl;
  // std::cout << std::endl;
  // std::cout << XEVs_Dir.block<3,1>(0,2).cross(XEVs_Dir.block<3,1>(0,0)) << std::endl;
  // std::cout << "---" << std::endl;

  // get orthonormal matrix using Gram-Schimdt

  Eigen::Vector3f ax1 = XEVs_Dir.block<3,1>(0,0);
  ax1.normalize();
  
  Eigen::Vector3f ev2 = XEVs_Dir.block<3,1>(0,1);
  ev2.normalize();

  Eigen::Vector3f ax2 = ev2 - (ax1.transpose() * ev2) * ax1 / (ax1.transpose()* ax1);
  ax2.normalize();

  Eigen::Vector3f ax3 = ax1.cross(ax2);

  transformation.block<3,1>(0,0) = ax1;
  transformation.block<3,1>(0,1) = ax2;
  transformation.block<3,1>(0,2) = ax3;

  // NB : all vectors in transformation are orthogonal to each of them.
  // std::cout << "---" << std::endl;
  // std::cout << "Referential computed : " << std::endl;
  // std::cout << transformation << std::endl;
  // // std::cout << ax1.dot(ax2) << std::endl;
  // // std::cout << ax2.dot(ax3) << std::endl;
  // // std::cout << ax1.dot(ax3) << std::endl;
  // std::cout << "---" << std::endl;



}


/**
 * @brief Goal : (originaly, get positions of each markers accordingly to CoR. However, no idea on how to do it).
 * Now, goal (for each body part) :
 * - Get position of centroid of points, expressed into body part referential.
 * - Get position of centroid of points, expressed into global referential, for one position at time t.
 * - For this position at time t, express position of CoR. 
 * 
 * Replacement on local ref : 
 * - Replace such as position centroid in ground referential = position centroid in local body,
 * + orientation of body in local = orientation of body in ground referential. 
 *
 * NB : centroid put into local Placement as "centroid"
 * 
 * @param bodyMarkerDataX First body. Normally, parent.
 * @param bodyMarkerDataY Second body. Normally, child.
 * @param m 
 * @param calib_time 
 * @param globalPublish 
 */
void CoREstimateWithMarkers_DoublePart(std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerDataX, 
std::vector< std::map<std::string,Eigen::Vector3d> > bodyMarkerDataY,
std::map<std::string,Eigen::Vector3f> & localPlacementX,
std::map<std::string,Eigen::Vector3f> & localPlacementY,
ros::Time calib_time,
globalPublisher globalPublish )
{
  std::map<int,Eigen::Vector3d> combinateDisplayPC;
  std::map<int,Eigen::Vector3d> centeringCombinateDisplayPC;
  std::map<int,Eigen::Vector3d> originDisplayPC;

  pcl::PointCloud<pcl::PointXYZ>::Ptr originCloud(new pcl::PointCloud<pcl::PointXYZ>);

  int m = std::min(bodyMarkerDataX.size(), bodyMarkerDataY.size() );

  Eigen::VectorXf Xi(3*m, 1);
  Eigen::VectorXf Yj(3*m, 1);

  std::vector<Eigen::VectorXf> allX;
  std::vector<Eigen::VectorXf> allY;

  for (std::map<std::string,Eigen::Vector3d>::iterator it = bodyMarkerDataX[0].begin(); it!=bodyMarkerDataX[0].end(); it++)
  {
    allX.push_back(Xi);
  }
  for (std::map<std::string,Eigen::Vector3d>::iterator it = bodyMarkerDataY[0].begin(); it!=bodyMarkerDataY[0].end(); it++)
  {
    allY.push_back(Yj);
  }

  std::vector<Eigen::Quaternion<float> > qRx;
  std::vector<Eigen::Quaternion<float> > qRy;
  std::vector<Eigen::Quaternion<float> > qTRx;
  std::vector<Eigen::Quaternion<float> > qTRy;

  Eigen::MatrixXf Rx(3*m, 3);
  Eigen::MatrixXf Ry(3*m, 3);

  Eigen::MatrixXf A(3*m,6);

  for (int i= 0; i < m; i++)
  {

    std::map<std::string,Eigen::Vector3d> frameX = bodyMarkerDataX[i];
    std::map<std::string,Eigen::Vector3d> frameY = bodyMarkerDataY[i];

    pcl::PointCloud<pcl::PointXYZ>::Ptr XCloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr YCloud(new pcl::PointCloud<pcl::PointXYZ>);
    int l =0;

    for (std::map<std::string,Eigen::Vector3d>::iterator it = frameX.begin(); it!=frameX.end(); it++)
    {
      Eigen::Vector3f vecX(it->second[0],it->second[1], it->second[2]);
      allX[l].block<3,1>(i*3,0) = vecX;

      pcl::PointXYZ ptX(it->second[0],it->second[1], it->second[2]);
      XCloud->points.push_back(ptX);
      l++;
    }

    l = 0;
    for (std::map<std::string,Eigen::Vector3d>::iterator it = frameY.begin(); it!=frameY.end(); it++)
    {
      Eigen::Vector3f vecY(it->second[0],it->second[1], it->second[2]);
      allY[l].block<3,1>(i*3,0) = vecY;      
      pcl::PointXYZ ptY(it->second[0],it->second[1], it->second[2]);
      YCloud->points.push_back(ptY);
      l++;
    }    

    Eigen::Matrix4f xTrans;
    getTransformationMatrixOfPointCloudByPCA(XCloud,xTrans);

    Eigen::Matrix3f RFx = xTrans.block<3,3>(0,0);

    A.block<3,3>(3*i,0) = RFx;
    Eigen::Quaternion<float> q(RFx);
    q.normalize();
    qRx.push_back(q);

    Eigen::Matrix4f yTrans;
    getTransformationMatrixOfPointCloudByPCA(YCloud,yTrans);

    Eigen::Matrix3f RFy = yTrans.block<3,3>(0,0);

    A.block<3,3>(3*i,3) = -RFy;
    Eigen::Quaternion<float> q2(RFy);
    q2.normalize();
    qRy.push_back(q2);


  }

  // first, get means

  Eigen::VectorXf means(6,1);

  Eigen::VectorXf B(3*m, 1);
  B.setZero();
  for (int i = 0; i < allX.size(); i++)
  {
    for (int j = 0; j < allY.size(); j++)
    {
      B += allX[i] - allY[j];
    }

  }
  B = B/(allX.size()*allY.size());

  means = ( (A.transpose()*A).inverse() )*(A.transpose() )*B;

  Eigen::Vector3f localCentroidX = means.block<3,1>(0,0);
  Eigen::Vector3f localCentroidY = means.block<3,1>(3,0);

  localPlacementX["centroid"]= localCentroidX;
  localPlacementY["centroid"] = localCentroidY;

  std::map<std::string,Eigen::Vector3d>::iterator it;

  for (int i = 0; i < allX.size(); i++)
  {
    it = bodyMarkerDataX[0].begin();
    std::advance(it,i);
    std::string markX = it->first;

    Xi = allX[i];


    Eigen::Vector3f bpart;

    Eigen::Vector3f xMeanPart;
    xMeanPart.setZero();

    bpart.setZero();

    for (int k = 0; k < m ; k++)
    {
      Eigen::Vector3f initb; initb.setZero();
      Eigen::Vector3f Xik = Xi.block<3,1>(k*m,0);
      for (int j = 0; j < allY.size(); j++)
      {
        Eigen::Vector3f Yjk = allY[j].block<3,1>(k*m,0);
        initb += Xik - Yjk;
      }
      initb = initb/allY.size();

      rotate_vector_by_quaternion(initb,qRx[k].conjugate(), initb);
      bpart+= initb;

      Eigen::Vector3f initMeanPart = localCentroidX;
      rotate_vector_by_quaternion(initMeanPart,qRy[k], initMeanPart);
      rotate_vector_by_quaternion(initMeanPart,qRx[k].conjugate(), initMeanPart);
      xMeanPart += initMeanPart;


    }
    Eigen::Vector3f localPos = (bpart + xMeanPart)/m;

    localPlacementX[markX] = localPos;
    
  }
  // same with body part Y

  for (int j = 0; j < allY.size(); j++)
  {
    it = bodyMarkerDataY[0].begin();
    std::advance(it,j);
    std::string markY = it->first;

    Eigen::VectorXf Yj = allY[j];

    Eigen::Vector3f bpart;

    Eigen::Vector3f yMeanPart;
    yMeanPart.setZero();

    bpart.setZero();

    for (int k = 0; k < m ; k++)
    {
      Eigen::Vector3f initb; initb.setZero();
      Eigen::Vector3f Yjk = Yj.block<3,1>(k*m,0);
      for (int i = 0; i < allX.size(); i++)
      {
        Eigen::Vector3f Xik = allX[i].block<3,1>(k*m,0);
        initb += Xik - Yjk;
      }
      initb = initb/allX.size();

      rotate_vector_by_quaternion(initb,qRy[k].conjugate(), initb);
      bpart+= -initb;

      Eigen::Vector3f initMeanPart = localCentroidY;
      rotate_vector_by_quaternion(initMeanPart,qRx[k], initMeanPart);
      rotate_vector_by_quaternion(initMeanPart,qRy[k].conjugate(), initMeanPart);
      yMeanPart += initMeanPart;
    }
    Eigen::Vector3f localPos = (bpart + yMeanPart)/m;

    localPlacementY[markY] = localPos;
    
  }


}
/**
 * @brief Based on https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
 * 
 * @param vec1 
 * @param vec2 
 * @param rot 
 */
void rotationFromVec1ToVec2(Eigen::Vector3f vec1, Eigen::Vector3f vec2, Eigen::Matrix3f & rot)
{
  Eigen::Vector3f v = vec1.cross(vec2);
  double c = vec1.dot(vec2);  
  if (v.isApprox(Eigen::Vector3f::Zero()))
  {
    // angle = 0
    rot.setIdentity();
  }
  else if (abs(c+1) < 1e-6)
  {
    // angle = PI
    rot.setIdentity();
    rot = -rot;
  }
  else
  {
    Eigen::Matrix3f ssm; ssm.setZero();
    ssm(0,1) = -v[2];
    ssm(1,0) = v[2];
    ssm(0,2) = v[1];
    ssm(2,0) = -v[1];  
    ssm(1,2) = -v[0];
    ssm(2,1) = v[0];

    // std::cout << "ssm : " << ssm << std::endl;
    // std::cout << "ssm2 : " << ssm*ssm << std::endl;
    // std::cout << "1/1+c : " << (1/(1+c) ) << std::endl;
    rot.setIdentity();
    rot = rot+ ssm + (ssm*ssm)*(1/(1+c) );

  }

}

void getBodyGuess2(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
std::vector<Eigen::Vector3d> corresPoints, 
Eigen::Matrix4f & transf,
std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> & returnPC,
std::vector< std::map<int,Eigen::Vector3d> > & returnPCSimpl,
bool multiZeroCase)
{

  double distToCentroid = 0.2;
  double K;

  getTransformationMatrixOfPointCloudByPCA(cloud, transf);

  Eigen::Vector3d axP(transf(0,0), transf(1,0), transf(2,0));


  Eigen::Vector3d centroid(transf(0,3), transf(1,3), transf(2,3));

  int num_corres = corresPoints.size();

  pcl::PointCloud<pcl::PointXYZ>::Ptr PCForGuess(new pcl::PointCloud<pcl::PointXYZ>);
  std::map<int,Eigen::Vector3d> PCSimpl;
  

  int n = 0;

  PCSimpl[n] = centroid;
  n++;
  pcl::PointXYZ pt(centroid[0], centroid[1], centroid[2]);
  PCForGuess->points.push_back(pt);
  pcl::PointXYZ ptCentroid = pt;

  // Check to be sure if there is a correspondance point Pc that has
  // its vector (from centroid to point) close to Principal Axis point.
  // If this is the case, make sure that the direction of the principal
  // axis point corresponds to the direction of the vector centroid->Pc. 

  Eigen::Vector3d closestVecFromAxp; closestVecFromAxp.setZero();
  double crossMin = 0.4;
  for (int i  = 0; i < corresPoints.size(); i++ )
  {  
    Eigen::Vector3d vec = corresPoints[i]-centroid;
    vec.normalize();
    if ( vec.cross(axP).norm() < crossMin  )
    {
      closestVecFromAxp = vec;
      crossMin = vec.cross(axP).norm();
    }
  }

  if (!closestVecFromAxp.isZero() && axP.dot(closestVecFromAxp) < 0 )
  {
    axP *=-1;
    transf.block<3,1>(0,0) *= -1;

    // TO CHECK
    transf.block<3,1>(0,2) *= -1;

    std::cout << "After changing side : " << axP << std::endl;
  }
  else if (closestVecFromAxp.isZero())
  {
    std::cout << "No pt found." << std::endl;
  }
  axP.normalize();

  K = distToCentroid/axP.norm();

  Eigen::Vector3d ptOnAxP( axP[0]*K+ centroid[0],axP[1]*K+ centroid[1],axP[2]*K+ centroid[2]  );


  PCSimpl[n] = ptOnAxP;
  n++;
  pt.x = (float) ptOnAxP[0];
  pt.y = (float) ptOnAxP[1];
  pt.z = (float) ptOnAxP[2];    
  PCForGuess->points.push_back(pt);
  pcl::PointXYZ ptAx = pt;


  



  Eigen::Vector3d ptC;

  // 
  if (num_corres ==1)
  {

    Eigen::Vector3d vec;
    Eigen::Vector3d crossVec;
    int numPC;

    if (multiZeroCase)
    {
      numPC = 2; //pow(2,2-corresPoints.size());
    }
    else
    {
      numPC = 1;
    }
    for (int j = 0; j < numPC; j++)
    {
      for (int i  = 0; i < corresPoints.size(); i++ )
      {
        PCSimpl[n] =  corresPoints[i];
        n++;
        pt.x = corresPoints[i][0];
        pt.y = corresPoints[i][1];
        pt.z = corresPoints[i][2];
        PCForGuess->points.push_back(pt);

        // cross prod axP et 0->i

        vec = corresPoints[i] - centroid;
        vec.normalize();
        crossVec = axP.cross(vec);
        // for display purpose
        K = pow(-1,j)*distToCentroid/crossVec.norm();      
        std::cout << "K : " << K << std::endl;



        ptC[0] = K*crossVec[0]+centroid[0];
        ptC[1] = K*crossVec[1]+centroid[1];
        ptC[2] = K*crossVec[2]+centroid[2];
        PCSimpl[n] =  ptC;
        n++;

        pt.x = K*crossVec[0]+centroid[0];
        pt.y = K*crossVec[1]+centroid[1];
        pt.z = K*crossVec[2]+centroid[2];

        PCForGuess->points.push_back(pt);      
      }

      pcl::PointCloud<pcl::PointXYZ>::Ptr PCForGuess2(new pcl::PointCloud<pcl::PointXYZ>);
      PCForGuess2->points = PCForGuess->points;

      returnPC.push_back(PCForGuess2);
      returnPCSimpl.push_back(PCSimpl);
    
      // reboot
      PCForGuess->points.clear();
      PCForGuess->points.push_back(ptCentroid);
      PCForGuess->points.push_back(ptAx);



      PCSimpl.clear();
      PCSimpl[0] = centroid;
      PCSimpl[1] = ptOnAxP;

      n = 2;

    }


    
  }

  else if (num_corres ==2)
  {

    Eigen::Vector3d vec;
    Eigen::Vector3d crossVec;


    for (int i  = 0; i < corresPoints.size(); i++ )
    {
      PCSimpl[n] =  corresPoints[i];
      n++;
      pt.x = corresPoints[i][0];
      pt.y = corresPoints[i][1];
      pt.z = corresPoints[i][2];
      PCForGuess->points.push_back(pt);

      // // cross prod axP et 0->i

      // vec = corresPoints[i] - centroid;
      // vec.normalize();
      // crossVec = axP.cross(vec);
      // // for display purpose
      // K = distToCentroid/crossVec.norm();      

      // ptC[0] = K*crossVec[0]+centroid[0];
      // ptC[1] = K*crossVec[1]+centroid[1];
      // ptC[2] = K*crossVec[2]+centroid[2];
      // PCSimpl[n] =  ptC;
      // n++;

      // pt.x = K*crossVec[0]+centroid[0];
      // pt.y = K*crossVec[1]+centroid[1];
      // pt.z = K*crossVec[2]+centroid[2];

      // PCForGuess->points.push_back(pt);      
    }
      // cross prod 0->1 and 0->2

    Eigen::Vector3d vec1(corresPoints[0] - centroid);
    vec1.normalize();
    Eigen::Vector3d vec2(corresPoints[1] - centroid);
    vec2.normalize();

    crossVec = vec1.cross(vec2);
    K = distToCentroid/crossVec.norm(); 
    ptC[0] = K*crossVec[0]+centroid[0];
    ptC[1] = K*crossVec[1]+centroid[1];
    ptC[2] = K*crossVec[2]+centroid[2];
    PCSimpl[n] =  ptC;
    n++;

    pt.x = K*crossVec[0]+centroid[0];
    pt.y = K*crossVec[1]+centroid[1];
    pt.z = K*crossVec[2]+centroid[2];
    PCForGuess->points.push_back(pt);     

    returnPC.push_back(PCForGuess);
    returnPCSimpl.push_back(PCSimpl);
    
  }  
  // 0 case; idea : 
  // *use PCA
  // * return Principal Axis
  // *as we don't know if vectors into PCA are correct, rotate of 90° around
  // principal axis
  // *at the end : 8 PC with 4 points each (if multiZeroCase asked)
  // otherwise, only one PC returned;
  else
  {

    int num_PC;
    int num_return_axP;


    if (multiZeroCase)
    {
      num_PC = 4;
      num_return_axP = 2;
    }
    else
    {
      num_PC = 1;
      num_return_axP = 1;
    }

    for (int l = 0; l < num_return_axP; l++)
    {

      std::cout << "coeff : " << pow(-1,l) << std::endl;
      axP *= pow(-1,l);
      transf.block<3,1>(0,0) *= pow(-1,l);

      // TO CHECK
      transf.block<3,1>(0,2) *= pow(-1,l);

      axP.normalize();

      K = distToCentroid/axP.norm();

      ptOnAxP = axP*K+centroid;
      ptAx.x = (float) ptOnAxP[0];
      ptAx.y = (float) ptOnAxP[1];
      ptAx.z = (float) ptOnAxP[2];          

      // reboot
      PCForGuess->points.clear();
      PCForGuess->points.push_back(ptCentroid);
      PCForGuess->points.push_back(ptAx);

      PCSimpl.clear();
      PCSimpl[0] = centroid;
      PCSimpl[1] = ptOnAxP;

      n = 2;      

      Eigen::Vector3d vec1( transf(0,1), transf(1,1), transf(2,1) );
      Eigen::Vector3d vec2( transf(0,2), transf(1,2), transf(2,2) );    

      for (int i = 0; i < num_PC; i++)
      {
        vec1 = vec1*pow(-1,i);
        vec1.normalize();
        K = distToCentroid/vec1.norm();    

        ptC[0] = K*vec1[0]+centroid[0];
        ptC[1] = K*vec1[1]+centroid[1];
        ptC[2] = K*vec1[2]+centroid[2];
        PCSimpl[n] =  ptC;
        n++;

        pt.x = K*vec1[0]+centroid[0];
        pt.y = K*vec1[1]+centroid[1];
        pt.z = K*vec1[2]+centroid[2];
        PCForGuess->points.push_back(pt);

        vec2 = vec2*pow(-1, (int)i/2 );
        vec2.normalize();

        K = distToCentroid/vec2.norm();   

        ptC[0] = K*vec2[0]+centroid[0];
        ptC[1] = K*vec2[1]+centroid[1];
        ptC[2] = K*vec2[2]+centroid[2];
        PCSimpl[n] =  ptC;
        n++;
  
        pt.x = K*vec2[0]+centroid[0];
        pt.y = K*vec2[1]+centroid[1];
        pt.z = K*vec2[2]+centroid[2];
        PCForGuess->points.push_back(pt);

        pcl::PointCloud<pcl::PointXYZ>::Ptr PCForGuess2(new pcl::PointCloud<pcl::PointXYZ>);
        PCForGuess2->points = PCForGuess->points;

        //std::cout << PCForGuess->points.size() << std::endl;
        returnPC.push_back(PCForGuess2);
        returnPCSimpl.push_back(PCSimpl);

        // reboot
        PCForGuess->points.clear();
        PCForGuess->points.push_back(ptCentroid);
        PCForGuess->points.push_back(ptAx);

        PCSimpl.clear();
        PCSimpl[0] = centroid;
        PCSimpl[1] = ptOnAxP;

        n = 2;

      }
    }

  }


}


/**
 * @brief Get the Body Guess object
 * NO SCALING IN THIS METHOD
 * 
 * @param cloud 
 * @param corresPoints 
 * @param transf 
 * @param returnPC 
 * @param returnPCSimpl 
 * @param multiZeroCase 
 * @param axCorres Index of concerned ax : < index of closest correspondance element in corresPoints, sign of the
 *  dot product between the ax and the vector with the closest element  >.
 */
void getBodyGuess(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
std::vector<Eigen::Vector3d> corresPoints, 
Eigen::Matrix4f & transf,
std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> & returnPC,
std::vector< std::map<int,Eigen::Vector3d> > & returnPCSimpl,
bool multiZeroCase,
std::map<int,std::vector<int> > & axCorres)
{

  bool initAxCorres = ( axCorres.size() == 0 && !multiZeroCase);

  double distToCentroid = 0.2;
  double K;

  getTransformationMatrixOfPointCloudByPCA(cloud, transf);

  Eigen::Vector3d axP(transf(0,0), transf(1,0), transf(2,0));


  Eigen::Vector3d centroid(transf(0,3), transf(1,3), transf(2,3));

  int num_corres = corresPoints.size();

  pcl::PointCloud<pcl::PointXYZ>::Ptr PCForGuess(new pcl::PointCloud<pcl::PointXYZ>);
  std::map<int,Eigen::Vector3d> PCSimpl;
  

  int n = 0;

  PCSimpl[n] = centroid;
  n++;
  pcl::PointXYZ pt(centroid[0], centroid[1], centroid[2]);
  PCForGuess->points.push_back(pt);
  pcl::PointXYZ ptCentroid = pt;

  // Check to be sure if there is a correspondance point Pc that has
  // its vector (from centroid to point) close to Principal Axis point.
  // If this is the case, make sure that the direction of the principal
  // axis point corresponds to the direction of the vector centroid->Pc.

    Eigen::Vector3d ptC;

  if (num_corres > 0)
  {

    if (initAxCorres)
    {
      // normally, Opensim case.
      // needs to determine which of the correspondences points are the closest from
      // each axes.
      double crossMin = 0.5; // 0.4
      Eigen::Vector3d indexCorres(-1,-1,-1);
      std::vector<double> currentCrossMin(3,crossMin);
      std::vector<Eigen::Vector3d> curCorresPoints = corresPoints;
      std::vector<int> curIndexes;
      for (int i = 0; i < curCorresPoints.size(); i++)
      {
        curIndexes.push_back(i);
      }

      for (int j = 0; j<3;j++)
      {  
        Eigen::Vector3d Ax(transf(0,j), transf(1,j), transf(2,j));
        //std::cout << "Axis : " << j << std::endl;

        for (int i  = 0; i < curCorresPoints.size(); i++ )
        {
          //std::cout << "Point " << i << " : " << curCorresPoints[i] << std::endl;
          Eigen::Vector3d vec = curCorresPoints[i]-centroid;
          vec.normalize();
          
          if ( vec.cross(Ax).norm() < currentCrossMin[j]  )
          {
            indexCorres(j,0) = i;
            currentCrossMin[j] = vec.cross(Ax).norm();
          }
        }
        if (indexCorres(j,0) != -1)
        {
          std::vector<int> axProps;
          int index = indexCorres(j,0);
          axProps.push_back(curIndexes[index]);

          axCorres[j] = axProps;

          curCorresPoints.erase( curCorresPoints.begin()+index  );
          curIndexes.erase(  curIndexes.begin() + index);
        }
      }

    }

    std::vector<Eigen::Matrix4f> allTransf;

    if (axCorres.size() == 1)
    {
      //std::cout << "Case one axis corres" << std::endl;
      int corIndex = axCorres.begin()->second[0];
      int ax = axCorres.begin()->first;
      Eigen::Vector3d vec = corresPoints[corIndex]-centroid;
      Eigen::Vector3d Ax(transf(0,ax), transf(1,ax), transf(2,ax));
      int signAx;
      if (initAxCorres)
      {
        // simply save the sign
        signAx = signbit(Ax.dot(vec) );
      }
      else
      {
        signAx = axCorres.begin()->second[1];
      }

      int numTries;
      if (multiZeroCase)
      {
        numTries = 2;
      }
      else
      {
        numTries = 1;
      }

      if ( signbit(Ax.dot(vec) ) != signAx )
      {
        // One correspondence (ex : x)
        // Two poss : rotate z (x,y move) and y (x,z move).
        int i = 0;
        while(numTries > 0)
        {
          if (i != ax)
          {
            Eigen::Matrix4f transf2push = transf;
            transf2push.block<3,1>(0,ax) *= -1;
            transf2push.block<3,1>(0,i) *= -1;
            allTransf.push_back(transf2push);
            numTries -= 1;
          }
          i++;
        }
      }
      else
      {
        // directly use the first computed transf
        allTransf.push_back(transf);
      }

    }

    else if (axCorres.size() == 2)
    {
      std::map<int,std::vector<int> >::iterator it = axCorres.begin();

      int corIndex1 = it->second[0];
      int ax1 = it->first;
      int signAx1;

      Eigen::Vector3d vec1 = corresPoints[corIndex1]-centroid;
      Eigen::Vector3d Ax1(transf(0,ax1), transf(1,ax1), transf(2,ax1));

      if (initAxCorres)
      {
        // simply save the sign
        signAx1 = signbit(Ax1.dot(vec1) );
      }
      else
      {

        signAx1 = axCorres.begin()->second[1];
        //std::cout << "In theory : Ax1 : " << signAx1 << std::endl;
      }
      std::advance(it,1);
      int corIndex2 = it->second[0];
      int ax2 = it->first;
      int signAx2;
      Eigen::Vector3d vec2 = corresPoints[corIndex2]-centroid;
      Eigen::Vector3d Ax2(transf(0,ax2), transf(1,ax2), transf(2,ax2));            
      if (initAxCorres)
      {
        // simply save the sign
        signAx2 = signbit(Ax2.dot(vec2) );
      }
      else
      {
        signAx2 = axCorres.begin()->second[1];
        //std::cout << "In theory : Ax2 : " << signAx2 << std::endl;        
      }

      int ax3;
      if ( (ax2 == 0 && ax1 == 1) || (ax2 == 1 && ax1 == 0)  )
      {
        ax3 = 2;
      }
      else if ( (ax2 == 1 && ax1 == 2) || (ax2 == 2 && ax1 == 1) )
      {
        ax3 = 0;
      }
      else
      {
        ax3 = 1;
      }

      // Two correspondences (ex : x,y)
      // One possibility : rotate z.
      if ( signbit(Ax1.dot(vec1) ) != signAx1  && signbit(Ax2.dot(vec2)) != signAx2)
      {
        Eigen::Matrix4f transf2push = transf;
        transf2push.block<3,1>(0,ax1) *= -1;
        transf2push.block<3,1>(0,ax2) *= -1;
        allTransf.push_back(transf2push);
      }
      else
      {

        if ( ( (signbit(Ax2.dot(vec2)) == signAx2 ) && ( signbit(Ax1.dot(vec1) ) != signAx1 )    ) || ( (signbit(Ax2.dot(vec2)) != signAx2 ) && ( signbit(Ax1.dot(vec1) ) == signAx1 )    ) )
        {

          int ax2rotate;

          if ( (signbit(Ax2.dot(vec2)) != signAx2 ) && ( signbit(Ax1.dot(vec1) ) == signAx1 )    )
          {
            // at least Ax1 looks ok. Let's rotate 90° around this axis. 
            ax2rotate = ax2;
          }
          else if ( (signbit(Ax2.dot(vec2)) == signAx2 ) && ( signbit(Ax1.dot(vec1) ) != signAx1 )    )
          {
            // at least Ax2 looks ok. Let's rotate 90° around this axis.             
            ax2rotate = ax1;
          }

          for (int l = 0; l < 4 ; l++)
          {
            Eigen::Matrix4f transf2push = transf;
            transf2push.block<3,1>(0,ax2rotate) *= pow(-1,l);
            transf2push.block<3,1>(0,ax3) *= pow(-1,(int) l/2);
            allTransf.push_back(transf2push);
          }

        }

        else
        {
          if (multiZeroCase)
          {
            for (int l = 0; l < 2 ; l++)
            {
              Eigen::Matrix4f transf2push = transf;
              transf2push.block<3,1>(0,ax3) *= pow(-1,(int) l);
              allTransf.push_back(transf2push);
            }
          }
          else
          {
            allTransf.push_back(transf);
          }

        }

      }

    }

    else
    {
      allTransf.push_back(transf);
    }

    if (initAxCorres)
    {
      for (std::map<int,std::vector<int> >::iterator it = axCorres.begin(); it!= axCorres.end(); it++)
      {
        int corIndex = axCorres.begin()->second[0];
        int ax = axCorres.begin()->first;
        Eigen::Vector3d vec = corresPoints[corIndex]-centroid;
        Eigen::Vector3d Ax(transf(0,ax), transf(1,ax), transf(2,ax));
        axCorres[it->first].push_back( signbit( Ax.dot(vec)) );
      }
    }


    Eigen::Vector3d transfVec;

    int numPC = allTransf.size();
    //std::cout << "allTransf size : " << numPC << std::endl;

    Eigen::Vector3d ptOnAxP;
    pcl::PointXYZ ptAx;
    Eigen::Vector3d axP(transf(0,0), transf(1,0), transf(2,0));
    axP.normalize();

    int num_PC;
    int num_return_axP;


    if (multiZeroCase)
    {
      num_PC = 4;
      num_return_axP = 2;
    }
    else
    {
      num_PC = 1;
      num_return_axP = 1;
    }

    for (int l = 0; l < num_return_axP; l++)
    {

      axP *= pow(-1,l);
      transf.block<3,1>(0,0) *= pow(-1,l);

      // TO CHECK
      transf.block<3,1>(0,2) *= pow(-1,l);

      axP.normalize();

      K = distToCentroid/axP.norm();

      ptOnAxP = axP*K+centroid;
      ptAx.x = (float) ptOnAxP[0];
      ptAx.y = (float) ptOnAxP[1];
      ptAx.z = (float) ptOnAxP[2];          

      // reboot
      PCForGuess->points.clear();
      PCForGuess->points.push_back(ptCentroid);
      PCForGuess->points.push_back(ptAx);

      PCSimpl.clear();
      PCSimpl[0] = centroid;
      PCSimpl[1] = ptOnAxP;

      n = 2;      

      Eigen::Vector3d vec1( transf(0,1), transf(1,1), transf(2,1) );
      Eigen::Vector3d vec2( transf(0,2), transf(1,2), transf(2,2) );    

      for (int i = 0; i < num_PC; i++)
      {

        for (int i  = 0; i < corresPoints.size(); i++ )
          {
            PCSimpl[n] =  corresPoints[i];
            n++;
            pt.x = corresPoints[i][0];
            pt.y = corresPoints[i][1];
            pt.z = corresPoints[i][2];
            PCForGuess->points.push_back(pt);
          }


        vec1 = vec1*pow(-1,i);
        vec1.normalize();
        K = distToCentroid/vec1.norm();    

        ptC[0] = K*vec1[0]+centroid[0];
        ptC[1] = K*vec1[1]+centroid[1];
        ptC[2] = K*vec1[2]+centroid[2];
        PCSimpl[n] =  ptC;
        n++;

        pt.x = K*vec1[0]+centroid[0];
        pt.y = K*vec1[1]+centroid[1];
        pt.z = K*vec1[2]+centroid[2];
        PCForGuess->points.push_back(pt);

        vec2 = vec2*pow(-1, (int)i/2 );
        vec2.normalize();

        K = distToCentroid/vec2.norm();   

        ptC[0] = K*vec2[0]+centroid[0];
        ptC[1] = K*vec2[1]+centroid[1];
        ptC[2] = K*vec2[2]+centroid[2];
        PCSimpl[n] =  ptC;
        n++;
  
        pt.x = K*vec2[0]+centroid[0];
        pt.y = K*vec2[1]+centroid[1];
        pt.z = K*vec2[2]+centroid[2];
        PCForGuess->points.push_back(pt);

        pcl::PointCloud<pcl::PointXYZ>::Ptr PCForGuess2(new pcl::PointCloud<pcl::PointXYZ>);
        PCForGuess2->points = PCForGuess->points;

        //std::cout << PCForGuess->points.size() << std::endl;
        returnPC.push_back(PCForGuess2);
        returnPCSimpl.push_back(PCSimpl);

        // reboot
        PCForGuess->points.clear();
        PCForGuess->points.push_back(ptCentroid);
        PCForGuess->points.push_back(ptAx);

        PCSimpl.clear();
        PCSimpl[0] = centroid;
        PCSimpl[1] = ptOnAxP;

        n = 2;

      }
    }



    // for (int j = 0; j < numPC; j++)
    // {
 
    //   for (int i  = 0; i < corresPoints.size(); i++ )
    //   {
    //     PCSimpl[n] =  corresPoints[i];
    //     n++;
    //     pt.x = corresPoints[i][0];
    //     pt.y = corresPoints[i][1];
    //     pt.z = corresPoints[i][2];
    //     PCForGuess->points.push_back(pt);
    //   }

    //   int i = 0;
    //   while (n < 4 && i < 3)
    //   {
    //     if (axCorres.find(i) == axCorres.end() )
    //     {
    //       // we do not take any axes where a correspondance point is parallal to it
    //       // (it won't add any useful informations)
    //       transfVec(0,0) = allTransf[j](0,i);
    //       transfVec(1,0) = allTransf[j](1,i);
    //       transfVec(2,0) = allTransf[j](2,i);

    //       K = distToCentroid/transfVec.norm();

    //       ptC[0] = K*transfVec[0]+centroid[0];
    //       ptC[1] = K*transfVec[1]+centroid[1];
    //       ptC[2] = K*transfVec[2]+centroid[2];
    //       PCSimpl[n] =  ptC;
    //       n++;

    //       pt.x = ptC[0];
    //       pt.y = ptC[1];
    //       pt.z = ptC[2];

    //       PCForGuess->points.push_back(pt);    

    //     }

    //     i++;
    //   }

    //   pcl::PointCloud<pcl::PointXYZ>::Ptr PCForGuess2(new pcl::PointCloud<pcl::PointXYZ>);
    //   PCForGuess2->points = PCForGuess->points;

    //   returnPC.push_back(PCForGuess2);
    //   returnPCSimpl.push_back(PCSimpl);
    
    //   // reboot
    //   PCForGuess->points.clear();
    //   PCForGuess->points.push_back(ptCentroid);

    //   PCSimpl.clear();
    //   PCSimpl[0] = centroid;

    //   n = 1;

    // }

  }
  // 0 case; idea : 
  // * use PCA
  // * return Principal Axis
  // *as we don't know if vectors into PCA are correct, rotate of 90° around
  // principal axis
  // *at the end : 8 PC with 4 points each (if multiZeroCase asked)
  // otherwise, only one PC returned;
  else
  {

    Eigen::Vector3d ptOnAxP;
    pcl::PointXYZ ptAx;
    Eigen::Vector3d axP(transf(0,0), transf(1,0), transf(2,0));
    axP.normalize();

    int num_PC;
    int num_return_axP;


    if (multiZeroCase)
    {
      num_PC = 4;
      num_return_axP = 2;
    }
    else
    {
      num_PC = 1;
      num_return_axP = 1;
    }

    for (int l = 0; l < num_return_axP; l++)
    {

      axP *= pow(-1,l);
      transf.block<3,1>(0,0) *= pow(-1,l);

      // TO CHECK
      transf.block<3,1>(0,2) *= pow(-1,l);

      axP.normalize();

      K = distToCentroid/axP.norm();

      ptOnAxP = axP*K+centroid;
      ptAx.x = (float) ptOnAxP[0];
      ptAx.y = (float) ptOnAxP[1];
      ptAx.z = (float) ptOnAxP[2];          

      // reboot
      PCForGuess->points.clear();
      PCForGuess->points.push_back(ptCentroid);
      PCForGuess->points.push_back(ptAx);

      PCSimpl.clear();
      PCSimpl[0] = centroid;
      PCSimpl[1] = ptOnAxP;

      n = 2;      

      Eigen::Vector3d vec1( transf(0,1), transf(1,1), transf(2,1) );
      Eigen::Vector3d vec2( transf(0,2), transf(1,2), transf(2,2) );    

      for (int i = 0; i < num_PC; i++)
      {
        vec1 = vec1*pow(-1,i);
        vec1.normalize();
        K = distToCentroid/vec1.norm();    

        ptC[0] = K*vec1[0]+centroid[0];
        ptC[1] = K*vec1[1]+centroid[1];
        ptC[2] = K*vec1[2]+centroid[2];
        PCSimpl[n] =  ptC;
        n++;

        pt.x = K*vec1[0]+centroid[0];
        pt.y = K*vec1[1]+centroid[1];
        pt.z = K*vec1[2]+centroid[2];
        PCForGuess->points.push_back(pt);

        vec2 = vec2*pow(-1, (int)i/2 );
        vec2.normalize();

        K = distToCentroid/vec2.norm();   

        ptC[0] = K*vec2[0]+centroid[0];
        ptC[1] = K*vec2[1]+centroid[1];
        ptC[2] = K*vec2[2]+centroid[2];
        PCSimpl[n] =  ptC;
        n++;
  
        pt.x = K*vec2[0]+centroid[0];
        pt.y = K*vec2[1]+centroid[1];
        pt.z = K*vec2[2]+centroid[2];
        PCForGuess->points.push_back(pt);

        pcl::PointCloud<pcl::PointXYZ>::Ptr PCForGuess2(new pcl::PointCloud<pcl::PointXYZ>);
        PCForGuess2->points = PCForGuess->points;

        //std::cout << PCForGuess->points.size() << std::endl;
        returnPC.push_back(PCForGuess2);
        returnPCSimpl.push_back(PCSimpl);

        // reboot
        PCForGuess->points.clear();
        PCForGuess->points.push_back(ptCentroid);
        PCForGuess->points.push_back(ptAx);

        PCSimpl.clear();
        PCSimpl[0] = centroid;
        PCSimpl[1] = ptOnAxP;

        n = 2;

      }
    }

  }


}
/**
 * Convenience method to call bodyMarkerRecoveryByICP
*/
int bodyMarkerRecoveryByICP(
            managementParameters & manParams,
            std::map<std::string, Eigen::Vector3d > theoricalOpensimMarkers,
            std::map<int, Eigen::Vector3d > optitrackPoints,
            fosim::OsimModel & osMod,
            std::map<std::string,int> & corresNameID,
            double lim,
            globalPublisher globalPublish,
            std::map<int,std::string> CINCalib,
            bool canTryPartial
             ) 
{
  return(bodyMarkerRecoveryByICP(manParams, theoricalOpensimMarkers,
  optitrackPoints, osMod.D_mark_all_dist, osMod.min_mark_dist, osMod.D_markers_body,
  corresNameID, lim, globalPublish, CINCalib, canTryPartial) );
}


/**
 * @brief Method trying to recover the associations between the markers of a body part and the
 * optitrack markers, from a number of retained associations.
 * 
 * Idea here : 
 * 	* ICP :
		- Between optitrack theory / optitrack current
		- If no marker assigned :
https://pointclouds.org/documentation/tutorials/random_sample_consensus.html/
			* RANSAC: - Define sphere model to part of theory.
					- Apply RANSAC to practice
					- Take centroid from inliers.
					- Transformation : * Rotation = I
							* Trans: tq centroid theory / practice combined.

		- If a marker assigned :
				Transformation: Rot: I
						Trans: tq pt assigned / pt tho combined
		- If two markers assigned:
				Trans: Rot: calculated by https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
						Trans: tq centroid between assigned pts / pt tho combined.
	* Apply ICP
	* Verif: - if allocated pts do not remain the same → cancel.
		- if marker allocation distance by ICP > lim → cancel
		Lim def by min_dist_osim.	
		- for now, remains ok if a tho marker is not allocated. 

WARNING : IF WORKING, THIS METHOD WILL CLEAR corresNameID; please use a specific
variable for this method.
 * 
 * @param manParams 
 * @param theoricalOpensimMarkers 
 * @param optitrackPoints 
 * @param osMod 
 * @param q_calib 
 * @param corresNameID 
 * @param result_transf transformation from Opensim to Optitrack position.
 * @param scaling_factors 
 * @return int result : 
 * -1 : association failed and we want to debug it (pause_mode can be activated)
 * 0 : association failed
 * 1 : association succeeded
 * 2 : association succeded AND changed some previous associations
 * 3 : association is good but not giving any new informations; so useless
 * 
 * NB : maybe add filtering according to type of transform (rotation, translation at first)
 * 
 * NB : q_calib must be set to osMod before executing method.
 * 
 */
int bodyMarkerRecoveryByICP(
            managementParameters & manParams,
            std::map<std::string, Eigen::Vector3d > theoricalOpensimMarkers,
            std::map<int, Eigen::Vector3d > optitrackPoints,
            std::map<std::string,std::map<std::string,double> > D_mark_all_dist,
            double min_mark_dist,
            std::map<std::string,std::string> D_markers_body,
            std::map<std::string,int> & corresNameID,
            double lim,
            globalPublisher globalPublish,
            std::map<int,std::string> CINCalib,
            bool canTryPartial
             ) 
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr osimPC(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr optiPC(new pcl::PointCloud<pcl::PointXYZ>);    
    std::vector<Eigen::Vector3d> opti_vec;
    std::vector<int> idOpti;
    std::vector<std::string> osim_name;

    std::map<std::string,int> currentCorresNameID;
    std::vector<Eigen::Vector3d> cur_opti_vec;
    std::vector<int> cur_idOpti;



    std::vector<Eigen::Vector3d> osimCorresPts;

    std::vector<Eigen::Vector3d> optiCorresPts;   
    
    std::vector<int> osimPCIndexesAlreadyAssociated;

    int l = 0;
    for ( std::map<std::string, Eigen::Vector3d >::iterator it=theoricalOpensimMarkers.begin(); it!=theoricalOpensimMarkers.end(); it++ )
    {
      Eigen::Vector3d vec = it->second;
      pcl::PointXYZ pt(vec[0],vec[1],vec[2]);

      std::string name = it->first;
      int id = corresNameID[name];

      if (optitrackPoints.find(id) != optitrackPoints.end() )
      {
        currentCorresNameID[it->first]= id;
        cur_idOpti.push_back(id);
        cur_opti_vec.push_back(optitrackPoints[id]);
        osimCorresPts.push_back(it->second);
        optiCorresPts.push_back(optitrackPoints[id]);      
        osimPCIndexesAlreadyAssociated.push_back(l);
      }
      (*osimPC).push_back(pt);
      osim_name.push_back(it->first);
      l+=1;
    }

    // int minNumForICP = osim_name.size();
    // if (minNumForICP <= 3)
    // {
    //   minNumForICP = 3;
    // }

    int minNumForICP = 3;


    for ( std::map<int, Eigen::Vector3d >::iterator it=optitrackPoints.begin(); it!=optitrackPoints.end(); it++ )
    {
      Eigen::Vector3d vec = it->second;
      pcl::PointXYZ pt(vec[0],vec[1],vec[2]);
      (*optiPC).push_back(pt);
      opti_vec.push_back(vec);
      idOpti.push_back(it->first);
    }

    Eigen::Matrix4f transf;
    Eigen::Matrix4f init_guess;
    init_guess.setZero();
    
    int result = 1;

    // first, get approximate of sphere that should be detected
    // if we have still some correspondances, use this info
    double max_dist = 0.0;
    double lim_dist = 0.0;
 
    // max_dist : max distance from all points currently recorded
    for (std::map<std::string,int>::iterator it = currentCorresNameID.begin(); it!= currentCorresNameID.end(); it++)
    {

      std::map<std::string,double> D_mark_dist = D_mark_all_dist[it->first];

      for (std::map<std::string,double>::iterator it2 = D_mark_dist.begin(); it2!= D_mark_dist.end(); it2++)
      {
        if (it2->second > max_dist)
        {
          max_dist = it2->second;
        }
      }
    }

    //max_dist = max_dist*1.5;
    //std::cout << "max_dist : " << max_dist << std::endl;


    Eigen::Matrix4f osimTheoTransfo;
    Eigen::Vector4f oriPt;
    pcl::PointXYZ ptSimpl;
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> returnPC;
    std::vector< std::map<int,Eigen::Vector3d> > returnPCSimpl;

    std::map<int,std::vector<int> > axCorres;

    getBodyGuess(osimPC,osimCorresPts,osimTheoTransfo,returnPC, returnPCSimpl, false, axCorres);
    //  getBodyGuess(osimPC,osimCorresPts,osimTheoTransfo,returnPC, returnPCSimpl, false);
    

    // for (  std::map<int,std::vector<int> >::iterator it = axCorres.begin(); it!=axCorres.end(); it++)
    // {
    //   std::cout << "Axis : " << it->first << " ; closest : " << it->second[0] << " ; direction : " << it->second[1] << std::endl;
    // }

    pcl::PointCloud<pcl::PointXYZ>::Ptr simplifiedOsimCloud(new pcl::PointCloud<pcl::PointXYZ>);    
    simplifiedOsimCloud = returnPC[0];

    std::map<int,Eigen::Vector3d> osimPCSimp = returnPCSimpl[0];
    
    Eigen::Vector4f centroidOsim = osimTheoTransfo.block<4,1>(0,3);
    
    Eigen::Vector3d centroidOsimD(centroidOsim[0], centroidOsim[1], centroidOsim[2]);
  
    double min_rad = 9999;
    double max_rad = 0.0;

    std::map<std::string,double> thoDistCentroid;
    std::vector<double> thoDistCenVec;
    // max_rad : max radius of centroid from all points, in theory
    for ( std::map<std::string, Eigen::Vector3d >::iterator it=theoricalOpensimMarkers.begin(); it!=theoricalOpensimMarkers.end(); it++ )
    {

      Eigen::Vector3d vec = it->second;
      double rad = (centroidOsimD - vec).norm();
      if (rad < min_rad)
      {
        min_rad = rad;
      }
      if (rad > max_rad )
      {
        max_rad = rad;
      }
      thoDistCentroid[it->first] = rad;
      thoDistCenVec.push_back(rad);
    }   



    // from https://pointclouds.org/documentation/tutorials/kdtree_search.html    
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
    kdtree.setInputCloud(optiPC);

    pcl::PointCloud<pcl::PointXYZ>::Ptr selectedPC(new pcl::PointCloud<pcl::PointXYZ>);
    std::vector<int> selectedIndexes;    

    int situation = currentCorresNameID.size();

    int alreadyAssociatedInSelectedSet = 0;

    // according to current situation, adapt the method to get a guess of
    // the transformation between Opensim and Optitrack
    if (situation == 0)
    {

      if (manParams.filtering_verbose)
      {
        std::cout << "Try with 0 guess..." << std::endl;
      }

      //minNumForICP = osim_name.size();
      // RANSAC situation (worse situation)

      // other idea : 

      // prendre pt
      // prendre pts + proches ds rayon = 2*max_rad (max pt : osimPC.size() -1)
      // noter pts
      // repeter jusqu'a ce que plus de pts dispo
      // prendre centroid des ss-ens crees.
      // check distance centroid / pts.
      // noter score de proximite ac tho
      // prendre centroid où meilleur score.

      std::vector< pcl::PointCloud<pcl::PointXYZ>::Ptr > lowerSetsOpti;
      std::vector< std::vector<int> > lowerSetsIndexes;

      std::vector<int> curIdOpti = idOpti;
      std::vector<int>::iterator IdOptiIt;
      while (curIdOpti.size() != 0)
      {
        pcl::PointCloud<pcl::PointXYZ>::Ptr setPC(new pcl::PointCloud<pcl::PointXYZ>);
        std::vector<int> setIndex;
        std::map<int,Eigen::Vector3d> setOpti;
        int optiSelect = curIdOpti[0];
        curIdOpti.erase(curIdOpti.begin());
        //setOpti[optiSelect] = optitrackPoints[optiSelect];

        std::vector<int> pointIdxRadiusSearch;
        std::vector<float> pointRadiusSquaredDistance;
        Eigen::Vector3d selectPt = optitrackPoints[optiSelect];
        pcl::PointXYZ searchPoint(selectPt[0], selectPt[1], selectPt[2]);
        //setPC->points.push_back(searchPoint);

        if ( kdtree.radiusSearch (searchPoint, 2.0*max_rad, pointIdxRadiusSearch, pointRadiusSquaredDistance, osimPC->points.size()) > 0 )
        {

          if (manParams.filtering_verbose)
          {
            std::printf("Set of size : %ld found.\n", pointIdxRadiusSearch.size());
          }
          for (int i = 0; i < pointIdxRadiusSearch.size(); i++)
          {
            pcl::PointXYZ pt = optiPC->points[pointIdxRadiusSearch[i]];

            int optiIdNeigh = idOpti[pointIdxRadiusSearch[i] ];
            //setOpti[optiIdNeigh] = optitrackPoints[optiIdNeigh];
            setPC->points.push_back(pt);
            setIndex.push_back(optiIdNeigh);
            IdOptiIt = std::find(curIdOpti.begin(), curIdOpti.end(), optiIdNeigh);
            if (IdOptiIt != curIdOpti.end())
            {
              curIdOpti.erase(IdOptiIt);
            }
          }
        }

        // check distances between centroid and points, compare to theoretical,
        // suppress points that do not respect the limits

        // add a lower limit : if less than 3 points --> not recorded.
        // reason : even if selected, ICP won't work, useless.
        if (setPC->points.size() >= 3)
        {
          lowerSetsOpti.push_back(setPC);
          lowerSetsIndexes.push_back(setIndex);
        }

      }

      //std::printf("Selection of possible PC done; try now to set best \n");

      if (lowerSetsOpti.size() == 1)
      {
        selectedPC = lowerSetsOpti[0];
        selectedIndexes = lowerSetsIndexes[0];
      }
      else if (lowerSetsOpti.size() > 1)
      {
        double bestScoreAss = 9999;

        for (int i = 0; i < lowerSetsOpti.size(); i++)
        {
          double score = 0;
          pcl::PointCloud<pcl::PointXYZ>::Ptr setPC = lowerSetsOpti[i];
          Eigen::Vector4f centroid;
          std::vector<double> curThoDistCenVec = thoDistCenVec;
          pcl::compute3DCentroid( (*setPC),centroid);          

          for (int j = 0; j < setPC->points.size(); j++)
          {
            pcl::PointXYZ pt = setPC->points[j];
            double curDist = sqrt(  pow(pt.x-centroid[0],2.0) + pow(pt.y-centroid[1],2.0) + pow(pt.z - centroid[2], 2.0)     ); 
            double minDiff = 9999;
            int index = -1;
            for (int l = 0; l < curThoDistCenVec.size(); l++)
            {
              if (minDiff > abs(curThoDistCenVec[l] - curDist ) )
              {
                minDiff = abs(curThoDistCenVec[l] - curDist );
                index = l;
              }
            }
            score += minDiff;
            curThoDistCenVec.erase(curThoDistCenVec.begin() + index);
          }

          if (score < bestScoreAss)
          {
            bestScoreAss = score;
            selectedPC = lowerSetsOpti[i];
            selectedIndexes = lowerSetsIndexes[i];
          }
          
        }

      }

      //std::printf("Selection done \n");

    }

    else if (situation == 1)
    {

      if (manParams.filtering_verbose)
      {
        std::cout << "Try with 1 guess..." << std::endl;
        std::cout << currentCorresNameID.begin()->first << " with : " << currentCorresNameID.begin()->second << std::endl;
        // if (currentCorresNameID.begin()->second == 6)
        // {
        //   printf("-- Optitrack distances -- \n");
        //   for (std::map<int, Eigen::Vector3d>::iterator it = optitrackPoints.begin(); it!=optitrackPoints.end(); it++ )
        //   {
        //      std::cout << it->first << " : " << (it->second - optitrackPoints[currentCorresNameID.begin()->second]).norm() << std::endl;
        //   }          
        // }
      }      
      // associated point will obviously be here, so +1
      alreadyAssociatedInSelectedSet +=1;
      // combined points situation
      std::map<std::string,int>::iterator it = currentCorresNameID.begin();


      std::map<int,Eigen::Vector3d> setOpti;
      int optiSelect = it->second;
      
      std::vector<int> pointIdxRadiusSearch;
      std::vector<float> pointRadiusSquaredDistance;
      Eigen::Vector3d selectPt = optitrackPoints[optiSelect];

      pcl::PointXYZ searchPoint(selectPt[0], selectPt[1], selectPt[2]);
        //setPC->points.push_back(searchPoint);
      double chosen_dist = max_dist;

      bool keep = true;
      while (keep)
      {
        pcl::PointCloud<pcl::PointXYZ>::Ptr setPC(new pcl::PointCloud<pcl::PointXYZ>);
        std::vector<int> setIndex;

        if ( kdtree.radiusSearch (searchPoint, chosen_dist, pointIdxRadiusSearch, pointRadiusSquaredDistance, osimPC->points.size()) > 0 )
        {
          if (manParams.filtering_verbose)
          {
            std::printf("Set of size : %ld found.\n", pointIdxRadiusSearch.size());
          }
          // add a lower limit : if less than 3 points --> not recorded.
          // reason : even if selected, ICP won't work, useless.
          
          if (pointIdxRadiusSearch.size() >= 2)
          {
            bool alreadyIn = false;
            for (int i = 0; i < pointIdxRadiusSearch.size(); i++)
            {
              pcl::PointXYZ pt = optiPC->points[pointIdxRadiusSearch[i]];

              int optiIdNeigh = idOpti[pointIdxRadiusSearch[i] ];
              setPC->points.push_back(pt);
              setIndex.push_back(optiIdNeigh);
              
            }
            // final check, just to be sure index already associated has been added.
            int id = optiSelect;
            if (std::find(setIndex.begin(), setIndex.end(), id) == setIndex.end() )
            {
              Eigen::Vector3d ptD = cur_opti_vec[0];
              pcl::PointXYZ pt(ptD[0], ptD[1], ptD[2]);
              setIndex.push_back(id);
              setPC->points.push_back(pt);
            }


            if (setIndex.size() >= 3)
            {
              selectedPC = setPC;
              selectedIndexes = setIndex;
              keep = false;
            }
          }
          if (keep)
          {
            if (chosen_dist < 2.2*max_rad)
            {
              // retry one time with a bigger distance
              chosen_dist = 2.2*max_rad;
            }
            else
            {
              keep = false;
            }

          }
        
        }
        else
        {
          if (manParams.filtering_verbose)
          {
            std::cout << "No marker found with given restrictions : ";
          }
          keep = false;
        }

      }

    }

    else if (situation >= 2)
    {

      if (manParams.filtering_verbose)
      {
        std::cout << "Try with 2 or more guess..." << std::endl;
        for (std::map<std::string,int>::iterator it = currentCorresNameID.begin(); it!= currentCorresNameID.end(); it++)
        {
          std::cout << it->first << " with : " << it->second << std::endl;
        }
      }      
      // (best situation)
      // check closest points for the two 
      // gets those in commun in the two sets.

      std::vector< pcl::PointCloud<pcl::PointXYZ>::Ptr > lowerSetsOpti;
      std::vector< std::vector<int> > lowerSetsIndexes;
      // more than 2.0*max_rad, because we will have more checking steps to correct false positive
      double chosen_dist = std::max(2.9*max_rad, 1.1*max_dist);

      for (std::map<std::string, int>::iterator it = currentCorresNameID.begin(); it!= currentCorresNameID.end(); it++)
      {
        pcl::PointCloud<pcl::PointXYZ>::Ptr setPC(new pcl::PointCloud<pcl::PointXYZ>);
        std::vector<int> setIndex;
        std::map<int,Eigen::Vector3d> setOpti;
        int optiSelect = it->second;

        std::vector<int> pointIdxRadiusSearch;
        std::vector<float> pointRadiusSquaredDistance;
        Eigen::Vector3d selectPt = optitrackPoints[optiSelect];
        pcl::PointXYZ searchPoint(selectPt[0], selectPt[1], selectPt[2]);
        //setPC->points.push_back(searchPoint);

        if ( kdtree.radiusSearch (searchPoint,chosen_dist, pointIdxRadiusSearch, pointRadiusSquaredDistance, osimPC->points.size()) > 0 )
        {

          for (int i = 0; i < pointIdxRadiusSearch.size(); i++)
          {
            pcl::PointXYZ pt = optiPC->points[pointIdxRadiusSearch[i]];

            int optiIdNeigh = idOpti[pointIdxRadiusSearch[i] ];
            //setOpti[optiIdNeigh] = optitrackPoints[optiIdNeigh];
            setPC->points.push_back(pt);
            setIndex.push_back(optiIdNeigh);
          }
          // also add points that may not be into those sets, but
          // where the correspondance was already found
          for (int i = 0; i < cur_idOpti.size(); i++)
          {
            int id = cur_idOpti[i];
            if (std::find(setIndex.begin(), setIndex.end(), id) == setIndex.end() )
            {
              Eigen::Vector3d ptD = cur_opti_vec[i];
              pcl::PointXYZ pt(ptD[0], ptD[1], ptD[2]);
              setIndex.push_back(id);
              setPC->points.push_back(pt);
            }
          }
          if (manParams.filtering_verbose)
          {
            std::printf("Set of size : %ld found. \n", setIndex.size());

            
          }

        }
        lowerSetsOpti.push_back(setPC);
        lowerSetsIndexes.push_back(setIndex);
      }

      std::vector<int>::iterator itInd;
      // take commun points between all the sets
      std::vector<int> communSet = lowerSetsIndexes[0];

      for (int i = 1; i < lowerSetsIndexes.size(); i++)
      {
        std::vector<int> checkedSet = lowerSetsIndexes[i];
        int cnt = 0;
        while (cnt < communSet.size())
        {
          itInd = std::find(checkedSet.begin(), checkedSet.end(), communSet[cnt] );
          if (itInd == checkedSet.end() )
          {
            // point cnt in communSet is not in all the sets : thus, we can suppress it
            itInd = communSet.begin();
            std::advance(itInd, cnt);
            communSet.erase(itInd);
          }
          else
          {
            cnt+=1;
          }
        }
      }

      for (int i = 0; i < communSet.size(); i++)
      {
        // finally, add commun elements
        selectedIndexes.push_back(communSet[i]);
        // check if communSet index is a associated index

        if (std::find(cur_idOpti.begin(), cur_idOpti.end(), communSet[i]) != cur_idOpti.end() )
        {
          alreadyAssociatedInSelectedSet +=1;
        }

        // add point
        int l = 0;
        bool found = false;
        while (l < lowerSetsIndexes[0].size() && !found)
        {
          if (lowerSetsIndexes[0][l] == communSet[i])
          {
            found = true;
          }
          else
          {
            l++;
          }
        }


        pcl::PointXYZ pt = lowerSetsOpti[0]->points[l ];
        selectedPC->points.push_back(pt);
      }

      // final check, just to be sure indexes already associated have been added.
      for (int i = 0; i < cur_idOpti.size(); i++)
      {
        int id = cur_idOpti[i];
        if (std::find(selectedIndexes.begin(), selectedIndexes.end(), id) == selectedIndexes.end() )
        {
          Eigen::Vector3d ptD = cur_opti_vec[i];
          pcl::PointXYZ pt(ptD[0], ptD[1], ptD[2]);
          selectedIndexes.push_back(id);
          selectedPC->points.push_back(pt);
        }
      }
      if (manParams.filtering_verbose)
      {
        std::printf("Resulting set size (from fusion) : %ld \n", selectedIndexes.size());
      }      

    }

    if ( selectedIndexes.size() > 0 && manParams.filtering_verbose)
    {
      std::cout << "Optitrack indexes selected : ";
      for (int i = 0; i < selectedIndexes.size(); i++)
      {
        std::cout << selectedIndexes[i] << ", ";
      }
      std::cout << std::endl;
    }

    if ( selectedIndexes.size() > 0 && selectedIndexes.size() == alreadyAssociatedInSelectedSet)
    {
      // no new informations from this association, we can skip
      if (manParams.filtering_verbose)
      {
        std::printf("No new informations, skip \n");
      }            
      result = 3;
    }
    // selectedIndexes = indexes of optitrack markers
    else if (selectedIndexes.size() >= minNumForICP)
    {

      int k = 0;

      // control if only a partial ICP could be made : if we don't have enough optitrack indexes to
      // get all opensim points, then it is useless to disqualify because of a bad association


      bool partialICPOnly = selectedIndexes.size() < osimPC->points.size();

      // we will then try an ICP by trying different pointClouds combinaisons of Opensim points,
      // where we eliminate the Opensim points that are not associated one at a time.
      
      // need for loop to add : 
      // * osimPC vector
      // *osimPCSimp vector
      std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr > osimPCVec;
      // same as osimPC, but expressed by map
      std::vector< std::map<int,Eigen::Vector3d> > osimPCSimpVec;
      std::vector < pcl::PointCloud<pcl::PointXYZ>::Ptr > simplifiedOsimCloudVec;
      std::vector< std::vector<std::string> > osimNameVec;
      std::vector<Eigen::Matrix4f> osimTheoTransfoVec;
      std::vector<  std::vector<Eigen::Vector3d> > optiCorresPtsVec;
      int sizeSelect = selectedIndexes.size();
      if (!partialICPOnly)
      {
        // as we can have a bad selection of optitrack points,
        // also test possibilities where one of the selected optitrack points
        // might be bad
        // TO TEST
        osimPCVec.push_back(osimPC);
        osimPCSimpVec.push_back(osimPCSimp);
        simplifiedOsimCloudVec.push_back(simplifiedOsimCloud);
        osimNameVec.push_back(osim_name);
        osimTheoTransfoVec.push_back(osimTheoTransfo);
        optiCorresPtsVec.push_back(optiCorresPts);
        sizeSelect = 3;
      }

      // else
      // {

        //std::printf("GOTO PARTIAL ICP \n");

        // first, create all combinaisons such as we always have osimPC.size == selectedIndexes.size
        int numberToSup = osimPC->points.size() - sizeSelect;

        if (numberToSup> 0)
        {


          std::vector< std::vector<int> > allCouples;
          // our couples need to be of the same size than numberToSup
          std::vector<int> cple;
          for (int qs = 0; qs < numberToSup; qs++)
          {
            cple.push_back(0);
          }
          int curVal = 0;
          int vcnt = 0;

          // create all those couples totally different (not same number)
          while (vcnt >= 0)
          {

            int maxValLevel = osimPC->points.size() - (numberToSup - vcnt);
            int minValLevel = vcnt;
            if (vcnt > 0 && minValLevel == cple[vcnt-1])
            {
              minValLevel+=1;
            }

            if ( cple[vcnt] <= maxValLevel)
            {
              if (vcnt < cple.size()-1 )
              {
                cple[vcnt+1] = cple[vcnt]+1;
                vcnt++;
              }
              else
              {
                allCouples.push_back(cple);
                cple[vcnt]+=1;
              }
            }
            else
            {
              vcnt-=1;
              if (vcnt >= 0)
              {
                cple[vcnt]+=1;
              }
            }
          }

          //std::printf("ALL COUPLES MADE \n");
          // disqualify the couples where there is an already associated ID
          k = 0;
          while (k < allCouples.size())
          {
            bool erased = false;
            int j = 0;
            while (!erased && j < allCouples[k].size())
            {
              if (std::find(osimPCIndexesAlreadyAssociated.begin(), osimPCIndexesAlreadyAssociated.end(), allCouples[k][j]) != osimPCIndexesAlreadyAssociated.end())
              {
                allCouples.erase(allCouples.begin()+k);
                erased = true;
              }
              else
              {
                j+=1;
              }
            }
            if (!erased)
            {
              k+=1;
            }
          }
          // std::printf("SELECTED COUPLES MADE \n");
          // std::cout << "Number of couples selected : " << allCouples.size() << std::endl;
          // create the osimPC - osimPCSimp in osimPCVec with the associated
            // for (int ze = 0; ze != allCouples.size(); ze++)
            // {
            //   std::cout <<"elements in couple : " << ze << " : ";
            //   for (int zf = 0; zf < allCouples[ze].size(); zf ++)
            //   {
            //     std::cout << allCouples[ze][zf] << " , " << std::endl;
            //   }
            //   std::cout << std::endl;
            // }
          for (int k = 0; k < allCouples.size(); k++)
          {
            int alreadyAssosInCouple = 0;            
            pcl::PointCloud<pcl::PointXYZ>::Ptr tempOsimPC(new pcl::PointCloud<pcl::PointXYZ>);
            std::vector<std::string> temposim_name;
            std::vector<Eigen::Vector3d> tempOsimCorresPts;
            std::vector<Eigen::Vector3d> tempOptiCorresPts;
            //std::cout << "begin treatment for " << k << std::endl;

            std::vector<int> curCouple = allCouples[k];
            for (int i = 0; i < osimPC->points.size(); i++)
            {

              if (std::find(curCouple.begin(), curCouple.end(), i) == curCouple.end())
              {

                pcl::PointXYZ pt = osimPC->points[i];
                (*tempOsimPC).push_back(pt);
                temposim_name.push_back(osim_name[i]);

                int id = corresNameID[osim_name[i]];

                if (optitrackPoints.find(id) != optitrackPoints.end())
                {
                  tempOsimCorresPts.push_back(theoricalOpensimMarkers[osim_name[i]]);
                  tempOptiCorresPts.push_back(optitrackPoints[id]);
                  alreadyAssosInCouple+=1;
                }

              }

            }

            if (alreadyAssosInCouple == temposim_name.size())
            {
              // this is a combinaison where we have all opensim-optitrack correspondences
              // we already know this combinaison : there is not use to test it. At best, it will gives us same result,
              // and at worse, it could suppress it.
            }
            else
            {

              //std::cout << "tempPC " << k << " made" << std::endl;

              // create PCs from the original objects without the points of allCouples[k]

              // create the necessary other variables from those modified PC

              Eigen::Matrix4f tempOsimTheoTransfo;

              std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> tempReturnPC;
              std::vector< std::map<int,Eigen::Vector3d> > tempReturnPCSimpl;

              std::map<int,std::vector<int> > tempAxCorres;          

              getBodyGuess(tempOsimPC,tempOsimCorresPts,tempOsimTheoTransfo,tempReturnPC, tempReturnPCSimpl, false, tempAxCorres);

              //std::cout << "body guess " << k << " made " << std::endl;

              pcl::PointCloud<pcl::PointXYZ>::Ptr tempSimplifiedOsimCloud(new pcl::PointCloud<pcl::PointXYZ>);    
              tempSimplifiedOsimCloud = tempReturnPC[0];

              std::map<int,Eigen::Vector3d> tempOsimPCSimp = tempReturnPCSimpl[0];
              
              osimPCVec.push_back(tempOsimPC);
              osimPCSimpVec.push_back(tempOsimPCSimp);
              simplifiedOsimCloudVec.push_back(tempSimplifiedOsimCloud);
              osimNameVec.push_back(temposim_name);
              osimTheoTransfoVec.push_back(tempOsimTheoTransfo);
              optiCorresPtsVec.push_back(tempOptiCorresPts);

            }

            //std::cout << "treatment for " << k << " made" << std::endl;
          }

          //std::printf("ALL READY \n");

        }
      //}
      k = 0;

      std::vector< std::map<std::string,int> > allCNI;
      int bestGlobalIndex = -1;
      // bestMaxError : amoung all osim / opti combinaison, minimal distance error
      // distance error is defined as the max distance between 2 associated markers
      double bestMaxError = INFINITY;
      int bestResult = 0;
      int bestSetSize = -1;

      // std::map<int, Eigen::Vector3d > optitrackPointsOri = optitrackPoints;



      while (k < osimPCVec.size())
      {
        result = 1;
        osimPC = osimPCVec[k];
        osimPCSimp = osimPCSimpVec[k];
        simplifiedOsimCloud = simplifiedOsimCloudVec[k];
        osim_name = osimNameVec[k];
        osimTheoTransfo = osimTheoTransfoVec[k];
        optiCorresPts = optiCorresPtsVec[k];

        // normally, optitrackPoints (and correspondances to opensim points) are not modified anymore after
        // this selection; but we want to guarantee that the object is not the same
        // (if a mod not seen occured during treatment)

        std::map<int,Eigen::Vector3d> smallOpti;

      
        for (int i = 0; i < selectedIndexes.size(); i++)
        {
          // optitrack id
          int ind = selectedIndexes[i];
          smallOpti[ind] = optitrackPoints[ind];

        }

        optiPC.reset(new pcl::PointCloud<pcl::PointXYZ>);
        idOpti.clear();
        // necessary to be sure that the order of points in optitrackPoints
        // matches the order of points in optiPC
        for ( std::map<int, Eigen::Vector3d >::iterator it=smallOpti.begin(); it!=smallOpti.end(); it++ )
        {
          Eigen::Vector3d vec = it->second;
          pcl::PointXYZ pt(vec[0],vec[1],vec[2]);
          (*optiPC).push_back(pt);
          idOpti.push_back(it->first);
        }


        if (manParams.filtering_verbose)
        {

          printf("---- Optitrack positions after set ---- \n");
          for (std::map<int, Eigen::Vector3d>::iterator it = smallOpti.begin(); it!=smallOpti.end(); it++ )
          {
              std::printf("Optitrack ID : %d ; position : [%3.4f, %3.4f, %3.4f] \n ", it->first, it-> second[0], it->second[1], it->second[2]);
          }
          printf("---- Optitrack correspondences ---- \n");
          for (int i = 0; i < optiCorresPts.size(); i++)
          {
            std::printf("Optitrack pt :  position : [%3.4f, %3.4f, %3.4f] \n ", optiCorresPts[i][0], optiCorresPts[i][1], optiCorresPts[i][2]);
          }
        }

        Eigen::Matrix4f optiTransfo;
        returnPC.clear();
        returnPCSimpl.clear();

        //getBodyGuess(optiPC,optiCorresPts, optiTransfo, returnPC, returnPCSimpl, true);
        getBodyGuess(optiPC,optiCorresPts, optiTransfo, returnPC, returnPCSimpl, true, axCorres);

        if (manParams.filtering_verbose)
        {
          std::cout << "Number of points of selected set : " << selectedIndexes.size() << std::endl;
          std::cout << "Number of transformations that will be tested : " << returnPC.size() << std::endl;
          //std::cout << "Transformation computed : " << optiTransfo << std::endl;
        }

        globalPublish.publishOpensim(ros::Time::now(), theoricalOpensimMarkers, "theorical");

        double bestScore = INFINITY;
        int bestIndex = -1;
        int bestICPResult = 0;
        std::map<std::string,int> bestIcpCorresNameID;
        double bestICPMaxError = INFINITY;

        pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> bestIcp;      
        Eigen::Matrix4f bestTransf; bestTransf.setZero();
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd;       
        pcl::PointCloud<pcl::PointXYZ>::Ptr simplifiedOptiCloud(new pcl::PointCloud<pcl::PointXYZ>);    

        for (int l = 0; l < returnPC.size(); l++)
        {

          simplifiedOptiCloud = returnPC[l];
          svd.estimateRigidTransformation(*simplifiedOsimCloud,*simplifiedOptiCloud,init_guess); 

          Eigen::Matrix3f rot = init_guess.block<3,3>(0,0);
          Eigen::Vector3f ea = (rot).eulerAngles(2,0,1);

          if (manParams.icp_verbose)
          {
            std::printf("In optitrack tested cloud : ");
            for (int d = 0 ; d < simplifiedOptiCloud->points.size(); d++)
            {
              std::printf("Point %d; value : [%3.4f, %3.4f, %3.4f]\n",d,simplifiedOptiCloud->points[d].x,simplifiedOptiCloud->points[d].y,simplifiedOptiCloud->points[d].z );
            }

            std::cout << "Guess obtained : " << std::endl;
            std::cout << init_guess << std::endl;
            std::cout << "Translation : " << init_guess.block<3,1>(0,3) << std::endl;
            std::cout << "Rotation (euler) : " << ea << std::endl;

            // check step : display closest optitrack point for each opensim point, and its distance
            pcl::PointCloud<pcl::PointXYZ>::Ptr guessOptiPC(new pcl::PointCloud<pcl::PointXYZ>);
            std::vector<int> idOpti2 = selectedIndexes;
            pcl::transformPointCloud(*optiPC, *guessOptiPC, init_guess.inverse());
            for (int i = 0; i < osimPC->points.size(); i++)
            {
              double min_n = max_rad;
              int index = -1;
              Eigen::Vector3d ptOs(osimPC->points[i].x, osimPC->points[i].y, osimPC->points[i].z );
              for (int j = 0; j < guessOptiPC->points.size(); j++)
              {
                Eigen::Vector3d ptOp(guessOptiPC->points[j].x, guessOptiPC->points[j].y, guessOptiPC->points[j].z );
                if ( (ptOs-ptOp).norm() < min_n )
                {
                  min_n = (ptOs-ptOp).norm();
                  index = j;
                }
              }

              std::cout << "For opensim point : " << osim_name[i];
              if (index != -1)
              {
                std::cout << " ; associated optitrack point : " << idOpti2[index];
                std::cout << " ; with distance : " << min_n << std::endl;

                guessOptiPC->points.erase(guessOptiPC->points.begin()+index);
                idOpti2.erase(idOpti2.begin()+index);
              }
              else
              {
                std::cout << " ; error : no point found ! " << std::endl;
              }

            }

            
          }
          // an init guess has been found, let's try ICP ! 
          double score = 10;
          int iter = 0;
          int max_iter = 10;

          pcl::PointCloud<pcl::PointXYZ>::Ptr transformed_cloud_ptr(new pcl::PointCloud<pcl::PointXYZ>);
          pcl::IterativeClosestPoint<pcl::PointXYZ, pcl::PointXYZ> icp;

          int max_iters = manParams.icp_max_iter;

          double euclidean_fitness_epsilon = min_mark_dist/max_iters;

          icp.setMaximumIterations(manParams.icp_max_iter);
          icp.setTransformationEpsilon(manParams.transformation_epsilon); // 1e-40
          icp.setRANSACOutlierRejectionThreshold(max_rad);

          // source : original point cloud : here, theoretical opensim PC
          // target : pointcloud that we want to obtain : guessed optitrack points
          // transform : from source to target

          icp.setInputSource(osimPC);
          icp.setInputTarget(optiPC);    
          
          Eigen::Matrix4f init_guess2;
          init_guess2.setIdentity();

          init_guess2 = init_guess;
          bool derivation = false;
          if (manParams.icp_verbose)
          {
            std::cout << "Start ICP." << std::endl;
          }
          double max_score;
          max_score = 1e-4;

          icp.setEuclideanFitnessEpsilon(max_score*0.1);

          double best_score = INFINITY;
          Eigen::Matrix4f best_transf;
          Eigen::Matrix4f result_transf;

          while ( ( score > max_score) && (iter < max_iter)     )
          {

            icp.align(*transformed_cloud_ptr, init_guess2); 
            if (manParams.icp_verbose)
            {
              std::cout << "ICP has converged:" << icp.hasConverged()
                    << " score: " << icp.getFitnessScore() << std::endl;
            }

            Eigen::Matrix4f final_transf =  icp.getFinalTransformation();
            rot = final_transf.block<3,3>(0,0);
            ea = (rot).eulerAngles(2,0,1);

            if (manParams.icp_verbose)
            {
              std::cout << "Translation : " << final_transf.block<3,1>(0,3) << std::endl;
              std::cout << "Rotation (euler) : " << ea << std::endl;
            }

            if (abs(score - icp.getFitnessScore()) < max_score)
            {
              if (manParams.icp_verbose)
              {
                  std::cout << "No more changes. Stop icp." << std::endl;
              }
              iter = max_iter;
            }
            else
            {
              if (score - icp.getFitnessScore() < 0)
              {
                derivation = true;
              }
              else if ( icp.getFitnessScore()< best_score)
              {
                best_score = icp.getFitnessScore();
                best_transf = final_transf;
              }
              
            }

            score = icp.getFitnessScore();

            if ( (score > max_score) && (iter < max_iter)   )
            {

              if (derivation)
              {
                if (manParams.icp_verbose)
                {
                    std::cout << "Derivation recorded. Try to fix." << std::endl;
                }
                  // transformFiltering(final_transf, manParams.maximum_translation, manParams.maximum_rotation);
                  // init_guess2 = final_transf;
                  init_guess2 = best_transf;
              }
        
            }
            iter++;

          }

          if (icp.hasConverged())
          {
            result = 1;
            // added (test) : try all validations step on all ICP result,
            // to be sure that a possibly good result will not be discared because a false result would have
            // a better ICP score
            std::map<std::string,int> icpCorresNameID = currentCorresNameID;
            std::map<std::string,double> distOsimOpti;

            int check =   startBodyFiltering( 
                                      manParams,
                                      icp,
                                      D_markers_body,
                                      osimPC,
                                      optiPC,
                                      osim_name,
                                      idOpti,
                                      smallOpti,
                                      best_transf,
                                      icpCorresNameID,
                                      distOsimOpti,
                                      false);  

            // if (manParams.filtering_verbose)
            // {
            //   std::printf("Check result of 1st filtering : %d. \n", check);
            // }          

            // last checks : 
            // - previous associations have been kept

            // - distances between markers are close to limit
            int l2 = 0;
            std::map<std::string,double>::iterator it2 = distOsimOpti.begin();
            double max_dist = 0.0;

            while (l2 < distOsimOpti.size() )
            {

              if (manParams.filtering_verbose)
              {

                std::printf("Distance between point %s and point %d : %f. \n",it2->first.c_str(), icpCorresNameID[it2->first],it2->second);
              }

              if (max_dist < it2->second)
              {
                max_dist = it2->second;
              }

              if (it2->second > lim && result != 0)
              {
                if (manParams.filtering_verbose)
                {
                  std::printf("Distance for this point is superior to lim (%f). Association is refused. \n", lim);
                }              
                result = 0;
              }
              l2++;
              if (l2 < distOsimOpti.size())
              {
                it2 = distOsimOpti.begin();
                std::advance(it2,l2);  
              }
        
            }

            if (result != 0)
            {
                
              std::vector<int> indexes;
              for (std::map<std::string,int>::iterator it = currentCorresNameID.begin(); it!= currentCorresNameID.end(); it++)
              {
                if (icpCorresNameID[it->first] != it->second)
                {
                  if (manParams.filtering_verbose)
                  {
                    std::printf("Error : %s not assigned anymore with %d (now with %d). \n", it->first.c_str(), it->second, icpCorresNameID[it->first]);
                  }   
                  if (max_dist > 0.0001 || ( (CINCalib.find(it->second) != CINCalib.end() ) && (CINCalib.find(icpCorresNameID[it->first]) != CINCalib.end() ) ) )                   
                  {
                    // had to put an extremly hard max_dist threshold because radius body
                    // is a pain in the ass (always switch between LRS <=> LWJC)
                    // if the max distance association is superior to limit OR if the detected switch is concerning
                    // one of the optitrack calibration points (considered as perfect), cancel the whole association
                    result = 0;
                    if (manParams.filtering_verbose)
                    {
                      std::printf("Association refused, whether because of too big distance for swapping association OR because swap concerns an ID from calibration frame. \n");
                    }   
                  }
                  else
                  // just suppress this association
                  {
                    // normally should do it by itself
                  if (manParams.filtering_verbose)
                  {
                    std::printf("After a more precise check, %s association (%d -> %d) is accepted. \n", it->first.c_str(), it->second, icpCorresNameID[it->first]);
                  }   
                  result = 2;
                    //std::map<std::string,int>::iterator it = corresNameID.find()
                  }

                }
                else if (std::find(indexes.begin(), indexes.end(), it->second) != indexes.end() )
                {
                  if (manParams.filtering_verbose)
                  {
                    std::printf("Error : %d is assigned to 2 Opensim markers. \n", it->second);
                  }                       
                  result = 0;
                }
                else
                {
                  indexes.push_back(it->second);
                }
              }
            }

            // to try : do not considers ICP score anymore, as it do not gives as much informations as
            // max_error
            //if ( result!= 0 && best_score < bestScore )
            if ( result!= 0 && max_dist < bestICPMaxError )
            {
              // icp infos
              bestScore = best_score;
              bestIndex = l;
              bestTransf = best_transf;
              bestIcp = icp;
              // global infos
              bestIcpCorresNameID = icpCorresNameID;
              bestICPMaxError = max_dist;
              bestICPResult = result;
            }
          }
        }
        result = bestICPResult;
        if (result != 0)
        {
          if (manParams.icp_verbose)
          {      
            std::cout << "Best index : " << bestIndex << std::endl;
          }
          std::map<int,Eigen::Vector3d> optiPCSimp = returnPCSimpl[bestIndex];


          globalPublish.displayArrows(ros::Time::now(), osimTheoTransfo, globalPublish.ArrowsOs);
          globalPublish.displayArrows(ros::Time::now(), optiTransfo, globalPublish.ArrowsOp);
          

          globalPublish.markerPublisher(ros::Time::now(), optiPCSimp, globalPublish.opBody);
          globalPublish.markerPublisher(ros::Time::now(), osimPCSimp, globalPublish.osBody);

          // control the ICP result

          
          // all check were made. Let's record the new computed associations.
          if (manParams.icp_verbose)
          {      
            std::cout << "ICP guess : try of index " << k << " in tolerances" << std::endl;
          }

          allCNI.push_back(bestIcpCorresNameID);
          int setOsimSizeTested = osim_name.size();
          if (bestICPMaxError < bestMaxError)
          {
            bestMaxError = bestICPMaxError;
            bestGlobalIndex = k;
            bestResult = result;
            bestSetSize = setOsimSizeTested;
          }
          // else if ( setOsimSizeTested > bestSetSize && (bestMaxError - max_dist) < 5e-3 )
          // {
          //   // if it gives new information (because there is more points) and
          //   // the error is slightly over the best, rather select this.

          // }
          //corresNameID.clear();
          //   if (manParams.filtering_verbose)
          //   {
          //     std::printf("Let's now update the previous associations. \n");
          //   }
          // for (std::map<std::string,int>::iterator it = icpCorresNameID.begin(); it!= icpCorresNameID.end(); it++)
          // {
          //   corresNameID[it->first] = it->second;
          // }
        }

        if (result == 0)
        {
          // just to be sure that allCNI is of the same size as allCouples
          allCNI.push_back(corresNameID);
        }

        k+=1;

      }

      if (bestGlobalIndex != -1)
      {
       
        std::map<std::string,int> selectedCNI = allCNI[bestGlobalIndex];
        if (manParams.filtering_verbose)
        {
          std::cout << "best index selected : " <<bestGlobalIndex << " with error : " << bestMaxError << std::endl;
          std::printf("Let's now update the previous associations. \n");
        }
        for (std::map<std::string,int>::iterator it = selectedCNI.begin(); it!= selectedCNI.end(); it++)
        {
          corresNameID[it->first] = it->second;
        }

        // printAssociationByBody(corresNameID);




      }
      // correct result (if no association, should be 0)
      result = bestResult;

    }
    else
    {
      std::cout << "Error : no enoughly big set found. Final size is :  " << selectedIndexes.size() << std::endl;
      result = 0;
    }

    if (result == 0 && selectedIndexes.size() == osimPCSimp.size())
    {
      result = -1;
    }

    return(result);
};

  void controlForbiddenAreas( std::map<int, Eigen::Vector3d> & frame, std::vector<std::vector<Eigen::Vector3d> > forbid_areas)
  {
    std::map<int, Eigen::Vector3d> new_frame;
    for (std::map<int,Eigen::Vector3d>::iterator it = frame.begin(); it!= frame.end(); it++)
    {
      Eigen::Vector3d mark = it->second;
      int i = 0;
      bool valid = true;
      while (valid && i < forbid_areas.size())
      {
        bool inMin = ( (forbid_areas[i][0]- mark).maxCoeff() <= 0 );
        bool inMax = ( (mark - forbid_areas[i][1]).maxCoeff() <= 0 );
        valid = !(inMin && inMax);
        i += 1;
      }

      if (valid)
      {
        new_frame[it->first] = it->second;
      }
    }
    frame = new_frame;
  }

  void manageACINAdding(
    std::string name,
    int id,
    int frameNumber,
    std::map<int, std::map<int,LabelCarac> > & allCorresIDName,
    bool repeat,
    bool byUser,
    bool verbose,
    bool canClean
  )
  // TODO : if not canClean, refuse to change any ID association
  // made by User.
  // If that is an issue (e.g an Optitrack Index was badly made in .trc file),
  // you can just be more selective on filtering (e.g any dissapearance of marker
  // can be considered as a new marker)
  {
    if (repeat && name.size()> 0)
    {

      std::map<int, std::map<int,LabelCarac> >::iterator checkID_it = allCorresIDName.find(id);
      if (checkID_it == allCorresIDName.end())
      {
        // first time that id was found in allCorresIDName
        std::map<int,LabelCarac> mp;
        LabelCarac lc;
        lc.osim_name = name;
        lc.byUser = byUser;
        mp[-1] = lc;
        allCorresIDName[id] = mp;
      }

      else
      {
        LabelCarac lc;
        //it = allCorresIDName[id].find(-1);
        int framePicked = -2;
        std::string nameFound = "";

        checkACINCorres(id,frameNumber,allCorresIDName,nameFound,framePicked,true,verbose);
        // found is not the same as usual, because a [-1] = "" is not a problem for our
        // treatments
        bool found = (framePicked != -2);
        if (found)
        { 
          // other control : 
          // found labelCarac is not a byUser adding 
          // OR found labelCarac is a byUser adding, and byUser = True
          // OR canClean activated. 
          LabelCarac check = allCorresIDName[id][framePicked];
          found = ( (check.byUser && byUser) || (!check.byUser && !byUser) || canClean );

        }
        if (found) 
        { 
          // different cases possible
          int index = std::max(frameNumber -1,0);
          if (framePicked == -1)
          {
            if (verbose)
            {
              ROS_INFO("Adding : -1 selection case");
            }
            // check for this id is not finished; new elements could be added
            if (nameFound != name)
            {
              if ( nameFound.size() > 0 && !byUser )
              {
                // second condition is added to avoid any spam of
                // the same name-id combinaison added at different frameNumber
                // thrid condition is to refuse errors terms
                // association will be it->second until frame index

                // condition !byUser is used to not save the last association
                // ((if an user had to interfere, we consider that it should be because
                // last association was false).) 
                LabelCarac lc_found;
                lc_found.osim_name = nameFound;
                lc_found.byUser = byUser;
                allCorresIDName[id][index] = lc_found;           
              }
              lc.osim_name = name;
              lc.byUser = byUser;
              allCorresIDName[ id ][framePicked] = lc;    
            }
          }
          else
          {
            if (verbose)
            {
              ROS_INFO("Adding : Other frame case");
            }
            int chosenFrame = index;
            if (nameFound.size() > 0)
            {
              // if previous element was valid, check its value
              if ( nameFound != name )
              {
              // if optitrack marker is associated with a new element (name != nameFound) from before,
              // treats it differently
                if (canClean)
                {
                  // if that case, we suppress all future elements for this id; those elements could be based on false data
                  suppressACINCorres(id,frameNumber-1, allCorresIDName);
                  chosenFrame = -1;

                }
                else
                {
                  // otherwise, just insert it among other element
                  chosenFrame = index;
                }
                // add new information             
                lc.osim_name = name;
                lc.byUser = byUser;
                allCorresIDName[ id ][chosenFrame] = lc;       
              }
              else
              {
                // no new info, ignore
              }

            }
            else
            {
              // otherwise, smthg weird happened : suppress everything
              suppressACINCorres(id,framePicked -1, allCorresIDName);
              chosenFrame = -1;

              // add new information
                lc.osim_name = name;
                lc.byUser = byUser;
                allCorresIDName[ id ][chosenFrame] = lc;  
            }            


          }


        }


      }

      if (verbose)
      {
        ROS_INFO("All associations for id %d (in frame : %d) : ", id, frameNumber);
        for (std::map<int,LabelCarac>::iterator it = allCorresIDName[id].begin(); it!=allCorresIDName[id].end(); it++)
        {
          ROS_INFO("\\\\ Ending at frame %d : association %s", it->first, it->second.osim_name.c_str());
        }
      }
    }
  }

  // takes all -1, set them as maxFrameNumber,
  // set all -1 values as first value (in found); otherwise, let them
  // at their current values
  // firstFrame : frame at return
  // lastFrame : frame before return
  void manageACINBeginReturn(
    int maxFrameNumber,
    std::map<int, Eigen::Vector3d> begFrame,
    std::map<int,Eigen::Vector3d> lastFrame,
    std::map<int, std::map<int,LabelCarac> > & allCorresIDName,
    std::map<std::string, int> & corresNameID,
    bool repeat,
    bool verbose
  )
  {
    if (repeat)
    {
        std::string nameFound;
        int framePicked;      
      // std::vector<std::string> modifiedMarkers;
      // reset only the lastFrame
      // for (std::map<int,Eigen::Vector3d>::iterator it = lastFrame.begin(); it!=lastFrame.end(); it++)
      // {
      //   int id = it->first;

      //   std::map<int, std::map<int,std::string> >::iterator it2 = allCorresIDName.find(id);
      //   if (it2 != allCorresIDName.end())
      //   {
      //     // poss 1 
      //     // won't work because lastAssos =  it2->second[-1] (thus, security in manageACINAdding)
      //     // std::string lastAssos = it2->second[-1];
      //     // if (lastAssos.size()> 0)
      //     // {
      //     //   manageACINAdding(lastAssos,id,maxFrameNumber,allCorresIDName,true,verbose);
      //     //   allCorresIDName[id][-1] = "";
      //     // }

      //    // poss 2

      //     bool found = checkACINCorres(it->first,maxFrameNumber,allCorresIDName,nameFound,framePicked,true,verbose);
      //     if (found)
      //     {
      //       allCorresIDName[id][maxFrameNumber+1] = nameFound;
      //       allCorresIDName[id][-1] = "";
      //     }



      //     // set it

      //   }
      // }
      //reset all values of corresNameID; they will be corrected by allCorresNameID
      for (std::map<std::string,int>::iterator it3 = corresNameID.begin();it3!=  corresNameID.end(); it3++)
      {
          corresNameID[it3->first] = -1;
      }

      
      //reset ALL
      for ( std::map<int, std::map<int,LabelCarac> >::iterator it = allCorresIDName.begin(); it!=allCorresIDName.end(); it++)
      {
        int id = it->first;


         // poss 2

          bool found = checkACINCorres(it->first,maxFrameNumber,allCorresIDName,nameFound,framePicked,true);
          if (found)
          {
            LabelCarac lc;
            lc.osim_name = nameFound;
            lc.byUser = false;
            allCorresIDName[id][maxFrameNumber+1] = lc;
            LabelCarac lc2;
            allCorresIDName[id][-1] = lc2;
          }


          if (begFrame.find(id)!= begFrame.end())
          {
            // reset corresNameID
            found = checkACINCorres(id,0,allCorresIDName,nameFound,framePicked,true,verbose);
            if (found)
            {
              corresNameID[nameFound] = id;
            }

          }


        }      

    }

     


  }
  /**
   * Given an Optitrack ID, will return :
   *  - name : The opensim marker associated (if it exists)
   *  - framePicked : The last frame to which the association was considered
   *  as valid. 
   * You can then gain access to LabelCarac concerned with
   * ACIN[id][framePicked]
   * 
  */
  bool checkACINCorres(
    int id,
    int frameNumber,
    std::map<int, std::map<int,LabelCarac> > allCorresIDName,
    std::string & name,
    int & framePicked,
    bool repeat,
    bool verbose
  )
  // starting from ID, we want the associated name
  {
    bool found = false;

    if (repeat)
    {
      std::map<int, std::map<int,LabelCarac> >::iterator it = allCorresIDName.find(id);
      if (it != allCorresIDName.end())
      {

        // find the frame that is the closest from frameNumber
        // and which is superior to frameNumber

        framePicked = -1;
        int dist = 99999;
        bool done = false;
        int i =  it->second.size()-1;
        std::map<int,LabelCarac>::iterator it2 = it->second.begin();
        // id before the beggining of the labelization;
        // in order to not select -1, we had this condition
        bool canMinusOneBeASolution = true;
        //bool idBeforeBeg = false;
        if (it->second.size() > 1)
        {
          std::advance(it2, it->second.size()-1);
          // if biggest recorded frame is superior to frameNumber, then we can
          // guarantee that [-1] cannot be a solution
          canMinusOneBeASolution = (it2->first < frameNumber );

        }

        if (verbose)
        {
          ROS_INFO("For id %d : ", id);
          ROS_INFO("Number of differents associations : %ld", it->second.size());
        }        

        if (canMinusOneBeASolution)
        {
          framePicked = -1;
        }

        else 
        {
          // we do not check 0 element, as it will corresponds to [-1]
          while (!done && i > 0)
          {
            it2 = it->second.begin();
            std::advance(it2, i);
            if (verbose)
            {
              ROS_INFO("Frame selected : %d (where association is : %s); compared to frame : %d ",  it2->first, it2->second.osim_name.c_str(), frameNumber );
            }   
            // as long as frameNumber <= recordedFrame, continues to check
            if ( (it2->first >= frameNumber ) &&  (it2->first - frameNumber < dist )  )
            {
              dist = it2->first - frameNumber;
              framePicked = it2->first;

            }
            // if frameNumber > recordedFrame, then we reached the end (because elements are sorted)
            else if (frameNumber > it2->first)
            {
              done = true;
            }
            i--;
          }
          
          // if (!done && canMinusOneBeASolution)
          // {
          //   // we checked all elements, but frameNumber is always inferior to all elements
          //   framePicked = -1;
          // }
        }

        name = it->second[framePicked].osim_name;
        if (name.size() > 0)
        {
          found = true;
        }
        else
        {
          // if no idea
          found = false;
        }
        if (verbose)
        {
          if (found)
          {
            ROS_INFO("Selected association : %s ; ending at frame : %d",  name.c_str() ,framePicked);
          }
          else
          {
            ROS_INFO("No association selected (only association found is not attribued)");
          }
      
        }   
      }
      else if (verbose)
      {

        ROS_INFO("Error : id %d was never assigned ", id);  
        
      }
    }


    return(found);

  }

/** Suppress all elements from [currentFrame +1 , inf]; also suppress [-1] (by gving it "" value)
 * With cleanFORCE, suppress everything (except [-1] which will get "" value)
 * 
*/
  void suppressACINCorres(
    int optiID,
    int currentFrame,
    std::map<int, std::map<int,LabelCarac> > & allCorresIDName,
    bool cleanFORCE,
    bool byUser
  )
  {
    std::map<int, std::map<int,LabelCarac> >::iterator it3 = allCorresIDName.find(optiID);
    if (it3 != allCorresIDName.end())
    {
      std::map<int,LabelCarac> temp;
      if (cleanFORCE)
      {
        allCorresIDName.erase(it3);         
      }
      else
      {

        int l = it3->second.size()-1;
        bool found = false; 
        std::map<int,LabelCarac>::iterator it9;
        std::vector<int> userToKeep;
        while (l > 0 && !found)
        {
          it9 = it3->second.begin();
          std::advance(it9,l);
          if (it9->first <= currentFrame)
          {
            found = true;
          }
          else
          {
            if (byUser)
            {
              if (it9->second.byUser)
              {
                userToKeep.push_back(l);
              }
            }
            l--;
          }
      
        }
        int v = 0;
        while (v <= l)
        {
          it9 = it3->second.begin();
          std::advance(it9,v);
          temp[it9->first] = it9->second;
          v++;
        }
        for (int i = 0; i < userToKeep.size(); i++)
        {
          int v= userToKeep[i];
          it9 = it3->second.begin();
          std::advance(it9,v);
          temp[it9->first] = it9->second;
        }
    }
    LabelCarac lc;
    temp[-1] = lc;
    allCorresIDName[optiID] = temp;
  }

  }



    
} // namespace markersManagement
