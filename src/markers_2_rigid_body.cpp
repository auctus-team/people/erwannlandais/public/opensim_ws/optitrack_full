#include "optitrack_full/markers_management.h"
#include "optitrack_full/trc_manager.h"

#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"

#include "std_srvs/SetBool.h"

#include <XmlRpcValue.h>


/*
 * @brief Simply reads a .trc file, and returns the position and orientation of the rigid body associated with the .trc file.
 ()
 */

namespace markers_2_rigid_body
{
    /**
     * @brief 
     * 
     */
  class RosReception
  {
  public:
  /**
   * @brief Construct a new Ros Reception object
   * 
   * TODO : find an other way to define the rigid body orientation (ex : define a group of markers as x-y-z axis; or just give an approximiation of their position
   * in the referential of the body marker, then do a calibration to get their true position, and then define the orientation of the rigid body according to the 
   * different associations of the markers).
   *  
   * @param nh 
   * @param trc_filename 
   * @param rigidBodyMarkers 
   * @param initRigidBodyPose : < frame_number, x_orientation_wrt_global_frame , y_orientation_wrt_global_frame, z_orientation_wrt_global_frame >; here, the global frame
   * can be the classical ROS frame (X,Y,Z)
   */
    RosReception(
        ros::NodeHandle& nh,
        std::string trc_filename,
        std::vector<std::string> rigidBodyMarkers,
        int calibFrame,
        std::vector<double> initRigidBodyPose,
        std::map<std::string,int> markerNamesAxis
        ) :
         nh(nh),
         calibFrame(calibFrame),
         TRCManager(trc_filename,true,false,rigidBodyMarkers),
         initCloud(new pcl::PointCloud<pcl::PointXYZ>),
         globalPublish(nh, "/Q_pub")

    {
        TRCManager.run();
        int frameRate = TRCManager.dataRate;
        isPaused = false;

        Eigen::AngleAxisd x_rot(M_PI_2, Eigen::Vector3d::UnitX ());
        Eigen::AngleAxisd z_rot(M_PI_2, Eigen::Vector3d::UnitZ ());
        Eigen::Quaternion<double> qos = z_rot * x_rot;
        osMpi = qos.matrix();

        Eigen::AngleAxisf x_rotf(M_PI_2, Eigen::Vector3f::UnitX ());
        Eigen::AngleAxisf z_rotf(M_PI_2, Eigen::Vector3f::UnitZ ());
        Eigen::Quaternion<float> qosf = z_rotf * x_rotf;
        osMpi_4f.setIdentity();
        osMpi_4f.block<3,3>(0,0) = qosf.matrix();

        initTransform.setIdentity();
        prevTransf.setIdentity();

        pub_ps = nh.advertise<geometry_msgs::PoseStamped>("/end_effector_pose",1000);        

        pause_continue = nh.advertiseService("/pause_continue",  &RosReception::ask_pause_continue,this);
        // first, initialize the rigid body

        int numMarkers = rigidBodyMarkers.size();

        if (markerNamesAxis.size() == 3)
        {
            std::cout << "One marker was given for each axis. Try to alignate the local body according to those markers." << std::endl;
            initRigidBodyWithMarkers(markerNamesAxis, numMarkers); 
        }
        else
        {
            std::cout << "There is not exactly one marker for each axis. Try to alignate the local body according to the given angles." << std::endl;
            initRigidBody(initRigidBodyPose, numMarkers);     
        }

        if (initCloud->points.size() != rigidBodyMarkers.size() )
        {
            ROS_INFO("Error : the size of the markers into the calibration frame ([%ld]) and the number of markers that should be tracked [%ld] do not match.\n",initCloud->points.size(), rigidBodyMarkers.size() );
        }
        else
        {
            ROS_INFO("Calibration good, let's start. \n");
            while (ros::ok())
            {
                if (!isPaused)
                {
                    uint64_t beg_run = ros::Time::now().toNSec();
                    processNextFrame(TRCManager);
                    uint64_t end_run = ros::Time::now().toNSec();

                    double dur_run = (end_run - beg_run )*1e-3;

                    // ROS_INFO("Exec time : %f .", std::max(0.0,(1e6/frameRate - dur_run)*1e-6) ); 

                    usleep( std::max(1e6/frameRate - dur_run,0.0) );
                    //usleep( std::max(1e6*0.1 - dur_run,0.0) );

                }
                ros::spinOnce();   
            }
        }

        // std::cout << "init ok" << std::endl;
        
        // then, iteratively process all the frames of the .trc file.

    };

    /**
     * @brief Track the rigid body into the next frame, and publish its new position and orientation.
     * The position of the rigid body is set up as the barycenter of the set of tracked markers.
     * 
     * @param TRCManager 
     */
    void processNextFrame(file_management::TRCManager & TRCManager)
    {
        // if (TRCManager.Nframe < 300)
        // {
        //     TRCManager.Nframe++;
        // }
        TRCManager.Nframe++;
        //TRCManager.Nframe = calibFrame;
        //std::cout << TRCManager.Nframe << std::endl;

        if (TRCManager.Nframe >= TRCManager.optitrackData.size())
        {
            TRCManager.Nframe = 0;          
            TRCManager.numRepeat++;
        }

        std::map<double,std::map<int,Eigen::Vector3d> >::iterator it = TRCManager.optitrackData.begin();
        std::advance(it,TRCManager.Nframe);
        
        double time = it->first;
        std::map<int,Eigen::Vector3d> frame = it->second;

        pcl::PointCloud<pcl::PointXYZ>::Ptr newCloud(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::PointCloud<pcl::PointXYZ>::Ptr currInitCloud(new pcl::PointCloud<pcl::PointXYZ>);

        //int indexInitCloud = 0;
        for (std::map<std::string,int>::iterator it2 = TRCManager.trcCorresNameId.begin(); it2!= TRCManager.trcCorresNameId.end(); it2++  )
        {
            if (frame.find(it2->second)!= frame.end())
            {
                //Eigen::Vector3d vec = osMpi*frame[it2->second];
                Eigen::Vector3d vec = frame[it2->second];
                pcl::PointXYZ pt(vec[0], vec[1], vec[2]);
                newCloud->points.push_back(pt);
                auto itID= std::find_if(std::begin(corresIDNameInitFrame), std::end(corresIDNameInitFrame), [&](const std::pair<int,std::string> &pair)
                {
                        return pair.second == it2->first;
                });
                if (itID != corresIDNameInitFrame.end() )
                {
                    currInitCloud->points.push_back(initCloud->points[itID->first]);
                }

            }
            //indexInitCloud++;
        }

        if ( (initCloud->points.size() != newCloud->points.size()) && (newCloud->points.size() < 3 ) )
        {
            ROS_INFO("Error : the size of the markers into the current frame ([%ld]) and the number of markers in the calibration frame [%ld] do not match.\n",newCloud->points.size(),initCloud->points.size() );
        }       
        else
        {
            Eigen::Matrix4f transf;
            pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 
            svd.estimateRigidTransformation(*currInitCloud, *newCloud,transf);

            // debug purpose
            pcl::PointCloud<pcl::PointXYZ>::Ptr newInitCloud(new pcl::PointCloud<pcl::PointXYZ>);
            pcl::transformPointCloud(*initCloud, *newInitCloud, transf );

            //transf = transf*(osMpi_4f.inverse().eval() );
            transf = transf*initTransform;            

            Eigen::Vector4f centroidOrigin;
            pcl::compute3DCentroid( (*newInitCloud),centroidOrigin);

            std::map<int,Eigen::Vector3d> optidebug;

            for (int i = 0; i < newInitCloud->points.size(); i++)
            {
                Eigen::Vector3d optipt(newInitCloud->points[i].x, newInitCloud->points[i].y, newInitCloud->points[i].z);
                optidebug[i] = optipt;
            }

            Eigen::Matrix4f transf_osMpi  = osMpi_4f*transf;
            //Eigen::Matrix4f transf_osMpi  = transf;
            Eigen::Quaternion<float> q(transf_osMpi.block<3,3>(0,0) );
            q.normalize();

            // if ( abs(q.norm()- 1.0) > 1e-6 )
            // {
            //     std::cout << q.norm() << std::endl;
            // }

            ros::Time pubTime = ros::Time::now();

            // publish Pose message
            geometry_msgs::PoseStamped pose_msg;

            pose_msg.pose.position.x = transf_osMpi(0,3);
            pose_msg.pose.position.y = transf_osMpi(1,3);
            pose_msg.pose.position.z = transf_osMpi(2,3);

            pose_msg.pose.orientation.x = q.x();
            pose_msg.pose.orientation.y = q.y();
            pose_msg.pose.orientation.z = q.z();
            pose_msg.pose.orientation.w = q.w();

            pose_msg.header.seq = seqHeader;
            pose_msg.header.stamp = pubTime;

            pub_ps.publish(pose_msg);

            // if ( !prevTransf.isIdentity() )
            // {
            //     Eigen::VectorXf Stht(7);
            //     computeMotionSpeedBetweenTwoFrames(prevTransf,transf_osMpi, Stht);
            //     std::cout << "New speed vector : [ ";
            //     for (int i = 0; i < 6; i++)
            //     {
            //         std::cout << " " << Stht(i,0)*Stht(6,0);
            //     }
            //     std::cout << " ]" << std::endl;
            //     // std::cout << "Before : " << prevTransf << std::endl;                
            //     // std::cout << "Now : " << transf_osMpi << std::endl;  
            //     std::cout << "Translation : " << ( transf_osMpi.block<3,1>(0,3) - prevTransf.block<3,1>(0,3) )/100 << std::endl;
            // }
            prevTransf = transf_osMpi;

            globalPublish.markerPublisher(pubTime,frame, globalPublish.optitrack_publisher);
            globalPublish.markerPublisher(pubTime,frameCalib, globalPublish.opensim_theorical_publisher);
            globalPublish.markerPublisher(pubTime,optidebug, globalPublish.opensim_publisher);

            // we send transf, as the transformation osMpi_4f*transf will be done into displayArrows.
            // (resulting in the same result as transf_osMpi)
            globalPublish.displayArrows(pubTime,transf,globalPublish.ArrowsOp);
            globalPublish.displayArrows(pubTime,initTransform,globalPublish.ArrowsOs);


            seqHeader++;
        }

    }
    /**
     * @brief Find w (angular speed) and v (linear speed) between two matrix T expressed in the same
     * referential.
     *  
     * @param prevT 
     * @param currT 
     * @param Stht Vector (S, tht). dim(S) = 6x1; S = (w1, w2, w3, v1, v2, v3)
     */
    void computeMotionSpeedBetweenTwoFrames(Eigen::Matrix4f prevT, Eigen::Matrix4f currT, Eigen::VectorXf & Stht)
    {   
        // We seek the screw motion that displaces the frame from prevT to currT.
        Eigen::Matrix4f Tcomb = currT * (prevT.inverse().eval() );

        // std::cout << "Tcomb red : " << Tcomb.block<3,3>(0,0) << std::endl;
        //std::cout << "Tcomb : " << Tcomb << std::endl;        

        // Let's express it by exponential formula
        // p.106 (p.124) Lynch and Park, 2017
        if (Tcomb.block<3,3>(0,0).isIdentity(1e-2) )
        {
            //std::cout << "Identity case" << std::endl;
            Eigen::Vector3f normp = ( Tcomb.block<3,1>(0,3) /  Tcomb.block<3,1>(0,3).norm() );
            for (int i = 0; i <3; i++)
            {
                Stht(i,0) = 0.0;
                Stht(i+3,0) = normp(i,0);
            }
            Stht(6,0) = Tcomb.block<3,1>(0,3).norm();
        }
        else
        {
            // Find w
            // p.87 (p.106) Lynch and Park, 2017

            // Check if tr(R) = -1
            if ( abs(Tcomb(0,0) + Tcomb(1,1) + Tcomb(2,2) +1) < 1e-6)
            {
                Stht(6,0) = M_PI;
                double val;
                // solution 1
                if (1+Tcomb(2,2) > 0.0)
                {
                    val = 1.0/sqrt(2*(1+Tcomb(2,2)));
                    Stht(0,0) =val*Tcomb(0,2);
                    Stht(1,0) = val*Tcomb(1,2);
                    Stht(2,0) = val*(1+Tcomb(2,2));
                }
                // solution 2
                else if (1+Tcomb(1,1) > 0.0)
                {
                    val = 1.0/sqrt(2*(1+Tcomb(1,1)));
                    Stht(0,0) =val*Tcomb(0,1);
                    Stht(1,0) = val*(1+Tcomb(1,1) );
                    Stht(2,0) = val*Tcomb(2,1);
                }
                // solution 3
                else
                {
                    val = 1.0/sqrt(2*(1+Tcomb(0,0)));
                    Stht(0,0) =val*(1+Tcomb(0,0));
                    Stht(1,0) = val*Tcomb(1,0);
                    Stht(2,0) = val*(1+Tcomb(2,0));
                }     
            }
            else
            {
                Stht(6,0) = acos(0.5*(Tcomb(0,0) + Tcomb(1,1) + Tcomb(2,2) - 1));
                Eigen::Matrix3f skewW = (Tcomb.block<3,3>(0,0) - Tcomb.block<3,3>(0,0).transpose() )/(2*sin(Stht(6,0)));
                Stht(0,0) = skewW(2,1);
                Stht(1,0) = skewW(0,2);
                Stht(2,0) = skewW(1,0);                                
            }
            double norm = 0;
            for (int i = 0; i < 3; i++)
            {
                norm += Stht(i,0)*Stht(i,0);
            }
            norm = sqrt(norm);
            for (int i = 0; i < 3; i++)
            {
                Stht(i,0) = Stht(i,0)/norm;
            }        

            // find v
            // p.106 (p.124) Lynch and Park, 2017
            if (Stht(6,0) != 0.0)
            {
                Eigen::Matrix3f skewW;
                skewW.setZero();
                skewW(0,1) = -Stht(2,0);
                skewW(0,2) = Stht(1,0);
                skewW(1,0) = Stht(2,0);
                skewW(1,2) = -Stht(0,0);
                skewW(2,0) = -Stht(1,0);
                skewW(2,1) = Stht(0,0);                                                                                
                Eigen::Vector3f GinvThtp = ( Eigen::Matrix3f::Identity()/Stht(6,0) - 0.5*skewW + (1/Stht(6,0) - 0.5/tan(0.5*Stht(6,0)) )*skewW*skewW )*Tcomb.block<3,1>(0,3);
                for (int i = 0; i<3; i++)
                {
                    Stht(3+i,0) = GinvThtp(i,0);
                }
            }
            else
            {
                for (int i = 0; i<3; i++)
                {
                    Stht(3+i,0) = 0.0;
                }
            }
        }
    }


    bool reexpressCalibFrameInLocalBody(int numMarkers, pcl::PointCloud<pcl::PointXYZ>::Ptr notAlignedCloud )
    {
        bool foundFrame = false;
        pcl::PointCloud<pcl::PointXYZ>::Ptr pcaAppliedCloud(new pcl::PointCloud<pcl::PointXYZ>);   
        int j =0;
        if (calibFrame>= 0)
        {
            std::map<double,std::map<int,Eigen::Vector3d> >::iterator it = TRCManager.optitrackData.begin();
            std::advance(it,calibFrame);
            std::map<int, Eigen::Vector3d> frame = it->second;
            foundFrame = true;
            frameCalib = frame;
            for (std::map<std::string,int>::iterator it2 = TRCManager.trcCorresNameId.begin(); it2!= TRCManager.trcCorresNameId.end(); it2++  )
            {
                if (frame.find(it2->second)!= frame.end())
                {
                    Eigen::Vector3d vec = osMpi*frame[it2->second];
                    pcl::PointXYZ pt(vec[0], vec[1], vec[2]);
                    // std::cout << vec << std::endl;        
                    pcaAppliedCloud->points.push_back(pt);
                    corresIDNameInitFrame[j] = it2->first;
                    j++;
                }
                else
                {
                    foundFrame = false;
                    it2 = TRCManager.trcCorresNameId.end();
                    corresIDNameInitFrame.clear();
                    pcaAppliedCloud->points.clear();
                }
            }
        }

        else
        {
            int i = 0;     
            std::map<double,std::map<int,Eigen::Vector3d> >::iterator it = TRCManager.optitrackData.begin();
            while (!foundFrame && i < TRCManager.optitrackData.size() )
            {
                foundFrame = true;
                if (it->second.size() == numMarkers )
                {
                    std::map<int, Eigen::Vector3d> frame = it->second;
                    for (std::map<std::string,int>::iterator it2 = TRCManager.trcCorresNameId.begin(); it2!= TRCManager.trcCorresNameId.end(); it2++  )
                    {
                        if  (frame.find(it2->second)!= frame.end() )
                        {
                            Eigen::Vector3d vec = osMpi*frame[it2->second];
                            pcl::PointXYZ pt(vec[0], vec[1], vec[2]);
                            // std::cout << vec << std::endl;            
                            pcaAppliedCloud->points.push_back(pt);
                            corresIDNameInitFrame[j] = it2->first;
                            j++;
                        }
                        else
                        {
                            foundFrame = false;
                            it2 = TRCManager.trcCorresNameId.end();
                            corresIDNameInitFrame.clear();
                        }
                    }
                }
                else
                {
                    foundFrame = false;
                }
                if (!foundFrame)
                {
                    pcaAppliedCloud->points.clear();
                    i++;
                    if (i < TRCManager.optitrackData.size())
                    {
                        std::advance(it,i);
                    }
                }
                else
                {
                    calibFrame = i;
                }
            }

        }
        if (foundFrame)
        {
            std::cout << "Calibration frame chosen : "<< calibFrame << std::endl;
            Eigen::Matrix4f pcaTransform;
            markersManagement::getTransformationMatrixOfPointCloudByPCA(pcaAppliedCloud, pcaTransform );
       
            pcl::transformPointCloud(*pcaAppliedCloud, *notAlignedCloud, pcaTransform.inverse().eval() );
        }
 
        return(foundFrame);

    }


    /**
     * @brief Initialize the necessary informations about the rigid body that we want to track (especially, its initial orientation).
     * 
     */

    void initRigidBody(std::vector<double> initRigidBodyPose, int numMarkers)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr notAlignedCloud(new pcl::PointCloud<pcl::PointXYZ>);  
        bool foundFrame = reexpressCalibFrameInLocalBody(numMarkers,notAlignedCloud);

        if (foundFrame)
        {
            double roll = initRigidBodyPose[0];
            double pitch = initRigidBodyPose[1];
            double yaw = initRigidBodyPose[2];                

            Eigen::AngleAxisf rollAngle(roll, Eigen::Vector3f::UnitX());
            Eigen::AngleAxisf pitchAngle(pitch, Eigen::Vector3f::UnitY());
            Eigen::AngleAxisf yawAngle(yaw, Eigen::Vector3f::UnitZ());
            Eigen::Quaternion<float> q = rollAngle * pitchAngle * yawAngle;
            Eigen::Matrix3f rotationMatrix = q.matrix();
            initTransform.block<3,3>(0,0) = rotationMatrix;

            initCloud = notAlignedCloud;

            for (int i = 0; i < initCloud->points.size(); i++)
            {
                pcl::PointXYZ pt = initCloud->points[i];
                Eigen::Vector3d ptE(pt.x,pt.y,pt.z);
                frameCalib[i] = ptE;
            }
            Eigen::Vector4f centroidOrigin;
            pcl::compute3DCentroid( (*initCloud),centroidOrigin);

            initTransform.block<4,1>(0,3) = centroidOrigin;

            initTransform = (osMpi_4f.inverse().eval() )*initTransform;

        }


    }



    /**
     * @brief 
     * 
     * @param markerNamesAxis map nameOfMarker:int
     *  int : 11 < integrer < 33. First value is for the priority of the axis (= how much the marker and the axis should match),
     * the second is for the axis concerned (x,y,z) (expressed in XYZ frame = pinocchio frame)
     * Ex :  3|3 : priority max, axis 3 = z
     *  2|1 : priority middle, axis 1 = x
     *  1|2 : priority low, axis 2 = y
     * 
     * 
     * Steps : 
     *  - find first frame with all the markers on it.
     *  - find the inverse transform of origin -> this frame (use getTransformationMatrixOfPointCloudByPCA)
     *  - 
     */

    void initRigidBodyWithMarkers(std::map<std::string,int> markerNamesAxis, int numMarkers)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr notAlignedCloud(new pcl::PointCloud<pcl::PointXYZ>);  
        bool foundFrame = reexpressCalibFrameInLocalBody(numMarkers,notAlignedCloud);

        if (foundFrame)
        {
            Eigen::Matrix4f markerRef;
            markerRef.setIdentity();

            Eigen::Matrix4f trueAxisMatrix;
            trueAxisMatrix.setZero();
            trueAxisMatrix(3,3) = 1.0;

            for (int i = 0; i < notAlignedCloud->points.size(); i++)
            {
                std::map<std::string,int>::iterator itMark = markerNamesAxis.find(corresIDNameInitFrame[i]);
                if ( itMark != markerNamesAxis.end() )
                {
                    int axis = itMark->second%10-1;
                    int priority = itMark->second/10-1;

                    std::cout << "axis : " << axis << " ; priority : " << priority << std::endl;

                    pcl::PointXYZ pt = notAlignedCloud->points[i];                    
                    markerRef(0,2-priority) = pt.x;
                    markerRef(1,2-priority) = pt.y;
                    markerRef(2,2-priority) = pt.z;

                    trueAxisMatrix(axis,2-priority) = 1.0;
                }
            }

            // centroid computed for the seek of prevency; but it should be equal to zero.
            Eigen::Vector4f centroid;
            pcl::compute3DCentroid( (*notAlignedCloud),centroid);   
            
            // get orthonormal matrix using Gram-Schimdt
            Eigen::Vector3f ax1 = markerRef.block<3,1>(0,0)-centroid.block<3,1>(0,0);
            ax1.normalize();

            Eigen::Vector3f ev2 = markerRef.block<3,1>(0,1)-centroid.block<3,1>(0,0);
            ev2.normalize();
            Eigen::Vector3f ax2 = ev2 - (ax1.transpose() * ev2) * ax1;
            ax2.normalize();

            Eigen::Vector3f ev3 = markerRef.block<3,1>(0,2)-centroid.block<3,1>(0,0);
            ev3.normalize();            
            Eigen::Vector3f ax3 = ev3 - (ax1.transpose() * ev3) * ax1 - (ax2.transpose() * ev3) * ax2;
            ax3.normalize();

            markerRef.block<3,1>(0,0) = ax1;
            markerRef.block<3,1>(0,1) = ax2;
            markerRef.block<3,1>(0,2) = ax3;
            markerRef.block<4,1>(0,3) = centroid;

            pcl::transformPointCloud(*notAlignedCloud, *initCloud, trueAxisMatrix* osMpi_4f.inverse().eval() * markerRef.inverse().eval() );
            
            for (int i = 0; i < initCloud->points.size(); i++)
            {
                pcl::PointXYZ pt = initCloud->points[i];
                Eigen::Vector3d ptE(pt.x,pt.y,pt.z);
                frameCalib[i] = ptE;
            }

            initTransform = osMpi_4f.inverse().eval();
        }

    }        

    bool ask_pause_continue(std_srvs::SetBool::Request &req,
                std_srvs::SetBool::Response &res)
    {
    isPaused = req.data;

    res.success = true;
    res.message = "pause_continue ok";

    return(true);

    }

    bool isPaused;

    Eigen::Matrix4f prevTransf;

    ros::NodeHandle& nh;
    
    ros::Publisher  pub_ps;

    ros::ServiceServer pause_continue;

    ros::Time pubTime;

    int frame;
    int lastFrame;


    int seqHeader;

    int dataRate;    
    int calibFrame;

    file_management::TRCManager TRCManager;  

    Eigen::Matrix4f initTransform;  

    Eigen::Matrix3d osMpi;

    Eigen::Matrix4f osMpi_4f;    

    pcl::PointCloud<pcl::PointXYZ>::Ptr initCloud; 

    markersManagement::globalPublisher globalPublish;

    std::map<int,Eigen::Vector3d> frameCalib;

    std::map<int, std::string> corresIDNameInitFrame;


  };


}


/**
 * @brief Main method of the program. Creates the Q_move object.
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char **argv)
{
    const std::string rbm_key = "markers_2_rigid_body/rigid_body_markers";
    const std::string cf_key = "markers_2_rigid_body/calib_frame";
    const std::string rbp_key = "markers_2_rigid_body/rigid_body_pose";
    const std::string tlf_key = "markers_2_rigid_body/trc_labeled_file";
    const std::string rm_key = "markers_2_rigid_body/referential_markers";
    
    //const std::string default_trc_filename = "/home/auctus/catkin_ws/src/human_control/description/pendulum/pendulum.urdf.xacro";

    //const std::string default_rigid_body_markers_string = "HL1 HL2 HL3 HL4";

    //const std::string default_init_rigid_body_pose_string = "501 0.0 1.57078 0.0";


    ros::init(argc, argv, "markers_2_rigid_body");
    ros::NodeHandle nh("~");

    std::string trc_file;

    std::vector<std::string> markerNames;
    std::vector<double> initRigidBodyPose;
    std::map<std::string,int> markerNamesAxis;
    int calibFrame;

    if (nh.hasParam(rbm_key))
    {
        XmlRpc::XmlRpcValue markerList;
        nh.getParam(rbm_key, markerList);
        for (int i=0; i < markerList.size(); i++)
        {
            markerNames.push_back( markerList[i] );
        }
    }
    if (nh.hasParam(cf_key))
    {
        nh.getParam(cf_key,calibFrame);
    }

    if (nh.hasParam(tlf_key))
    {
        nh.getParam(tlf_key,trc_file);
    }  
    
    if (nh.hasParam(rbp_key))
    {
        XmlRpc::XmlRpcValue initList;
        nh.getParam(rbp_key, initList);
        for (int i=0; i < initList.size(); i++)
        {
            initRigidBodyPose.push_back( initList[i] );
        }
    }
    if (nh.hasParam(rm_key))
    {
        XmlRpc::XmlRpcValue initList;
        nh.getParam(rm_key, initList);

        if (initList.getType() == XmlRpc::XmlRpcValue::TypeStruct && initList.size() > 0)
        {
            XmlRpc::XmlRpcValue::iterator iter;

            for (auto const& iter : initList)
            {
                std::string key = iter.first;
                if (std::find(markerNames.begin(), markerNames.end(), key) != markerNames.end() )
                {
                    markerNamesAxis[key] = iter.second;
                }
            }
        }
    }


    markers_2_rigid_body::RosReception(nh,trc_file,markerNames,calibFrame,initRigidBodyPose, markerNamesAxis);

  return 0;
}