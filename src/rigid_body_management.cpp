/* 
 * Copyright (c) 2018, Houston Mechatronics Inc., JD Yamokoski
 * Copyright (c) 2012, Clearpath Robotics, Inc., Alex Bencz
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its 
 *    contributors may be used to endorse or promote products derived from 
 *    this software without specific prior written permission. 
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
// Local includes

#include <optitrack_full/rigid_body_management.h>

namespace fs = std::experimental::filesystem;

namespace rigid_body_management
{

  /**
   * @brief Class managing the bridge from Optitrack message to ROS.
   * It also manages simulation and scaling operations.
   * 
   */
    RigidBodyManagement::RigidBodyManagement(optitrack_full::ServerDescription serverDescription) 
    {
        std::map<std::string, std::map<std::string,int> > allRigidBodiesMarkersAxis = serverDescription.allRigidBodiesMarkersAxis;
        isActivated = allRigidBodiesMarkersAxis.size() > 0;
        colorValue = 126;
        seqHeader = 0;
        if (isActivated)
        {
            ROS_INFO("RIGID_BODY_MANAGEMENT ACTIVATED");
        }
    };

/**
 * @brief Initialize the mocap_node. Inspired from initialize function of original optitrack_mocap package.
 * 
 * For now, it suppposed that the points are expressed into the Opensim referential (y up).
 * 
 */
void RigidBodyManagement::initialize_rigid_body(optitrack_full::DataModel * dataModel, 
bool record_trc, 
file_management::TRCManager & TRCManager,
optitrack_full::ServerDescription serverDescription,
markersManagement::globalPublisher & globalPublish,
ros::NodeHandle& nh
)
{

    Eigen::AngleAxisd x_rot(M_PI_2, Eigen::Vector3d::UnitX ());
    Eigen::AngleAxisd z_rot(M_PI_2, Eigen::Vector3d::UnitZ ());
    Eigen::Quaternion<double> qos = z_rot * x_rot;
    osMpi = qos.matrix();

    Eigen::AngleAxisf x_rotf(M_PI_2, Eigen::Vector3f::UnitX ());
    Eigen::AngleAxisf z_rotf(M_PI_2, Eigen::Vector3f::UnitZ ());
    Eigen::Quaternion<float> qosf = z_rotf * x_rotf;
    osMpi_4f.setIdentity();
    osMpi_4f.block<3,3>(0,0) = qosf.matrix();

    std::map<std::string, std::map<std::string,int> > allRigidBodiesMarkersAxis = serverDescription.allRigidBodiesMarkersAxis;
    int l = 0;
    int N = allRigidBodiesMarkersAxis.size();
    for (std::map<std::string, std::map<std::string,int> >::iterator it = allRigidBodiesMarkersAxis.begin(); it!= allRigidBodiesMarkersAxis.end(); it++)
    {
        optitrack_full::MarkerSet markSet;
        markSet.name = it->first;
        markSet.markerNamesAxis = it->second;
        markSet.markerMissing = true;
        markSet.markersDetected.clear();        
        markSet.currentMotivePose.setZero();
        markSet.initTransform.setIdentity();
        markSet.prevTransf.setIdentity();

        markSet.objectInitialized = true;

        set_init_markerSet(markSet,globalPublish,nh, allRigidBodiesMarkersAxis.size(), l);

        dataModel->allMarkerSets.push_back(markSet);
        l++;
    }
    if (record_trc)
    {
        for (std::map<std::string,std::vector<std::string> >::iterator it = serverDescription.allRigidBodiesMarkersNames.begin(); it!=serverDescription.allRigidBodiesMarkersNames.end(); it++ )
        {
            for (int i = 0; i < it->second.size(); i++)
            {
                TRCManager.necessaryMarkers.push_back(it->second[i]);
            }
        }
    }

      ROS_INFO("Initialization complete for Rigid Body side.");

}




void RigidBodyManagement::reexpressCalibFrameInLocalBody(pcl::PointCloud<pcl::PointXYZ>::Ptr notAlignedCloud,
std::map<std::string,Eigen::Vector3d> markersPosition,
bool allMarkers,
Eigen::Matrix4f currentMotivePose)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr pcaAppliedCloud(new pcl::PointCloud<pcl::PointXYZ>);   

    int j = 0;
    for (std::map<std::string,Eigen::Vector3d>::iterator it = markersPosition.begin(); it!= markersPosition.end(); it++)
    {
        Eigen::Vector3d vec = osMpi*it->second;
        pcl::PointXYZ pt(vec[0], vec[1], vec[2]);
        pcaAppliedCloud->points.push_back(pt);
        corresIDNameInitFrame[j] = it->first;
        j++;
    }
    Eigen::Matrix4f pcaTransform;
    if (!allMarkers && currentMotivePose != Eigen::Matrix4f::Zero() )
    {
        
        Eigen::Vector4f centroid = osMpi_4f*currentMotivePose.block<4,1>(0,3);
        markersManagement::getTransformationMatrixOfPointCloudByPCA(pcaAppliedCloud, pcaTransform, centroid );
    }
    else
    {
        markersManagement::getTransformationMatrixOfPointCloudByPCA(pcaAppliedCloud, pcaTransform);
    }
    //std::cout << "pcaTransform : " << pcaTransform << std::endl;
    pcl::transformPointCloud(*pcaAppliedCloud, *notAlignedCloud, pcaTransform.inverse().eval() );
}

/**
 * @brief 
 * 
 * NB : as axes are expressed in XYZ ref, markersPositionOrigin are expressed in Optitrack referential at the end.
 * 
 * @param markerNamesAxis 
 * @param markersPosition 
 * @param allMarkers 
 * @param currentMotivePose 
 * @param initTransform 
 * @param markersPositionOrigin 
 * @param markersDistance 
 * @param markerNames 
 */
void RigidBodyManagement::initRigidBodyWithMarkers(std::map<std::string,int> markerNamesAxis,
std::map<std::string,Eigen::Vector3d> markersPosition,
bool allMarkers,
Eigen::Matrix4f currentMotivePose,
Eigen::Matrix4f & initTransform,
std::map<std::string,Eigen::Vector3d> & markersPositionOrigin,
std::map<std::string,std::map<std::string,double> > & markersDistance,
std::vector<std::string> & markerNames)
{


    corresIDNameInitFrame.clear();

    pcl::PointCloud<pcl::PointXYZ>::Ptr notAlignedCloud(new pcl::PointCloud<pcl::PointXYZ>);  
    reexpressCalibFrameInLocalBody(notAlignedCloud, markersPosition, allMarkers, currentMotivePose);

    Eigen::Matrix4f markerRef;
    markerRef.setIdentity();

    Eigen::Matrix4f trueAxisMatrix;
    trueAxisMatrix.setZero();
    trueAxisMatrix(3,3) = 1.0;

    for (int i = 0; i < notAlignedCloud->points.size(); i++)
    {
        std::map<std::string,int>::iterator itMark = markerNamesAxis.find(corresIDNameInitFrame[i]);
        if ( itMark != markerNamesAxis.end() )
        {
            int axis = itMark->second%10-1;
            int priority = itMark->second/10-1;

            //std::cout << itMark->first  << " : priority : " << priority << " ; axis : " << axis << std::endl;

            pcl::PointXYZ pt = notAlignedCloud->points[i];                    
            markerRef(0,2-priority) = pt.x;
            markerRef(1,2-priority) = pt.y;
            markerRef(2,2-priority) = pt.z;

            trueAxisMatrix(axis,2-priority) = 1.0;
        }
    }
    Eigen::Vector4f centroid;
    // centroid computed for the seek of prevency; but it should be equal to zero.
    pcl::compute3DCentroid( (*notAlignedCloud),centroid);   
   
    // get orthonormal matrix using Gram-Schimdt
    Eigen::Vector3f ax1 = markerRef.block<3,1>(0,0)-centroid.block<3,1>(0,0);
    ax1.normalize();

    Eigen::Vector3f ev2 = markerRef.block<3,1>(0,1)-centroid.block<3,1>(0,0);
    ev2.normalize();
    Eigen::Vector3f ax2 = ev2 - (ax1.transpose() * ev2) * ax1;
    ax2.normalize();

    Eigen::Vector3f ev3 = markerRef.block<3,1>(0,2)-centroid.block<3,1>(0,0);
    ev3.normalize();            
    Eigen::Vector3f ax3 = ev3 - (ax1.transpose() * ev3) * ax1 - (ax2.transpose() * ev3) * ax2;
    ax3.normalize();

    markerRef.block<3,1>(0,0) = ax1;
    markerRef.block<3,1>(0,1) = ax2;
    markerRef.block<3,1>(0,2) = ax3;
    markerRef.block<4,1>(0,3) = centroid;
    
    pcl::PointCloud<pcl::PointXYZ>::Ptr initCloud(new pcl::PointCloud<pcl::PointXYZ>);  

    pcl::transformPointCloud(*notAlignedCloud, *initCloud, trueAxisMatrix* markerRef.inverse().eval() ); // x = z
    //pcl::transformPointCloud(*notAlignedCloud, *initCloud, markerRef.inverse().eval() ); // x = y    
    //pcl::transformPointCloud(*notAlignedCloud, *initCloud, Eigen::Matrix4f::Identity() ); // x = y  
    //pcl::transformPointCloud(*initCloud, *initCloud,  trueAxisMatrix  ); // x = x; z = y; y = z
    //std::cout << trueAxisMatrix << std::endl;

    // save the new Transformation matrix

    // first possibility
    //markersManagement::getTransformationMatrixOfPointCloudByPCA(initCloud, initTransform);
    //initTransform = trueAxisMatrix* markerRef.inverse().eval() ;

    //second possibility : normally, initCloud should be such as initTransform = Identity
    initTransform.setIdentity();

    // initialize and manage markers distance
    std::map<std::string,double> tempDist;
    markerNames.clear();
    for (std::map<std::string,Eigen::Vector3d>::iterator it = markersPosition.begin(); it!= markersPosition.end(); it++)
    {
        tempDist[it->first] = 99999;
        markerNames.push_back(it->first);
        
    }
    for (std::map<std::string,Eigen::Vector3d>::iterator it = markersPosition.begin(); it!= markersPosition.end(); it++)
    {
        markersDistance[it->first] = tempDist;
    }


    for (int i = 0; i < initCloud->points.size(); i++)
    {
        pcl::PointXYZ pt = initCloud->points[i];
        Eigen::Vector3d ptE(pt.x,pt.y,pt.z);
        markersPositionOrigin[ corresIDNameInitFrame[i] ] = ptE;
        for (int j = i+1; j < initCloud->points.size(); j++)
        {
            pcl::PointXYZ pt2 = initCloud->points[j];
            Eigen::Vector3d ptE2(pt2.x,pt2.y,pt2.z);
            double distBetweenMarkers = (ptE2-ptE).norm();
            //std::cout << "distbetmarkers : " << distBetweenMarkers << std::endl;
            markersDistance[ corresIDNameInitFrame[i]][ corresIDNameInitFrame[j]] = distBetweenMarkers;
            markersDistance[ corresIDNameInitFrame[j]][ corresIDNameInitFrame[i]] = distBetweenMarkers;
        }
    }

}        

void RigidBodyManagement::set_init_markerSet(optitrack_full::MarkerSet & markSet,
markersManagement::globalPublisher & globalPublish,
ros::NodeHandle& nh,
int N,
int l)
{
    if (!markSet.objectInitialized)
    {
        int axisPriority = 11;
        for (std::map<std::string, Eigen::Vector3d >::iterator it = markSet.markersPosition.begin(); it!= markSet.markersPosition.end(); it++)
        {
            markSet.markersNames.push_back(it->first);

            // set Marker Axis. Might need to be changed later to something less random.
            if (markSet.markerNamesAxis.size() < 3)
            {
                markSet.markerNamesAxis[it->first] = axisPriority;
                axisPriority += 11;
            }
        }
    }
    // 
    markSet.markersPositionOrigin.clear();

    // initialize bools
    markSet.alreadyFullyDetected = false;
    markSet.verbosePoseNotDetected = false;

    // initialize topics
    std::string name = markSet.name;
    std::string topicPoseName = "/"+name+"/pose";
    std::string topicMarkerName = "/"+name+"/cloud";      
    std::string topicMarkerCalibName = "/"+name+"/cloudCalib";               
    std::string topicArrowName = "/"+name+"/arrow";    
    allPosePublishers[name] = nh.advertise<geometry_msgs::PoseStamped>(topicPoseName,1000);

    // add new topics to globalPublisher
    markersManagement::markerPubInfo pubInfoMarker;
    std::string pubMarkerName = name+"Cloud";      
    pubInfoMarker.markerPub = nh.advertise<sensor_msgs::PointCloud2>(topicMarkerName,1);
    if (N == -1)
    {
        pubInfoMarker.rgb[0] = 255;
        pubInfoMarker.rgb[1] = 255;
        pubInfoMarker.rgb[2] = colorValue; 
    }
    else
    {
        pubInfoMarker.rgb[0] = std::min(255, int(2*255*l/N) );
        pubInfoMarker.rgb[1] = std::min(255, std::max(0,int(255*(2*l-N)/(N) ) ) );
        pubInfoMarker.rgb[2] = 30; 
    }
    globalPublish.allPublishers[pubMarkerName] = pubInfoMarker;

    markersManagement::markerPubInfo pubInfoMarkerCalib;
    std::string pubMarkerCalibName = name+"CloudCalib";      
    pubInfoMarker.markerPub = nh.advertise<sensor_msgs::PointCloud2>(topicMarkerCalibName,1);
    if (N == -1)
    {
        pubInfoMarker.rgb[0] = 255; 
        pubInfoMarker.rgb[1] = 255; 
        pubInfoMarker.rgb[2] = colorValue-125; 
    }
    else
    {
        pubInfoMarker.rgb[0] = std::min(255, int(2*255*l/N) );
        pubInfoMarker.rgb[1] = std::min(255, std::max(0,int(255*(2*l-N)/(N) ) ) );
        pubInfoMarker.rgb[2] = 100; 
    }    
    globalPublish.allPublishers[pubMarkerCalibName] = pubInfoMarker;

    markersManagement::markerPubInfo pubInfoArrow;
    std::string pubArrowName = name+"Arrow";      
    pubInfoArrow.markerPub = nh.advertise<visualization_msgs::MarkerArray>(topicArrowName,1);

    pubInfoArrow.rgb[0] = 255;
    pubInfoArrow.rgb[1] = 0;
    pubInfoArrow.rgb[2] = 8;    

    globalPublish.allPublishers[pubArrowName] = pubInfoArrow;    

    std::vector<std::string> pubNames = {pubMarkerName, pubMarkerCalibName,pubArrowName};

    displayPublisherNames[name] = pubNames;                

    markSet.objectInitialized = true;

}


void RigidBodyManagement::initialize_new_markerSets(optitrack_full::DataModel * dataModel,
markersManagement::globalPublisher & globalPublish,
ros::NodeHandle& nh)
{
    if (dataModel->newMarkerSet)
    {
        for (int i = 0; i < dataModel->allMarkerSets.size(); i++)
        {
            if (!dataModel->allMarkerSets[i].objectInitialized)
            {
                set_init_markerSet(dataModel->allMarkerSets[i], globalPublish, nh);
            }
        }
        dataModel->newMarkerSet = false;
    }
    for (int i = 0; i < dataModel->allMarkerSets.size(); i++)
    {    
        if (!dataModel->allMarkerSets[i].alreadyFullyDetected)
        {
            std::map<std::string, Eigen::Vector3d> markersAxisPosition;
            dataModel->allMarkerSets[i].crucialMarkerMissing = false;
            if (!dataModel->allMarkerSets[i].markerMissing)
            {
                markersAxisPosition = dataModel->allMarkerSets[i].markersPosition;
                dataModel->allMarkerSets[i].alreadyFullyDetected = true;
            }
            else
            {
                for (std::map<std::string, bool>::iterator it = dataModel->allMarkerSets[i].markersDetected.begin(); it!= dataModel->allMarkerSets[i].markersDetected.end(); it++)
                {
                    if (dataModel->allMarkerSets[i].markerNamesAxis.find(it->first) != dataModel->allMarkerSets[i].markerNamesAxis.end() && !it->second)
                    {
                        std::cout << "Error for " << dataModel->allMarkerSets[i].name << " : marker " << it->first << " missing." << std::endl;
                        dataModel->allMarkerSets[i].crucialMarkerMissing = true;
                    }
                    else if (it->second)
                    {
                        markersAxisPosition[it->first] = dataModel->allMarkerSets[i].markersPosition[it->first];
                    }
                }
                if ( dataModel->allMarkerSets[i].markersDetected.size() == 0)
                {
                    dataModel->allMarkerSets[i].crucialMarkerMissing = true;
                }
            }
            // check if it gives any new information that might justify to use a new calibration frame
            if (!dataModel->allMarkerSets[i].crucialMarkerMissing && (markersAxisPosition.size() > dataModel->allMarkerSets[i].markersPositionOrigin.size() ) )
            {
                initRigidBodyWithMarkers(dataModel->allMarkerSets[i].markerNamesAxis,
                markersAxisPosition,
                dataModel->allMarkerSets[i].alreadyFullyDetected,
                dataModel->allMarkerSets[i].currentMotivePose,
                dataModel->allMarkerSets[i].initTransform, 
                dataModel->allMarkerSets[i].markersPositionOrigin,
                dataModel->allMarkerSets[i].markersDistance,
                dataModel->allMarkerSets[i].markersNames
                );
                std::cout << dataModel->allMarkerSets[i].name << " initialized" << std::endl;
            }
            else
            {
                dataModel->allMarkerSets[i].alreadyFullyDetected = false;
            }
        }
    }
}

/**
 * @brief Execute the classic publishing operations for an Optitrack message.
 * Calculate the new pose of the object and publish it.
 * NB : we are starting here from Opensim_ref points, and then we express in Optitrack_ref points.
 */
void RigidBodyManagement::classic_rigid_body_run(optitrack_full::DataModel * dataModel, markersManagement::globalPublisher globalPublish, ros::Time time)
{
    // find and express transform, if possible
    for (int i = 0; i < dataModel->allMarkerSets.size(); i++)
    {   
        std::string setName = dataModel->allMarkerSets[i].name;
        if (dataModel->allMarkerSets[i].markersPositionOrigin.size() >= 3)
        {
            optitrack_full::MarkerSet treatedMarkerSet = dataModel->allMarkerSets[i];      
            pcl::PointCloud<pcl::PointXYZ>::Ptr newCloud(new pcl::PointCloud<pcl::PointXYZ>);
            pcl::PointCloud<pcl::PointXYZ>::Ptr currInitCloud(new pcl::PointCloud<pcl::PointXYZ>);
            pcl::PointCloud<pcl::PointXYZ>::Ptr completeInitCloud(new pcl::PointCloud<pcl::PointXYZ>);
                        
            std::map<std::string,Eigen::Vector3d> displayOriginMark;
            std::map<std::string,Eigen::Vector3d> displayCurrentMark;

            for (std::map<std::string, Eigen::Vector3d >::iterator it = treatedMarkerSet.markersPositionOrigin.begin(); it!= treatedMarkerSet.markersPositionOrigin.end(); it++)
            {
                if (treatedMarkerSet.markersDetected[it->first])
                {
                    // as markersPositionOrigin is directly expressed in Optitrack ref, 
                    // we do a transform to express markersPosition in Optitrack ref.

                    //Eigen::Vector3d vec = osMpi*frame[it2->second];
                    Eigen::Vector3d vec = osMpi*treatedMarkerSet.markersPosition[it->first];
                    displayCurrentMark[it->first] = osMpi*treatedMarkerSet.markersPosition[it->first];
                    pcl::PointXYZ pt(vec[0], vec[1], vec[2]);
                    newCloud->points.push_back(pt);
                    
                    Eigen::Vector3d vecOri = it->second;
                    displayOriginMark[it->first] = it->second;                    
                    pcl::PointXYZ ptOri(vecOri[0], vecOri[1], vecOri[2]);
                    currInitCloud->points.push_back(ptOri);
                }
                Eigen::Vector3d vecOriC = it->second;              
                pcl::PointXYZ ptOriC(vecOriC[0], vecOriC[1], vecOriC[2]);
                completeInitCloud->points.push_back(ptOriC);                
            }

            if ( newCloud->points.size() < 3  )
            {
                ROS_INFO("Error for body %s : not enough markers to get the pose of the rigid body ([%ld]).\n",setName.c_str(),newCloud->points.size() );
            }
            else
            {

                // reexpress new cloud into the good orientation
                Eigen::Matrix4f transf_pca;
                Eigen::Matrix4f transf_pca_new;

                // Eigen::Matrix4f transf_pca_after_mod;       
                Eigen::Matrix4f transf_pca_ori;     

                markersManagement::getTransformationMatrixOfPointCloudByPCA(newCloud, transf_pca_new);

                markersManagement::getTransformationMatrixOfPointCloudByPCA(currInitCloud, transf_pca);
                // pcl::PointCloud<pcl::PointXYZ>::Ptr currInitCloud2(new pcl::PointCloud<pcl::PointXYZ>);
                // pcl::transformPointCloud(*currInitCloud, *currInitCloud2, treatedMarkerSet.initTransform *transf_pca.inverse().eval() );
                // markersManagement::getTransformationMatrixOfPointCloudByPCA(currInitCloud2, transf_pca_after_mod);
                markersManagement::getTransformationMatrixOfPointCloudByPCA(completeInitCloud, transf_pca_ori);


                // transf_pca.inverse().eval() : to go back to identity (to check)
                // initTransform : to go back to the true transformation of the markerSet
                Eigen::Matrix4f transfPartialToComplete =  treatedMarkerSet.initTransform * transf_pca.inverse().eval();                
                Eigen::Matrix4f transfCompleteToPartial =  transfPartialToComplete.inverse().eval();

                Eigen::Matrix4f transfPartialToNew;
                pcl::registration::TransformationEstimationSVD<pcl::PointXYZ, pcl::PointXYZ> svd; 
                //svd.estimateRigidTransformation(*currInitCloud2, *newCloud,transf);
                //svd.estimateRigidTransformation(*currInitCloud, *newCloud,transf);
                svd.estimateRigidTransformation(*currInitCloud, *newCloud,transfPartialToNew);

                // get the transformation from the initial cloud (with all the points) to the
                // current point cloud (with a smaller number of points), while respecting the
                // orientation of the initial cloud. Try with SVD.
                Eigen::Matrix4f transf = transfPartialToNew * transfCompleteToPartial;

                // get the transformation from the initial cloud (with all the points) to the
                // current point cloud (with a smaller number of points), while respecting the
                // orientation of the initial cloud. Try with PCA.
                Eigen::Matrix4f transf_by_pca = ( transfPartialToComplete * transf_pca * transf_pca_new.inverse().eval() ).inverse().eval();

                // apply transf_pca change, to be sure that everything will be good
                //transf = treatedMarkerSet.initTransform * transf_pca.inverse().eval() * transf;

                // debug purpose : apply the transformation of the points of the init cloud that are
                // on the newCloud to all the points of the init cloud. Save it in newInitCloud.
                // if ok, newInitCloud should be superposed on newCloud
                pcl::PointCloud<pcl::PointXYZ>::Ptr newInitCloud(new pcl::PointCloud<pcl::PointXYZ>);
                pcl::transformPointCloud(*completeInitCloud, *newInitCloud, transf );
                
                // debug purpose : try another method, where 
                pcl::PointCloud<pcl::PointXYZ>::Ptr newInitCloudPCA(new pcl::PointCloud<pcl::PointXYZ>);
                pcl::transformPointCloud(*completeInitCloud, *newInitCloudPCA, transf_by_pca );                
                if (setName == "Trideme_big")
                {
                    for (int l = 0; l < newCloud->points.size(); l++)
                    {
                        Eigen::Vector4f pt1(newInitCloud->points[l].x, newInitCloud->points[l].y, newInitCloud->points[l].z,1);
                        Eigen::Vector4f pt3(newInitCloudPCA->points[l].x, newInitCloudPCA->points[l].y, newInitCloudPCA->points[l].z,1);                        
                        Eigen::Vector4f pt2(newCloud->points[l].x, newCloud->points[l].y, newCloud->points[l].z,1);
                        std::cout << "Diff transf : " << (pt1 - pt2).norm() << std::endl;
                        std::cout << "Diff transf by pca : " << (pt3 - pt2).norm() << std::endl;
                    }
                    std::cout << (transf_pca - transf_pca_ori).norm() << std::endl;
                    std::cout << "====" << std::endl;
                }

                // if (setName == "Trideme")
                // {
                // std::cout << "transfo diff : " << (transf_pca_after_mod-transf_pca_ori).norm() << std::endl;
                // //std::cout << " transfo clean : " << transf_clean << std::endl;
                // }

                Eigen::Vector4f centroidOrigin;
                pcl::compute3DCentroid( (*newInitCloud),centroidOrigin);

                std::map<int,Eigen::Vector3d> optidebug;

                for (int i = 0; i < newInitCloud->points.size(); i++)
                {
                    Eigen::Vector3d optipt(newInitCloud->points[i].x, newInitCloud->points[i].y, newInitCloud->points[i].z);
                    optidebug[i] = optipt;
                }


                Eigen::Quaternion<float> q(transf.block<3,3>(0,0) );
                q.normalize();

                ros::Time pubTime = ros::Time::now();

                // publish Pose message
                geometry_msgs::PoseStamped pose_msg;

                pose_msg.pose.position.x = transf(0,3);
                pose_msg.pose.position.y = transf(1,3);
                pose_msg.pose.position.z = transf(2,3);

                pose_msg.pose.orientation.x = q.x();
                pose_msg.pose.orientation.y = q.y();
                pose_msg.pose.orientation.z = q.z();
                pose_msg.pose.orientation.w = q.w();

                pose_msg.header.seq = seqHeader;
                pose_msg.header.stamp = pubTime;
                
                //std::cout << "Try to publish pose" << std::endl;

                allPosePublishers[setName].publish(pose_msg);

                // if ( !prevTransf.isIdentity() )
                // {
                //     Eigen::VectorXf Stht(7);
                //     computeMotionSpeedBetweenTwoFrames(prevTransf,transf_osMpi, Stht);
                //     std::cout << "New speed vector : [ ";
                //     for (int i = 0; i < 6; i++)
                //     {
                //         std::cout << " " << Stht(i,0)*Stht(6,0);
                //     }
                //     std::cout << " ]" << std::endl;
                //     // std::cout << "Before : " << prevTransf << std::endl;                
                //     // std::cout << "Now : " << transf_osMpi << std::endl;  
                //     std::cout << "Translation : " << ( transf_osMpi.block<3,1>(0,3) - prevTransf.block<3,1>(0,3) )/100 << std::endl;
                // }
                treatedMarkerSet.prevTransf = transf;

                std::vector<std::string> pubNames = displayPublisherNames[setName];

                //std::cout << "Try to publish display" << std::endl;
                
                //std::cout << globalPublish.allPublishers[ pubNames[0] ].rgb[2] << std::endl;

                globalPublish.markerPublisher(pubTime,displayCurrentMark,globalPublish.allPublishers[ pubNames[0] ]);
                //std::cout << "1";
                globalPublish.markerPublisher(pubTime,displayOriginMark,globalPublish.allPublishers[ pubNames[1] ]);

                // we send osMpi_4f.inverse().eval()*transf, as the transformation osMpi_4f*transf will be done into displayArrows.
                // (resulting in the same result as transf)
                transf = osMpi_4f.inverse().eval()*transf;

                globalPublish.displayArrows(pubTime,transf,globalPublish.allPublishers[ pubNames[2] ]);

                dataModel->allMarkerSets[i].verbosePoseNotDetected = false;
            }         
        }
        else
        {
            if (!dataModel->allMarkerSets[i].verbosePoseNotDetected)
            {
                ROS_INFO("Error for body %s : not enough origin markers to accurately calculate its pose (only %ld).", setName.c_str(), dataModel->allMarkerSets[i].markersPositionOrigin.size());
                dataModel->allMarkerSets[i].verbosePoseNotDetected = true;
            }
            
        }
        seqHeader++;
    }
       


};

 void RigidBodyManagement::associateRigidBodiesMarkers(optitrack_full::DataModel * dataModel)
 {

 }

// void associateRigidBodiesMarkers(optitrack_full::DataModel * dataModel)
// {   
//     double limDist = 1e-12;
//     int j = 0;
//     std::map<std::string, Eigen::Vector3d >::iterator itPos;

//     std::vector<int> alreadyAssos;

//     for( std::map<int,std::map<int,Eigen::Vector3d> >::iterator it =  dataModel->opNameMarkers.opMarkRigidBodies.begin(); it!= dataModel->opNameMarkers.opMarkRigidBodies.end(); it++)
//     {
//         Eigen::Vector3d ptRigid = it->second[it->first];

//         int i = 0;
//         bool detected = false;

//         while (i <  dataModel->allMarkerSets.size() && !detected)
//         {    
//             if (std::find(alreadyAssos.begin(), alreadyAssos.end(), i) == alreadyAssos.end() )
//             {
//                 std::map<std::string, bool > markersDetected = dataModel->allMarkerSets[i].markersDetected;
//                 itPos = dataModel->allMarkerSets[i].markersPosition.begin();
//                 std::advance(itPos,dataModel->allMarkerSets[i].markersPosition.size() - j);
                
//                 if (markersDetected[itPos->first])
//                 {
//                     double markerDistance = (ptRigid-itPos->second).norm();
//                     if (markersDistance < limDist)
//                     {
//                         detected = true;
//                         i--;
//                     }
//                 }
//             }
//             i++;
//         }

//         if (detected)
//         {

//         }
//     }

// }



};


