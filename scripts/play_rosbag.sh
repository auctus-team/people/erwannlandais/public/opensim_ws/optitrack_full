#!/bin/bash

# Use this script if there is a bug with your tf (ex : some frames you may not want in it).

rosbag_name="2021-06-29-14-36-54.bag"

#"2021-06-29-13-48-16.bag"

#"2021-06-29-14-32-50.bag"

#"2021-06-29-14-36-54.bag"

#"2021-06-29-12-51-40.bag"

rosparam set use_sim_time true

rosbag play $rosbag_name --clock --pause /mocap_node/optitrack_markers:=/opti_mark_input #/tf:=/old_tf


