#!/bin/bash

# Use this script if there is a bug with your tf (ex : some frames you may not want in it).

for i in *.bag
	do
		rosbag filter $(basename $i .${i##*.}).bag $(basename $i .${i##*.})_new.bag "topic!='/tf' or (len(m.transforms)>0 and (m.transforms[0].header.frame_id=='camera' or m.transforms[0].child_frame_id=='camera') )"
		rm $(basename $i .${i##*.}).bag
		cp $(basename $i .${i##*.})_new.bag $(basename $i .${i##*.}).bag
		rm $(basename $i .${i##*.})_new.bag
	done


