

# Install

In order to get all the features of this packages, other packages must be installed. 

## Install fosim

This package is based on the methods of the library fosim for a quick access to some Opensim features.

## Install osim2urdf

The goal of this package is to transform Opensim models (in .osim) into URDF models (.urdf). This is necessary to use the informations of the .osim models into Pinocchio (and more broadly, ROS); and also for the scaling process.
This package can be found here : https://gitlab.inria.fr/auctus-team/components/modelisation/humanmodels/ospi2urdf. Use the devel_ospi branch for latest updates.

Please note that you can check your Opensim model and .mot files effects, on Linux, with the following software : https://www.opensimcreator.com/

# How to use : 

## Step 0 : Quick sum-up of the package

Informations about the association algorithm can be read in algorithms_mechanics.txt. 

### Input

* Parameters of the package (see Step 1 and Step 2).

### Output

* Position and ID of Optitrack markers (/optitrack_markers; sensor_msgs/PointCloud2).

* Position and ID of Opensim markers (/osim_markers; sensor_msgs/PointClou2).

* Position of markers of a specific body part (for debugging purpose, about the association of Opensim / Optitrack markers) (sensor_msgs/PointCloud2)

* Values of the Joints of the model (/Q_pub by default; sensor_msgs/JointState).

* Topics giving several metrics about the association process, which can be monitored and displayed by display_errors.py. 

## Step 1 : check the configuration files

* Check if the good urdf file will be loaded. 

If you're using the package osim2urdf, the loading of the .urdf file is made a .launch file inside the ROS package where your .urdf files are also created. This .launch file is automatically created when you are generating the .urdf file from your .osim model. You need to check if the latest .launch file generated corresponds to your .urdf; if this is not the case, you need to change the launchfile inside package (ex : by running ospi2urdf_parser).

* Setup the .launch file of optitrack_full (ex : calib_plane.launch): 

- Check the name of the variable "mocap_config_file". It contains the majority of the parameters for the package. Some configurations exists for convenience :

 neutral_pose, if the calibration pose of your model (= the pose of the model in order to make a first association between Opensim and Optitrack markers ) is a neutral pose (= torso straight, arms relaxed at sides, palms flat on thighs).

 calib_plane, if the calibration pose of your model is a non-symetrical pose (here, torso straight, right arm at 45 degrees to the body and palm facing the torso, left arm at 90 degrees to the body and palm facing the ground).

NB : the non-symetrical pose can be considered as deprecated if the placement of the markers on your model is not symetrical (according to the center of the body).

- Check the path to the .launch file which will load your .urdf model in Rviz. It corresponds to the following line in calib_plane.launch : 

~~~
  <include file="$(find human_control)/launch/human_control.launch">
    <arg name="activate_rviz" value="false"/>
  </include>
~~~

Normally, all references to "human_control" must be changed to the name of the ROS package where the .urdf files are installed.

* Setup the .yaml files of parameters (in config/). The description of all parameters is put inside the neutral_pose3.yaml file. Here are the most important parameters : 

 In optitrack_config section : 
    - frame_rate : frameRate associated with the publication of Optitrack data.
    - simulation : indicates whether this is a simulation or a real situation. In a simulation, the movements and the noise about the markers positions of the model are simulated.
    - maximum_residual : Maximum residual value obtained from the calibration of the Optitrack system (in m). This value is proportionnal to the abillity of the automatic labelling process to be more or less tolerant on the association of Optitrack and Opensim markers.
    - repeat : Indicates whether the data given to the package (whether they come from the Optitrack server, a rosbag or a .trc file) will be repeated in loop. 
    Thanks to this information, the automatic labelling algorithm can use the informations about the associations of the last loop to apply them to the next loop, and maybe find some missing associations.
    WARNING : THIS OPTION NEEDS TO ABSOLUTELY BE SET TO FALSE IF THE DATA ARE NOT REPEATED, OTHERWISE AN INFINITE NUMBER OF DATA MIGHT BE SAVED BY THE ALGORITHM!!!
    - ros_package_path :  Path to the ros package containing the URDF files.
    - type_record : type of data that should be loaded : "trc" for a .trc file, "rosbag" for a rosbag, "none" if you do not want to load datas (which will get data from the Optitrack server). 
    For more informations about these, please check Step 2.
    - trc_file : path to the .trc file that should be loaded, if type_record is set for .trc files.
  
  In opensim_config section :
    - q_calib : the q vector corresponding to the chosen calibration pose of the model. Each value corresponds to the value of a Coordinate of the Opensim model.
    Values are set by the couple 'id': value. All non indicated values will be set to 0. Values could be set as double, or as M_PI_2 / M_PI_4 for convenience. 
    q_calib is set in the same order as the Q vector of the state variable of the Opensim model. You may see it with the method SimTK::State::getQ().
    - osim_path : path to the .osim model.
    - geometry_path : Path to the .vtp files of the .osim model
    - <name>_verbose : Debuggings tools, for publishing informations  about the differents steps of the algorithm.

* If you want to use the scaling option, and you are using a new .osim file, you need to use the json_writer.py script inside osim2urdf. 
To do so, modify the script and add the necessary informations inside the different variables; then, executes the script.

## Step 2 : choose from where the data should come

### A : from a simulation

 *Set up the following variables in your .yaml file : 

~~~
  optitrack_config:
    simulation: true
  
  opensim_config:
    simu_speed:
    simu_dissapear:
    simu_same_frames:
    simu_noise:
~~~

### B : from the Optitrack server

#### In Live Mod

* Set up the following variables in your .yaml file : 

~~~
  optitrack_config:
    simulation: false
    repeat: false
    stream_markers_only: <true/false>
    type_record: "none"
~~~

With stream_markers_only if you want to stream only the Optitrack markers or other structures (ex : rigid bodies, skeletons).

* Launch Motive Software on Windows.

* If necessary, you need to calibrate the Optitrack system.

* Click on the "Live" Button on Motive Software.

#### In Edit Mod

* Set up the following variables in your .yaml file : 

~~~
  optitrack_config:
    simulation: false
    repeat: true
    stream_markers_only: <true/false>
    type_record: "none"
~~~

With stream_markers_only if you want to stream only the Optitrack markers or other structures (ex : rigid bodies, skeletons).

* Launch Motive Software on Windows.

* Click on the "Edit" Button on Motive Software.

* Click on "Play" button to start the streaming of parameters.

### C : from a .trc file

Set up the following variables in your .yaml file : 

~~~
  optitrack_config:
    simulation: false
    repeat: true
    type_record: "trc"
    trc_file: <path_to_trc_file>
~~~

### D : from a rosbag

This package can use rosbags where the Optitrack markers are saved as sensor_msgs/PointCloud2 messages. The input topic is called "/opti_mark_input". The best way to create those rosbags is the following : 

* Launch this package from data obtained thanks to A,B, or C. This package will then broadcast Optitrack markers as sensor_msgs/PointCloud2 messages.

* Executes scripts/record_bag.sh. This will save the Optitrack markers as sensor_msgs/PointCloud2 messages, and the /tf topic. Those topics will be saved into a rosbag inside of scripts.

* Once you do not need to record any new data, quit the .sh script. You may launch scripts/filter_tf_rosbag.sh. This script will filter the /tf to keep only the Transform linked to the referential "camera" (useful, for example, for an application with a camera).

Then, you may start the package with the following procedure :

* Set up the following variables in your .yaml file : 

~~~
  optitrack_config:
    simulation: false
    repeat: <true/false>
    type_record: "rosbag"
~~~

* Modify the script scripts/play_rosbag.sh, and put for the variable "rosbag_name" the path to the rosbag you want to play.

* Then, executes the script scripts/play_rosbag.sh.

## Step 3 : launch the package

* Launch : 

~~~
roslaunch optitrack_full neutral_pose.launch 
~~~

* Call the calibration service : 

~~~
rosservice call /mocap_node/calibration 3.0 2 False 70 155.0
~~~

The parts of the service are the following : 

~~~
rosservice call /mocap_node/calibration <duration_before_activation> <type_of_calibration> <use_scaling> <mass> <recording_time_for_scaling>
~~~

- duration_before_activation : the duration before the calibration process is completed. Starting from the time when the calibration call is processed, the program searches a calibration pose. It will save the most recent calibration pose until the duration duration_before_activation. Then, it will calibrate the model according to the most recent calibration pose.

- type_of_calibration : the kind of calibration you may want : 

0 : calibration pose is not perfect, the theoretical model do not have the same CoR than the theoretical model.
 --> a new model will be created according to recorded movements of user (a specific procedure is required).
1 : calibration pose is not perfect, the theoretical model have the same CoR than the theoretical model.
--> IK will be done on each part of the body, to get an approximation of the pose of the model and the positions of the markers.
2 : calibration pose is perfect (ex : arms pressed against the body)
--> movements of markers to match the pose of the user.

For now, only the calibration case 2 is working.

- scaling (only for type_of_calibration = 2): whether you want to use the scaling tool of Opensim. If this is the case, the program will use the new position of the markers to rescale the Opensim model, and then recreate a new .urdf file that will be loaded directly.

- mass : (only for scaling) : mass of the model, used for scaling.

- recording_time_for_scaling (only for type_of_calibration = 0 or = 1) : the duration of the recording of datas for the placement of the center of rotation of the joints of the models. The recording time will start at the end of the recording of data of the calibration process, and end at the end of the duration recording_time_for_scaling.

* When calibration is good, launch the secondary nodes (fpin_tst to get speed, display_errors to display acceleration / speed / position / errors and save results as .txt).

~~~
roslaunch optitrack_full after_init.launch 
~~~

## List of available services and how to use them

### Save articular angles

Articular angles are published through /Q_pub. You can save them in a rosbag.

### For mocap_node

* Calibration service : 

~~~
rosservice call /mocap_node/calibration <duration_before_activation> <type_of_calibration> <use_scaling> <mass> <recording_time_for_scaling>
~~~

- duration_before_activation : the duration before the calibration process is completed. Starting from the time when the calibration call is processed, the program searches a calibration pose. It will save the most recent calibration pose until the duration duration_before_activation. Then, it will calibrate the model according to the most recent calibration pose.

- type_of_calibration : the kind of calibration you may want : 

0 : calibration pose is not perfect, the theoretical model do not have the same CoR than the theoretical model.
 --> a new model will be created according to recorded movements of user (a specific procedure is required).
1 : calibration pose is not perfect, the theoretical model have the same CoR than the theoretical model.
--> IK will be done on each part of the body, to get an approximation of the pose of the model and the positions of the markers.
2 : calibration pose is perfect (ex : arms pressed against the body)
--> movements of markers to match the pose of the user.

For now, only the calibration case 2 is working.

- scaling (only for type_of_calibration = 2): whether you want to use the scaling tool of Opensim. If this is the case, the program will use the new position of the markers to rescale the Opensim model, and then recreate a new .urdf file that will be loaded directly.

- mass : (only for scaling) : mass of the model, used for scaling.

- recording_time_for_scaling (only for type_of_calibration = 0 or = 1) : the duration of the recording of datas for the placement of the center of rotation of the joints of the models. The recording time will start at the end of the recording of data of the calibration process, and end at the end of the duration recording_time_for_scaling.

* Pause service : 

~~~
rosservice call /mocap_node/pause_continue <Pause_or_Not>
~~~

- Pause_or_Not : Boolean. Set to True to "pause" the package; no more Optitrack informations from any source will be taken or processed. Set to False to reactivate the package.

* Save labelization service : 

To save your labelization result to a .trc file, do : 

rosservice call /mocap_node/save_trc '{pathToFolder: "/home/auctus/sensoring_ws/opensim_ws/Documents/MOTIVE_data/Session-2023-03-09/mocap-skin/computer/",data: [""]}'

"/home/auctus/sensoring_ws/opensim_ws/Documents/MOTIVE_data/Session 2023-03-07"

### For display_errors.py

* Call for displaying the position, speed and acceleration of specific joints (ex : arm_rot_l, pro_sup_l) : 

~~~
rosservice call /display_position_speed_acceleration_specific ["arm_rot_l","pro_sup_l"]
~~~

* Call for displaying the position, speed and acceleration of all joints : 

~~~
rosservice call /display_position_speed_acceleration_specific ["all"]
~~~

* Call for saving specific joint value, speed and acceleration as txt : 

~~~
rosservice call /save_txt_specific '{pathToFolder: "/tmp",data: ["pro_sup_l","arm_rot_l"]}'
~~~
WARNING : SPACES ARE VERY IMPORTANT INTO DICTIONNARY! 

The file will be organized as follows, for each line : 
 
[ time values_of_all_joints speeds_of_all_joints acceleration_of_all_joints]

* Call for saving all joints value, speed and acceleration as txt : 

~~~
rosservice call /save_txt_specific '{pathToFolder: "/tmp",data: ["all"]}'
~~~

* Call for displaying specific errors (only in a simulation mode) :

~~~
rosservice call /mocap_node/display_errors ["joints_errors","tolerance_values"]
~~~

4 types of errors are available : 

- "markers_pose_errors" : for each marker of the Opensim model, displays the error (in m) between the position of the true position (given by the simulation) and the position of the marker according to the Inverse Kinematics operation.

- "joints_errors" : for each Coordinate (= DOF of the joints) of the Opensim model, displays the error (in m or rad) between the true value of the joints (given by the simulation) and the value of the joints according to the Inverse Kinematics operation.

- "tolerance_values" : according to the automatic labelling process, for each marker of the Opensim model, displays the tolerance value (in m) computed for this marker. This tolerance value corresponds to the difference between the recorded distances of this marker to the other markers of the same body, and the current distances between this marker and the other markers of the same body. This tolerance is then used to determine whether an Optitrack marker can be attributed to an Opensim marker.

- "markers_association_errors" : simply displays whether the Opensim marker is associated with the good Optitrack marker or not. 

* Call for displaying all the errors (only in a simulation mode) :

~~~
rosservice call /mocap_node/display_errors ["all"]
~~~

* Call to display a specific pose at a given time (only with labelized trc files) : 

~~~
rosservice call /to_time "time: 15.0"
~~~

# How to use to get the position / orientation of a rigid body saved into a .trc file

Use after_init2.launch. 

Display and save the .txt file thanks to the services of display_errors.py.

The 3 filtering modes are "none", "polyfit", "butterworth".

To see the TF, check TF_test.m (in human_control package).

To manipulate the .txt files, check separeTrajectories.py (in thesis_ws/src/dmp_experimentation/src/dmpbbo/demos_python/dmp)

# Iées développement

Filtrage via accélération (si 1e détection de points très proches, mais refusé à cause d'ancienne association): 
  * Prendre ces points, les considérer comme rigidBody
  * Pendant un temps (et tant que association toujours confirmée), évaluer accélération de chaque point.
  * Si accélération < 3G (= accélération absurde pour un corps humain) et que association de chaque point est homogène, confirmer qu'une association erronée a eu lieu, et confirmer l'association via ICP.

Détection des moments où un bodyPart a des marqueurs Optitrack autour de lui : 
  * Noter dernier instant où marqueur fantôme détecté.
  * Si on arrive après un temps (1.0s par ex) à situation où association refusée, accepter l'association OU reset tous marqueurs.

Filtrage : 
  * Cas où : 
    * 1 seul point gêne pour l'association.
    * La distance entre les points est faible.
    * La réassociation est répétée pendant plus de N frames (en ayant noté la première frame N0 ayant repéré le problème).
  

  * Procédure : 
    * Initialement : allCorresNameID dépend du nombre de frames. 
    * Les maps allCorresNameID sont enregistrées dans frameAllCorresNameID,
    * pour avoir un couple frameInitiale:allCorresNameID.
    * Lorsqu'on dépasse la frameAllCorresNameID, il y a un reboot.

    * Supprimer toutes les associations pour cette partie du corps dans
    * corresNameID.
    * Cas répétition : 
      * Créer une nouvelle map allCorresIDName contenant uniquement corresNameID (et les associations de la frame de calibration) sans la partie du corps.
      * Noter dans frameAllCorresNameID la nouvelle map. 

Opensim : HL1 | Optitrack : 469 | 0.000954045
Opensim : HL2 | Optitrack : 469 | 0.000524582
Opensim : HL3 | Optitrack : 329 | 0.00130039
Opensim : HL4 | Optitrack : 691 | 0.000148098
Issue detected ; try to fix it ... 
---- Opensim positions ---- 
Opensim name : HL1 ; position : [0.0787, 0.2480, 0.1007] 
 Opensim name : HL2 ; position : [0.0684, 0.2272, 0.0758] 
 Opensim name : HL3 ; position : [0.1065, 0.2476, 0.1324] 
 Opensim name : HL4 ; position : [0.1310, 0.1152, 0.1451] 
 ---- Optitrack positions ---- 
Optitrack ID : 329 ; position : [0.1230, 0.2208, 0.1499] 
 Optitrack ID : 460 ; position : [0.1455, 0.1828, 0.0992] 
 Optitrack ID : 469 ; position : [0.0620, 0.2492, 0.0748] 
 Optitrack ID : 691 ; position : [0.1378, 0.1190, 0.1545] 
 Marker name : HL1 (0) ; optitrack associated : 691 
 Marker name : HL2 (1) ; optitrack associated : 469 
 Marker name : HL3 (2) ; optitrack associated : 329 
 Marker name : HL4 (3) ; optitrack associated : 460 
 Check result of 1st filtering : 1. 
Error : HL4 not assigned anymore with 329 (now with 460). 

--> association HL4 - 691 supprimee alors qu'aurait du etre meilleure. 
(devrait etre : HL4 --> 691
HL2 --> 469
HL1 --> 460
HL3 --> 329
)
Trouver pourquoi.
Verifier si commence 

### Recap

rosservice call /mocap_node/calibration 1.5 2 False 70 170.0

rosservice call /mocap_node/save_trc '{pathToFolder: "/home/auctus/sensoring_ws/opensim_ws/Documents/Expe_datas/NASBackup/AF/PC_motive",data: [""], typeSave: 2}'

In Rviz : 

osim_markers : green

optitrack_markers : orange

osim_theory_markers (used for calibration to identify a body part for example) : purple

optitrack_robust_markers (used to display a 1st rescaling of the full body) : gray

Clean associations : 

For ATL.py :
~~~
rosservice call /to_clean_assos ["hand_l","hand_r"]
~~~

For mocap_optitrack : 

~~~
rosservice call /clear_assos ["hand_l","hand_r"]
~~~